# Workflows for Family-Free Weak Common Intervals

FFWCI_PYEXEC = ' '.join(filter(None, [config['python_bin'], config['ff_bin'] + 'ffwci/']))

ACSI_OUT = '%s/acsi_%s' %(config['acsi_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR, 'd%s_m%s' %(config['acsi_delta'], config['acsi_min'])])))
MACSI_OUT = '%s/macsi_%s' %(config['acsi_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR, 'd%s_m%s_q%s' %(config['acsi_delta'], config['acsi_min'], config['acsi_quorum'])])))

rule all_weak_common_intervals_pw:
    input:
        expand(ACSI_OUT + '/{pw_dist}.wci', pw_dist=PW_SIMS)

rule all_weak_common_intervals_multi:
    input:
        expand(MACSI_OUT + '/macsi.mwci', pw_dist=PW_SIMS)

rule run_acsi:
    input:
        PW_OUT + '/{pw_sim}.active.sim'
    params:
        delta = config['acsi_delta'],
        min = config['acsi_min']
    output:
        ACSI_OUT + '/{pw_sim}.wci'
    shell:
        FFWCI_PYEXEC + 'acsi' + PYSUF + ' -d {params.delta} -m {params.min} '
        '{input} > {output}'

rule run_macsi:
    input:
        expand(PW_OUT + '/{pw_sim}.active.sim', pw_sim=PW_SIMS)
    params:
        delta = config['acsi_delta'],
        min = config['acsi_min'],
        quorum = config['acsi_quorum'],
        blocksize = config['acsi_blocksize']
    output:
        unpruned = temp(MACSI_OUT + '/macsi.mwci.unpruned'),
        pruned = MACSI_OUT + '/macsi.mwci'
    threads:
        config['solver_cpus']
    shell:
        FFWCI_PYEXEC + 'macsi' + PYSUF + ' -d {params.delta} -m {params.min} -q '
        '{params.quorum} -b {params.blocksize} {input} > {output.unpruned};' +
        FFWCI_PYEXEC + 'prune_clique_graph' + PYSUF + ' -q {params.quorum} '
        '{output.unpruned} {input} > {output.pruned}'

