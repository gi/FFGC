# Workflow for FFDCJ (Family-Free DCJ)

FFDCJ_PYEXEC = ' '.join(filter(None, [config['python_bin'], config['ff_bin'] + 'ffdcj/']))

FFDCJ_OUT = '%s/%s' %(config['ffdcj_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR, 'a%s' %config['ffdcj_alpha'], config['ffdcj_maximal_matching'] and 'maximal', config['ffdcj_matching_size'] and 'match_size' or 'genome_size'])))

rule all_ffdcj_exact:
    input:
        expand(FFDCJ_OUT + '/{pw_dist}.active.sim', pw_dist=PW_SIMS)

rule all_ffdcj_ilps:
    input:
        expand('%s/{pw_dist}.lp' % FFDCJ_OUT, pw_dist=PW_SIMS)

rule run_write_ffdcj_ilp:
    input:
        pw_sim = PW_OUT + '/{pw_dist}.active.sim'
    params:
        alpha = config['ffdcj_alpha'],
        maximal = config['ffdcj_maximal_matching'] and "--maximal" or "",
        matching_size = config['ffdcj_matching_size'] and "--matching_size" or "",
        self_edge_weight = config['ffdcj_self_edge_weight'],
    output:
        FFDCJ_OUT + '/{pw_dist}.lp'
    shell:
        FFDCJ_PYEXEC + 'write_ffdcj_ilp' + PYSUF + ' -a {params.alpha} {params.maximal} {params.matching_size}'
        ' -s {params.self_edge_weight} {input.pw_sim} > {output}'

rule run_ffdcj_cplex:
    input:
        FFDCJ_OUT + '/{pw_dist}.lp'
    params:
        threads = config['solver_cpus'],
        time = config['solver_time'],
        memory = config['solver_memory'],
        gapopt = ('-g %g' % float(config['solver_gap'])) if 'solver_gap' in config else ''
    output:
        FFDCJ_OUT + '/{pw_dist}.sol',
        report(FFDCJ_OUT + '/{pw_dist}.log', category='FFDCJ ILPs') if SOLVER == 'cplex' else expand([])
    benchmark:
        FFDCJ_OUT + '/{pw_dist}.benchmark.txt'
    threads:
        int(config['solver_cpus'])
    shell:
        FF_PYSCRIPTS + 'run_cplex' + PYSUF + ' -t {params.threads} -m '
        '{params.memory} -l {params.time} {params.gapopt} -L {input}'

rule run_ffdcj_gurobi:
    input:
        FFDCJ_OUT + '/{pw_dist}.lp'
    params:
        threads = config['solver_cpus'],
        time = config['solver_time'],
        gap = ('%g' % float(config['solver_gap'])) if 'solver_gap' in config else ''
    output:
        FFDCJ_OUT + '/{pw_dist}.mst',
        report(FFDCJ_OUT + '/{pw_dist}.log', category='FFDCJ ILPs') if SOLVER == 'gurobi' else expand([]),
    benchmark:
        FFDCJ_OUT + '/{pw_dist}.benchmark.txt'
    threads:
        int(config['solver_cpus'])
    shell:
        'THREADS={params.threads} GAP={params.gap} LIMIT={params.time} %s/run_gurobi_local.sh {input}' % SCRIPTS_DIR

rule ffdcj_sol_to_matching:
    input:
        sol = FFDCJ_OUT + '/{pw_dist}.' + SOLVER_EXT,
        sim = PW_OUT + '/{pw_dist}.active.sim'
    output:
        FFDCJ_OUT + '/{pw_dist}.active.sim'
    shell:
        FFDCJ_PYEXEC + 'ffdcj_sol_to_matching' + PYSUF + ' %s {input.sol} {input.sim} > {output}' %
        ('--gurobi' if SOLVER == 'gurobi' else '--cplex')
