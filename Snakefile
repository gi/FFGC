# Base rules common to workflows
from math import ceil

# increase the limit for the number of files opened
import resource
soft, hard = resource.getrlimit(resource.RLIMIT_NOFILE)
resource.setrlimit(resource.RLIMIT_NOFILE, (hard, hard))

FF_PYEXEC = ' '.join(filter(None, [config['python_bin'], config['ff_bin']]))
FF_PYSCRIPTS = ' '.join(filter(None, [config['python_bin'], config['ff_bin'] + 'scripts/']))
PYSUF = config['pysuf']

GENOMES = config['genomes']
GENOMES_DIR = config['genome_data_dir']
SPECIES = config['species']

SCRIPTS_DIR = config['ff_bin'] + 'scripts'

SOLVER = config['solver']
SOLVER_EXT = 'sol' if SOLVER == 'cplex' else 'mst' # mst = gurobi

ALIGN_PRG = config['align_prg']
ALIGN_EVALUE = config['align_evalue']

BLAST_DIR = config['blast_dir']

if ALIGN_PRG == 'blast':
    PARAMS = config['blast_params'].replace('-', '').replace(' ', '_')
    ALIGN_PARSTR = 'blast_evalue='+str(ALIGN_EVALUE)
    if PARAMS: ALIGN_PARSTR += '_'+PARAMS
else:
    PARAMS = config['diamond_params'].replace('-', '').replace(' ', '_')
    ALIGN_PARSTR = 'dmnd_evalue='+str(ALIGN_EVALUE)
    if PARAMS: ALIGN_PARSTR += '_'+PARAMS
BLAST_OUT = 'blast/%s_%s' %(config['blast_cmd'], ALIGN_PARSTR)
DMND_OUT = 'diamond/blastp_%s' %ALIGN_PARSTR # diamond works only in blastp mode, no blastn support
ALIGN_OUT = BLAST_OUT if ALIGN_PRG == 'blast' else DMND_OUT

PW_SIMS = config['pw_sims']
SELF_SIMS = config['self_sims']
PW_DIR = config['pw_params'].replace('-', '').replace(' ', '_') 
PW_OUT = '%s/%s' %(config['pw_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR])))

if ALIGN_PRG == 'diamond' and config.get('align_db_type') != 'prot':
    msg = 'ERROR: diamond supports only protein sequences, ' + \
          'create again the project with -p/--use_protein_sequence)'
    print('\n' + msg + '\n')
    raise Exception(msg)


include: 'ffadj.smk'
include: 'ffmedian.smk'
include: 'ffwci.smk'
include: 'ffdcj.smk'
include: 'ffdcjid.smk'

rule create_blast_db:
    input:
        expand('%s/{genome}.gos' %GENOMES_DIR, genome=GENOMES) 
    params:
        dbtype = config['align_db_type'],
        dbversion = config['blast_db_version'],
        title = 'BLAST database of genomes ' + ' '.join(GENOMES),
        dbname = 'blast/%s' %config['align_dbname']
    output:
        expand('blast/%s.{dbfile}' %config['align_dbname'],
        dbfile=config['blast_db_endings'])
    log:
        'blast/%s.log' %config['align_dbname']
    shell:
        BLAST_DIR + config['mkblastdb_cmd'] + ' -in \"{input}\" -hash_index -out {params.dbname} -dbtype '
        '{params.dbtype} -blastdb_version {params.dbversion} -title \"{params.title}\" -logfile {log} '

rule run_blast:
    input:
        fasta = GENOMES_DIR + '/{genome}.gos',
        blast_db = expand('blast/%s.{dbfile}' %config['align_dbname'],
                dbfile=config['blast_db_endings'])
    params:
        dbname = 'blast/%s' %config['align_dbname'],
        blast_params = config['blast_params'],
        evalue = ALIGN_EVALUE
    output:
        BLAST_OUT + '/{genome}.gos.blasttbl'
    benchmark:
        BLAST_OUT + '/{genome}.gos.benchmark.txt'
    threads: 
        ceil(workflow.cores / 2) # so we can run 2 blast jobs in parallel, workflow.cores works only in snakemake >= 5.10
    shell:
        BLAST_DIR + config['blast_cmd'] + ' -db "$(pwd)/"{params.dbname} -outfmt 6 '
        '-num_threads {threads} -evalue={params.evalue} '
        '{params.blast_params} < {input.fasta} > {output}'

rule create_dmnd_db:
    input:
        expand('%s/{genome}.gos' %GENOMES_DIR, genome=GENOMES) 
    output:
        dbfile = 'diamond/%s.dmnd' %config['align_dbname']
    log:
        'diamond/%s.log' %config['align_dbname']
    threads:
        workflow.cores
    shell:
        config['diamond_bin'] + ' makedb --threads {threads} '
        '--db {output.dbfile} >{log} 2>&1 < <(cat {input})'

rule run_diamond:
    input:
        fasta = GENOMES_DIR + '/{genome}.gos',
        dbfile = 'diamond/%s.dmnd' %config['align_dbname']
    params:
        diamond_params = config['diamond_params'],
        evalue = ALIGN_EVALUE
    output:
        DMND_OUT + '/{genome}.gos.blasttbl'
    benchmark:
        DMND_OUT + '/{genome}.gos.benchmark.txt'
    threads: 
        ceil(workflow.cores / 2) # so we can run 2 blast jobs in parallel, workflow.cores works only in snakemake >= 5.10
    shell:
        config['diamond_bin'] + ' blastp --threads {threads} --quiet '
        '--db {input.dbfile} --query {input.fasta} --evalue {params.evalue} '
        '--outfmt 6 --out {output} {params.diamond_params}'

rule run_pairwise_similarities_active:
    input:
        expand('%s/{genome}.gos.blasttbl' %ALIGN_OUT, genome=GENOMES)
    params:
        pw_params = config['pw_params'],
        gos_dir = GENOMES_DIR,
        out_dir = PW_OUT
    output:
        expand(PW_OUT + '/{pw_sim}.active.sim', pw_sim=PW_SIMS+SELF_SIMS),
        PW_OUT + '/genome_map.cfg'
    shell:
        FF_PYEXEC + 'pairwise_similarities' + PYSUF + ' --active-genes '
        '{params.pw_params} -f {params.gos_dir} -o {params.out_dir} --names "%s" {input}' %
        ','.join(SPECIES)

rule run_pairwise_similarities_all:
    input:
        expand('%s/{genome}.gos.blasttbl' %ALIGN_OUT, genome=GENOMES)
    params:
        pw_params = config['pw_params'],
        gos_dir = GENOMES_DIR,
        out_dir = PW_OUT
    benchmark:
        PW_OUT + '/benchmark.txt'
    output:
        expand(PW_OUT + '/{pw_sim}.sim', pw_sim=PW_SIMS+SELF_SIMS)
    shell:
        FF_PYEXEC + 'pairwise_similarities' + PYSUF + ' --all-genes '
        '{params.pw_params} -f {params.gos_dir} -o {params.out_dir} --names "%s" {input}' %
        ','.join(SPECIES)




