#!/usr/bin/env python3
from sys import exit, stdout, path
from os.path import dirname, abspath
from optparse import OptionParser

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readGenomeMap

if __name__ == '__main__':
    usage='usage: %prog [options] <GENOME MAP FILE>'
    parser= OptionParser(usage=usage)


    parser.add_option('-o', '--omit_header', dest='omit_header',
            help='omit headers in the generated output ',
            action='store_true', default=False)
    (options,args)=parser.parse_args()
    if len(args) != 1:
        parser.print_help()
        exit(1)

    dict_of_genomes=dict()
    with open(args[0],'r') as map_file:
        dict_of_genomes=readGenomeMap(map_file)

    if not options.omit_header:
        print('\t'.join(("GENOME_NAME","ACTIVE_GENES")), file = stdout)

    for gname, params in dict_of_genomes.items():
        print('\t'.join((gname, str(len(params['active_genes'])))), file =
                stdout)
