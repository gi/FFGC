#!/usr/bin/env python3

import sys
from cx_Freeze import setup, Executable
from os.path import join, dirname
from subprocess import check_output

if __name__ == '__main__':


#    excludes = ['numpy', 'scipy', 'matplotlib', 'IPython', 'PIL', 'OpenSSL',
#            'PyQt4', 'Tkinter', 'cryptography', 'ctypes', 'compiler', 'cffi',
#            '_codecs_cn', '_codecs_hk', '_codecs_iso2022', '_codecs_jp',
#            'mmap', 'pyexpat', 'readline', 'termios', '_elementtree',
#            '_hashlib', '_multibytecodec', '_multiprocessing', '_ssl', 'bz2',
#            '_codecs_kr', '_codecs_tw', '_csv', 'dateutil', 'datetime',
#            'future_builtins', 'distutils', 'email', 'gi', 'gio', 'glib',
#            'gobject', 'gtk', 'importlib', 'lib2to3', 'logging', 'nose',
#            'pexpect', 'pkg_resources', 'pyasn1', 'pycparser', 'pydoc_data',
#            'pytz', 'requests', 'setuptools', 'sqlite3', 'unittest', 'wx']


    wdir = dirname(sys.argv[0])
    import lib2to3
    lib23_path = dirname(lib2to3.__file__)

    include_files = [join(wdir, 'Snakefile'), join(wdir, 'config.yaml'),
            lib23_path]
    include_files.append(check_output(['which', 'blastn']).strip())
    include_files.append(check_output(['which', 'blastp']).strip())
    include_files.append(check_output(['which', 'makeblastdb']).strip())

    zip_includes = []
    includes = ['zlib']
    #zip_includes = [(join(lib23_path, 'Grammar.txt'), 'lib2to3/Grammar.txt')]

    excludes = ['numpy', 'scipy', 'matplotlib', 'IPython', 'PIL', 'OpenSSL',
            'PyQt4', 'Tkinter', 'cryptography', 'ctypes', 'compiler', 'cffi',
            'gtk', 'pydoc_data', 'sqlite3', 'unittest', 'wx', 'bz2']

    setup(name='ffgc', version='0.2', \
            description='Workflow for Family free Genome Comparison',
            author = 'Daniel Doerr',
            author_email = 'ddoerr@cebitec.uni-bielefeld.de',
            options= {'build_exe': {'include_files': include_files, 'excludes':
                excludes, 'includes': includes, 'zip_includes': zip_includes}},
            executables=[Executable(join(wdir, 'ffgc_create_project.py')),
                Executable(join(wdir, 'enumerate_adjs.py')),
                Executable(join(wdir, 'match_simple_adjs.py')),
                Executable(join(wdir, 'acsi.py')),
                Executable(join(wdir, 'macsi.py')),
                Executable(join(wdir, 'extract_annotated_sequences.py')),
                Executable(join(wdir, 'ffadj_mcs.py')),
                Executable(join(wdir, 'identify_anchors.py')),
                Executable(join(wdir, 'pairwise_similarities.py')),
                Executable(join(wdir, 'prune_clique_graph.py')),
                Executable(join(wdir, 'run_cplex.py')),
                Executable(join(wdir, 'sol_to_pwsims.py')),
                Executable(join(wdir, 'write_ffadj_ilp.py')),
                Executable(join(wdir, 'write_mm_ilp.py')),
                Executable(join(wdir, 'write_ffdcj_ilp.py')),
                Executable(join(wdir, 'ffdcj_sol_to_matching.py')),
                Executable(join(wdir, 'educated_guess.py')),
                Executable(join(wdir, 'write_ffmedian_ilp.py')),
                Executable(join(wdir, 'sol_to_cars.py'))])


