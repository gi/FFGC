#!/usr/bin/env python3
from sys import stdout, stderr, exit
from os.path import abspath, basename, dirname, exists, isfile
from os import unlink
from optparse import OptionParser
from multiprocessing import Pool, cpu_count
from functools import partial
import subprocess

DEFAULT_MEMORY=4
DEFAULT_THREADS=4
DEFAULT_TIME=120
PBS_FILENAME='cplex'

CPLEX_BINARY='cplex'

MAX_CPUS = int(round(0.9 * cpu_count()))

def run_cplex_local(cplex_file, cplex_binary):
    f_base = basename(cplex_file)
    i = f_base.rfind('.cplex')
    if i >= 0:
        f_base = f_base[:i]

    logfile = open('%s/%s.log' %(dirname(cplex_file), f_base), 'w')
    errfile = open('%s/%s.err' %(dirname(cplex_file), f_base), 'w')
    print(' ++ call CPLEX on %s' %cplex_file, file = stdout)
    subprocess.call([cplex_binary, '-f', cplex_file], stdout=logfile,
            stderr=errfile)
    logfile.close()
    errfile.close()

if __name__ == '__main__':

    usage = 'usage: %prog [options] <ILP FILE 1> ... <ILP FILE N>'

    parser = OptionParser(usage=usage)
    parser.add_option('-t', '--threads', dest='threads',
            help='number of threads CPLEX is allowed to start [default=%default]',
            type=int, default=DEFAULT_THREADS, metavar='INT')
    parser.add_option('-m', '--memory', dest='memory',
            help='amount of memory CPLEX is allowed to consume per thread [default=%default GB]',
            type=int, default=DEFAULT_MEMORY, metavar='GB')
    parser.add_option('-l', '--timelimit', dest='timelimit',
            help='timelimit for CPLEX process in minutes  [default=%default minutes]',
            type=int, default=DEFAULT_TIME, metavar='MINUTES')
    parser.add_option('-g', '--gap', dest='gap',
            help='relative tolerance on the gap between the best integer objective and the objective of the best node: |bestbound-bestinteger|/(1e-10+|bestinteger|), default: 1e-04 = 0.01%',
            type=float, metavar='FLOAT')
    parser.add_option('-L', '--local', dest='runLocal',
            help='run CPLEX locally, do not submit to OGE',
            action='store_true', default=False)

    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.print_help()
        exit(1)

    cFiles = list()

    for f in args:
        fName = '%s/%s' %(dirname(f) or '.', basename(f).rsplit('.', 1)[0])
        outFile = '%s.sol' %fName
        logFile = '%s.log'%fName
        errFile = '%s.err'%fName

        cFile = open('%s.cplex' %fName, 'w')

        print('SET WORKMEM %s' %(options.memory * options.threads * 1024), file
                = cFile)
        print('SET THREADS %s' %(options.threads), file = cFile)
        print('SET TIMELIMIT %s' %(options.timelimit * 60), file = cFile)
        if options.gap:
            print('SET MIP TOLERANCES MIPGAP %s' %(options.gap), file = cFile)
        print('READ %s lp' %abspath(f), file = cFile)
        # solve mixed integer linear program
        print('MIPOPT', file = cFile)
#    print >> cFile, 'DISPLAY SOLUTION OBJECTIVE'
#    print >> cFile, 'DISPLAY SOLUTION VARIABLES -'
        print('SET OUTPUT WRITELEVEL 3', file = cFile)
        print('WRITE %s' %outFile, file = cFile)

        cFile.close()

        print('Created cplex file %s' %abspath(cFile.name), file = stdout)

        cFiles.append(abspath(cFile.name))

        # delete output .sol file and log and error files if exists
        if isfile(outFile):
            unlink(outFile)
        if isfile(logFile):
            unlink(logFile)
        if isfile(errFile):
            unlink(errFile)

    if options.runLocal:
        print('Run CPLEX locally...', file = stdout)
        pool = Pool(processes=int(max(MAX_CPUS/options.threads, 1)))
        for f in cFiles:
            pool.apply_async(partial(run_cplex_local, cplex_binary=CPLEX_BINARY), [f])

        pool.close()
        pool.join()
        print('DONE!', file = stdout)
    else:
        # write array.pbs
        n_jobs = len(cFiles)
        aryPbs = """#!/bin/sh
#PBS -N arraytest
cd $SGE_O_WORKDIR

echo WORKDIR is $SGE_O_WORKDIR

# all files in this task array
TASK_ARY=(%s)
# current task file
CUR_TASK_FILE=${TASK_ARY[$SGE_TASK_ID - 1]}
LOG_FILE=${CUR_TASK_FILE%%.cplex}.log
ERR_FILE=${CUR_TASK_FILE%%.cplex}.err
echo "Solving ILP $CUR_TASK_FILE "
%s -f $CUR_TASK_FILE > $LOG_FILE 2> $ERR_FILE"""%(' '.join(map(lambda x: '"%s"'%x, cFiles)), CPLEX_BINARY)

        x = open('%s.pbs' %PBS_FILENAME, 'w')
        x.write(aryPbs)
        x.flush()
        x.close()
        # give the FS a moment time to update
        #time.sleep(600)
        print('Submitting %s jobs' %n_jobs, file = stdout)
        # call qsub
        subprocess.call(['qsub', '-wd', abspath(dirname(args[0])), '-l',
            'arch=lx24-amd64', '-pe', 'multislot', str(options.threads), '-l',
            'mem_free=%sG' %options.memory, '-l',
            'h_rt=%02.0f:%02.0f:00'%(options.timelimit/60, (options.timelimit+5)%60), '-o',
            '%s.log'%PBS_FILENAME, '-e', '%s.err'%PBS_FILENAME, '-t',
            '1-%s'%n_jobs, '%s.pbs' %PBS_FILENAME])

