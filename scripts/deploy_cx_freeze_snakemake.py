#!/usr/bin/env python3

import sys
from cx_Freeze import setup, Executable
from os.path import join, dirname
from subprocess import check_output
import snakemake

if __name__ == '__main__':


    executables = [Executable('snakemake', targetName='smake')]
    include_files = [dirname(snakemake.__file__)]

    packages = ['snakemake']
    excludes = []

    setup(name='snakemake', version=str(snakemake.__version__), \
            description='Python Workflow Engine',
            author = 'Johannes Koester',
            options= {'build_exe': {'excludes': excludes, 'include_files':
                include_files}},
            executables=executables)


