#!/bin/bash
# Runs gurobi solver for all .lp files passed as arguments.
# gurobi_cl must be in PATH

LANG=C
if [ -z "$THREADS" ]
then
    #THREADS=$(nproc) # all cores
    THREADS=4
fi
if [ -z "$GAP" ]
then
    GAP=1e-4 # stop after this
fi
if [ -z "$LIMIT" ]
then
    LIMIT=120
fi

[ $# -lt 1 ] && echo "Missing arguments" && exit 1

for f in "$@"
do
    echo "Running gurobi for $f (threads: $THREADS, gap: $GAP, time_limit: ${LIMIT} minutes)"
    LIMIT_SEC=$(("$LIMIT*60"))
    base="${f%.lp}"
    if [ -f "$base.mst" ]
    then
        echo -e "\nResults for $f already exist\n"
        continue
    fi
    MIPSTART=""
    if [ -f "$base.initial.mst" ]
    then
        echo -e "\nFound MIP start file with variables hinting an initial solution $base.initial.mst\n"
        MIPSTART="InputFile=$base.initial.mst"
    fi

    echo > "$base.log" # clear log
    #LogToConsole=1
    gurobi_cl LogFile="$base.log" LogToConsole=0 ResultFile="$base.mst" $MIPSTART Threads=$THREADS TimeLimit=$LIMIT_SEC TuneOutput=3 MIPGap=$GAP "$f"
    ret=$?
    # remove results if solver was interrupted
    grep -q 'Solve interrupted' "$base.log" && echo "Solver interrupted, removing result file (.mst)..." && rm -f "$base.mst"
done

exit 0

