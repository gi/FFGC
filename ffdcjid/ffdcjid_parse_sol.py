#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021,2022 Diego Rubert
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software. If not, see <https://www.gnu.org/licenses/>


from collections import defaultdict
from itertools import groupby, chain
from statistics import mean
from re import match, search
import sys
import xml.etree.ElementTree as ET
from argparse import ArgumentParser, FileType

from ffdcjid_util import readGeneNumberMap, readSimilaritiesFile, readGenomesCfg, FAMILY_FREE_ID, EXTREMITY_TELOMERE, EXTREMITY_TAIL, EXTREMITY_HEAD, PATTERN_LOCUS_TAG

TYPE_SOL = 'sol'
TYPE_MST = 'mst'

__author__ = 'diego'


class Solution:
    def __init__(self, fgenomes, alpha, fsim, fgidmap, fsol, sol_type, flog):
        self.alpha = alpha
        self.geneidmap, self.genome1, self.genome2 = readGeneNumberMap(fgidmap)
        #self.geneidmap_rev = { v:k for genome,genes in self.geneidmap.items() for k,v in genes.items() }
        self.geneidmap_rev = { genome: {v:k for k,v in genes.items()} for genome,genes in self.geneidmap.items() }
        self.genomes = readGenomesCfg(fgenomes)
        if sol_type == TYPE_SOL:
            self.parseSol(fsol)
            self.parseLogCPLEX(flog)
        else:
            self.parseMst(fsol)
            self.parseLogGurobi(flog)

        self.genome_labels = { g['name']:g['label'] for g in self.genomes.values() }
        self.genome1label = self.genome_labels[self.genome1]
        self.genome2label = self.genome_labels[self.genome2]
        self.translateGenomeNameToLabel(self.geneidmap)
        self.translateGenomeNameToLabel(self.geneidmap_rev)
        self.translateGenomeNameToLabel(self.genomes)

        sim = readSimilaritiesFile(fsim, FAMILY_FREE_ID)
        edges_genome1 = defaultdict(dict) # original edges, only from genome1 to genome2
        edges = defaultdict(dict)
        for s in sim: # we will translate original to unique IDs and append the genome label
            edges_genome1[s['g1']][s['g2']] = s['weight']
            g1 = "%d_%s" % (self.geneidmap[self.genome1label][s['g1']], self.genome1label)
            g2 = "%d_%s" % (self.geneidmap[self.genome2label][s['g2']], self.genome2label)
            edges[g1][g2] = edges[g2][g1] = s['weight']
        self.edges_genome1 = edges_genome1 # original similarity edges
        self.sim = edges


    def translateGenomeNameToLabel(self, d):
        """Translate keys of dict based on self.genome_labels."""
        for key in list(d.keys()):
            d[self.genome_labels[key]] = d.pop(key)


    def parseSol(self, fsol):
        """Extract information from solution file (CPLEX .sol format)."""
        tree = ET.parse(fsol)
        root = tree.getroot()
        header = root.find('header')
        self.objective = float(header.get('objectiveValue'))
        self.status = header.get('solutionStatusString')
        # generator expression to fetch all variables (instead of wasting memory storing them)
        # we round because "almost 0" sometimes happen, and that means 0
        allvars = ((var.get('name'), round(float(var.get('value')))) for vsection in root.findall('variables') for var in vsection)
        self.parseVars(allvars)


    def parseMst(self, fsol):
        """Extract information from solution file (Gurobi .mst format)."""
        # generator expression to fetch all variables (instead of wasting memory storing them)
        # we round because "almost 0" sometimes happen, and that means 0
        vartuple = lambda name,value : (name, round(float(value)))
        allvars = (vartuple(*line.split()) for line in fsol if not line.startswith('#'))
        self.parseVars(allvars)


    def parseVars(self, allvars):
        """Extract runs, indels, matching, transitions, etc, from ILP variables."""
        self.p = 0
        cycle_representatives = set()
        cycle_labels = defaultdict(list)
        indel = [] # indel edges
        indel_vertices = set()
        match = [] # matching edges
        adj = [] # adjacency edges
        neighbors = defaultdict(list)        
        transitions = set()
        #vertices_runs = set()

        for name,value in allvars:
            if name == 'p':
                self.p = value
                continue

            if value == 0:
                continue

            if name.startswith('z_'):
                cycle_representatives.add(name[2:])
                continue

            if name.startswith('y_'):
                cycle_labels[value].append(name[2:])
                continue

            if name.startswith('t_'):
                transitions.add(tuple(name[3:-1].split(','))) # transition edges are always adjacency edges
                continue

            if name.startswith('r_'):
                # vertices_runs.add(name[2:]) # useless to store, just labels 0/1 if vertices are part of a A-run/B-run respec.
                continue
            
            # must be x_
            g1,g2 = name[3:-1].split(',')
            
            #if g1[-1] not in ('h','t'): # cap
            #    continue

            neighbors[g1].append(g2)
            neighbors[g2].append(g1)

            num1,genome1,ext1 = g1.split('_') # gene number, genome and extremity type
            num2,genome2,ext2 = g2.split('_')

            if num1 == num2: # indel edge
                indel.append( (g1,g2) )
                indel_vertices.add(g1)
                indel_vertices.add(g2)
            elif genome1 == genome2: # adjacency edge 
                adj.append( (g1,g2) )
            else: # matching edge
                match.append( (g1,g2) )

        # print([ len(n) for n in neighbors.values() ]) # testing

        # indel-free cycles (there is no way to find indel-enclosing cycles this way because the z_ and y_ of their vertices is zero)
        idfree_cycles = dict()
        for c in cycle_labels.values():
            for v in c:
                if v in cycle_representatives:
                    idfree_cycles[v] = c
                                        
        # now we use transitions to look for indel-enclosing cycles
        next_vertex = lambda prev, v: neighbors[v][1] if neighbors[v][0] == prev else neighbors[v][0] # given prev and v, returns the next vertex in the cycle
        visited = set() # we have to control visited vertices because we don't know beforehand if transitions are part of the same cycle or not
        idencl_cycles = dict()
        for t in transitions:
            start, end = t
            if start in visited: 
                continue

            visited.add(end)
            visited.add(start)
            v, prev = start, end
            cycle = [end]
            
            while v != end:
                visited.add(v)
                cycle.append(v)
                prev, v = v, next_vertex(prev, v)
                
            idencl_cycles[end] = cycle # we choose this vertex to label the cycle
            
        # now we look for runs in indel-enclosing cycles
        runs = dict()
        for label, c in idencl_cycles.items(): # for each indel-enclosing cycle
            #print(label, c)
            c_runs = [] # runs of the cycle
            id_positions = [ (i, v.split('_')[1]) for i,v in enumerate(c) if v in indel_vertices ]
            id_groups = [ {'genome':genome, 'indels':[ grp[0] for grp in list(grp) ]} for genome, grp in groupby(id_positions, lambda x: x[1]) ]
            for grp in id_groups:
                grp['indels'] = c[grp['indels'][0]:grp['indels'][-1]+1] # get only the first and the last vertices of a run in the same genome
            if len(id_groups) > 1 and id_groups[0]['genome'] == id_groups[-1]['genome']: # if 1st and last runs are in the same genome, we join them
                id_groups[-1]['indels'].extend(id_groups[0]['indels'])
                id_groups[0]['indels'] = id_groups.pop()['indels']
            runs[label] = [ grp['indels'] for grp in id_groups ]
                    
        self.idfree_cycles = idfree_cycles
        self.idencl_cycles = idencl_cycles
        self.transitions = transitions
        self.runs = runs # grouped by the cycles they are part of
        self.indel_edges = indel
        self.indel_vertices = indel_vertices
        self.match_edges = match # careful: includes "matching" edges between telomeres
        self.adj_edges = adj

            
    def parseLogCPLEX(self, flog):
        """Parse the log file to obtain extra information."""
        # We just need to find the running time and the solution gap,
        # the objective value is obtained from the .sol file
        self.gap = 0.0
        for line in flog:
            if match('Current MIP best bound =.*gap =', line):
                self.gap = float(search('gap =[^,]*, *([^%]*)%', line).groups()[0])
            elif match('Solution time =', line):
                self.time = float(search('Solution time = *([^ ]*)', line).groups()[0])


    def parseLogGurobi(self, flog):
        """Parse the log file to obtain extra information."""
        # Need to look for the time, gap and objective value
        self.status = 'Unknown status'
        for line in flog:
            if match('Explored.*nodes.*in.*seconds', line):
                self.time = float(search(' ([^ ]*) seconds', line).groups()[0])
            elif match('Best objective.*best bound.*gap', line):
                self.objective = float(search('Best objective *([^,]*),', line).groups()[0])
                self.gap = float(search('gap *([^%]*)%', line).groups()[0])
            elif match('Time limit reached', line):
                self.status = line.strip()
            elif match('Optimal solution found', line):
                self.status = line.strip()


    def computeIndelEdgeWeights(self):
        """Find the weights of indel edges IN THE SOLUTION."""
        self.indel_weights = dict()
        for e in self.indel_edges:
            gene = e[0][:-2] # remove tail/head/cap of one endpoint
            self.indel_weights[gene] = max(self.sim[gene].values(), default=0)

    
    def printObjective(self, fobj):
        """Compute remaining values and print the objective function value
        and related values.
        """
        # we don't want to count edges between telomeres as "matching" edges
        M = len(self.match_edges) - len([t for t in self.match_edges if t[0].endswith(EXTREMITY_TELOMERE)])

        # we don't use head to avoid duplicated computation
        # we don't count telomeres, their weight is 0
        wM = sum(self.sim[g1[:-2]][g2[:-2]] for g1,g2 in self.match_edges if g1.endswith(EXTREMITY_TAIL))

        self.computeIndelEdgeWeights()
        wMcomplement = sum(self.indel_weights.values()) 
        
        obj = self.p + M - len(self.idfree_cycles) \
            + len(self.transitions)/2 - wM + self.alpha * wMcomplement
        
        print('p* + 2|M| - indel_free_cycles + transitions/2 - w(M) + alpha * w(complement(M)) =', file=fobj)
        print('%d + 2*%d - %d + %d/2 - %.2f + %.2f * %.2f = %f (%f was reported by the solver)' %
              (self.p,
               M/2, # is already doubled
               len(self.idfree_cycles),
               len(self.transitions),
               wM,
               self.alpha,
               wMcomplement,
               obj,
               self.objective), file=fobj)
        
        
    def printRuns(self, fruns):
        """Print information about runs. Cycle lenghts reported are the ones
        in the adjacency graph, not in the relational diagram, which have
        twice the length (e.g. a cycle of length 2 reported has length 4 in
        the relational diagram). The length of a run is the number of indel
        edges in it.
        """
        lenCycle = lambda c: len(c)//2
        lenRun = lambda r: sum(1 for v in r if v in self.indel_vertices)//2
        
        if len(self.idfree_cycles) > 0:
            print("Indel-free cycles: %d (avg. len.: %f)" % 
                  (len(self.idfree_cycles),
                   mean(lenCycle(c) for c in self.idfree_cycles.values())),
                  file=fruns)
        else:
            print("Indel-free cycles: 0", file=fruns)

        if len(self.idencl_cycles) > 0:
            print("Indel-enclosing cycles: %d (avg. len.: %f, avg. runs: %f)" %
                  (len(self.idencl_cycles),
                   mean(lenCycle(c) for c in self.idencl_cycles.values()),
                   mean(len(self.runs[label]) for label in self.idencl_cycles.keys())),
                  file=fruns)
        else:
            print("Indel-enclosing cycles: 0", file=fruns)
            
        i = 0
        for label,c in self.idencl_cycles.items():
            print("\tID-encl cycle #%d: len = %d, runs = %d, avg. run. len. = %f (%s)" %
                  (i, lenCycle(c), len(self.runs[label]), mean(lenRun(r) for r in self.runs[label]),
                  ','.join(str(lenRun(r)) for r in self.runs[label])),
                  file=fruns)
            i += 1
            
        if len(self.runs) > 0:
            print("Runs: %d (avg. run len.: %f, max run len: %d)" %
                  (sum(len(cruns) for cruns in self.runs.values()),
                   mean(lenRun(run) for cruns in self.runs.values() for run in cruns),
                   max(lenRun(run) for cruns in self.runs.values() for run in cruns)),
                   file=fruns)
        else:
            print("Runs: 0", file=fruns)


    def printSolverInfo(self, fsolver):
        """Print information about the solver (time, gap, etc)."""
        print("Solver time:", self.time, "seconds", file=fsolver)
        print("Solver gap: %g%%" % self.gap, file=fsolver)
        print("Solver status:", self.status, file=fsolver)
    
    
if __name__ == "__main__":
    parser = ArgumentParser(description= 'Reads solution and log files given by CPLEX or Gurobi, the mapping of the uniquely labeled genes given by write_ffdcjid_ilp.py and the corresponding genomes.cfg and similarities files given by FFGC, then shows the FFDCJID distance and other related values/information.')
    parser.add_argument('-C', '--cfg', required=True, type=FileType('r'), metavar='genomes.cfg',
                        help='Genomes configuration file (generated by FFGC).')
    parser.add_argument('-i', '--sim', required=True, type=FileType('r'), metavar='file.sim',
                        help='Gene similarities file (generated by FFGC).')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-s', '--sol', metavar='file.sol', type=FileType('r'),
                       help='Solution file (given by CPLEX).')
    group.add_argument('-m', '--mst', metavar='file.mst', type=FileType('r'),
                       help='Solution file (given by Gurobi).')
    parser.add_argument('-l', '--log', required=True, type=FileType('r'), metavar='file.log',
                        help='Log file (given by CPLEX or Gurobi).')
    parser.add_argument('-g', '--geneid-map', metavar='file.gidmap', required=True, type=FileType('r'),
                        help='File with gene IDs mapping (given by write_ffdcjid_ilp.py), that maps gene numbers in the .sim to the unique numbers used in the ILP file.')
    parser.add_argument('-a', '--alpha', required=True, dest='alpha', metavar='α', type=float,
                        help='Factor for the contribution of indels in the distance formula. Again, the same value used for the ILP generation must be passed.')
    parser.add_argument('-o', '--output', required=False, type=FileType('w'), default=sys.stdout, metavar='file.output',
                        help='Output file for distance/objective function (and other info). If not given, values are written to stdout.')

    args = parser.parse_args()
    s = Solution(args.cfg, args.alpha, args.sim, args.geneid_map,
                 args.sol if args.sol else args.mst,
                 TYPE_SOL if args.sol else TYPE_MST,
                 args.log)
    s.printObjective(args.output)
    s.printRuns(args.output)
    s.printSolverInfo(args.output)

    if args.output != sys.stdout:
        args.output.close()
        
    exit(0)

