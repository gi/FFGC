#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021 Diego Rubert
#
# (Based on DING, Copyright 2020 L. Bohnenkämper, M.D.V. Braga, D. Doerr and J. Stoye)
#
# (Based on GEN-DIFF, Copyright (C) 2020 Diego P. Rubert, https://gitlab.ub.uni-bielefeld.de/gi/gen-diff)
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software. If not, see <https://www.gnu.org/licenses/>


# Auxiliary functions needed by different FFDCJID scripts

from re import compile
from configparser import ConfigParser
from collections import OrderedDict
from string import ascii_uppercase
from sys import stderr
import xml.etree.ElementTree as ET

__author__ = 'diego'

CHR_CIRCULAR = ')'
CHR_LINEAR = '|'
ORIENT_POSITIVE = '+'
ORIENT_NEGATIVE = '-'
EXTREMITY_TAIL = 't' # must be a single char
EXTREMITY_HEAD = 'h' # must be a single char
EXTREMITY_TELOMERE = '$' # must be a single char
#all telomeres are equivalent concerning the distance
TELOMERE_ID = -1

FAMILY_FREE = 'ffdcj'
FAMILY_FREE_ID = 'ffdcjindel'
UNW_FAMILY_FREE_ID = 'unwffdcjindel'

# Here (?:...) makes a non-capturing group, because we CAN match pipe
# | inside a character class [] but we CAN'T match end of line $ ($
# looses its special meaning, representing then a literal $, there is
# no way to change that).
PATTERN_LOCUS = compile(r'^.*\|([0-9]+:[0-9]+)(?:$|\|)')
PATTERN_LOCUS_TAG = compile(r'(?:^|.*\|)locus\|([^\|]+)(?:$|\|)') # first group is non-capturing
PATTERN_PROTID = compile(r'^[^\|]*\|([^\|]+)')
PATTERN_CHR = compile(r'.*\|chromosome\|([^\|]+)(?:$|\|)')
PATTERN_STRAND = compile(r'.*\|strand\|([+-])(?:$|\|)')

CFG_FASTA_KEY = 'fasta_file'
CFG_CHR_KEY = 'chromosomes'
CFG_GENES_KEY = 'genes'
CFG_SP_KEY = 'species'


#TODO: protect for parallel execution
class Simple_Id_Generator:
    last = 0
    def get_new(self):
        self.last+=1
        return self.last


def parseGeneRecord(rec, order=None):
    """Parses a gene record of .gos fasta file.
       Optionally, add order to the gene object.
    """
    locus = PATTERN_LOCUS.match(rec)

    if locus:
        locus = locus.groups()[0]
    else:
        locus = None
        #raise Exception('Invalid format while parsing gene record, ' + \
        #                'locus not found in "%s"' % rec)    
    
    chromosome = PATTERN_CHR.match(rec)
    if not chromosome:
        raise Exception('Invalid format while parsing gene record, ' + \
                        'chromosome not found in "%s"' % rec)
    chromosome = chromosome.groups()[0]

    strand = PATTERN_STRAND.match(rec)
    if not strand:
        raise Exception('Invalid format while parsing gene record, ' + \
                        'strand not found in "%s"' % rec)
    strand = strand.groups()[0]

    if order:
        return {'id': rec, 'locus': locus, 'chr': chromosome, 'strand': strand, 'order': order}
    else:
        return {'id':rec, 'locus':locus, 'chr':chromosome, 'strand':strand}


def readGenomeGos(fname):
    """Reads one genome in .gos format given the file name. We suppose
    that fname ends with .gos. That file format stores the gene order
    sequence of a genome X in multi-record fasta format. The ID of
    each record is a concatenated string of key/value pairs delimited
    by |, the single most important being the chromosome|<chromosome
    ID> attribute. The succession of records corresponds to the genes'
    order in their corresponding gene order sequences.
    """

    genes = OrderedDict() # it's ordered (important!)
    order = 1
    
    with open(fname, 'r') as f:
        no_locus = False
        for l in f:
            if not l.startswith('>'):
                continue
            l = l[1:].strip() # remove >
            gene = parseGeneRecord(l)
            no_locus = no_locus or not gene['locus']
            gene['order'] = order
            genes[order] = gene
            order += 1

        if no_locus:
            print('Warning: Missing locus for one or more entries ' + \
                  'while parsing "%s"' % fname, file=stderr)
            
    return genes


def readGenomesCfg(genome_cfg_file):
    """Reads the genomes.cfg file given the File object, and the .gos files
    of genomes listed in genomes.cfg. For now, we assume just linear
    chromosomes, we have no such information stored in current file
    formats.
    """

    config = ConfigParser()
    config.read_file(genome_cfg_file)

    # generates at least the number of ascii labels we need
    labels = [l for l in ascii_uppercase]
    while len(labels) < len(config):  # the number of genomes is at most the number of sections in config
        labels = [l1 + l2 for l1 in labels for l2 in ascii_uppercase]

    data = OrderedDict()
    labels_idx = 0
    for Gx in config.sections():
        gdata = {'name': Gx, 'label': labels[labels_idx]}
        labels_idx += 1
        for k, v in config.items(Gx):
            if k == CFG_GENES_KEY:
                gene_order = OrderedDict((gid, order + 1) for order, gid in enumerate(map(lambda x: x.strip(), v.split(','))))
                v = OrderedDict((order, parseGeneRecord(gid, order)) for gid, order in gene_order.items())
            elif k == CFG_CHR_KEY:  # chromosomes are defined by the genes bellow
                continue
            gdata[k] = v

        required_keys = (CFG_GENES_KEY, CFG_SP_KEY)
        for k in required_keys:
            if k not in gdata:
                raise Exception('Error while parsing genomes configuration, ' + \
                                'key %s not found in section "%s" of' \
                                '"%s"' % (k, Gx, genome_map_file))

        chromosomes = OrderedDict()
        for gene in gdata['genes'].values():
            gid, gchr, gorder = gene['id'], gene['chr'], gene['order']
            gene['edges'] = list()
            if gchr not in chromosomes:
                chromosomes[gchr] = {'linear': True, 'circular': False, 'fake': False, 'genes': list()}
            chromosomes[gchr]['genes'].append(gorder)
        for c in chromosomes.values():
            c['genes'].sort()
        gdata['chromosomes'] = chromosomes

        data[Gx] = gdata

    return data


def readSimilaritiesFile(sim_file, setting, threshold=0.0, keep_chr_data=False):
    """Given the file name or File object, reads the file with computed
    gene similarities.  rel_orient is 1 if the strand of both genes is
    + or -, and -1 if the strands are different (+ and -, or - and +).
    All weights are set to 1 if setting == 'gc'. Edges with weights
    smaller than threshold are ignored. The save_chr parameter is used
    to keep chromosome data or discard it. The number of each gene is its
    order in **the list of active genes**, not the order in considering all
    genes (as in the .gos/fasta files). Probably you'll want to call
    translateSimilarityeEdges after calling this function.
    """
    edges = []
    keys = ('chr1', 'g1', 'chr2', 'g2', 'rel_orient', 'weight')
    weight_type = (lambda x: 1) if setting == UNW_FAMILY_FREE_ID else float
    if keep_chr_data:
        types = (str, int, str, int, int, weight_type)
    else:
        types = (None, int, None, int, int, weight_type) # not saving chromosomes

    if isinstance(sim_file, str):
        f = open(sim_file, 'r')
    else:
        f = sim_file
        
    for l in f:
        values = l.split()
        if float(values[-1]) < threshold: # float(values[-1]) => weight
            continue
        edges.append( { k: t(v) for k,v,t in zip(keys,values,types) if t is not None})
    return edges


def writeGeneNumberMap(genome, map_file):
    """Writes the gene number map. A map starts with ">genome_name"
    followed by lines in the format old_number new_number.
    """
    map_file.write(">%s\n" % genome['name'])
    for g in genome['genes'].values():
        map_file.write("%s %s\n" % (g['order'], g['unique_id']))
    

def readGeneNumberMap(map_file):
    """Reads the mapping of gene IDs and guesses the first and second
    genome names.
    """
    G1name = map_file.readline()[1:-1] # without starting > and ending \n
    G1map = dict()
    line = map_file.readline()
    while not line.startswith('>'):
        idorig, idnew = line.split()
        G1map[int(idorig)] = int(idnew)
        line = map_file.readline()
    G2name = line[1:-1]
    G2map = dict()
    for line in map_file: # remaining lines
        idorig, idnew = line.split()
        G2map[int(idorig)] = int(idnew)

    return { G1name:G1map, G2name:G2map }, G1name, G2name


def parseSolMatching(fsol):
    """Extract matching edges from a (cplex) solution file."""
    tree = ET.parse(fsol)
    root = tree.getroot()
    header = root.find('header')

    match = [] # matching edges

    # we round because "almost 0" sometimes happen, and that means 0
    allvars = ( ( var.get('name'), round(float(var.get('value'))) ) for vsection in root.findall('variables') for var in vsection )

    for name,value in allvars:
        if not name.startswith('x_') or value == 0:
            continue

        g1,g2 = name[3:-1].split(',')

        if g1[-1] not in (EXTREMITY_HEAD,EXTREMITY_TAIL): # cap
            continue

        num1,genome1,ext1 = g1.split('_') # gene number, genome and extremity type
        num2,genome2,ext2 = g2.split('_')

        #if num1 == num2 or genome1 == genome2: # indel or adjacency edge
        if genome1 == genome2:  # indel or adjacency edge
            continue

        match.append( (g1,g2) )

    return match


def parseMstMatching(fmst):
    """Extract matching edges from a (gurobi) solution file."""
    match = []
    for l in fmst:
        l = l.strip()
        if not l.startswith('x_'):
            continue

        name, value = l.split()
        value = float(value)

        if value < 0.01:  # x edge not chosen, 0 may be 0.00001
            continue

        g1, g2 = name[3:-1].split(',')

        if g1[-1] not in (EXTREMITY_HEAD, EXTREMITY_TAIL):  # cap
            continue

        num1, genome1, ext1 = g1.split('_')  # gene number, genome and extremity type
        num2, genome2, ext2 = g2.split('_')

        #if num1 == num2 or genome1 == genome2:  # indel or adjacency edge
        if genome1 == genome2:  # indel or adjacency edge
            continue

        match.append((g1, g2))

    return match

