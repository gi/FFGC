#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021,2022 Diego Rubert
#
# (Based on DING, Copyright 2020 L. Bohnenkämper, M.D.V. Braga, D. Doerr and J. Stoye)
#
# (Based on GEN-DIFF, Copyright (C) 2020 Diego P. Rubert, https://gitlab.ub.uni-bielefeld.de/gi/gen-diff)
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software. If not, see <https://www.gnu.org/licenses/>


import sys
from os import path
from ffdcjid_util import *
import argparse
import logging
from functools import reduce
from collections import defaultdict, Counter, deque
from math import factorial, ceil, floor
import networkx as nx
from random import choices
from string import ascii_lowercase
from copy import deepcopy


__author__ = 'diego'

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

COMMENT = "COMMENT"
HEURISTIC_CAPPING_THRESHOLD_ABSOLUTE = 3 # for each chromosome, keeps its caps connected only to the 3 top-sharing chromosomes
HEURISTIC_CAPPING_THRESHOLD_RELATIVE = 0.01 # for each chromosome, keeps its caps connected only to chromosomes that share at least 1% of its top-sharing pair

FAKE_CHR_PREFIX = "FAKE_"

class Vertex:
    vertex_id = -1
    extremity = EXTREMITY_HEAD
    gene_id = 0
    chr_id = ''
    genome_id = -1
    first_genome = False
    def __init__(self, v_id, ext, gene_id, chr_id, genome_id, first):
        self.vertex_id = v_id # a unique id, not the original gene number
        self.gene_id = gene_id
        self.chr_id = chr_id
        self.genome_id = genome_id
        self.first_genome = first
        if not ext in [EXTREMITY_HEAD,EXTREMITY_TAIL,EXTREMITY_TELOMERE]:
            raise Exception('Invalid extremity "%s" assigned to vertex!'%ext)
        self.extremity = ext
    def __repr__(self):
        return "%d%s_%s(%d)" % (self.vertex_id, self.extremity, self.genome_id, self.gene_id)

    def is_cap(self):
        return self.extremity == EXTREMITY_TELOMERE
    
    def get_i(self):
        d=0
        if self.extremity == EXTREMITY_HEAD:
            d=1
        if self.extremity == EXTREMITY_TAIL:
            d=2
        return 3*self.vertex_id+d
        

class Edge:
    vertex1 = 0
    vertex2 = 0
    #auxiliary variable for the (unlikely) case of a singleton circular chromosome
    can_be_self = True
    def __init__(self, v1, v2, w = 0):
        self.vertex1 = v1
        self.vertex2 = v2
        self.w = w
        self.singleton_circular = False # for catching the (rare) edge cases, that an edge belongs to a circular chromosome
    def __repr__(self):
        return "%s~%s[%.2f]" % (self.vertex1, self.vertex2, self.w)

    def set_weight(self, w):
        self.w = w
    def weight(self):
        return self.w
    def is_self(self):
        return self.can_be_self and (self.vertex1.vertex_id == self.vertex2.vertex_id)
    def is_adjacency(self):
        return (not self.is_self()) and (not self.is_cross_genome())
    def is_cross_genome(self):
        return self.vertex1.genome_id != self.vertex2.genome_id
    def is_cap(self):
        return self.vertex1.is_cap() # vertex2 should be the same
    def repress_self(self):
        self.can_be_self = False
    def set_in_singleton_circular(self):
        self.singleton_circular = True
    def is_in_singleton_circular(self):
        return self.singleton_circular


class ILP:
    variables = []
    objective_function = []
    conditions = []
    def addcomment(self, comment): # appends a empty condition as a comment
        self.conditions.append(([], COMMENT, [], ' \\ %s' % comment))
    def addcomment_obj(self, comment, before = ''): # appends a empty objective function item as a comment
        self.objective_function.append((COMMENT, '%s \\ %s' % (before, comment)))
    
class ILP_variable:
    Id = 0
    rangemin = 0
    rangemax = 1
    def __init__(self, Id, rangemin=0, rangemax=1):
        self.Id = Id
        self.rangemin = rangemin
        self.rangemax = rangemax
    def is_binary(self):
        return (self.rangemin==0) and (self.rangemax==1)


def create_chromosome_graph(chrname, chromosome, id_generator, genome, first_genome):
    ''' Creates the vertices and telomeres as well as adjacency and self edges
        of a single Chromosome (chr_kind, [(dir,gene)])
    '''
    vertices = []
    edges = []
    genome_name = genome['label']
    
    #CAUTION: Telomeres need different ids because they cannot be distinguished by h t
    if chromosome['linear']:
        vertices.append(Vertex(id_generator.get_new(), EXTREMITY_TELOMERE,
                                   TELOMERE_ID, chrname, genome_name, first_genome))

    for number in chromosome['genes']: 
        gene = genome['genes'][number]
        unique_id = gene['unique_id']
        v = (Vertex(unique_id,EXTREMITY_TAIL, number, chrname, genome_name, first_genome),
             Vertex(unique_id,EXTREMITY_HEAD, number, chrname, genome_name, first_genome))
        if gene['strand']==ORIENT_NEGATIVE:
            v = v[::-1]
        vertices.extend(v)

    if chromosome['linear']:
        vertices.append(Vertex(id_generator.get_new(), EXTREMITY_TELOMERE,
                                   TELOMERE_ID, chrname, genome_name, first_genome))
        old = 0
        start = 1
    else:
        old = -1
        start = 0
    #print(map(lambda x: (x.gene_id,x.extremity, x.vertex_id),vertices))
    for new in range(start,len(vertices)):
        edges.append(Edge(vertices[old],vertices[new]))
        old = new
    
    #spurious case that of a singleton circular
    if chromosome['circular'] and len(chromosome['genes']) == 1:
        edges[0].repress_self()
        edges[1].set_in_singleton_circular()

    return vertices, edges

def create_genome_graph(genome, id_generator, first_genome):
    ''' Creates the vertices and telomeres as well as adjacency and self edges
        for a complete genome (name,[(chr_kind, [(dir,gene)])])
    '''
    vertices = []
    edges = []
    for name, c in genome['chromosomes'].items():
        v, e = create_chromosome_graph(name, c, id_generator, genome, first_genome)
        vertices.extend(v)
        edges.extend(e)
    return vertices, edges

def vertices_by_genes(v_list):
    ''' Generate a dictionary d: (gene_id, extremity) -> [vertices].
    '''
    d = dict()
    for vertex in v_list:
        if (vertex.gene_id,vertex.extremity) not in d:
            d[(vertex.gene_id,vertex.extremity)] = []
        d[(vertex.gene_id,vertex.extremity)].append(vertex)
    return d

def get_neighbors(e_list):
    ''' Generate a dictionary d: vertex -> [neighbor vertices in the other genome].
    TAKES INTO ACCOUUNT ONLY CROSS GENOME EDGES
    '''
    d = defaultdict(list)
    for e in e_list:
        if e.is_cross_genome(): # or e.vertex1.first_genome != e.vertex2.first_genome:
            d[e.vertex1].append(e.vertex2)
            d[e.vertex2].append(e.vertex1)
    return d

def get_adjacent_edges(e_list):
    ''' Generate a dictionary d: self edge -> [cross genomes edges].
        TAKES INTO ACCOUNT ONLY CROSS GENOME EDGES ADJACENT TO SELF EDGES
    '''

    # first we find self edges (dict d: vertex -> self edge)
    self_edge = dict()
    for e in e_list:
        if e.is_self():
            self_edge[e.vertex1] = e
            self_edge[e.vertex2] = e
    
    d = defaultdict(list)
    for e in e_list:
        if e.is_cross_genome() and not e.vertex1.is_cap(): # ignore edges connecting cap extremities
            d[self_edge[e.vertex1]].append(e)
            d[self_edge[e.vertex2]].append(e)

    return d

def add_to_list_dict(d, key, elem):
    if key in d:
        d[key].append(elem)
    else:
        d[key] = [elem]

def get_cross_genome_edges(v1, genome1, v2, genome2, capping=True, heuristic_capping = None):
    ''' Generate the matching edges between the vertices v1,v2 of genomes 1 and 2
        in the modified adjacency graph. capping=False prevents the all x all capping
        edges to be added.
    '''
    genes1 = genome1['genes']
    genes2 = genome2['genes']
    edges = []

    # speedup code with some preprocessing
    v2_by_gene_id_ex = dict()
    caps2 = list()
    for v in v2:
        if not v.is_cap():
            v2_by_gene_id_ex[(v.gene_id, v.extremity)] = v
        else:
            caps2.append(v)
    
    caps1 = []
    for u in v1:
        if u.is_cap():
            caps1.append(u)
            continue

        for e in genes1[u.gene_id]['edges']:
            v = v2_by_gene_id_ex[(e['to'], u.extremity)]
            edges.append(Edge(u,v,e['weight']))

    if capping:
        if heuristic_capping:
            #edges.extend([ Edge(u,v) for u in caps1 for v in caps2 if (u.chr_id, v.chr_id) in heuristic_capping]) # this was too slow
            chrcap1 = defaultdict(list)
            chrcap2 = defaultdict(list)
            for u in caps1:
                chrcap1[u.chr_id].append(u)
            for v in caps2:
                chrcap2[v.chr_id].append(v)
            for chr1, chr2, trivial in heuristic_capping:
                if trivial: # optimization, see add_chromosomes_heuristic_capping
                    edges.append(Edge(chrcap1[chr1][0], chrcap2[chr2][0]))
                    edges.append(Edge(chrcap1[chr1][1], chrcap2[chr2][1]))
                else:
                    edges.extend(Edge(u,v) for u in chrcap1[chr1] for v in chrcap2[chr2])
        else:
            edges.extend(Edge(u,v) for u in caps1 for v in caps2)

    return edges

def perfect_matchings_ub_ILP(G, C):
    '''Compute an upper bound for the number of perfect matchings in the
       component in the ILP of its respective component C of G
       (contig intersection graph) by the Bregman-Cinc inequality.
    '''
    if len(C) == 2 and sum(1 for v in C if v.startswith(FAKE_CHR_PREFIX)) > 0: # optimization: fake chromosome connected to another contig -- only 1 possible matching
        return 1
    mul = 1
    for v in (v for v in C if G.nodes[v]['bipartite'] == 0): # equivalent to the row i of a bipartite adjacency matrix
        deg = len(G.edges(v)) # number of connections (equivalent to ri = the sum of 1's in row i)
        deg *= 2 # however, each vertex in C corresponds to 2 vertices in the ILP with twice the degree
        factor = factorial(deg) ** (1/deg)
        mul *= factor * factor # 2 times because of 2 vertices in the ILP

    return floor(mul)
        
def add_chromosomes_heuristic_capping(genome1, genome2, abs_threshold, rel_threshold):
    ''' Heuristic for defining capping edges. '''
    LOG.info("Capping heuristic: original genomes have %d and %d chromosomes.", len(genome1['chromosomes']), len(genome2['chromosomes']))

    def show_graph(graph, marked_edges=[]): # for debugging purposes, requires matplotlib
        import matplotlib.pyplot as plt
        #pos = nx.drawing.layout.bipartite_layout(graph, caps1, align='horizontal') # good, but doesn't draw nodes in (label) order
        pos = dict()
        pos.update((n, (i, 1)) for i, n in enumerate(c for c in genome1['chromosomes'].keys() if c in G))
        pos.update((n, (i, -1)) for i, n in enumerate(old2new.get(c,c) for c in genome2['chromosomes'].keys() if old2new.get(c,c) in G))
        e_labels = nx.get_edge_attributes(G, 'weight')
        nx.draw(graph, with_labels=True, pos=pos,
                bbox=dict(facecolor="skyblue", edgecolor='black', boxstyle='round,pad=0.2'))  # node_size=500)
        nx.draw_networkx_edges(G, pos, marked_edges, edge_color='red')
        nx.draw_networkx_edge_labels(G, pos, edge_labels=e_labels, font_size=6, label_pos=0.9) # draw labels twice
        nx.draw_networkx_edge_labels(G, pos, edge_labels=e_labels, font_size=6, label_pos=0.2)
        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()
        plt.show()

    def create_and_connect_fake_chr(side, vlist): 
        fake_nr, fake_chr, chrname = create_fake_chromosome()
        G.add_node(chrname, bipartite=side)
        G.add_weighted_edges_from((chrname, v, 0) for v in vlist) # connect to all vertices in vlist, also adds new vertex (fake chr)
        if side == 0:
            genome1['chromosomes'][chrname] = fake_chr
        else:
            genome2['chromosomes'][chrname] = fake_chr

    # may need to temporarily rename some chromosomes in genome2 if genome1 has chromosomes with the same names (nodes in networkx can't have the same name)
    new2old, old2new = dict(), dict()
    for chrname in genome2['chromosomes'].keys():
        if chrname in genome1['chromosomes']:
            newname = chrname + '_' +''.join(choices(ascii_lowercase, k=6))
            new2old[newname] = chrname
            old2new[chrname] = newname

    # count shared genes with other chromosomes
    shared = defaultdict(Counter)
    for gene1 in genome1['genes'].values():
        # the edge weight in the contig graph is the sum of edge weights between genes of the chromosome pair
        for e in gene1['edges']:
            shared[gene1['chr']][genome2['genes'][e['to']]['chr']] += e['weight']
        # originally, the weight was just the count of edges between genes of the chromosome pair
        # shared[gene1['chr']].update(genome2['genes'][e['to']]['chr'] for e in gene1['edges'])

    # create a graph with chromosomes
    G = nx.Graph(name='Contig intersection graph') # not related to vertices1/2, which store vertices of the adjacency graph
    G.add_nodes_from(genome1['chromosomes'], bipartite=0)
    G.add_nodes_from((old2new.get(chrname,chrname) for chrname in genome2['chromosomes']), bipartite=1)
    for chr1 in genome1['chromosomes']:
        G.add_weighted_edges_from((chr1, old2new.get(chr2,chr2), shared[chr1][chr2]) for chr2 in genome2['chromosomes'] if chr2 in shared[chr1])
    
    #LOG.info('Capping heuristic: original Contig intersection graph has %d vertices and %d edges' % (len(G), len(G.edges)))
    #LOG.info('Capping heuristic: original Contig intersection graph has max vertex degree %d' % max(d for v,d in G.degree))
    #largest_comp = G.subgraph(sorted(nx.connected_components(G), key=len, reverse=True)[0])
    #LOG.info('Capping heuristic: original Contig intersection graph has in the largest component %d vertices and %d edges' % (len(largest_comp), len(largest_comp.edges)))
    
    # keep only the top-sharing chromosomes
    for v in G:
        edges = list(G.edges(v, data=True))
        edges.sort(key=lambda e: e[2]['weight'], reverse=True)
        G.remove_edges_from(e for e in edges[abs_threshold:])

    # for each vertex, remove edges with weight smaller than some % of the top scoring edge
    edges_remove = set()
    for v in G:
        top_weight = max((e[2]['weight'] for e in G.edges(v, data=True)), default=0) # default in case the edge list is empty
        edges_remove.update(e[:2] for e in G.edges(v, data=True) if e[2]['weight'] < top_weight*rel_threshold) # better to remove them after iterating
    G.remove_edges_from(edges_remove)

    fake_nr = 0
    # for disconnected vertices, we can create fake chr without having to find (lots of) matchings
    for v in list(nx.isolates(G)):
        side = G.nodes[v]['bipartite'] # side 0 (genome1) or 1 (genome2)
        side = int(not side) # but we want to add a fake chromosome to the opposite side
        create_and_connect_fake_chr(side, (v,))
        fake_nr += 1

    # We need to be sure that a perfect matching exists (Hall’s Marriage Theorem is true). When using this heuristics,
    # not necessarely both bipartitions (chromosomes for each genome) have the same size initially
    S = find_Hall_S(G, genome1['chromosomes'])
    # Create (|S| - |N(S)|)/2 fake chromosomes the bipartition opposite to S to connect their cappings
    # to all cappings of chromosomes in S, each fake chromosome adds 2 caps.
    while len(S) > 0:
        side = G.nodes[S[0]]['bipartite'] # side 0 (genome1) or 1 (genome2)
        side = int(not side) # but we want to add a fake chromosome to the opposite side
        N_S = list(set( v for u in S for v in G.neighbors(u) )) # If | S | == 1, then the chromosome in S is not connected to any and |N(S)| == 0
        for _ in range(len(S) - len(N_S)):
            create_and_connect_fake_chr(side, S) # connect to all in S, also adds new vertex (fake chr)
            fake_nr += 1
        # print("S:", S)
        # print("N_S:", N_S)
        # show_graph(G, [e for v in S for e in G.edges(v)])
        S = find_Hall_S(G, genome1['chromosomes'])

    # Remove edges that are not maximally-machable
    maxMatchable = all_maximally_matchable_edges_perfect(G)
    notMaxMatchable = [ e for e in G.edges if e not in maxMatchable and tuple(reversed(e)) not in maxMatchable ]
    G.remove_edges_from(notMaxMatchable)
    LOG.info("Removed %d non max-matchable edges out of %d." % (len(notMaxMatchable), len(G.edges)+len(notMaxMatchable)))

    LOG.info("Capping heuristic: now genomes have %d and %d chromosomes (%d fake chromosomes added).", len(genome1['chromosomes']), len(genome2['chromosomes']), fake_nr)
    comp = list(nx.connected_components(G))
    count = sorted(Counter(len(c)//2 for c in comp).most_common())
    LOG.info("Capping heuristic: %d components in the (bipartite) capped contig graph, with the following distribution in format <# of contigs in each part>:<count> " % len(comp))
    LOG.info("Capping heuristic:    " + ", ".join("%d:%d" % c for c in count))
    LOG.info("Capping heuristic: %d edges in the (bipartite) capped contig graph" % len(G.edges))

    cset_count = 1
    trivial = set() # chromosomes that are part of trivial components
    for C in comp:
        # optimization: contig connected only to a single fake chromosome -- trivial component, only 1 possible matching
        if len(C) == 2 and sum(1 for v in C if v.startswith(FAKE_CHR_PREFIX)) > 0:
            cset_count *= 1
            trivial.update(v for v in C)
        else:
            cset_count *= perfect_matchings_ub_ILP(G, C)
    LOG.info("Capping heuristic: upper limit of %d distinct capping-sets in the ILP" % cset_count)
    # show_graph(G)

    # return chromosome pairs (chr_genome1, chr_genome2, true/false) that must have their caps connected, true if it is a trivial capping (normal contig connected to a single FAKE contig)
    return [ (u, new2old.get(v, v), u in trivial) for u in genome1['chromosomes'] for v in G.neighbors(u) ]

def find_Hall_S(G, top_nodes):
    '''Find a set S that does not satisfy Hall's Theorem (i.e. a Hall violator)'''
    matches = nx.bipartite.maximum_matching(G, top_nodes=top_nodes)
    if len(matches) == len(G):
        return []

    visited = { v:False for v in G }
    v = next(v for v in G if v not in matches)

    visited[v] = True
    S = [ v ] # list
    Q = deque(S) # queue (must be initialized with a list)
    # BFS, we push just edges from the same part of starting vertex
    while len(Q) > 0:
        v = Q.popleft()
        for u in G.neighbors(v):
            w = matches[u] # pair of u in the matching, is in the same set of the initial vertex
            if not visited[w]:
                visited[w] = True
                Q.append(w)
                S.append(w)
    return S

def all_maximally_matchable_edges_perfect(G):
    '''Implements Algorithm 2 -- Finding all maximally-matchable edges in a bipartite graph, given a perfect matching, in:
       Tassa, T. "Finding all maximally-matchable edges in a bipartite graph." Theoretical Computer Science 423 (2012): 50-58.
    '''
    # Here we need to keep the order of vertices in edges as (vertex in part 0, vertex in part 1)

    # Find a maximum matching that is **a perfect matching** (MUST BE ENSURED BEFORE CALLING!!!)
    top_nodes = set(n for n, d in G.nodes(data=True) if d["bipartite"] == 0)
    PM = nx.bipartite.maximum_matching(G, top_nodes=top_nodes)
    PM = [ (v, PM[v]) for v in top_nodes ]

    # Step 1:
    isMaxMatchable = set(PM)  # we'll use a set and add only if is max-matchable

    # Step 2:
    H = nx.DiGraph()
    U = []
    mapG2H = dict()  # map of vertices from G to H
    mapH2G0 = dict()  # map of vertices from H to its corresponding vertex in part 0 of G
    mapH2G1 = dict()  # map of vertices from H to its corresponding vertex in part 1 of G
    for i, e in enumerate(PM):
        u = 'u' + str(i)
        U.append(u)
        mapG2H[e[0]] = mapG2H[e[1]] = u
        mapH2G0[u] = e[0]
        mapH2G1[u] = e[1]
    F = []
    for e in G.edges:
        v0, v1 = e if e[0] in top_nodes else tuple(reversed(e))
        if mapG2H[v0] != mapG2H[v1]:
            F.append((mapG2H[v0], mapG2H[v1]))
    H.add_nodes_from(U)
    H.add_edges_from(F)

    # Step 3:
    mapComp = {u: C for C in nx.strongly_connected_components(H) for u in C}

    # Step 4:
    for e in F:  # or H.out_edges
        u0, u1 = e if e[0] in top_nodes else tuple(reversed(e))
        # Step 5:
        if mapComp[u0] == mapComp[u1]:
            # Step 6:
            isMaxMatchable.add((mapH2G0[u0], mapH2G1[u1]))

    # Step 9:
    # Already done in isMaxMatchable initialization

    # End
    return isMaxMatchable

def update_indel_edges_weigths(vertices, edges):
    '''Update indel (self) edges weights according to cross adjacent cross
    genomes edges.
    '''
    cross_edges = [ e for e in edges if e.is_cross_genome() ]
    self_edges = [ e for e in edges if e.is_self() ]
    adjacent = get_adjacent_edges(edges) # gets only cross edges adjacent to self edges
    for e in self_edges:
        w = max( (a.weight() for a in adjacent[e]), default=0)
        e.set_weight(w)

def create_fake_chromosome():
    fake_nr = create_fake_chromosome.__fake_nr
    fake_chr = {'linear':True, 'circular':False, 'fake':True, 'genes':[]}
    name = '%s%s' % (FAKE_CHR_PREFIX, fake_nr)
    create_fake_chromosome.__fake_nr += 1
    return fake_nr, fake_chr, name
create_fake_chromosome.__fake_nr = 1

def create_adjacency_graph(genome1, genome2, id_generator, heuristic_capping=False):
    ''' Create the modified adjacency graph including capping and padding from
        two genomes. The id generator is used to generate the
        null extremity ids.
    '''

    #introduce empty telomere-telomere chromosomes
    n_ch1 = sum(1 for x in genome1['chromosomes'].values() if x['linear'])
    n_ch2 = sum(1 for x in genome2['chromosomes'].values() if x['linear'])

    if heuristic_capping:
        heuristic_capping = add_chromosomes_heuristic_capping(genome1, genome2, heuristic_capping['absolute'], heuristic_capping['relative'])
        # need to update those
        n_ch1 = sum(1 for x in genome1['chromosomes'].values() if x['linear'])
        n_ch2 = sum(1 for x in genome2['chromosomes'].values() if x['linear'])
    elif n_ch1 != n_ch2: # if n_ch1 == n_ch2, no padding needed
        genome = genome1 if n_ch1 < n_ch2 else genome2
        LOG.debug("Padding genome: %s\n"%genome['name'])
        for i in range(0,abs(n_ch2 - n_ch1)):
            fake_nr, fake_chr, chrname = create_fake_chromosome()
            genome['chromosomes'][chrname] = fake_chr
    
    v1, e1 = create_genome_graph(genome1, id_generator, True) # first genome
    v2, e2 = create_genome_graph(genome2, id_generator, False) # second genome
    cross = get_cross_genome_edges(v1, genome1, v2, genome2, capping=True, heuristic_capping=heuristic_capping) # e1 and e2 have their adjacency+self edges already
    edges = e1 + e2 + cross
    vertices = v1 + v2
    
    update_indel_edges_weigths(vertices, edges)
    
    return vertices, edges, max(n_ch1, n_ch2)


VERTEX_NAME = "%d_%s_%s"
VERTEX_VARIABLE = "%s_%s"
EDGE_VARIABLE = "%s_(%s,%s)"

def vertex_name(v):
    ''' Given a vertex, return its name, that is id_genome_extremity.
    '''
    return VERTEX_NAME%(v.vertex_id, v.genome_id, v.extremity)

def vertex_var_n(v,varname):
    ''' Obtain the string of a vertex variable (baseName_vertexName) in the ILP
        given the vertex and the variable base name.
    '''
    return VERTEX_VARIABLE%(varname,vertex_name(v))

def edge_var_n(e,varname):
    ''' Obtain the string of an edge variable (baseName_(vertex1,vertex2)).
    '''
    return EDGE_VARIABLE%(varname,vertex_name(e.vertex1),vertex_name(e.vertex2))

#not quite clean here
def edge_var_n_tail(e,varname):
    return EDGE_VARIABLE%(varname
                          ,VERTEX_NAME%(e.vertex1.vertex_id,e.vertex1.genome_id,EXTREMITY_TAIL)
                          ,VERTEX_NAME%(e.vertex2.vertex_id,e.vertex2.genome_id,EXTREMITY_TAIL))

def vertex_var(v,varname,rangemin = 0, rangemax = 1):
    ''' Given a vertex and the variable base name, return the correpsonding ILP variable.
    '''
    return ILP_variable(vertex_var_n(v, varname),
     rangemin=rangemin, rangemax=rangemax)

def edge_var(e,varname):
    ''' Given an edge and the variable base name, return the correpsonding ILP variable.
    '''
    return ILP_variable(edge_var_n(e, varname))

#DELTA_R = 'delta_r'
DELTA_R = 't' # a better name
def set_variables(ilp, vertices, edges, p_star, setting, opt):
    ''' Introduce the existence of all variables to the ILP.
    '''
    ilp.variables.append(ILP_variable('p', p_star, p_star))
    for v in vertices:
        ilp.variables.append(vertex_var(v,'z'))
        if setting != FAMILY_FREE:
            ilp.variables.append(vertex_var(v,'r'))
        ilp.variables.append(vertex_var(v,'y',0, v.get_i()))
        if not opt:
            ilp.variables.append(vertex_var(v,'d'))
    for e in edges:
        ilp.variables.append(edge_var(e,'x'))
        if setting != FAMILY_FREE:
            ilp.variables.append(edge_var(e,DELTA_R))

GEQ = ">="
LEQ = "<="
EQ  = "="
CONST = "C"
def set_run_conditions(ilp, vertices, edges, opt):
    ''' Set the constraints dealing with the run counting variables r, delta_r,
        as well as d if opt=False. Otherwise the "z-reset" is done via y.
    '''

    selfedges = [ e for e in edges if e.is_self() ] # indel edges
    
    ilp.addcomment("(C.05) y_i <= i - i*x_(v_i,v_j) for each indel edge (v_i,v_j)")
    ilp.addcomment("(C.05) y_j <= j - j*x_(v_i,v_j) for each indel edge (v_i,v_j)")
    for e in selfedges:
        if not opt:
            #d_u >= x_(u,v)
            ilp.conditions.append(([(1,vertex_var_n(e.vertex1,'d'))],GEQ,[(1,edge_var_n(e,'x'))]))
            ilp.conditions.append(([(1,vertex_var_n(e.vertex2,'d'))],GEQ,[(1,edge_var_n(e,'x'))]))
        else:
            #i-i*x_(v_i,u_j) >= y_i
            ilp.conditions.append(([(e.vertex1.get_i(),CONST),(-1*e.vertex1.get_i(),edge_var_n(e,'x'))],GEQ,[(1,vertex_var_n(e.vertex1, 'y'))]))
            #j-j*x_(v_i,u_j) >= y_j
            ilp.conditions.append(([(e.vertex2.get_i(),CONST),(-1*e.vertex2.get_i(),edge_var_n(e,'x'))],GEQ,[(1,vertex_var_n(e.vertex2, 'y'))]))


    ilp.addcomment("(C.07) r_v <= 1 - x_(u,v) for each indel edge in E^A")
    ilp.addcomment("(C.07) r_v >= x_(u,v) for each indel edge in E^B")
    for e in selfedges:
        if e.vertex1.first_genome:
            #1 - x_(u,v) >= r_u
            ilp.conditions.append(([(1,CONST),(-1,edge_var_n(e,'x'))],GEQ,[(1,vertex_var_n(e.vertex1,'r'))]))
            #1 - x_(u,v) >= r_v
            ilp.conditions.append(([(1,CONST),(-1,edge_var_n(e,'x'))],GEQ,[(1,vertex_var_n(e.vertex2,'r'))]))
        else: # not e.vertex1.first_genome:
            #x_(u,v) <= r_u
            ilp.conditions.append(([(1,edge_var_n(e,'x'))],LEQ,[(1,vertex_var_n(e.vertex1,'r'))]))
            #x_(u,v) <= r_v
            ilp.conditions.append(([(1,edge_var_n(e,'x'))],LEQ,[(1,vertex_var_n(e.vertex2,'r'))]))

    
    ilp.addcomment("(C.08) t_(u,v) >= r_v - r_u - 1 + x_(u,v)")
    ilp.addcomment("(C.08) t_(u,v) >= r_u - r_v - 1 + x_(u,v)")
    for e in edges:
        #delta_r_(v,u) >= r_v - r_u -1 + x_(v,u)
        ilp.conditions.append(([(1,edge_var_n(e,DELTA_R))],GEQ,
                               [(1,vertex_var_n(e.vertex1,'r'))
                               ,(-1,vertex_var_n(e.vertex2,'r'))
                               ,(-1,CONST)
                               ,(1,edge_var_n(e,'x'))]))

        #delta_r_(v,u) >= r_u - r_v -1 + x_(v,u)
        ilp.conditions.append(([(1,edge_var_n(e,DELTA_R))],GEQ,
                               [(1,vertex_var_n(e.vertex2,'r'))
                               ,(-1,vertex_var_n(e.vertex1,'r'))
                               ,(-1,CONST)
                               ,(1,edge_var_n(e,'x'))]))

        if not opt:
            #d_v >= d_u + x_(u,v) -1
            ilp.conditions.append(([(1,vertex_var_n(e.vertex1,'d'))],GEQ,
                                  [(1,vertex_var_n(e.vertex2,'d'))
                                  ,(1,edge_var_n(e,'x'))
                                  ,(-1,CONST)]))
            #d_u >= d_v + x_(u,v) -1
            ilp.conditions.append(([(1,vertex_var_n(e.vertex2,'d'))],GEQ,
                                  [(1,vertex_var_n(e.vertex1,'d'))
                                  ,(1,edge_var_n(e,'x'))
                                  ,(-1,CONST)]))
            
    if not opt:
        for v in vertices:
            leftside = [(0,CONST)]
            #0 >= z_v + d_v -1
            ilp.conditions.append((leftside,GEQ,
                                   [(1,vertex_var_n(v,'z'))
                                   ,(-1,CONST)
                                   ,(1,vertex_var_n(v,'d'))]))

def edges_by_vertices(e_list):
    ''' Generate a dict d: vertex -> [edges]
    '''
    d = dict()
    for e in e_list:
        add_to_list_dict(d,e.vertex1,e)
        add_to_list_dict(d,e.vertex2,e)
    return d

def restrict_runs(edges):
    '''Add the special condition, that run labels cannot change in clean adjacency edges.
    '''
    conditions = []
    #for e in edges:
        # DELTA_R_e <= x_e
    #	conditions.append(([(1,edge_var_n(e,DELTA_R))],LEQ,[(1,edge_var_n(e,'x'))]))
    d = edges_by_vertices([e for e in edges if e.is_self()])
    for a in [e for e in edges if e.is_adjacency()]:
        if a.vertex1.extremity == EXTREMITY_TELOMERE or a.vertex2.extremity == EXTREMITY_TELOMERE:
            continue
        left =  (1,edge_var_n(d[a.vertex1][0], 'x')) # unsafe, but all genes should have a self edge
        right = (1,edge_var_n(d[a.vertex2][0], 'x'))
        conditions.append(([left,right, (-1, edge_var_n(a,DELTA_R))], GEQ, [(0,CONST)]))
        #no label changes between two indels (don't know if optimizes)
        conditions.append(([left,right, (1, edge_var_n(a,DELTA_R))], LEQ, [(2,CONST)]))
    return conditions

def add_vertex_consistency(ilp,vertices,edges):
    ''' Assure that in every feasible solution, each vertex has degree 2.
    '''
    vertex_summands = dict()
    for e in edges:
        v1 = (e.vertex1.vertex_id,e.vertex1.extremity)
        v2 = (e.vertex2.vertex_id,e.vertex2.extremity)
        if v1 not in vertex_summands:
            vertex_summands[v1] = []
        if v2 not in vertex_summands:
            vertex_summands[v2] = []
        vertex_summands[v1].append((1,edge_var_n(e,'x')))
        vertex_summands[v2].append((1,edge_var_n(e,'x')))

    ilp.addcomment("(C.02) SUM(x_(u,v) for each neighbor v of u) = 2 for each vertex u")
    for v in vertices:
        ilp.conditions.append((vertex_summands[(v.vertex_id,v.extremity)],EQ,[(2,CONST)]))

def set_decomp_conditions(ilp,vertices,edges):
    ''' Apply the conditions for a consistent decomposition, i.e. every marker
        matched once, adjacency edges always active etc.
    '''
    add_vertex_consistency(ilp,vertices,edges)


    ilp.addcomment("(C.01) x_e = 1 for each adjacency edge e")
    for e in edges:
        if e.is_adjacency():
            # x_e = 1
            ilp.conditions.append(([(1,edge_var_n(e,'x'))],EQ,[(1,CONST)]))


    ilp.addcomment("(C.03) x_e = x_d for each pair of sibling edges e,d")
    for e in edges:
        if e.is_cross_genome() and e.vertex1.extremity == EXTREMITY_HEAD:
            # x_(u_h,v_h) = x_(u_t, v_t)
            ilp.conditions.append(([(1,edge_var_n(e,'x'))],EQ,[(1,edge_var_n_tail(e,'x'))]))

    
    ilp.addcomment("(C.04) y_i <= y_i + i - i*x_(v_i,v_j) for each edge (v_i,v_j)")
    ilp.addcomment("(C.04) y_j <= y_j + j - j*x_(v_i,v_j) for each edge (v_i,v_j)")
    for e in edges:
        i = e.vertex1
        j = e.vertex2
        #y_i <= y_j + i -i*x_e
        ilp.conditions.append(([(1,vertex_var_n(i,'y'))],LEQ,
                               [(1,vertex_var_n(j,'y'))
                               ,(i.get_i(),CONST)
                               ,(-i.get_i(),edge_var_n(e,'x'))]))
        #y_j <= y_i + j - j*x_e
        ilp.conditions.append(([(1,vertex_var_n(j,'y'))],LEQ,
                               [(1,vertex_var_n(i,'y'))
                               ,(j.get_i(),CONST)
                               ,(-j.get_i(),edge_var_n(e,'x'))]))

    
    ilp.addcomment("(C.06) i*z_i <= y_i for each vertex v_i")
    for v in vertices:
        # i*z_i <= y_i
        ilp.conditions.append(([(v.get_i(),vertex_var_n(v,'z'))],LEQ,
                               [(1,vertex_var_n(v,'y'))]))

def set_objective_function(ilp, vertices, edges, setting, alpha):
    '''For a matching M:
         FFID: d_σ^ID = p* + 2|M| - cycles + transitions/2 - w(M) + α * w(complement(M))
         UNFFID: d_σ^ID = p* + 2|M| - cycles + transitions/2 - w(M) = |M| - cycles + transitions/2
         FF  : d_σ^ID = p* + 2|M| - cycles - w(M)
    '''
    if setting == FAMILY_FREE_ID:
        ilp.addcomment_obj('p* + 2|M| - cycles + transitions/2 - w(M) + alpha * w(complement(M))')
    elif setting == UNW_FAMILY_FREE_ID:
        ilp.addcomment_obj('p* + 2|M| - cycles + transitions/2 - w(M) = |M| - cycles - transitions/2')
    else: # FAMILY_FREE
        ilp.addcomment_obj('p* + 2|M| - cycles - w(M)')

    ilp.addcomment_obj('p*')
    ilp.objective_function.append((1,'p'))
    
    ilp.addcomment_obj('2|M| - w(M)', '\n')
    for e in edges: # 2|M| - w(M)
        if e.is_cross_genome() and not e.is_cap() and e.vertex1.extremity == EXTREMITY_TAIL:
            ilp.objective_function.append((2-alpha*e.weight(),edge_var_n(e,'x')))
            
    if setting == FAMILY_FREE_ID:
        ilp.addcomment_obj('+ alpha * w(complement(M))', '\n')
        for e in edges: # + α * w(complement(M))
            if e.is_self() and e.weight() > 0:
                ilp.objective_function.append((+alpha*e.weight(),edge_var_n(e,'x')))
                
    ilp.addcomment_obj('- cycles', '\n')
    for v in vertices: # - cycles
        ilp.objective_function.append((-1,vertex_var_n(v,'z')))

    if setting != FAMILY_FREE:
        ilp.addcomment_obj('+ transitions/2', '\n')
        for e in edges: # + transitions/2
            ilp.objective_function.append((+.5,edge_var_n(e,DELTA_R)))


def set_mm_conditions(ilp, vertices, edges):
    ''' Conditions to force a maximum matching in the gene similarity graph. '''   
    # dict with the self edge of each vertex
    self_edges = dict()
    for e in edges:
        if e.is_self():
            self_edges[e.vertex1.vertex_id] = e

    ilp.addcomment("(C.12) x_(u_t,u_h) + x_(v_t,v_h) <= 1 for self edges of vertices such that (u,v) is in the gene similarity graph (enforces maximal matching)")
    for e in edges:
        # to avoid duplicate constraints, we just look into cross edges connecting tails (not heads) of genes in the gene similarity graph
        if e.is_cross_genome() and e.vertex1.extremity == EXTREMITY_TAIL:
            # x_(u_t,u_h) + x_(v_t, v_h) <= 1
            id1 = self_edges[e.vertex1.vertex_id] # indel edge 1
            id2 = self_edges[e.vertex2.vertex_id] # indel edge 2
            ilp.conditions.append(([(1, edge_var_n(id1,'x')), (1, edge_var_n(id2,'x'))],LEQ,[(1,CONST)])) #id1 e id2


def set_mm_optimizations(ilp, vertices, edges):
    ''' Apply all optimizations following from applying the MM model to the ILP.
    '''
    cross_edges = [ e for e in edges if e.is_cross_genome() ]
    neighbors = get_neighbors(cross_edges) # only cross genome edges
    degree = lambda v: len(neighbors[v])

    ilp.addcomment("(Optimization) x_(u,v) = 1 for connected vertices u,v with degree 1 in the gene similarity graph")
    for e in cross_edges:
        if degree(e.vertex1) == 1 and degree(e.vertex2) == 1:
            # x_e = 1
            ilp.conditions.append(([(1,edge_var_n(e,'x'))],EQ,[(1,CONST)]))
            ilp.conditions.append(([(1,vertex_var_n(e.vertex1,'y'))],EQ,[(1,vertex_var_n(e.vertex2,'y'))]))

    
    # ilp.addcomment("(Optimization) x_(u,v) = 0 for indel edges of genes g such that its genome has less copies of g than the other genome")
    # for e in [x for x in edges if x.is_self()]:
    #     g = (e.vertex1.gene_id, e.vertex1.extremity)
    #     genome = e.vertex1.genome_id
    #     if occ(this(genome),g) <= occ(other(genome),g):
    #         ilp.conditions.append(([(1,(edge_var_n(e,'x')))],EQ,[(0,CONST)]))

    ilp.addcomment("(Optimization) x_(u,v) = 1 for indel edges of genes g such that g doesn't happen in the other genome (also setting z_u = 0 and z_v = 0 if they are not in a circular singleton)")
    for e in [x for x in edges if x.is_self()]:
        g = (e.vertex1.gene_id, e.vertex1.extremity)
        genome = e.vertex1.genome_id
        if degree(e.vertex1) == 0: # if it has no cross genome edge
            ilp.conditions.append(([(1, edge_var_n(e,'x'))],EQ,[(1,CONST)]))
            #this condition works for all except circular singletons
            if not e.is_in_singleton_circular():
                ilp.conditions.append(([(1, vertex_var_n(e.vertex1,'z'))],EQ,[(0,CONST)]))
                ilp.conditions.append(([(1, vertex_var_n(e.vertex2,'z'))],EQ,[(0,CONST)]))
    

def preset_delta_r(ilp, edges):
    '''Only allow lable changes along adjacency edges.
    '''

    ilp.addcomment("(C.10) t_e = 0 for all edges except adjacency edges in E^B") #TODO: maybe replace by E^A to follow the paper
    for e in [x for x in edges if x.is_self() or x.is_cross_genome() or x.vertex1.first_genome]:
        ilp.conditions.append(([(1,edge_var_n(e, DELTA_R))], EQ, [(0,CONST)]))
    ilp.addcomment("(C.09) SUM(x_f for each indel edge f neighbor of e in E^A) - t_e >= 0 for each adjacency edge in E^A") #TODO: the implementation doesn't follow the comment and the paper: if creates the restriction to ALL adjacency edges...
    ilp.addcomment("(additionally one optimization?)")
    ilp.conditions.extend(restrict_runs(edges))


def create_ILP(vertices, edges, p_star, alpha, setting=FAMILY_FREE_ID, mm=False, optimize=False):
    ''' Generate the ILP from given vertices and edges of the modified adjacency graph.
    '''
    ilp = ILP()
    set_objective_function(ilp, vertices, edges, setting, alpha)
    set_variables(ilp, vertices, edges, p_star, setting, optimize)
    set_decomp_conditions(ilp, vertices,edges)
    if setting != FAMILY_FREE:
        set_run_conditions(ilp, vertices, edges, optimize)
    if mm:
        set_mm_conditions(ilp, vertices, edges)
        set_mm_optimizations(ilp, vertices, edges)
    if optimize and setting != FAMILY_FREE:
        preset_delta_r(ilp, edges)
    return ilp


EDGE_OUTPUT = "%s_%s -> %s_%s (%s) (%s,%s)\n"
def print_graph(vertices,edges):
    '''Print the generated adjacency graph to debug out.
    '''
    LOG.debug("Adjacency Graph:\n")
    for e in edges:
        g1 = e.vertex1.gene_id
        e1 = e.vertex1.extremity
        g2 = e.vertex2.gene_id
        e2 = e.vertex2.extremity
        id1 = e.vertex1.vertex_id
        id2 = e.vertex2.vertex_id
        kind = "cross"
        if (e.is_adjacency()):
            kind = "adj"
        if (e.is_self()):
            kind="self"
        LOG.debug(EDGE_OUTPUT%(id1,e1,id2,e2,kind,g1,g2))

def smd(kx):
    ''' Turn a summand of form (n, x_i), where n is a constant and x_i a variable
        into a corresponding string nx_i while catching the edge cases n==1 or x_i
        being the identifier for a constant.
    '''
    k = abs(kx[0])
    x = kx[1]
    kk="%g "%k
    if k == 1:
        kk=""
    if x == CONST:
        x=""
        kk="%g "%k
    return "%s%s"%(kk,x)

def sum_str(ls, print_comments=False, is_obj=False):
    ''' Express a list of constant, variable pairs (n,x_i) as the string of a
        sum of these.
    '''
    i = 0
    prefix = ' ' if is_obj else ''
    r = ""
    while ls[i][0] == COMMENT:
        if print_comments:
            r += ls[i][1] + '\n'
        i += 1

    r+=prefix
    if ls[i][0] < 0 :
        r+='- '
    r+=smd(ls[i])

    first = False
    for i in range(i+1, len(ls)):
        term = ls[i]
        if term[0] == COMMENT:
            if print_comments:
                r += term[1] + '\n'
                first = True
            continue

        if first:
            r+=prefix
            sign = '+ ' if term[0] > 0 else '- '
        else:
            sign = ' + ' if term[0] > 0 else ' - '
        r+=sign+smd(term)
        first = False
    return r

def normalize_equation(left,sign, right):
    ''' Rewrite a given equation, such that the constant is on the right
        and all variables are to the left.
    '''
    right_ = [x for x in right if x[1]==CONST]
    right_.extend([(-k,x) for (k, x) in left if x==CONST])
    if len(right_) == 0:
        right_.append((0,CONST))
    right_ = reduce(lambda x, y: (x[0]+y[0],CONST),right_)
    right_ = [right_]
    left_ = [x for x in left if x[1] != CONST]
    left_.extend([(-k,x) for (k,x) in right if x != CONST])
    return left_,sign,right_


def print_ILP_cpl(ilp, handle, print_comments):
    ''' Writes a given ILP to the handle in CPLEX lp format
    (http://lpsolve.sourceforge.net/5.0/CPLEX-format.htm).
    '''

    # For a matching M:
    # d_σ^ID = 2|M| - cycles + transitions/2 - w(M) - α * w(complement(M))
    handle.write("Minimize\n")
    handle.write(" obj: ")
    handle.write(sum_str(ilp.objective_function, print_comments, True))
    handle.write("\n")
    gen = Simple_Id_Generator()
    handle.write("Subject To\n")
    #print(max([len(x) for x in ilp.conditions]))
    for s1, e, s2, *comment in ilp.conditions:
        if e == COMMENT: # empty constraint, just a comment
            if print_comments:
                handle.write("%s\n" % comment[0])
            continue
        s1_,e_,s2_ = normalize_equation(s1,e,s2)
        handle.write(" c%d: "%gen.get_new())
        handle.write(sum_str(s1_))
        handle.write(" %s "%e_)
        handle.write(sum_str(s2_))
        if print_comments and len(comment) > 0:
            handle.write("%s" % comment[0])
        handle.write("\n")
    handle.write("Bounds\n")
    for x in [x for x in ilp.variables if not x.is_binary()]:
        handle.write(" ")
        if x.rangemin == x.rangemax:
            handle.write("%s = %d\n"%(x.Id, x.rangemax))
        else:
            handle.write("%d <= %s <= %d\n"%(x.rangemin,x.Id, x.rangemax))
    handle.write("Binary\n")
    for x in [x for x in ilp.variables if x.is_binary()]:
        handle.write(" ")
        handle.write(x.Id)
        handle.write("\n")
    handle.write("General\n")
    for x in [x for x in ilp.variables if not x.is_binary()]:
        handle.write(" ")
        handle.write(x.Id)
        handle.write("\n")
    handle.write("End\n")

def id_genomes(genomes, gen):
    ''' Assign unique identifiers to the genes via the provided generator.
    '''
    return [id_genome(x,gen) for x in genomes]

def id_genome(genome,gen):
    '''Assign unique identifiers to the genes via the provided generator.'''
    for g in genome['genes'].values():
        g['unique_id'] = gen.get_new()

def id_chr(chr, gen):
    (orient, list) = chr
    return (orient, [id_gene(x,gen) for x in list])

def id_gene(gene, gen):
    (orient,name) = gene
    return (orient, name, gen.get_new())

#TODO: not converted from DING to gen-diff. No support for circular genomes for now. 
def singleton_constraints(genomes, ilp, edges):
    gen = Simple_Id_Generator()
    d = dict([(e.vertex1.vertex_id, e) for e in edges if e.is_self()])
    ilp.addcomment("(C.11) SUM(x_e for each indel edge e of E^k) - |k| <= s_k for each circular chromosome k")
    for g in genomes.values():
        for chromosome in [c for c in g['chromosomes'].values() if c['circular']]:
            cid = gen.get_new()
            s = 's_%d'%cid
            ilp.variables.append(ILP_variable(s,0,1))
            smm = []
            sz = len(chromosome['genes'])
            for o, g, gid in chromosome['genes']: #TODO: not sure how to convert to gen-diff
                e = d[gid]
                smm.append((1,edge_var_n(e,'x')))
            smm.append((-sz+1,CONST))
            ilp.conditions.append(([(1,s)],GEQ,smm))
            ilp.objective_function.append((+1,s))
    return ilp

def initial_sol(vertices, edges, f):
    """Find and write variables hinting and initial solution in MST format."""
    G = nx.Graph()

    # we create a graph with only the head vertices, it will be equivalent to the gene similarity graph
    for v in vertices:
        if v.extremity == EXTREMITY_HEAD:  # not tail or cap
            G.add_node(v, bipartite=0 if v.first_genome else 1)

    indels = dict()
    for e in edges:
        if e.is_cross_genome() and e.vertex1.extremity == EXTREMITY_HEAD:
            G.add_edge(e.vertex1, e.vertex2, weight=e.w, edgeref=e)
        elif e.is_self():
            indels[e.vertex1.vertex_id] = e

    sol = []
    for C in nx.connected_components(G):
        H = G.subgraph(C)
        M = nx.max_weight_matching(H, maxcardinality=True)
        matched = set()
        for e in M:
            sol.append(edge_var(H.get_edge_data(*e)['edgeref'],'x').Id)
            matched.update(e)
        for v in C - matched:  # unmacthed vertices
            sol.append(edge_var(indels[v.vertex_id],'x').Id)

    f.write('# MIP start\n')
    f.writelines('%s 1\n' % var for var in sol)
    f.close()


class Range(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __eq__(self, other):
        return self.start <= other <= self.end

    def __contains__(self, item):
        return self.__eq__(item)

    def __iter__(self):
        yield self

    def __str__(self):
        return '[{0},{1}]'.format(self.start, self.end)

    def __repr__(self):
        return self.__str__()

def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False

def auto_alpha(avg_weight):
    return round(1.0 / avg_weight, 2)
    # if avg_weight >= 0.9:
    #     return 1.0
    # else:
    #     return round(10.0 - 10.0*avg_weight, 2)



def main(): # so variables are NOT global

    descr="""\
Given a genomes.cfg given by FFGC, two genome names Gx and Gy as in
that file (usually G followed by a number), and the file Gx_Gy.sim
given by FFGC, generates the ILP for computing the family-free DCJ
indel distance for natural genomes. The corresponding paths for .gos
fasta files are obtained from genomes.cfg. See:
https://gitlab.ub.uni-bielefeld.de/gi/FFGC or
https://bibiserv.cebitec.uni-bielefeld.de/ffgc (possibly outdated binary).
"""
    epilog="""\
The script assumes that, in the pairwise similarities file given, the first
columns refer to the parameter given in -1/--genome1 and the latter columns
refer to the parameter given in -2/--genome2.
"""
    
    parser = argparse.ArgumentParser(description=descr, epilog=epilog)
    parser.add_argument('-1', '--genome1', dest='genome1', metavar='Gx', type=str, required=True,
                        help='Name of the first genome we want to compare (must be one of the genomes in the genome cfg file).')
    parser.add_argument('-2', '--genome2', dest='genome2', metavar='Gy', type=str, required=True,
                        help='Name of the second genome we want to compare.')
    parser.add_argument('-C', '--cfg', required=True, type=argparse.FileType('r'), metavar='genomes.cfg',
                        help='Genomes configuration file generated by FFGC.')
    parser.add_argument('-S', '--sim', required=True, type=argparse.FileType('r'), metavar='Gx_Gy.sim',
                        help='Pairwise similarity file for genomes defined in -1/--genome1 and -2/--genome2.')
    parser.add_argument('-u', required=True, type=argparse.FileType('w'), metavar='output.map',
                        help='Output file mapping original to unique gene numbers (because often the two genomes have the same gene numbers 1,2,...).')
    parser.add_argument('-o', type=argparse.FileType('w'), default=sys.stdout, metavar='output.lp',
                        help='Output file for the resulting ILP in CPLEX lp format.')
    parser.add_argument('-I', type=argparse.FileType('w'), required=False, metavar='initial_sol.mst',
                        help='Output file for hints of an initial solution (MIP start) to the solver in MST format (optional).')
    parser.add_argument('-s', '--setting', choices=(FAMILY_FREE_ID, UNW_FAMILY_FREE_ID, FAMILY_FREE), default=FAMILY_FREE_ID,
                        help='Setting for handling similar genes: family-free (w/ or w/o indels) or unweighted family-free w/ indels (default: %s).' % FAMILY_FREE_ID)
    parser.add_argument('-M', '--matching', choices=('max','any'), default='any',
                        help='Which kind of matching of genes to consider: maximum or any matching (default: any).')
    parser.add_argument('-a', '--alpha', dest='alpha', metavar='α', type=str, default=1.0,
                        help='Factor for the contribution of indels in the distance formula (used in family-free only, must be a positive float or \'auto\', default: 1.0).')
    parser.add_argument('-H', '--heuristic-capping', dest='heuristic_capping', action='store_true', default=False,
                        help='Use heuristic for capping.')
    parser.add_argument('-A', '--absolute-threshold', dest='heuristic_capping_abs', metavar='int', type=int, default=HEURISTIC_CAPPING_THRESHOLD_ABSOLUTE,
                        help='Absolute threshold for heuristic capping (max number of contig connections, default: %d).' % HEURISTIC_CAPPING_THRESHOLD_ABSOLUTE)
    parser.add_argument('-R', '--relative-threshold', dest='heuristic_capping_rel', type=float, choices=Range(0.0, 1.0), default=HEURISTIC_CAPPING_THRESHOLD_RELATIVE,
                        help='Relative threshold for heuristic capping (relative to the contig that shares the most markers, default: %g).' % HEURISTIC_CAPPING_THRESHOLD_RELATIVE)
    parser.add_argument('-l', metavar='log.txt',
                        help='Log file.')
    parser.add_argument('-v', '--verbose', dest='verbose', action="store_true", default=True,
                        help="Be verbose.")
    parser.add_argument('-q', '--quiet', dest='verbose', action="store_false",
                        help="Be quiet.")
    parser.add_argument('-d', '--debug', action="store_true",
                        help="Debug (to file log only, not stdout).")
    parser.add_argument('-c', action="store_true",
                        help="Add some comments to the output lp file.")
    parser.add_argument('--ignore-circular-singletons', action="store_true",
                        help="Use only if ABSOLUTELY certain, that the matching solution will not contain circular singletons. Else distances and matchings computed using this option could be corrupted.")
    args = parser.parse_args()

    if args.heuristic_capping_abs < 1:
        raise argparse.ArgumentTypeError('-A/--absolute-threshold must be a positive integer.')

    if args.alpha != 'auto':
        if isfloat(args.alpha):
            args.alpha = float(args.alpha)
            if args.alpha < 0:
                raise argparse.ArgumentTypeError('-a/--alpha must be a positive float.')
        else:
            raise argparse.ArgumentTypeError('-a/--alpha must be a positive float or \'auto\'.')

    if args.l:
        st = logging.FileHandler(args.l, mode='w')
        
        st.setLevel(logging.DEBUG if args.debug else logging.INFO)
        st.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
        LOG.addHandler(st)
    
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO if args.verbose else logging.WARNING)
    ch.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(ch)

    mm = args.matching == 'max'
    genome1 = args.genome1
    genome2 = args.genome2

    LOG.info("Reading genomes configuration.")
    G = readGenomesCfg(args.cfg)

    if genome1 == genome2: # then we must duplicate the object and change the name of genome2
        genome2 += '_'
        G[genome2] = deepcopy(G[genome1])

    # relabel? actually we don't care...
    #G[genome1]['label'] = 'A'
    #G[genome2]['label'] = 'B'
    
    # assigns a unique ID for each gene (because genes in different genomes may have the same number/name/id)
    gen = Simple_Id_Generator()
    id_genome(G[genome1], gen)
    id_genome(G[genome2], gen)
    
    LOG.info("Writing map of original to unique gene numbers to file %s."%args.u.name)
    writeGeneNumberMap(G[genome1], args.u)
    writeGeneNumberMap(G[genome2], args.u)

    LOG.info("Reading similarities file.")
    edges = readSimilaritiesFile(args.sim, args.setting)
    LOG.info("%d gene similarity edges." % len(edges))

    if args.alpha == 'auto':
        v_weights = defaultdict(float)
        for e in edges:
            id1 = 'g1'+str(e['g1'])
            id2 = 'g2' + str(e['g2'])
            v_weights[id1] = max(v_weights[id1], e['weight'])
            v_weights[id2] = max(v_weights[id2], e['weight'])
        args.alpha = auto_alpha(sum(v_weights.values()) / len(v_weights))
        LOG.info("Setting automatically alpha to %.2f and writing it to file." % args.alpha)
        f = open(args.o.name + '.alpha', 'wt')
        print("%.2f" % args.alpha, file=f)
        f.close()

    genes1 = G[genome1]['genes']
    genes2 = G[genome2]['genes']
    estats = Counter()
    for e in edges:
        e1 = {'to': e['g2'], 'rel_orient': e['rel_orient'], 'weight': e['weight'] }
        genes1[e['g1']]['edges'].append(e1)
        e2 = {'to': e['g1'], 'rel_orient': e['rel_orient'], 'weight': e['weight'] }
        genes2[e['g2']]['edges'].append(e2)
        estats['A_%d' % e['g1']] += 1
        estats['B_%d' % e['g2']] += 1
    gt1 = [ c for c in estats.values() if c > 1 ]
    if len(gt1) > 0:
        LOG.info("%d vertices with degree > 1, avg. degree for them: %f" % (len(gt1), sum(gt1)/len(gt1)))
    else:
        LOG.info("0 vertices with degree > 1")
        
    LOG.info("Creating weighted adjacency graph.")
    heuristic_capping = {'absolute':args.heuristic_capping_abs, 'relative':args.heuristic_capping_rel} if args.heuristic_capping else False
    V, E, p_star = create_adjacency_graph(G[genome1], G[genome2], gen, heuristic_capping)
    print_graph(V, E)

    LOG.info("Generating ILP.")
    ilp = create_ILP(V, E, p_star, args.alpha, args.setting, mm, True) # optimize always True... remove parameter?
    if args.ignore_circular_singletons:
       LOG.warning("Circular singletons will not be handeled. If any exist, the solution will be corrupted. There will be no further warning on this issue.")
    else:
       ilp = singleton_constraints(G, ilp, E)

    LOG.info("Writing ILP.")
    print_ILP_cpl(ilp, args.o, args.c)

    if args.I:
        LOG.info("Defining and writing MIP start file with variables hinting an initial solution.")
        initial_sol(V, E, args.I)

    LOG.info("DONE. Exiting.")
    
    
if __name__ == '__main__':
    main()
