#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021 Diego Rubert
#
# (Based on GEN-DIFF, Copyright (C) 2020 Diego P. Rubert, https://gitlab.ub.uni-bielefeld.de/gi/gen-diff)
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software. If not, see <https://www.gnu.org/licenses/>


from os import path, fdopen
from collections import OrderedDict, Counter, defaultdict
import sys
from lxml import etree
from argparse import ArgumentParser, FileType, RawDescriptionHelpFormatter
import networkx as nx
from re import compile

from ffdcjid_util import readGeneNumberMap, readSimilaritiesFile, readGenomesCfg, parseSolMatching, parseMstMatching, FAMILY_FREE_ID, EXTREMITY_TELOMERE, EXTREMITY_TAIL, EXTREMITY_HEAD, PATTERN_LOCUS_TAG, PATTERN_PROTID

PATTERN_COMPLETE_GENEID = compile(r'(.*)')
TYPE_SOL = 'sol'
TYPE_MST = 'mst'
TYPE_SIM = 'sim'  # heuristic already gives its solution in this format

FAMILY_TYPE_ANY = 0
FAMILY_TYPE_AMBIGUOUS = 1
FAMILY_TYPE_RESOLVED = 2
FAMILY_TYPE_COMPLETE = 4 # 1 2 4 ... for bitwise comparisons
FAMILY_TYPE_SUFFIXES = {FAMILY_TYPE_AMBIGUOUS:'.ambiguous', FAMILY_TYPE_RESOLVED:'.resolved', FAMILY_TYPE_COMPLETE:'.complete'}

__author__ = 'diego'


class FamiliesBuilder:
    def __init__(self, fgenomes, sim_files, sol_files, sol_type, mcldump, log, level, identifier_pattern):
        self.S = self.M = self.MCL = None
        self.families0 = self.families1 = self.families2 = None
        self.log = log
        self.identifier_pattern = identifier_pattern
        
        self.genomes = readGenomesCfg(fgenomes) # no info on paralog edges!
        self.updateMetadata()
        self.species_of_gene = self.mapAllGenesToSpecies()
        self.genome_labels = { g['name']:g['label'] for g in self.genomes.values() }
        self.genome_names = { g['label']:g['name'] for g in self.genomes.values() }

        self.maximum_weighted_matching = level == -1
        self.level = max(0, level)
        if self.maximum_weighted_matching:
            self.level = 0
            self.S = self.buildMarkerSimilarityGraphMWMatching(sim_files)
        else:
            self.S = self.buildMarkerSimilarityGraph(sim_files)
        if self.level > 0: self.M = self.buildMatchingsGraph(sol_files, sol_type)
        if self.level > 1: self.MCL = self.buildMCLGraph(mcldump)
        self.defineFamilies()

        self.printGenesStats()

        self.printGraphStats(self.S)
        if self.level > 0: self.printGraphStats(self.M)
        if self.level > 1: self.printGraphStats(self.MCL)

        self.filtered_G = nx.Graph()
        self.filtered_families = OrderedDict()

    def filter(self, ftype):
        if ftype == FAMILY_TYPE_ANY:
            self.filtered_G = (self.S, self.M, self.MCL)[self.level]
            self.filtered_families = (self.families0, self.families1, self.families2)[self.level]
            return
        # filter family list
        families = (self.families0, self.families1, self.families2)[self.level]
        self.filtered_families = OrderedDict((family, data) for family, data in families.items() if self.family_type(data['genes']) & ftype and len(data['genes']) > 1)
        # filter family graph
        G = (self.S, self.M, self.MCL)[self.level].copy()
        to_remove = []
        for c in nx.connected_components(G):
            if not self.family_type(c) & ftype:
                to_remove.extend(c)
        G.remove_nodes_from(to_remove)
        self.filtered_G = G

    def family_type(self, family):
        genomes = [self.species_of_gene[gene] for gene in family]
        if any(genomes.count(genome) > 1 for genome in genomes):
            return FAMILY_TYPE_AMBIGUOUS
        return FAMILY_TYPE_RESOLVED | (FAMILY_TYPE_COMPLETE if len(genomes) == len(self.genomes) else 0)

    def writeTXT(self, txt, filtered=False):
        """Write file with highest-level families in text (space-separated) format."""
        self.consistencyCheck() 
        families = (self.families0, self.families1, self.families2)[self.level] if not filtered else self.filtered_families
        getIdentifier = lambda g: self.genomes[self.species_of_gene[g]]['ids'][g]['identifier']
        families = [ sorted(getIdentifier(gene) for gene in family['genes']) for family in families.values() ]
        families.sort()
        families.sort(key=len, reverse=True)
        for family in families:
            print(' '.join(family), file=txt)
        txt.close()

    def writeDump(self, dump, filtered=False):
        """Write file with highest-level families in text (tab-separated) format using full gene IDs."""
        families = (self.families0, self.families1, self.families2)[self.level] if not filtered else self.filtered_families
        families = [ sorted(family['genes']) for family in families.values() ]
        families.sort()
        families.sort(key=len, reverse=True)
        for family in families:
            print('\t'.join(family), file=dump)
        dump.close()
    
    def writeABC(self, abc, filtered=False):
        """Write file with edges of connected componentes in ABC format (for MCL https://micans.org/mcl/)."""
        G = (self.S, self.M, self.MCL)[self.level] if not filtered else self.filtered_G
        for c in nx.connected_components(G):
            C = G.subgraph(c)
            for gene1, gene2, data in C.edges(data=True):
                w = data['weight']
                print('%s %s %f' % (gene1, gene2, w), file=abc)
        abc.close()
    
    def writeSIF(self, sif, filtered=False):
        """Write file with edges of connected componentes in SIF format (for MCL https://micans.org/mcl/)."""
        G = (self.S, self.M, self.MCL)[self.level] if not filtered else self.filtered_G
        for c in nx.connected_components(G):
            C = G.subgraph(c)
            E = defaultdict(list) # maps each vertex to its edges
            for gene1, gene2, data in C.edges(data=True):
                w = data['weight']
                E[gene1].append( (gene2, w) )
                E[gene2].append( (gene1, w) )
            if len(C) == 1: # no edge, still has to be in the sif file
                gene = list(C.nodes())[0]
                E[gene] = []
            for u, edges in E.items():
                print('%s -> %s' % (u, ' '.join('%s:%f' % (v, w) for v, w in edges)), file=sif)
        sif.close()
    
    def printGenesStats(self):
        """Print some statistics about genes and families."""
        print("Number of genes: %d" % sum(len(genome['genes']) for genome in self.genomes.values()), file=self.log)
        print("Number of genes in level 0 families: %d (+%d singletons)" %
              (self.S.number_of_nodes() - self.S.graph['singletons'], self.S.graph['singletons']), file=self.log)
        if self.level > 0: print("Number of genes in level 1 families: %d (+%d singletons)" %
                                 (self.M.number_of_nodes() - self.M.graph['singletons'], self.M.graph['singletons']), file=self.log)
        if self.level > 1: print("Number of genes in level 2 families: %d (+%d singletons)" %
                                 (self.MCL.number_of_nodes() - self.MCL.graph['singletons'], self.MCL.graph['singletons']), file=self.log)
        print("", file=self.log)
    
    def printGraphStats(self, G):
        """Print some statistics about graph G."""
        print(G.name, 'with', G.number_of_nodes(), 'nodes and', G.number_of_edges(), 'edges', file=self.log)

        if G.number_of_edges() == 0:
            print('Graph has no edges\n', file=self.log)
            return

        print('Average edge weight: %f' % (sum(w for u,v,w in G.edges.data('weight'))/G.number_of_edges()), file=self.log)
        print('Density: %g' % (nx.density(G)), file=self.log)

        n = sum_density = 0
        n_without_singletons = 0
        for c in nx.connected_components(G):
            if len(c) > 1: # otherwise density = 0 for singletons
                sum_density += sum(G.degree(v) for v in c) / (len(c) * (len(c)-1))
                n_without_singletons += 1
            n += 1
        print('Average component density: %f' % (sum_density/n), file=self.log)
        print('Average component density (excluding singletons): %f' % (sum_density/n_without_singletons), file=self.log)
                
        print('Degree histogram: %s' % nx.degree_histogram(G), file=self.log)
        print(file=self.log)
        
    def writeXML(self, out):
        """Write the XML file with data."""
        document = etree.Element('orthoFFDCJID')
        self.buildXMLgenelist(document)
        self.buildXMLfamilies(document)
        et = etree.ElementTree(document)
        et.write(out, pretty_print=True, encoding='utf-8', xml_declaration=True)

    def buildXMLgenelist(self, document):
        """Build the "genes" part of the XML (the list of all genes)."""
        genesXML = etree.SubElement(document, 'genes')
        for genome in self.genomes.values():
            sp = etree.SubElement(genesXML, 'species', ffgc_name=genome['name'])
            if 'species' in genome:
                sp.set('name', genome['species'])
            for g in genome['genes'].values():
                etree.SubElement(sp, 'gene', attrib={
                    #'identifier': g['identifier'],
                    #'order': str(g['order']),
                    'id': g['numeric_id'], # numeric id
                    'locus_tag': g['locus_tag'],
                    'protId': g['protId'],
                    'fullId': g['id'],
                    'chromosome': g['chr'],
                    'strand': g['strand']})

    def buildXMLfamilies(self, document):
        """Build the "families" part of the XML."""
        familiesXML = etree.SubElement(document, 'families')
        for number, family in self.families0.items():
            self.buildXMLfamily(familiesXML, number, family)

    def buildXMLfamily(self, element, number, family):
        """Build one family recursively in the XML."""
        familyXML = etree.SubElement(element, 'family')
        for k,v in family.items():
            if k == 'genes':
                if 'genes_in_subfamilies' in family: # has subfamilies
                    self.buildXMLgenes(familyXML, [g for g in v if g not in family['genes_in_subfamilies']])
                else:
                    self.buildXMLgenes(familyXML, v)
            elif k != 'subfamilies' and k != 'genes_in_subfamilies':
                familyXML.attrib[k] = str(v)

        if 'subfamilies' in family:
            for n, f in family['subfamilies'].items():
                self.buildXMLfamily(familyXML, n, f)
    
    def buildXMLgenes(self, element, genes):
        """Build the genes of a family in the XML."""
        for gene in genes:
            geneXML = etree.SubElement(element, 'gene', id=self.id2numeric[gene])
            for u,v,data in self.S.edges(gene, data=True):
                if u in genes and v in genes: # only if is an edge to the genes in the same family-level
                    etree.SubElement(geneXML, 'orthology', # orthologTo instead of orthology?
                                     id=self.id2numeric[u] if u != gene else self.id2numeric[v],
                                     weight=str(data['weight']))

    def addSingletonsToGraph(self, G):
        """Add to G genes that are not there yet (add genes as isolated vertices)."""
        singletons = 0
        for genome in self.genomes.values():
            for gene in genome['genes'].values():
                if gene['id'] not in G.nodes:
                    G.add_node(gene['id'], **gene)
                    singletons += 1
        G.graph['singletons'] = singletons
                
    def buildMarkerSimilarityGraph(self, sim_files):
        """Build an extended marker similarity graph for all genome
           pairs. This graph is the union of marker similarity graphs
           for genome pairs plus edges between similar genes in the
           same genome.
        """
        G = nx.Graph(name='Extended Marker Similarity Graph (level 0)')
        for fsim in sim_files:
            genome1, genome2 = path.splitext(path.basename(fsim.name))[0].split('_') # genome names
            edges = readSimilaritiesFile(fsim, FAMILY_FREE_ID)
            for e in edges:
                gene1 = self.genomes[genome1]['genes'][e['g1']]
                id1 = gene1['id']
                gene2 = self.genomes[genome2]['genes'][e['g2']]
                id2 = gene2['id']
                w = e['weight']
                gene1['edges'].append(id2) # can't reference the gene object, otherwise we'll get an infinite loop when printing a node
                gene2['edges'].append(id1)
                if id1 not in G: G.add_node(id1, **gene1)
                if id2 not in G: G.add_node(id2, **gene2)
                G.add_edge(id1, id2, weight=w)
                        
        # add isolated vertices (they are not in the similarity .sim files)
        self.addSingletonsToGraph(G)
        
        return G # to retrieve edges with weights: G.edges(data=True)

    def maxWeightMatching(self, edges):
        G = nx.Graph()
        G.add_nodes_from((('g1', e['g1']) for e in edges), bipartite=0)
        G.add_nodes_from((('g2', e['g2']) for e in edges), bipartite=1)
        G.add_weighted_edges_from((('g1', e['g1']), ('g2', e['g2']), e['weight']) for e in edges)
        # mwm = nx.max_weight_matching(G) # very slow if computed for the whole graph
        mwm = set.union(*(nx.max_weight_matching(G.subgraph(C)) for C in nx.connected_components(G)))
        H = G.edge_subgraph(mwm)
        return [e for e in edges if H.has_edge(('g1', e['g1']), ('g2', e['g2']))]


    def buildMarkerSimilarityGraphMWMatching(self, sim_files):
        """Build a marker similarity graph for all genome pairs.
           This graph is the union of edges in maximum-weight matchings
           in maker similarity graphs for genome pairs.
        """
        G = nx.Graph(name='Maximum-weight-matchings of Marker Similarity Graphs')
        for fsim in sim_files:
            genome1, genome2 = path.splitext(path.basename(fsim.name))[0].split('_')  # genome names
            if genome1 == genome2:
                continue
            edges = readSimilaritiesFile(fsim, FAMILY_FREE_ID)
            mwm = self.maxWeightMatching(edges)
            for e in mwm:
                gene1 = self.genomes[genome1]['genes'][e['g1']]
                id1 = gene1['id']
                gene2 = self.genomes[genome2]['genes'][e['g2']]
                id2 = gene2['id']
                w = e['weight']
                gene1['edges'].append(id2)  # can't reference the gene object, otherwise we'll get an infinite loop when printing a node
                gene2['edges'].append(id1)
                if id1 not in G: G.add_node(id1, **gene1)
                if id2 not in G: G.add_node(id2, **gene2)
                G.add_edge(id1, id2, weight=w)

        # add isolated vertices (they are not in the similarity .sim files)
        self.addSingletonsToGraph(G)

        return G  # to retrieve edges with weights: G.edges(data=True)

    def getGeneID(self, geneidmap, gene):
        """Finds the gene id from a gene tag used in the ILP, needs the
           unique gene ID map (from unique to original ID) together
           with the ILP file.
        """
        num,genome,ext = gene.split('_') # gene number, genome and extremity type
        genome = self.genome_names[genome]
        num = geneidmap[genome][int(num)]
        genes = self.genomes[genome]['genes']
        gid = genes[num]['id']
        return gid

    def buildMatchingsGraph(self, sol_files, sol_type):
        """Build a graph from matchings in solution files for all genome
           pairs. Marker similarity graph must be built already (in
           order to find edge weights).
        """
        if sol_type == TYPE_SIM:
            return self.buildMatchingsGraphHeuristic(sol_files)
        else:
            return self.buildMatchingsGraphILP(sol_files, sol_type)

    def buildMatchingsGraphHeuristic(self, sim_files):
        G = nx.Graph(name='Matchings Graph (level 1)')

        for fsim in sim_files:
            genome1, genome2 = path.splitext(path.basename(fsim.name))[0].split('_')  # genome names
            edges = readSimilaritiesFile(fsim, FAMILY_FREE_ID)
            for e in edges:
                gene1 = self.genomes[genome1]['genes'][e['g1']]
                id1 = gene1['id']
                gene2 = self.genomes[genome2]['genes'][e['g2']]
                id2 = gene2['id']
                w = e['weight']
                if id1 not in G: G.add_node(id1, **gene1)
                if id2 not in G: G.add_node(id2, **gene2)
                G.add_edge(id1, id2, weight=w)
        # add isolated vertices (a gene may have edges but would not be added to the graph of it is in no matching)
        self.addSingletonsToGraph(G)
        return G

    def buildMatchingsGraphILP(self, sol_files, sol_type):
        """Build a graph from matchings in ILP solution files."""
        G = nx.Graph(name='Matchings Graph (level 1)')

        for fsol in sol_files:
            if sol_type == TYPE_SOL:
                edges = parseSolMatching(fsol)
            else: # sol_type == TYPE_MST
                edges = parseMstMatching(fsol)
            fmapname = '.'.join(path.splitext(fsol.name)[:-1]) + '.gidmap'
            with open(fmapname) as fmap:
                geneidmap, genome1, genome2 = readGeneNumberMap(fmap)
            geneidmap_rev = { genome: {v:k for k,v in genes.items() } for genome,genes in geneidmap.items() }
            for e in edges:
                id1 = self.getGeneID(geneidmap_rev, e[0])
                gene1 = self.genomes[genome1]['ids'][id1]
                id2 = self.getGeneID(geneidmap_rev, e[1])
                gene2 = self.genomes[genome2]['ids'][id2]
                w = self.S.get_edge_data(id1, id2)['weight']
                if id1 not in G: G.add_node(id1, **gene1)
                if id2 not in G: G.add_node(id2, **gene2)
                G.add_edge(id1, id2, weight=w)
        # add isolated vertices (a gene may have edges but would not be added to the graph of it is in no matching)
        self.addSingletonsToGraph(G)
        return G

    def buildMCLGraph(self, mcldump):
        """Build a graph from MCL dump file with labels. Marker similarity
           graph must be build already (in order to find edge weights).
        """
        G = nx.Graph(name='MCL Graph (level 2)')
        get_genome = lambda l: next(g for g in self.genomes.values() if l in g['ids'])
        for line in mcldump:
            family = line.split()
            if len(family) < 2:
                continue
            for gid in family:
                if gid not in G:
                    gene = get_genome(gid)['ids'][gid]
                    G.add_node(gid, **gene)
            
            C = self.M.subgraph(family) # component of induced subgraph
            for id1, id2, data in C.edges(data=True):
                w = data['weight']
                G.add_edge(id1, id2, weight=w)
        # add isolated vertices (if any in MCL clustering)
        self.addSingletonsToGraph(G)
        return G

    def componentSpeciesAndGenes(self, component):
        """Find and return a string of species and the list of genes of component."""
        genes = []
        species = set()
        for gene in component:
            species.add(self.species_of_gene[gene])
            genes.append(gene)
        species = [sp for sp in self.genomes.keys() if sp in species] # to keep the same order of genomes.cfg
        species = ','.join(species)
        return species, genes

    def printFamiliesStats(self, families, level, hist_limit):
        """Print some statistics about families."""
        print("Number of level %d families: %d" % (level, len(families)), file=self.log)
        print("Number of level %d families by size:" % level, file=self.log)
        counter = Counter(len(f['genes']) for f in families.values())
        for i in range(2,hist_limit):
            print("%d: %d" % (i, counter[i]), file=self.log)
        print("%d+: %d" % (hist_limit, sum(v for k,v in counter.items() if k >= hist_limit)), file=self.log)
        if len(counter) > 0:
            print("Largest level %d family size: %d\n" % (level, max(counter.keys())), file=self.log)
    
    def defineFamilies(self):
        """Define families of the requested level."""
        self.families0 = self.defineLevel0Families()
        if self.level > 0:
            self.families1 = self.defineLevel1Families(self.families0)
        if self.level > 1:
            self.families2 = self.defineLevel2Families(self.families1)

    def defineLevel0Families(self):
        """Define level 0 families based on the extended marker similarity graph."""
        families = OrderedDict()
        for c in nx.connected_components(self.S):
            if len(c) == 1: continue # components with only 1 vertex are kept out of families
            species, genes = self.componentSpeciesAndGenes(c)
            families[len(families)+1] = { 'id':len(families)+1, 'species':species, 'genes':genes, 'subfamilies':OrderedDict(), 'genes_in_subfamilies':set() }

        self.printFamiliesStats(families, 0, 51)
        return families

    def defineLevel1Families(self, families0):
        """Define families based on the conected components of the matchings."""
        family_of_gene = { gene:family for family in families0.values() for gene in family['genes'] }
        families1 = OrderedDict()
        #cliques = Counter()

        for c in nx.connected_components(self.M):
            if len(c) == 1: continue # ignore unmatched (deleted) genes
            species, genes = self.componentSpeciesAndGenes(c)
            parent_family = family_of_gene[genes[0]]
            parent_family['genes_in_subfamilies'].update(genes)
            fid = "%s.%s" % (parent_family['id'], len(parent_family['subfamilies'])+1)
            families1[fid] = parent_family['subfamilies'][fid] = { 'id':fid, 'species':species, 'genes':genes, 'subfamilies':OrderedDict(), 'genes_in_subfamilies':set() }

            #H = self.M.subgraph(c)
            #n = len(c)
            #if H.size() == n * (n-1) / 2:
            #    cliques[n] += 1
        
        self.printFamiliesStats(families1, 1, 21)
        #print('Cliques (size: count): [', ', '.join('%d: %d' % (size, count) for size, count in sorted(cliques.items())), ']')
        return families1

    def defineLevel2Families(self, families1):
        """Define families based on the conected components of the matchings."""
        family_of_gene = { gene:family for family in families1.values() for gene in family['genes'] }
        families2 = OrderedDict()

        for c in nx.connected_components(self.MCL):
            if len(c) == 1: continue # ignore singleton genes
            species, genes = self.componentSpeciesAndGenes(c)
            parent_family = family_of_gene[genes[0]]
            parent_family['genes_in_subfamilies'].update(genes)
            fid = "%s.%s" % (parent_family['id'], len(parent_family['subfamilies'])+1)
            families2[fid] = parent_family['subfamilies'][fid] = { 'id':fid, 'species':species, 'genes':genes }

        self.printFamiliesStats(families2, 2, 21)
        return families2

    def mapAllGenesToSpecies(self):
        """Maps each gene to its species."""
        species_of_gene = dict()
        for name,genome in self.genomes.items():
            for gene in genome['genes'].values():
                species_of_gene[gene['id']] = name
        return species_of_gene
    
    def updateMetadata(self):
        """For all genes, find and set some metadata. Also adds to each
           genome a dict mapping each gene full id to its gene object.
        """
        count = 0
        self.id2numeric = dict()
        for genome in self.genomes.values():
            genome['ids'] = dict()
            for gene in genome['genes'].values():
                identifier = self.identifier_pattern.match(gene['id']).groups()[0]
                gene['identifier'] = identifier # used only for plain text output
                match = PATTERN_LOCUS_TAG.match(gene['id'])
                if match:
                    locus_tag = match.groups()[0]
                else:
                    locus_tag = gene['id']
                gene['locus_tag'] = locus_tag
                protid = PATTERN_PROTID.match(gene['id']).groups()[0]
                gene['protId'] = protid
                genome['ids'][gene['id']] = gene
                count += 1
                numeric_id = str(count)
                gene['numeric_id'] = numeric_id
                self.id2numeric[gene['id']] = numeric_id

    def consistencyCheck(self):
        """Check consistency of the identifier selected for plain text
           output. Some providers use the same locus tag along different
           species, for instance.
        """
        idmap = dict() # maps each identifier to the genome it appeared
        for gname, genome in self.genomes.items():
            species = genome['species']
            for gene in genome['genes'].values():
                identifier = gene['identifier']
                if identifier not in idmap:
                    idmap[identifier] = gname
                else:
                    print('*****')
                    if idmap[identifier] != gname:
                        print('ERROR: ambiguous identifier "%s" found in genomes "%s" (%s) and "%s" (%s).'
                              % (identifier, gname, species, idmap[identifier], self.genomes[idmap[identifier]]['species']))
                    else:
                        print('ERROR: duplicate identifier "%s" found in genome "%s" (%s).'
                              % (identifier, gname, species))
                    print('Consider using another option among --use-locus-tag/--use-protein-id/--use-complete-id for plain text output.')
                    print('*****')
                    exit(2)


def open_with_proper_suffix(family_type, filename):
    suf = FAMILY_TYPE_SUFFIXES.get(family_type, '')
    parts = list(path.splitext(filename))
    parts.insert(-1, suf)
    return open(''.join(parts), 'wt')

def main():
    description = 'Reads multiple FFGC files and computes gene families (with at least 2 genes).'
    epilog = 'Gene clustering (family) levels are:\n' + \
             '\t0: Only from connected components of the Extended Marker Similarity Graph\n' + \
             '\t1: Families are refined by the Matchings Graph (-s/--sol required)\n' + \
             '\t2: Families are further refined using results given by MCL (-d/--mcl-dump required)\n' + \
             '\t-1: Similar to 0, but takes only the edges of a maximum-weight matching in the Marker Similarity Graph for each genome pair. Automatically ignores passed .sim file of a genome to itself.\n\n' + \
             'IMPORTANT: abc file format doesn\'t support singleton vertices, be adviced to use sif if that is the case!\n'
    parser = ArgumentParser(description=description, epilog=epilog, formatter_class=RawDescriptionHelpFormatter)

    parser.add_argument('-C', '--cfg', required=True, type=FileType('r'), metavar='genomes.cfg',
                        help='Genomes configuration file generated by FFGC.')
    parser.add_argument('-i', '--sim', metavar='file.sim', required=True, type=FileType('r'), nargs='+',
                        help='Marker similarities files (given by FFGC), genome names are guessed from the file name in the format Gx_Gy.sim, where Gx and Gy are the genome names (as in genomes.cfg). **PROVIDE Gx_Gx.sim IF YOU WANT PARALOGS IN FAMILIES**.')

    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-s', '--sol', metavar='file.sol', type=FileType('r'), nargs='+',
                       help='Solution files (given by CPLEX), genome names are guessed from the PREFIX of the file name before the first dot in the format Gx_Gy, where Gx and Gy are the genome names (as in genomes.cfg). In addition, a file with gene IDs mapping (given by write_ffdcjid_ilp.py) with the same name but extension .gidmap is expected to exist in the same directory.')
    group.add_argument('-m', '--mst', metavar='file.mst', type=FileType('r'), nargs='+',
                       help='Solution files (given by Gurobi), genome names are guessed from the PREFIX of the file name before the first dot in the format Gx_Gy, where Gx and Gy are the genome names (as in genomes.cfg). In addition, a file with gene IDs mapping (given by write_ffdcjid_ilp.py) with the same name but extension .gidmap is expected to exist in the same directory.')
    group.add_argument('-M', '--matching', metavar='file.sim', type=FileType('r'), nargs='+',
                       help='Solution files (given by ffdcjid-heuristic or postprocessed ILP solutions) with gene matchings in similarity format with numeric gene IDs.')

    parser.add_argument('-d', '--mcl-dump', required=False, metavar='file.dump', type=FileType('r'),
                        help='Read the MCL output in labels format given by mcxdump to further refine families.')
    parser.add_argument('-l', '--log', required=False, metavar='file.log', type=FileType('w'), default=sys.stderr,
                        help='Write to this file some information and statistics about the families (default: stderr).')
    parser.add_argument('-L', '--level', required=True, type=int, choices=[0, 1, 2, -1], default=1,
                        help='Level of gene custering (families) (see epilog).')

    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('--use-locus-tag', dest='identifier_pattern', required=False, action='store_const', const=PATTERN_LOCUS_TAG,
                       help='Use locus tags to identify genes in plain text output.')
    group.add_argument('--use-protein-id', dest='identifier_pattern', required=False, action='store_const', const=PATTERN_PROTID,
                       help='Use protein IDs as locus tags to identify genes in plain text output.')
    group.add_argument('--use-complete-id', dest='identifier_pattern', required=False, action='store_const', const=PATTERN_COMPLETE_GENEID,
                       help='Use complete gene IDs as locus tags to identify genes in plain text output (default).')

    parser.set_defaults(identifier_pattern=PATTERN_COMPLETE_GENEID)

    group = parser.add_argument_group(title='Output files')
    group.add_argument('-f', '--families', metavar='file.xml', required=False, type=FileType('wb'), default=None,
                        help='XML output file for families.')
    group.add_argument('-T', '--text-families', metavar='file.txt', required=False, type=FileType('w'), default=None,
                        help='Plain text output file for families (only highest-level-families).')
    group.add_argument('-a', '--abc', required=False, metavar='file.abc', type=FileType('w'),
                        help='Write the *edges* of highest-level-families in abc format for MCL (see https://micans.org/mcl/).')
    group.add_argument('-S', '--sif', required=False, metavar='file.sif', type=FileType('w'),
                        help='Write the *edges* of highest-level-families in sif format for MCL (see https://micans.org/mcl/).')
    group.add_argument('-D', '--dump', required=False, metavar='file.dump', type=FileType('w'),
                        help='Output as in -T/--text-families, but always with full gene IDs and tab-separated.')

    group = parser.add_argument_group(title='Extra output files', description='Extra output files with proper suffixes (e.g. family.ambiguous.sif) are written, except XML.')
    group.add_argument('--ambiguous-extra', dest='family_types', required=False, action='append_const', const=FAMILY_TYPE_AMBIGUOUS, default=[],
                       help='Output extra files (.ambiguous) with ambiguous families only.')
    group.add_argument('--resolved-extra', dest='family_types', required=False, action='append_const', const=FAMILY_TYPE_RESOLVED,
                       help='Output extra files (.resolved) with resolved families only.')
    group.add_argument('--resolved-complete-extra', dest='family_types', required=False, action='append_const', const=FAMILY_TYPE_COMPLETE,
                       help='Output extra files (.complete) with resolved complete families only.')

    args = parser.parse_args()

    if args.level > 0 and not args.sol and not args.mst and not args.matching:
        parser.print_usage()
        print('%s: error: when using -L/--level 1, the following arguments are required: -s/--sol, -m/--mst or -M/--matching' % (path.basename(sys.argv[0])))
        exit(1)

    if args.level > 1 and not args.mcl_dump:
        parser.print_usage()
        print('%s: error: when using -L/--level 2, the following arguments are required: -m/--mcl-dump' % (path.basename(sys.argv[0])))
        exit(1)

    print('Log for:', ' '.join(sys.argv), file=args.log)
    print('(* families consist of at least 2 genes)\n', file=args.log)
    if args.sol:
        sol, soltype = args.sol, TYPE_SOL
    elif args.mst:
        sol, soltype = args.mst, TYPE_MST
    else:
        sol, soltype = args.matching, TYPE_SIM
    families = FamiliesBuilder(args.cfg, args.sim, sol, soltype, args.mcl_dump, args.log, args.level, args.identifier_pattern)

    # default files with all families
    if args.families: families.writeXML(args.families)
    if args.text_families: families.writeTXT(args.text_families)
    if args.abc: families.writeABC(args.abc)
    if args.sif: families.writeSIF(args.sif)
    if args.dump: families.writeDump(args.dump)

    # filtered files
    for family_type in args.family_types:
        families.filter(family_type)
        if args.text_families: families.writeTXT(open_with_proper_suffix(family_type, args.text_families.name), filtered=True)
        if args.abc: families.writeABC(open_with_proper_suffix(family_type, args.abc.name), filtered=True)
        if args.sif: families.writeSIF(open_with_proper_suffix(family_type, args.sif.name), filtered=True)
        if args.dump: families.writeDump(open_with_proper_suffix(family_type, args.dump.name), filtered=True)

    exit(0)

if __name__ == "__main__":
    main()
