#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2021 Diego Rubert
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software. If not, see <https://www.gnu.org/licenses/>


import xml.etree.ElementTree as ET
from argparse import ArgumentParser, FileType
from ffdcjid_util import readGenomesCfg, parseSolMatching, parseMstMatching, readSimilaritiesFile, readGeneNumberMap, FAMILY_FREE_ID, PATTERN_LOCUS_TAG
from sys import stdout, stderr

__author__ = 'diego'


def getGeneAndLocusTag(genome, geneidmap, gene):
    """Finds the number and locus tag from a gene extremity tag used in
       the ILP, needs the unique gene ID map (from unique to original
       ID) and the genome.
    """
    num,label,ext = gene.split('_') # gene number, genome and extremity type
    gname = genome['name']
    num = geneidmap[gname][int(num)]
    gene = genome['genes'][num]
    locus = PATTERN_LOCUS_TAG.match(gene['id']).groups()[0]
    return gene, locus

def getGeneNumber(genome, geneidmap, gene_ext_tag):
    """Finds the gene number from a gene extremity tag used in the ILP,
       needs the unique gene ID map (from unique to original ID) and
       the genome.
    """
    num,label,ext = gene_ext_tag.split('_') # gene number, genome and extremity type
    gname = genome['name']
    num = geneidmap[gname][int(num)]
    return num


if __name__ == "__main__":
    description = 'Reads a similarity (.sim) file, a (cplex) solution (.sol) file and other auxiliary files and prints the edges in the similarity file corresponding to the matching in the solution.'
    parser = ArgumentParser(description=description)

    parser.add_argument('-C', '--cfg', required=True, type=FileType('r'), metavar='genomes.cfg',
                        help='Genomes configuration file generated by FFGC.')
    parser.add_argument('-i', '--sim', metavar='file.sim', required=True, type=FileType('r'),
                        help='Marker similarities file (given by FFGC).')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-s', '--sol', metavar='file.sol', type=FileType('r'),
                        help='Solution file (given by CPLEX), genome names are taken from the guess in --sim option.')
    group.add_argument('-m', '--mst', metavar='file.mst', type=FileType('r'),
                       help='Solution file (given by Gurobi), genome names are taken from the guess in --sim option.')
    parser.add_argument('-g', '--geneid-map', metavar='file.gidmap', required=True, type=FileType('r'),
                        help='File with gene IDs mapping (given by write_ffdcjid_ilp.py), that maps gene numbers in the .sim to the unique numbers used in the ILP file. This file is also used to guess genome names Gx (string after 1st \'>\') and Gy (string after 2nd \'>\'), where Gx and Gy are the genome names as in genomes.cfg and should match to the name of other files (e.g. Gx_Gy.sim).')
    parser.add_argument('--omit-orientation', action='store_true', required=False,
                        help='Omit column with relative orientation of gene pairs.')
    parser.add_argument('--omit-chromosome-names', action='store_true', required=False,
                        help='Omit columns with chromosome names (columns 1 and 3).')
    parser.add_argument('-o', '--sim-out', metavar='out.sim', required=False, type=FileType('w'), default=stdout,
                        help='Output file with matching edges (default: stdout).')
 
    args = parser.parse_args()

    print("Reading genomes configuration.", file=stderr)
    genomes = readGenomesCfg(args.cfg)
    print("Reading solution file.", file=stderr)
    if args.sol:
        matching = parseSolMatching(args.sol)
    else:
        matching = parseMstMatching(args.mst)
    print("Reading gene ID map.", file=stderr)
    geneidmap, gname1, gname2 = readGeneNumberMap(args.geneid_map)

    geneidmap_rev = { genome: {v:k for k,v in genes.items() } for genome,genes in geneidmap.items() }
    matching = { ( getGeneNumber(genomes[gname1], geneidmap_rev, e[0]),
                   getGeneNumber(genomes[gname2], geneidmap_rev, e[1])
                 ) for e in matching }

    print("Reading similarities.", file=stderr)
    edges = readSimilaritiesFile(args.sim, FAMILY_FREE_ID, keep_chr_data=not args.omit_chromosome_names)

    if args.omit_chromosome_names:
        keys = ['g1', 'g2', 'rel_orient', 'weight']
    else:
        keys = ['chr1', 'g1', 'chr2', 'g2', 'rel_orient', 'weight']
    if args.omit_orientation:
        keys.remove('rel_orient')
    
    print("Writing output.", file=stderr)
    for e in edges:
        num1, num2 = e['g1'], e['g2'] # to check if belongs to matching
        if (num1, num2) in matching:
            print('\t'.join(str(e[k]) for k in keys), file=args.sim_out)
    
    exit(0)

