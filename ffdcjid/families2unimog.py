#!/usr/bin/env python3


from argparse import ArgumentParser, FileType
from sys import stdout, stderr
from os import path
from ffdcjid_util import parseGeneRecord, PATTERN_LOCUS_TAG


def readGenome(f):
    """Read a fasta file and return a list of chromosomes and their
       genes. Genes of each chromosome are expected to appear in their
       order in the chromosome.
    """
    chromosomes = []
    last_chr = ""
    
    for line in f:
        if not line.startswith('>'):
            continue
        line = line[1:].strip() # remove >
        gene = parseGeneRecord(line)
        gene['locus_tag'] = PATTERN_LOCUS_TAG.match(gene['id']).groups()[0]

        if gene['chr'] != last_chr:
            last_chr = gene['chr']
            chromosomes.append(list())

        chromosomes[-1].append(gene)

    return chromosomes    


def main(): # so variables are NOT global
    descr = "Given a file with gene families and fasta files (.gos) generated by FFGC (in which the order of genes is their order in chromosomes), generates a unimog file. Useful for analyzing genomes with family-based tools after families were defined using FFGC."
    epilog = "Each family is a sequence of locus tags of genes in one line, and the line number in that file defines the number of each family."

    parser = ArgumentParser(description=descr, epilog=epilog)
    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-l', '--linear', dest="linear", required=False, action="store_true", default=True,
                        help='Chromosomes are linear (default).')
    group.add_argument('-c', '--circular', dest="linear", required=False, action="store_false", default=True,
                        help='Chromosomes are circular.')
    parser.add_argument('-g', '--genomes', required=True, type=FileType('r'), nargs='+', metavar='file.gos',
                        help='Fasta files with the genes of genomes.')
    parser.add_argument('-f', '--families', required=True, type=FileType('r'), metavar='file.families',
                        help='File with gene families.')
    parser.add_argument('-u', '--unimog', default=stdout, type=FileType('w'), metavar='file.unimog',
                        help='Output unimog file (default: stdout).')
    

    args = parser.parse_args()
    
    genomes = []
    labels = [ path.splitext(path.basename(g.name))[0] for g in args.genomes ]
    for label, genome_file in zip(labels, args.genomes):
        print("Reading genome %s (label %s)." % (path.basename(genome_file.name), label), file=stderr)
        genome = readGenome(genome_file)
        genomes.append(genome)

    print("Reading families.", file=stderr)
    familymap = dict()
    for n, family in enumerate(args.families, start=1):
        familymap.update( (gene, n) for gene in family.split())
    nfamilies = n

    print("Writing unimog file.", file=stderr)
    unimog = args.unimog

    for label, genome in zip(labels, genomes):
        print('>%s' % label, file=unimog)
        for chromosome in genome:
            for gene in chromosome:
                if gene['locus_tag'] not in familymap:
                    family = nfamilies = nfamilies + 1 # new family with just this gene
                else:
                    family = familymap[gene['locus_tag']]
                strand = '-' if gene['strand'] == '-' else ''
                print('%s%s' % (strand, family), end=' ', file=unimog)
            print('|' if args.linear else ')', file=unimog)
    unimog.close()
    
    exit(0)


if __name__ == '__main__':
    main()

