#!/usr/bin/env python3

from sys import argv, exit, stdout, stderr, argv
from os.path import basename

# Compatible with both pre- and post Biopython 1.78:
try:
    from Bio.Alphabet import generic_protein
except ImportError:
    generic_protein = None

from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio import SeqIO
from optparse import OptionParser
from os.path import basename, splitext
import logging
import csv
import re
import gzip, bz2, lzma # allows reading of compressed files

PAT_ID = re.compile(r'(^|.*;)\s*id=([^;]+)(;|$)', re.IGNORECASE)
PAT_PARENT = re.compile(r'(^|.*;)\s*parent=([^;]+)(;|$)', re.IGNORECASE)
PAT_LONGEST = re.compile(r'(^|.*;)LONGEST=([^;]+)(;|$)', re.IGNORECASE)

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]

COMPRESS_HANDLERS = { '.gz' : gzip,
                      '.bz2' : bz2,
                      '.xz' : lzma }

# Returns an open handler for the file path (optionally compressed)
# If the file is already open, then reopen (using the correct handler)
def openCompressed(f):
    if isinstance(f, str): # path
        fname = f
    else: # file object
        f.close()
        fname = f.name
    extension = splitext(fname)[-1]
    if extension in COMPRESS_HANDLERS:
        return COMPRESS_HANDLERS[extension].open(f, mode='rt')
    else:
        return open(f, 'rt')

# Returns the name of file given its path or file handler
# can't do better to find species name with fasta...
def speciesFromPath(f):
    if isinstance(f, str):
        species = f
    else:
        species = f.name
    species = basename(species)
    species = species.replace(',', '_') # , is not safe, will be used as separator later
    return species

# Return the start of locus, to be used with start-based order (instead of left-based)
def locusStartBased(gene):
    if gene[2] == -1: # - strand
        return (gene[1], gene[0])
    else:
        return (gene[0], gene[1])

def removeDupl(marker_order, verbose=False):
    res = [marker_order[0]]
    for i in range(1, len(marker_order)):
        # check if disjoint
        if res[-1][1] < marker_order[i][0]:
            res.append(marker_order[i])
        # check if not contained
        elif res[-1][1] < marker_order[i][1]:
            # if both have same start pos, then, because marker_order is sorted,
            # marker_order[i] must be the longer gene and hence will replace
            # res[-1]
            if res[-1][0] == marker_order[i][0]:
                res[-1] = marker_order[i]
            else:
                if res[-1][2] == marker_order[i][2] and verbose:
                    LOG.warning(('Markers %s and %s are overlapping on the ' + \
                            'same strand.') %(res[-1][3].id,
                                marker_order[i][3].id))
                res.append(marker_order[i])

        # skip (i.e. do not append) otherwise
    return res


def extractAllAnnotationsFromGBK(record, onlyLongest, onlyAnnot, doTranslate, verbose=False):
    parents = dict()

    res = list()
    chrx = record.name
    for f in record.features:

        if onlyAnnot and f.type.upper() not in onlyAnnot:
            continue

        locus = 'locus_tag' in f.qualifiers and f.qualifiers['locus_tag'][0]
        if 'locus_tag' not in f.qualifiers:
            if 'gene' in f.qualifiers:
                locus = f.qualifiers['gene'][0]
            else:
                locus = f.qualifiers['name'][0]
            if verbose:
                LOG.warning(('Unknown locus tag for feature %s. Using gene ID ' + \
                        'or name as locus tag instead.') %f)

        if onlyLongest and (locus in parents) and parents[locus][1] >= len(f):
            continue
        if locus in parents:
            parents[locus][1] = len(f)
        else:
            parents[locus] = [len(res), len(f)]

        seq = None
        annotations = None
        if doTranslate:
            if 'translation' in f.qualifiers:
                seq = f.qualifiers['translation'][0]
                if generic_protein:
                    seq = Seq(seq.replace('\n', '').replace('\r', '').replace('\t', '').replace(' ', ''), generic_protein)
                else: # newer BioPython, >= 1.78
                    seq = Seq(seq.replace('\n', '').replace('\r', '').replace('\t', '').replace(' ', ''))
                    annotations = {"molecule_type":"protein"}
            else:
                seq = f.extract(record.seq).translate()
                if seq[:-1].count('*') and verbose:
                    LOG.error(('Translation failed for some codons, ' + \
                            'for gene in locus %s resulting in some amino ' + \
                            'acid being unknown (marked as *): %s') %(locus,
                                seq))
        else:
            seq = f.extract(record.seq)

        # sort out protein identifier
        protId = None
        gi = None
        geneID = None
        if 'protein_id' in f.qualifiers:
            protId = f.qualifiers['protein_id'][0]
        elif 'db_xref' in f.qualifiers:
            gis = [x[3:] for x in f.qualifiers['db_xref'] if x.startswith('GI')]
            gi = gis and gis[0]
            if not gi:
                ged = [x[7:] for x in f.qualifiers['db_xref'] if
                        x.startswith('GeneID')]
                geneID = ged and ged[0]

        seq_id = '|'.join(filter(None, (gi and 'gi|%s' %gi, geneID and ('ge|%s'
            %geneID), protId and 'gb|%s' %protId, locus and 'locus|%s' %locus,
            '%s:%s' %(int(f.location.start), int(f.location.end)), chrx
            and 'chromosome|%s' %chrx, 'strand|%s' %(f.location.strand == 1 and '+' or
                '-'))))
        seq_id = seq_id.replace(' ', '_') # can't allow whitespaces here
        rec = SeqRecord(seq, id=seq_id, description='', annotations=annotations)
        if parents[locus][0] < len(res):
            res[parents[locus][0]] = (int(f.location.start),
                    int(f.location.end), f.location.strand, rec)
        else:
            # we can safely assume that len(res) == parents[locus][0]
            res.append((int(f.location.start), int(f.location.end),
                f.location.strand, rec))

    if onlyLongest and len(res) > 0:
        res.sort(key = lambda x: x[:2]) # first sort left-based to remove shortest duplicates
        res = removeDupl(res, verbose)
    res.sort(key = locusStartBased) # then sort start-based
    
    LOG.info('Processed record %s.' % chrx)
    return map(lambda x: x[3], res)


def extractAnnotationsFromGFF(gffData, recids, recs, onlyLongest, onlyAnnot):
    res = dict((recid, list()) for recid in recids)
    parents = dict()
    for line in csv.reader(gffData, delimiter='\t'):
        if not line or line[0].startswith('#') or (onlyAnnot and \
                line[2].upper() not in onlyAnnot):
            continue

        recid = line[0]
        if recid not in recs:
            LOG.error('No fasta record with ID %s found, skipping.' %recid)
            continue


        start = int(line[3])
        end = int(line[4])
        name = None
        m = PAT_ID.match(line[8])
        if m:
            name = m.group(2).replace(' ', '.')
        else:
            name = '%s_%s_%s' %(recid, start, end)
        strand = line[6]

        rec = recs[recid][start-1:end]
        rec.id='%s|%s:%s|chromosome|%s|strand|%s'%(name, start, end,
                recs[recid].id, strand)
        rec.description=''

        if onlyLongest:
            p = PAT_LONGEST.match(line[8])
            if p and p.group(2) != '1':
                continue

            p = PAT_PARENT.match(line[8])
            if p:
                parent = p.group(2)
            else:
                parent = name

            if parent in parents:
                if len(res[recid][parents[parent]][2]) >= end-start+1:
                    continue
                else:
                    res[recid][parents[parent]] = (start, end, strand, rec)
            else:
                parents[parent] = len(res[recid])
                res[recid].append((start, end, strand, rec))
        else:
            res[recid].append((start, end, strand, rec))

    return res


def addOptions(parser):

    parser.add_option('-a', '--only_annotations', dest='onlyAnnot', \
            help='Output only annotations of certain type. Multiple ' + \
            'types are separated by comma. [default: %default]', type=str,
            metavar='ANNOTATION TYPE1,ANNOTATION TYPE2,...', default='CDS')
    parser.add_option('-l', '--only_longest', dest='longest',
            help='Only report the longest annotation of one locus',
            action='store_true', default=True)
    parser.add_option('-A', '--all_isoforms', dest='longest',
            help='Report all isoforms instead of only the longest',
            action='store_false', default=True)
    parser.add_option('-t', '--translate_to_aa', dest='doTranslate',
            help='Translate into protein sequence', action='store_true',
            default=False)
    parser.add_option('--ignore-unknown-chr', dest='ignore_unknown_chr',
            help='Ignore features located in unknown chromosomes',
            action='store_true', default=False)
    parser.add_option('--ignore-organelles', dest='ignore_organelles',
            help='Ignore features located in organelles',
            action='store_true', default=False)
    parser.add_option('--ignore-scaffolds', dest='ignore_scaffolds',
            help='Ignore features located in genomic scaffolds ' + \
                    '(GBK only), i.e., keep only chromosome-level assembly',
            action='store_true', default=False)
    parser.add_option('--ignore-patches', dest='ignore_patches',
            help='Ignore features located in genomic patches',
            action='store_true', default=False)
    parser.add_option('--ignore-alternate', dest='ignore_alternate',
            help='Ignore features located in alternate scaffolds',
            action='store_true', default=False)
    parser.add_option('--verbose', dest='verbose',
            help='Print extra information',
            action='store_true', default=False)


def main(options, args, out):

    onlyAnnot = list()
    if options.onlyAnnot:
        onlyAnnot = list(map(lambda x: x.upper(),
            options.onlyAnnot.split(',')))

    if len(args) not in [1,2]:
        LOG.fatal('Improper number of arguments. Exiting')
        exit(1)


    if len(args) == 2:
        #
        # read fasta records
        #
        recids = list()
        recs = dict()
        for rec in SeqIO.parse(openCompressed(args[0]), 'fasta'):
            recid = rec.id
            # XXX nasty bugfix for CoGe data
            if recid.find('|') > -1:
                recid = recid.split('|', 2)[1]
                rec.id = rec.id.replace('|', '.')
            recids.append(recid)
            recs[recid] = rec

        #
        # parse GFF file
        #
        extrAnnots = extractAnnotationsFromGFF(openCompressed(args[1]), recids, recs,
                options.longest, onlyAnnot)

        for recid in recids:
            markers = extrAnnots[recid]
            if options.longest:
                markers.sort(key = lambda x: x[:2]) # first sort left-based to remove shortest duplicates
                markers = removeDupl(markers, options.verbose)
            markers.sort(key = locusStartBased) # then sort start-based

            for _, _, strand, rec in markers:
                if options.doTranslate:
                    if strand == '+':
                        rec = SeqRecord(rec.seq.translate(), id=rec.id,
                                description=rec.description)
                    else:
                        rec = SeqRecord(rec.seq.reverse_complement().translate(),
                                id=rec.id, description=rec.description)
                    if rec.seq.count('*') and options.verbose:
                        LOG.error(('Translation failed for some codons, ' + \
                                'in marker %s resulting in some amino acid ' + \
                                'being unknown (marked as *): %s') %(rec.id,
                                    rec.seq))
                if len(rec.seq):
                    SeqIO.write(rec, out, 'fasta')
        return speciesFromPath(args[0])
    else:
        seqRecords = SeqIO.parse(openCompressed(args[0]), 'genbank')
        species = ""
        lower = lambda x : [ s.lower() for s in x ]
        
        for record in seqRecords:
            if not species: # try to find the species name in some record
                if 'organism' in record.annotations and len(record.annotations['organism']) > 1:
                    species = record.annotations['organism']
            if options.ignore_unknown_chr and \
               'chromosome' in record.features[0].qualifiers and \
               'unknown' in lower(record.features[0].qualifiers['chromosome']):
                LOG.info('Ignoring (unknown chromosome) record %s' %record.description)
                continue
            if options.ignore_organelles and \
               'organelle' in record.features[0].qualifiers:
                LOG.info('Ignoring (organelle) record %s' %record.description)
                continue
            if options.ignore_scaffolds and \
               ('map' in record.features[0].qualifiers and \
                'unlocalized' in lower(record.features[0].qualifiers['map']) or \
                'genomic scaffold' in record.description.lower()): # the chromosome may be known or unknown
                LOG.info('Ignoring (unlocalized scaffold) record %s' %record.description)
                continue
            if options.ignore_patches and \
               'genomic patch' in record.description.lower():
                LOG.info('Ignoring (genomic patch) record %s' %record.description)
                continue
            if options.ignore_alternate and \
               'alternate' in record.description.lower():
                LOG.info('Ignoring (genomic patch) record %s' %record.description)
                continue

            LOG.info('Scanning record %s' %record.description)
            recs = extractAllAnnotationsFromGBK(record, options.longest,
                    onlyAnnot, options.doTranslate, options.verbose)
            SeqIO.write(recs, out, 'fasta')
        if not species:
            species = speciesFromPath(args[0])
        return species

if __name__ == '__main__':

    usage = 'usage: %prog [options] (<FASTA FILE> <GFF FILE> | <GBK FILE>)'
    parser = OptionParser(usage=usage)
    addOptions(parser)
    (options, args) = parser.parse_args()

    if len(args) not in [1, 2]:
        parser.print_help()
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.ERROR)
    ch.setFormatter(logging.Formatter('!! %(message)s'))

    cf = logging.FileHandler(LOG_FILENAME, mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))

    LOG.addHandler(cf)
    LOG.addHandler(ch)

    main(options, args, stdout)
