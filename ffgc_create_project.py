#!/usr/bin/env python3
from collections import OrderedDict
from os.path import basename, dirname, isdir, isfile, join, abspath, realpath, relpath, splitext
from sys import stdout, stderr, exit, argv
from re import compile
from multiprocessing import cpu_count
from itertools import combinations, repeat
from functools import reduce
from optparse import OptionParser
from os import mkdir, listdir
from Bio import SeqIO
from configparser import ConfigParser
import logging

try:
    from ruamel.yaml import YAML # replaces yaml, supports round trip preservation of comments
    RUAMEL_YAML_AVAILABLE = True
except ImportError or ModuleNotFoundError:
    # ruamel.yaml not available...
    from yaml import load, dump, BaseLoader, dumper
    RUAMEL_YAML_AVAILABLE = False

from extract_annotated_sequences import main as extractAnnotatedSequences, LOG as aLOG
from extract_annotated_sequences import COMPRESS_HANDLERS, openCompressed, speciesFromPath
from pairwise_similarities import PAT_CHR

YAML_CONFIG = 'config.yaml'
DNA_ALPHABET = 'ACGTNX'

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]

FASTA_EXT = ['.fasta', '.fas', '.fa', '.faa', '.fna', '.gos']
GBK_EXT = ['.genbank', '.gbk', '.gbff']

CFG_FASTA_KEY = 'fasta_file'
CFG_CHR_KEY = 'chromosomes'
CFG_GENES_KEY = 'genes'
CFG_SP_KEY = 'species'

PATTERN_CHR = compile(r'.*\|chromosome\|([^\|]+)(?:$|\|)')


def readGenomeGos(fname):
    """Reads one genome in .gos format."""
    genes = list()
    chromosomes = OrderedDict()

    with open(fname, 'r') as f:
        for line in f:
            if not line.startswith('>'):
                continue
            gid = line[1:].strip()  # remove > to obtain the gene ID

            chromosome = PATTERN_CHR.match(gid)
            if not chromosome:
                raise Exception('Invalid format while parsing gene record, ' + \
                                'chromosome not found in "%s"' % gid)
            chromosome = chromosome.groups()[0]
            chromosomes[chromosome] = True
            genes.append(gid)

    return list(chromosomes.keys()), genes


def buildAndWriteGenomesCfg(cfgfile, fastaFiles, genomes, species):
    data = dict()
    for f, gName, spName in zip(fastaFiles, genomes, species):
        data[gName] = dict()
        data[gName][CFG_FASTA_KEY] = relpath(f, dirname(cfgfile))
        data[gName][CFG_CHR_KEY], data[gName][CFG_GENES_KEY] = readGenomeGos(f)
        LOG.info('Genome %s (%s): %d genes in %d chromosomes/scaffolds/contigs.' %
                 (gName, spName, len(data[gName][CFG_GENES_KEY]), len(data[gName][CFG_CHR_KEY])))

    config = ConfigParser()
    for Gx, Sx in zip(genomes, species):
        config.add_section(Gx)
        config.set(Gx, CFG_SP_KEY, Sx)
        for k, v in data[Gx].items():
            if hasattr(v, '__iter__') and not isinstance(v, str):
                v = ','.join(map(str, v))
            config.set(Gx, k, v)
    with open(cfgfile, 'w') as f:
        config.write(f)


def main():
    usage = 'usage: %prog [options] <PROJECT DIR> (<FASTA FILE 1> ... ' + \
            '<FASTA FILE N> | <GBK FILE 1> ... <GBK FILE N)'
    epilog = 'Accepts compressed FASTA, GBK and GFF files (gz, bz2, xz).'
    parser = OptionParser(usage=usage, epilog=epilog)
    parser.add_option('-f', '--file_type', dest='fileType',
            help='Select whether file is fasta of genbank format ',
            metavar='(GBK|FASTA)')
    parser.add_option('-a', '--only_annotations', dest='onlyAnnot', \
            help='Output only annotations of certain type. Multiple ' + \
            'types are separated by comma. [default: %default]', type=str,
            metavar='ANNOTATION TYPE1,ANNOTATION TYPE2,...', default='CDS')
    parser.add_option('-l', '--only_longest', dest='longest',
            help='Only report the longest annotation of one locus (default)',
            action='store_true', default=True)
    parser.add_option('-A', '--all_isoforms', dest='longest',
            help='Report all isoforms instead of only the longest',
            action='store_false', default=True)
    parser.add_option('-p', '--use_protein_sequence', dest='isProteinSeq',
            help='Perform analysis on protein sequence level; This ' + \
                    'means that the input data is translated into ' + \
                    'protein sequences if necessary', action='store_true',
                    default=False)
    parser.add_option('--ignore-unknown-chr', dest='ignore_unknown_chr',
            help='Ignore features in unknown chromosomes (GBK only)',
            action='store_true', default=False)
    parser.add_option('--ignore-organelles', dest='ignore_organelles',
            help='Ignore features located in organelles (GBK only)',
            action='store_true', default=False)
    parser.add_option('--ignore-scaffolds', dest='ignore_scaffolds',
            help='Ignore features located in genomic scaffolds and patches ' + \
                    '(GBK only), i.e., keep only chromosome-level assembly',
            action='store_true', default=False)
    parser.add_option('--ignore-patches', dest='ignore_patches',
            help='Ignore features located in genomic patches',
            action='store_true', default=False)
    parser.add_option('--ignore-alternate', dest='ignore_alternate',
            help='Ignore features located in alternate scaffolds',
            action='store_true', default=False)
    parser.add_option('--verbose', dest='verbose',
            help='Print extra information',
            action='store_true', default=False)
    (options, args) = parser.parse_args()
    setattr(options, 'doTranslate', False)

    if len(args) < 3:
        parser.print_help()
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.WARNING)
    ch.setFormatter(logging.Formatter('!! %(message)s'))
    aLOG.addHandler(ch)
    LOG.addHandler(ch)

    fileType = options.fileType
    fileName, fileExt = splitext(args[1])
    if fileExt in COMPRESS_HANDLERS.keys():
        fileExt = splitext(fileName)[-1]
    if not fileType and fileExt in FASTA_EXT:
        fileType = 'FASTA'
    elif not fileType and fileExt in GBK_EXT:
        fileType = 'GBK'

    if fileType not in ['FASTA', 'GBK']:
        LOG.fatal('Unknown filetype of input files %s. Exiting' %(' '.join(args[1:])))
        exit(1)

    src_dir = dirname(realpath(argv[0]))

    if isdir(args[0]) and listdir(args[0]):
        LOG.fatal('Non-empty project directory %s already exists. Exiting' %args[0])
        exit(1)

    if not isdir(args[0]):
        mkdir(args[0])

    cf = logging.FileHandler(join(args[0], LOG_FILENAME), mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    aLOG.addHandler(cf)
    LOG.addHandler(cf)

    if RUAMEL_YAML_AVAILABLE:
        # Using default RoundTripLoader/RoundTripDumper (also used with no typ parameter given)
        yamlLoaderDumper = YAML(typ='rt', pure=True)
        yamlConfig = yamlLoaderDumper.load(open(join(src_dir, YAML_CONFIG)))
    else:
        yamlConfig = load(open(join(src_dir, YAML_CONFIG)), Loader=BaseLoader)
        LOG.warning('ruamel.yaml not installed, yaml config files will have no comments.')

    data_dir = yamlConfig['genome_data_dir']

    mkdir(join(args[0], data_dir))

    gNames = list()
    spNames = list()
    gosFiles = list()  # the FASTA files we'll build

    isProcessedGeneOrderSeq = False

    if fileType == 'FASTA':
        it = SeqIO.parse(openCompressed(args[1]), 'fasta')
        rec = next(it)
        isProcessedGeneOrderSeq = not not PAT_CHR.match(rec.id)
        if isProcessedGeneOrderSeq and not options.isProteinSeq:
            #__transtable__ = str.maketrans('A', 'A') # old Python
            __transtable__ = str.maketrans('A', 'A', DNA_ALPHABET.upper() + DNA_ALPHABET.lower())
            while rec:
                #if str(rec.seq).translate(__transtable__, DNA_ALPHABET.upper() + DNA_ALPHABET.lower()): # old Python
                if str(rec.seq).translate(__transtable__):
                    options.isProteinSeq = True
                    LOG.warning(('FASTA input files contain characters ' + \
                            'other than %s. Assuming protein coding ' + \
                            'sequence.') %DNA_ALPHABET)
                    break
                rec = next(it, None)

    if not isProcessedGeneOrderSeq and options.isProteinSeq:
        setattr(options, 'doTranslate', True)

    for i in range(1, len(args)):
        myargs = None
        gName = 'G%s' %i
        gosFile = join(args[0], data_dir, '%s.gos' %gName)
        gosFiles.append(gosFile)
        LOG.info('Process %s into %s' %(args[i], gosFile))
        if not isProcessedGeneOrderSeq:
            if fileType == 'FASTA':
                fileName, fileExt = splitext(args[i])
                if fileExt in COMPRESS_HANDLERS.keys():
                    fileName,fileExt = splitext(fileName)
                gff_fnames = [ '%s.gff' % fileName, '%s.gff' % (fileName+fileExt) ]
                gff_fnames += [ f + e for f in gff_fnames for e in COMPRESS_HANDLERS.keys() ]
                gff = [ fname for fname in gff_fnames if isfile(fname) ]
                if not gff:
                    LOG.fatal(('Unable to locate GFF file corresponding ' + \
                            'to fasta file %s. Expected to locate file ' + \
                            '%s.gff or %s.gff (or those files compressed).  Exiting') \
                            %(args[1], fileName+fileExt, fileName))
                    exit(1)
                
                gff = gff[0]
                myargs = [args[i], gff]
            else:
                myargs = [args[i]]
            out = open(gosFile, 'w')
            spName = extractAnnotatedSequences(options, myargs, out)
            out.close()
        else:
            spName = speciesFromPath(args[i])
            out = open(gosFile, 'w')
            out.write(openCompressed(args[i]).read())
            out.close()
        LOG.info('%s renamed to %s' % (spName, gName))
        gNames.append(gName)
        spNames.append(spName)

    buildAndWriteGenomesCfg(join(args[0], data_dir, 'genomes.cfg'), gosFiles, gNames, spNames)

    if options.isProteinSeq:
        yamlConfig['align_db_type'] = 'prot'
        yamlConfig['blast_cmd'] = 'blastp'
        yamlConfig['blast_db_endings'] = ['phd', 'phi', 'phr', 'pin', 'pog', 'psd', 'psi', 'psq']
    else:
        yamlConfig['align_db_type'] = 'nucl'
        yamlConfig['blast_cmd'] = 'blastn'
        yamlConfig['blast_params'] = '-task=blastn ' + yamlConfig['blast_params']
        yamlConfig['blast_db_endings'] = ['nhd', 'nhi', 'nhr', 'nin', 'nog', 'nsd', 'nsi', 'nsq']

    if not argv[0].endswith('.py'):
        yamlConfig['python3_bin'] = ''
        yamlConfig['pysuf'] = ''
        yamlConfig['blast_dir'] = src_dir + '/'

    yamlConfig['ff_bin'] = src_dir + '/'
    yamlConfig['genomes'] = gNames
    yamlConfig['species'] = spNames
    yamlConfig['pw_sims'] = list(map(lambda x: '_'.join(x),
        combinations(gNames, 2)))
    yamlConfig['self_sims'] = [ "%s_%s" % (g,g) for g in gNames ]
    if len(gNames) > 2:
        yamlConfig['ffmedian_triples'] = list(map(lambda x: '_'.join(x),
                combinations(gNames, 3)))
        yamlConfig['ffadjaftermedian_triples'] = list(reduce(lambda a,b: a + b,
                list(map(lambda x: list(map(lambda z: '.sol.'.join(z), zip(repeat('_'.join(x),
                    3), list(map(lambda y: '_'.join(y), combinations(x, 2)))))),
                    combinations(gNames, 3)))))
    else:
        yamlConfig['ffmedian_triples'] = []
        yamlConfig['ffadjaftermedian_triples'] = []

    #yamlConfig['threads'] = cpu_count()
    yamlOut = open(join(args[0], YAML_CONFIG), 'w')
    if RUAMEL_YAML_AVAILABLE:
        yamlLoaderDumper.dump(yamlConfig, yamlOut)
    else:
        dump(yamlConfig, yamlOut, Dumper=dumper.SafeDumper)
    yamlOut.close()

    sout = open(join(args[0], 'Snakefile'), 'w')
    print('configfile: \'%s\'' %YAML_CONFIG, file = sout)
    print('include: \'%s/Snakefile\'\n' %src_dir, file = sout)
    # rule all must be in main file, snakemake doesn't import the default target rule from included files
    print('''
rule all:
    input:
        expand(PW_OUT + '/{pw_dist}{active}.sim', pw_dist=PW_SIMS, active=['', '.active'])
''', file = sout)
    sout.close()


if __name__ == '__main__':
    main()
