#!/usr/bin/env python3

import networkx as nx
from sys import path
from os.path import dirname, abspath

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import DIRECTION_CRICK_STRAND, \
        DIRECTION_WATSON_STRAND, GM_CHR_CIRCULAR, GM_CHR_LINEAR, \
        TELOMERE_START, TELOMERE_END

def __next_start_node_car__(M):

    for node in M.nodes():
        if M.degree(node) <= 1:
            return GM_CHR_LINEAR, node

    # break up circles
    if len(M):
        n1 = next(M.nodes_iter())
        n2 = [x for x in M.neighbors(n1) if x[1] != n1[1]][0]
        M.remove_edge(n1, n2)
        return GM_CHR_CIRCULAR, n1

    return None, None

def matching2Cars(matching, Gf2Genes):

    M = nx.Graph(data=matching)
    M.add_edges_from((('h', l), ('t', l)) for l in map(lambda x: x[1],
        M.nodes()))

    cars = list()

    chr_type, curNode = __next_start_node_car__(M)
    cars.append((chr_type, list()))

    while curNode:

        orient = curNode[0] == 't' and DIRECTION_CRICK_STRAND or \
                DIRECTION_CRICK_STRAND
        cars[-1][1].append((orient, sorted(Gf2Genes[curNode[1]])))

        midNode = M.neighbors(curNode)
        M.remove_node(curNode)
        neighs = M.neighbors(midNode[0])
        M.remove_node(midNode[0])

        if neighs:
            curNode = neighs[0]
        else:
            chr_type, curNode = __next_start_node_car__(M)
            cars.append((chr_type, list()))

    cars.pop()

    for chr_type, car in cars:
        if len(car) > 1 and car[0][1][1][1][1] > car[1][1][1][1][1]: #or car[-2][0] > car[-1][0]:
            car.reverse()

    cars.sort(key=lambda x: x[1][0][1][1][1])

    return cars


def writeCars(cars, genomes, out):
    carCount = 1
    for chr_type, car in cars:
        print('CAR %s, %s' %(carCount, chr_type), file = out)
        carCount += 1

        isFirst = True
        for orient, genes in car:
            genes = dict(genes)

            if orient == DIRECTION_WATSON_STRAND:
                out.write('-')
            elif orient == DIRECTION_CRICK_STRAND:
                out.write('+')

            for g1 in genomes:
                if g1 in genes:
                    chr1, g1i = genes[g1]
                    if g1i == TELOMERE_START or g1i == TELOMERE_END:
                        g1i = isFirst and 'TELOMERE_START' or 'TELOMERE_END'
                    out.write('\t%s:%s:%s'%(g1, chr1, g1i))
                else:
                    out.write('\t')
            out.write('\n')

            isFirst = False

