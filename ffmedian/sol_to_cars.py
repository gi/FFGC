#!/usr/bin/env python3

from sys import stdout, exit, argv
import xml.etree.ElementTree as et
import networkx as nx
import re
from functools import reduce

from ancestral_reconstruction import matching2Cars, writeCars


NODE_PATTERN = re.compile(r'^a_(\w+)_([^_]+)_([0-9]+)_(\w+)_([^_]+)_([0-9]+)_(\w+)_([^_]+)_([0-9]+)')
ADJ_PATTERN = re.compile(r'^b_(\w+)_([^_]+)_([0-9]+)_(\w+)_([^_]+)_([0-9]+)_(\w+)_([^_]+)_([0-9]+)_(h|t)_(\w+)_([^_]+)_([0-9]+)_(\w+)_([^_]+)_([0-9]+)_(\w+)_([^_]+)_([0-9]+)_(h|t)')


def readVariables(data):

    adjs = set()
    nodes = set()

    tree = et.parse(data)
    root = tree.getroot()
    for var in root.iterfind('./variables/variable'):
        name = var.get('name')
        value = int(round(float(var.get('value'))))
        m = ADJ_PATTERN.match(name)
        if m:
            (G1, chr1i, g1i, G2, chr2k, g2k, G3, chr3m, g3m, ext1, _, chr1j, g1j, _, chr2l, g2l, _, chr3n, g3n, ext2) = m.groups()
            g1i, g2k, g3m, g1j, g2l, g3n = map(int, (g1i, g2k, g3m, g1j, g2l, g3n))
            chr1i, chr2k, chr3m, chr1j, chr2l, chr3n = map(lambda x:
                    x.replace(',', '_'), (chr1i, chr2k, chr3m, chr1j, chr2l,
                        chr3n))

            G1 = G1.replace('abcdefghijklmnopqrs', '-')
            G2 = G2.replace('abcdefghijklmnopqrs', '-')
            G3 = G3.replace('abcdefghijklmnopqrs', '-')

            if value == 1:
                adjs.add((((G1, (chr1i, g1i)), (G2, (chr2k, g2k)), (G3, (chr3m,
                    g3m))), ext1, ((G1, (chr1j, g1j)), (G2, (chr2l, g2l)), (G3,
                        (chr3n, g3n))), ext2))
            continue

        m = NODE_PATTERN.match(name)
        if m:
            (G1, chr1, g1i, G2, chr2, g2k, G3, chr3, g3m) = m.groups()
            G1 = G1.replace('abcdefghijklmnopqrs', '-')
            G2 = G2.replace('abcdefghijklmnopqrs', '-')
            G3 = G3.replace('abcdefghijklmnopqrs', '-')
            chr1, chr2, chr3, = map(lambda x: x.replace(',', '_'), (chr1, chr2,
                chr3))

            if value == 1:
                nodes.add(((G1, (chr1, int(g1i))), (G2, (chr2, int(g2k))), (G3,
                    (chr3, int(g3m)))))

    return adjs, nodes


def check_output(adjs, nodes, G):
    #
    # check usage of forbidden nodes
    #
    for (m1, ext1, m2, ext2) in adjs:
        if m1 not in nodes:
            print(('!! ERROR, node %s is unsaturated' + \
                    '(corresp. variable set to zero), it occurs ' + \
                    'in adjacency (%s, %s, %s, %s).') %(str(m1), str(m1),
                            ext1, str(m2), ext2), file = stdout)
        if m2 not in nodes:
            print(('!! ERROR, node %s is unsaturated' + \
                    '(corresp. variable set to zero), it occurs ' + \
                    'in adjacency (%s, %s, %s, %s).') %(str(m2), str(m1),
                            ext1, str(m2), ext2), file = stdout)
    for node in G.nodes():
        if G.degree(node) > 1:
            print(('!! ERROR, node %s connected to more than' + \
                    'one other node in the constructed genome sequence.') \
                    %(str(node)), file = stdout)


if __name__ == '__main__':

    if len(argv) != 2:
        print('\tusage: %s <SOL FILE> ' %argv[0], file = stdout)
        exit(1)


    adjs, nodes = readVariables(open(argv[1]))

    matching = list()
    gf2genes = dict(zip(range(1, len(nodes)+1), nodes))
    genes2gf = dict((v, k) for k,v in gf2genes.items())

    for genes1, ext1, genes2, ext2 in adjs:
        matching.append(((ext1, genes2gf[genes1]), (ext2, genes2gf[genes2])))

    check_output(adjs, nodes, nx.Graph(data=matching))

    cars = matching2Cars(matching, gf2genes)
    genomes = sorted(set(reduce(lambda x,y: x+list(y), map(lambda x: map(lambda
        z: z[0], x), genes2gf.keys()), list())))

    writeCars(cars, genomes, stdout)

