#!/usr/bin/env python3

from sys import stdout, stderr, exit, argv, path
from itertools import product, chain, combinations
from optparse import OptionParser
from os.path import basename, dirname, abspath
import xml.etree.ElementTree as et
from math import sqrt
import logging
import csv
import re

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDistsAndOrder, \
        reverseDistMap, DIRECTION_CRICK_STRAND, DIRECTION_WATSON_STRAND, \
        TELOMERE_START, TELOMERE_END

VARIABLE_PATTERN = re.compile(r'^Variable ([^_]+) has value ([0-9.]+)$')
EDGE_PATTERN = re.compile(r'^a_([^_]+)_([^_]+)_([^_]+)_([^_]+)_([0-9]+)_([0-9]+)$')
NODE_PATTERN = re.compile(r'^b_([^_]+)_([^_]+)_([0-9]+)$')
ADJ_PATTERN = re.compile(r'^d_([^_]+)_([^_]+)_([^_]+)_([^_]+)_([0-9]+)_([0-9]+)_([0-9]+)_([0-9]+)$')
MEDIAN_NODE_PATTERN = re.compile(r'^a_([^_]+)_([^_]+)_([0-9]+)_([^_]+)_([^_]+)_([0-9]+)_([^_]+)_([^_]+)_([0-9]+)')

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]


def readVariables(data):

    res = dict()
    forbidden_edges = set()
    forbidden_nodes = set()
    adjs = set()
    nodes = set()

    tree = et.parse(data)
    root = tree.getroot()
    for var in root.iterfind('./variables/variable'):
        name = var.get('name')
        value = int(round(float(var.get('value'))))
        m = EDGE_PATTERN.match(name)
        if m:
            (G1, chr1, G2, chr2, g1i, g2k) = m.groups()
            g1i, g2k = map(int, (g1i, g2k))

            G1 = G1.replace('abcdefghijklmnopqrs', '-')
            G2 = G2.replace('abcdefghijklmnopqrs', '-')
            chr1 = chr1.replace(',', '_')
            chr2 = chr2.replace(',', '_')


            if (G1, G2) not in res:
                res[(G1, G2)] = dict()
            if value == 1:
                if (chr1, g1i) not in res[(G1, G2)]:
                    res[(G1, G2)][(chr1, g1i)] = dict()
                res[(G1, G2)][(chr1, g1i)][(chr2, g2k)] = ()
            else:
                forbidden_edges.add(((G1, chr1, g1i), (G2, chr2, g2k)))
            continue

        m = NODE_PATTERN.match(name)
        if m:
            (G1, chr1, g1i) = m.groups()
            G1 = G1.replace('abcdefghijklmnopqrs', '-')
            chr1 = chr1.replace(',', '_')

            if value:
                nodes.add((G1, chr1, int(g1i)))
            else:
                forbidden_nodes.add((G1, chr1, int(g1i)))
            continue

        m = MEDIAN_NODE_PATTERN.match(name)
        if m:
            (G1, chr1, g1i, G2, chr2, g2k, G3, chr3, g3m) = m.groups()
            G1 = G1.replace('abcdefghijklmnopqrs', '-')
            chr1 = chr1.replace(',', '_')
            G2 = G2.replace('abcdefghijklmnopqrs', '-')
            chr2 = chr2.replace(',', '_')
            G3 = G3.replace('abcdefghijklmnopqrs', '-')
            chr3 = chr3.replace(',', '_')
            g1i, g2k, g3m = map(int, (g1i, g2k, g3m))
            if value:
                nodes.add((G1, chr1, g1i))
                nodes.add((G2, chr2, g2k))
                nodes.add((G3, chr3, g3m))
                for Gx, Gy in combinations((G1, G2, G3), 2):
                    if (Gx, Gy) not in res:
                        res[(Gx, Gy)] = dict()
                if (chr1, g1i) not in res[(G1, G2)]:
                    res[(G1, G2)][(chr1, g1i)] = dict()
                res[(G1, G2)][(chr1, g1i)][(chr2, g2k)] = ()
                if (chr1, g1i) not in res[(G1, G3)]:
                    res[(G1, G3)][(chr1, g1i)] = dict()
                res[(G1, G3)][(chr1, g1i)][(chr3, g3m)] = ()
                if (chr2, g2k) not in res[(G2, G3)]:
                    res[(G2, G3)][(chr2, g2k)] = dict()
                res[(G2, G3)][(chr2, g2k)][(chr3, g3m)] = ()
            else:
                forbidden_nodes.add((G1, chr1, g1i))
                forbidden_nodes.add((G2, chr2, g2k))
                forbidden_nodes.add((G3, chr3, g3m))

                forbidden_edges.add(((G1, chr1, g1i), (G2, chr2, g2k)))
                forbidden_edges.add(((G1, chr1, g1i), (G3, chr3, g3m)))
                forbidden_edges.add(((G2, chr2, g2k), (G3, chr3, g3m)))
            continue

        m = ADJ_PATTERN.match(name)
        if m and value:
            (G1, chr1, G2, chr2, g1i, g2k, g1j, g2l) = m.groups()
            g1i, g2k, g1j, g2l = map(int, (g1i, g2k, g1j, g2l))
            # g1j can never be start telomere, because it always comes after g1i

            G1 = G1.replace('abcdefghijklmnopqrs', '-')
            G2 = G2.replace('abcdefghijklmnopqrs', '-')

            chr1 = chr1.replace(',', '_')
            chr2 = chr2.replace(',', '_')

            adjs.add((((G1, chr1, g1i), (G2, chr2, g2k)), ((G1, chr1, g1j),
                (G2, chr2, g2l))))

    return res, forbidden_edges, forbidden_nodes, adjs, nodes


def check_output(dists, revDists, forbidden_edges, forbidden_nodes, adjs, nodes):
    #
    # check usage of forbidden nodes
    #
    for node in forbidden_nodes:
        for gs, pwDists in dists.items():
            if (gs[0] == node[0] and (node[1], node[2]) in pwDists) or \
                    (gs[1] == node[0] and (node[1], node[2]) in
                            revDists[(gs[1], gs[0])]):
                LOG.error(('Node %s is unsaturated (corresp. variable ' + \
                        'set to zero), it occurs in a matched edge.') %str(node))
    #
    # check adjacency constraints and usage of forbidden edges
    #

    # check for unused nodes
    used_nodes = set()
    # construct gene orders
    geneorders = dict()
    for (G1, _), pwDists in dists.items():
        for g1i in pwDists.keys():
            if G1 not in geneorders:
                geneorders[G1] = set()
            geneorders[G1].add(g1i)
            used_nodes.add((G1, g1i[0], g1i[1]))

    for (G1, _), pwDists in revDists.items():
        for g1i in pwDists.keys():
            if G1 not in geneorders:
                geneorders[G1] = set()
            geneorders[G1].add(g1i)
            used_nodes.add((G1, g1i[0], g1i[1]))

    # sort & construct index mapping
    genepos = dict()
    for gs in dists.keys():
        genepos[gs] = dict()
        g1 = sorted(dists[gs].keys())
        g2 = sorted(set(chain(*dists[gs].values())))
        genepos[gs][gs[0]] = dict((g1[i], i) for i in range(len(g1)))
        genepos[gs][gs[1]] = dict((g2[i], i) for i in range(len(g2)))

    unused_nodes = nodes.difference(used_nodes)
    if unused_nodes:
        LOG.warning(('There are %s activated nodes in the ' + \
                'matching that are not incident to a matched edge.') %len(unused_nodes))

    # it looks crazy but it's not - G1's and G2's etc are identical
    for (((G1, chr1, g1i), (G2, chr2, g2k)), ((G1, chr1, g1j), (G2, chr2,
        g2l))) in adjs:
        if ((G1, chr1, g1i), (G2, chr2, g2k)) in forbidden_edges:
            LOG.error(('Edge %s is unsaturated (corresp. variable set ' + \
                    'to zero), but contained in adjacency with edge %s') %(str(((G1, chr1,
                        g1i), (G2, chr2, g2k))), str(((g1, chr1, g1j), (g2,
                            chr2, g2l)))))
        if ((G1, chr1, g1j), (G2, chr2, g2l)) in forbidden_edges:
            LOG.error(('Edge %s is unsaturated (corresp. variable set ' + \
                    'to zero), but contained in adjacency with edge %s') %(str(((G1,
                        chr1, g1j), (G2, chr2, g2l))), str(((G1, chr1, g1i),
                            (G2, chr2, g2k)))))

        if (chr1, g1i) not in dists[(G1, G2)] or (chr2, g2k) not in dists[(G1,
            G2)][(chr1, g1i)]:
            LOG.error(('Edge %s is unsaturated (not contained in ' + \
                    'matching graph), but contained in adjacency with edge %s') %(str(((G1,
                        chr1, g1i), (G2, chr2, g2k))), str(((G1, chr1, g1j),
                            (G2, chr2, g2l)))))

        if (chr1, g1j) not in dists[(G1, G2)] or (chr2, g2l) not in dists[(G1,
            G2)][(chr1, g1j)]:
            LOG.error(('Edge %s is unsaturated (not contained in ' + \
                    'matching graph), but contained in adjacency with edge %s') %(str(((G1,
                        chr1, g1j), (G2, chr2, g2l))), str(((G1, chr1, g1i),
                            (G2, chr2, g2k)))))

        pwPos = genepos[(G1, G2)]
        if pwPos[G1][(chr1, g1i)] + 1 != pwPos[G1][(chr1, g1j)]:
            LOG.error(('%s and %s are part of an adjacency, ' + \
                    'but saturated genes are in between') %(str((G1,
                        chr1, g1i)), str((G1, chr1, g1j))))

        if g2k < g2l and pwPos[G2][(chr2, g2k)] + 1 != pwPos[G2][(chr2, g2l)]:
            LOG.error(('%s and %s are part of an adjacency, ' + \
                    'but saturated genes are in between') %(str((G2,
                        chr2, g2k)), str((G2, chr2, g2l))))

        if g2k > g2l and pwPos[G2][(chr2, g2k)] - 1 != pwPos[G2][(chr2, g2l)]:
            LOG.error(('%s and %s are part of an adjacency, ' + \
                    'but saturated genes are in between') %(str((G2,
                        chr2, g2l)), str((G2, chr2, g2k))))

def computeObjective(adjs, mDists, dists, alpha):
    res = 0
    for ((G0, chr1, g1i), (G1, chr2, g2k)), ((_, _, g1j), (_, _, g2l)) in adjs:
        we = dists[(G0, G1)][(chr1, g1i)][(chr2, g2k)][1]
        wf = dists[(G0, G1)][(chr1, g1j)][(chr2, g2l)][1]
        res += alpha*sqrt(we*wf)

    for gs, edges in mDists.items():
        for g1i, val in edges.items():
            res += (1-alpha) * dists[gs][g1i][val.keys()[0]][1]

    return res

if __name__ == '__main__':

    usage = 'usage: %prog [options] <SOL FILE> <PW DIST FILE 1> ... <PW DIST FILE N>'
    parser = OptionParser(usage=usage)
    parser.add_option('-a', '--anchor_format', dest='doAnchor', default=False,
            action='store_true', help='Output matching in anchor format to' + \
                    ' stdout [default=%default]')

    (options, args) = parser.parse_args()
    if len(args) < 2:
        parser.print_help()
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.ERROR)
    ch.setFormatter(logging.Formatter('!! %(message)s'))
    cf = logging.FileHandler(LOG_FILENAME, mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(cf)
    LOG.addHandler(ch)

    dists = dict()
    for f in args[1:]:
        g1name, g2name = basename(f).rsplit('.', 2)[0].split('_')
        _, _, _, pwDists= readDistsAndOrder(open(f), 0)
        dists[(g1name, g2name)] = pwDists

    mDists, forbidden_edges, forbidden_nodes, adjs, nodes = readVariables(open(args[0]))
    revMDists = dict(((y, x), reverseDistMap(d)) for ((x,y), d) in mDists.items())

#    LOG.info(('Objective value: %s' %computeObjective(adjs, mDists, dists,
#        0.9)))
    check_output(mDists, revMDists, forbidden_edges, forbidden_nodes, adjs, nodes)

    for gs, pwDists in mDists.items():
        if gs not in dists:
            LOG.fatal(('Oops, distance file for genome pair %s not supplied in ' + \
                    'input. Exiting!') %'-'.join(gs))
            exit(1)
        if not options.doAnchor:
            out = open('%s.%s_%s' %(args[0], gs[0], gs[1]), 'w')
        else:
            out = stdout
        for g1i in sorted(pwDists.keys()):
            for g2k in sorted(pwDists[g1i].keys()):
                direction, weight = dists[gs][g1i][g2k]
                if direction == DIRECTION_CRICK_STRAND and direction == DIRECTION_WATSON_STRAND:
                    if (g1i[1] == TELOMERE_START and g2k[1] == TELOMERE_START) \
                            or (g1i[1] == TELOMERE_END and g2k[1] == TELOMERE_END):
                        direction = 1
                    elif (g1i[1] == TELOMERE_START and g2k[1] == TELOMERE_END) or \
                            (g1i[1] == TELOMERE_END and g2k[1] == TELOMERE_START):
                        direction = -1
                    else:
                        direction = 0
                elif direction == DIRECTION_CRICK_STRAND:
                    direction = 1
                else:
                    direction = -1

                i = g1i[1]
                k = g2k[1]

                if i == TELOMERE_START:
                    i = 'TELOMERE_START'
                if i == TELOMERE_END:
                    i = 'TELOMERE_END'
                if k == TELOMERE_START:
                    k = 'TELOMERE_START'
                if k == TELOMERE_END:
                    k = 'TELOMERE_END'
                if not options.doAnchor:
                    print('\t'.join(map(str, (g1i[0], i, g2k[0], k,
                        direction, weight))), file = out)
                else:
                    print('%s\t%s\t%s\t%s\t%s\t%s' %(gs[0], g1i[0], i,
                            gs[1], g2k[0], k), file = out)
        if not options.doAnchor:
            out.close()

