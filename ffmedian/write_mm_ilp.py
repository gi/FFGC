#!/usr/bin/env python3

from sys import stdout, stderr, exit, path
from os.path import basename, isfile, dirname, abspath
from optparse import OptionParser
from itertools import chain, product, combinations
from collections import deque, OrderedDict
from bisect import bisect, bisect_left
from math import sqrt
import logging
import csv

#
# enable import from FFGC
#

FFGC_SRC = '/opt/biotools/ffgc_v0.1/src'
import sys; sys.path.insert(1, FFGC_SRC)

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDistsAndOrder, reverseDistMap

LOG = logging.getLogger(__name__)
LOG_FILENAME = 'info.log'

def readAnchors(data, subgenomes=None):
    res = dict()

    for row in csv.reader(data, delimiter='\t'):
        if len(row) == 4:
            if subgenomes and (row[0] not in subgenomes or row[2] not in
                    subgenomes):
                continue
            key = tuple(sorted((row[0], row[2])))
            if row[0] == key[0]:
                val = (row[1] and (0, row[1] != '-1' and int(row[1]) or 0) or
                        None, row[3] and (0, row[3] != '-1' and int(row[3]) or
                            0) or None)
            else:
                val = (row[3] and (0, row[3] != '-1' and int(row[3]) or 0) or
                        None, row[1] and (0, row[1] != '-1' and int(row[1]) or
                            0) or None)
        elif len(row) == 6:
            if subgenomes and (row[0] not in subgenomes or row[3] not in
                    subgenomes):
                continue
            key = tuple(sorted((row[0], row[3])))
            if row[0] == key[0]:
                val = (row[1] and (int(row[1]), row[2] != '-1' and int(row[2])
                    or 0) or None, row[4] and (int(row[4]), row[5] != '-1' and
                        int(row[5]) or 0) or None)
            else:
                val = (row[4] and (int(row[4]), row[5] != '-1' and int(row[5])
                    or 0) or None, row[1] and (int(row[1]), row[2] != '-1' and
                        int(row[2]) or 0) or None)
        else:
            print('Oops, row %s in anchor file is of illegal length. Exiting.'
                    %str(row), file = stderr)
            exit(1)

        if key not in res:
            res[key] = set()
        res[key].add(val)

    for gs, adjs in res.items():
        res[gs] = sorted(adjs)
    return res

def writeObjectiveFunction(dists, out):
    out.write('max ')

    isFirst = True

    for gs, pwDists in dists.items():
        for g1i, g2genes in pwDists.items():
            for g2k, (_, w) in g2genes.items():
                if isFirst:
                    isFirst = None
                else:
                    out.write(' + ')

                out.write('%sa_%s_%s_%s_%s_%s_%s' %(w, gs[0], g1i[0], gs[1],
                    g2k[0], g1i[1], g2k[1]))

    out.write('\n')

def writePartialMatchingConstraints(dists, revDists, out):

    for gs1, gs2 in combinations(dists.keys(), 2):

        if gs1[0] == gs2[0]:
            gx1, gx2, gx3 = (gs1[0], gs1[1], gs2[1])
        elif gs1[0] == gs2[1]:
            gx1, gx2, gx3 = (gs1[0], gs1[1], gs2[0])
        elif gs1[1] == gs2[0]:
            gx1, gx2, gx3 = (gs1[1], gs1[0], gs2[1])
        elif gs1[1] == gs2[1]:
            gx1, gx2, gx3 = (gs1[1], gs1[0], gs2[0])

        dists12 = dists.get((gx1, gx2), revDists.get((gx1, gx2), None))
        dists13 = dists.get((gx1, gx3), revDists.get((gx1, gx3), None))
        dists23 = dists.get((gx2, gx3), revDists.get((gx2, gx3), None))
        dists32 = dists.get((gx3, gx2), revDists.get((gx3, gx2), None))

        for g1i, g2dists in dists12.items():
            if g1i not in dists13:
                continue
            for g2k, g3z in product(g2dists.keys(), dists13[g1i].keys()):
                if g2k not in dists23:
                    continue

                condition1213 = ''
                if gx1 < gx2:
                    condition1213 = 'a_%s_%s_%s_%s_%s_%s + ' %(gx1, g1i[0], gx2,
                            g2k[0], g1i[1], g2k[1])
                else:
                    condition1213 = 'a_%s_%s_%s_%s_%s_%s + ' %(gx2, g2k[0], gx1,
                            g1i[0], g2k[1], g1i[1])
                if gx1 < gx3:
                    condition1213 += 'a_%s_%s_%s_%s_%s_%s <= 2\n' %(gx1, g1i[0],
                            gx3, g3z[0], g1i[1], g3z[1])
                else:
                    condition1213 += 'a_%s_%s_%s_%s_%s_%s <= 2\n' %(gx3, g3z[0],
                            gx1, g1i[0], g3z[1], g1i[1])

                if g2k in dists23:
                    for g3y in dists23[g2k]:
                        if g3y != g3z:
                            if gx2 < gx3:
                                out.write('a_%s_%s_%s_%s_%s_%s + ' %(gx2,
                                    g2k[0], gx3, g3y[0], g2k[1], g3y[1]))
                            else:
                                out.write('a_%s_%s_%s_%s_%s_%s + ' %(gx3,
                                    g3y[0], gx2, g2k[0], g3y[1], g2k[1]))
                    out.write(condition1213)

                if g3z in dists32:
                    for g2l in dists32[g3z]:
                        if g2l != g2k:
                            if gx2 < gx3:
                                out.write('a_%s_%s_%s_%s_%s_%s + ' %(gx2, g2l[0], gx3,
                                    g3z[0], g2l[1], g3z[1]))
                            else:
                                out.write('a_%s_%s_%s_%s_%s_%s + ' %(gx3, g3z[0], gx2,
                                    g2l[0], g3z[1], g2l[1]))
                    out.write(condition1213)

def writeComplete3MatchingConstraints(geneorders, dists, revDists, out):

    genomes = sorted(geneorders.keys())

    #
    # enforce that each active node must have at least two active incident
    # edges
    #
    for gx1, gx1s in geneorders.items():
        gx2, gx3 = set(genomes).difference((gx1,))

        dists12 = dists.get((gx1, gx2), revDists.get((gx1, gx2), None))
        dists13 = dists.get((gx1, gx3), revDists.get((gx1, gx3), None))
        dists23 = dists.get((gx2, gx3), revDists.get((gx2, gx3), None))

        for g1i in gx1s:
            if g1i not in dists13 or g1i not in dists12:
                print('b_%s_%s_%s = 0' %(gx1, g1i[0], g1i[1]), file = out)
                continue

            g2ks = set(dists12[g1i].keys())
            g3ms = set(dists13[g1i].keys())

            if gx1 < gx2:
                min2cons_out = ' + '.join('a_%s_%s_%s_%s_%s_%s' %(gx1, g1i[0],
                    gx2, g2k[0], g1i[1], g2k[1]) for g2k in g2ks)
            else:
                min2cons_out = ' + '.join('a_%s_%s_%s_%s_%s_%s' %(gx2, g2k[0],
                    gx1, g1i[0], g2k[1], g1i[1]) for g2k in g2ks)

            if gx1 < gx3:
                min2cons_out = ' + '.join([min2cons_out] +
                        ['a_%s_%s_%s_%s_%s_%s' %(gx1, g1i[0], gx3, g3m[0],
                            g1i[1], g3m[1]) for g3m in g3ms])
            else:
                min2cons_out = ' + '.join([min2cons_out] +
                        ['a_%s_%s_%s_%s_%s_%s' %(gx3, g3m[0], gx1, g1i[0],
                            g3m[1], g1i[1]) for g3m in g3ms])

            print('%s - 2b_%s_%s_%s >= 0' %(min2cons_out, gx1, g1i[0],
                    g1i[1]), file = out)


    #
    # if two active edges e13 and e23 that are both incident to vertex g3m,
    # edge e12 must be active
    #
    for gx1, gx2 in combinations(genomes, 2):
        (gx3, ) = set(genomes).difference((gx1, gx2))
        dists23 = dists.get((gx2, gx3), revDists.get((gx2, gx3), None))
        for g1i, g2dists in dists[(gx1, gx2)].items():
            g3ms = set(dists.get((gx1, gx3), revDists.get((gx1, gx3),
                None)).get(g1i, dict()).keys())
            for g2k in g2dists.keys():
                e12 = 'a_%s_%s_%s_%s_%s_%s' %(gx1, g1i[0], gx2, g2k[0],
                        g1i[1], g2k[1])
                g3m_match = g3ms.intersection(dists23.get(g2k, set()))
                if not g3m_match:
                    print('%s = 0' %e12, file = out)
                    continue

                for g3m in g3m_match:
                    if gx1 < gx3:
                        e13 = 'a_%s_%s_%s_%s_%s_%s' %(gx1, g1i[0], gx3, g3m[0],
                                g1i[1], g3m[1])
                    else:
                        e13 = 'a_%s_%s_%s_%s_%s_%s' %(gx3, g3m[0], gx1, g1i[0],
                                g3m[1], g1i[1])
                    if gx2 < gx3:
                        e23 = 'a_%s_%s_%s_%s_%s_%s' %(gx2, g2k[0], gx3, g3m[0],
                                g2k[1], g3m[1])
                    else:
                        e23 = 'a_%s_%s_%s_%s_%s_%s' %(gx3, g3m[0], gx2, g2k[0],
                                g3m[1], g2k[1])

                    print('%s + b_%s_%s_%s - %s - %s <= 0' %(e12, gx3,
                            g3m[0], g3m[1], e13, e23), file = out)


def write3MatchingConstraints(dists, revDists, out):

    for gs, pwDists in dists.items():
        for g1i, g2Dists in pwDists.items():
            if not g2Dists:
                continue
            print('b_%s_%s_%s - %s >= 0' %(gs[0], g1i[0], g1i[1],
                    ' - '.join(map( lambda g2k: 'a_%s_%s_%s_%s_%s_%s' %(gs[0],
                        g1i[0], gs[1], g2k[0], g1i[1], g2k[1]),
                        g2Dists.keys()))), file = out)

    for gs, pwDists in revDists.items():
        for g2k, g1Dists in pwDists.items():
            if not g1Dists:
                continue
            print('b_%s_%s_%s - %s >= 0' %(gs[0], g2k[0], g2k[1],
                    ' - '.join(map( lambda g1i: 'a_%s_%s_%s_%s_%s_%s' %(gs[1],
                        g1i[0], gs[0], g2k[0], g1i[1], g2k[1]),
                        g1Dists.keys())), ), file = out)


def writeFixedVariables(anchors, dists, revDists, geneorders, out):

    already_fixed = set()
    for gs, edge in anchors.items():
        for g1i, g2k in edge:
            if g1i and g2k:
                print('a_%s_%s_%s_%s_%s_%s = 1' %(gs[0], g1i[0], gs[1],
                        g2k[0], g1i[1], g2k[1]), file = out)
            if g1i:
                bx = (gs[0], g1i[0], g1i[1])
                if bx not in already_fixed:
                    already_fixed.add(bx)
                    print('b_%s_%s_%s = 1' %bx, file = out)

                if g2k == None:
                    continue
                for g2l in dists[gs][g1i].keys():
                    if g2l != g2k:
                        print('a_%s_%s_%s_%s_%s_%s = 0' %(gs[0], g1i[0],
                                gs[1], g2l[0], g1i[1], g2l[1]), file = out)
            if g2k:
                bx = (gs[1], g2k[0], g2k[1])
                if bx not in already_fixed:
                    already_fixed.add(bx)
                    print('b_%s_%s_%s = 1' %bx, file = out)

                if g1i == None:
                    continue
                for g1j in revDists[(gs[1], gs[0])][g2k].keys():
                    if g1j != g1i:
                        print('a_%s_%s_%s_%s_%s_%s = 0' %(gs[0], g1j[0],
                                gs[1], g2k[0], g1j[1], g2k[1]), file = out)

    genomes = set(geneorders.keys())
    for gx, g1 in geneorders.items():
        for g1i in g1:
            unconnected = True
            for gy in genomes.difference([gx]):
                distxy = dists.get((gx, gy), revDists.get((gx, gy), None))
                if g1i in distxy and distxy[g1i]:
                    unconnected = False
                    break
            if unconnected:
                print('b_%s_%s_%s = 0' %(gx, g1i[0], g1i[1]), file = out)

def writeVariables(geneorders, dists, out):
    for gs, g1 in geneorders.items():
        for g1i in g1:
            print('b_%s_%s_%s' %(gs, g1i[0], g1i[1]), file = out)

    for gs, pwDists in dists.items():
        for g1i, g2genes in pwDists.items():
            for g2k in g2genes.keys():
                print('a_%s_%s_%s_%s_%s_%s' %(gs[0], g1i[0], gs[1],
                        g2k[0], g1i[1], g2k[1]), file = out)

if __name__ == '__main__':

    usage = 'usage: %prog [options] <PAIRWISE DIST FILE 1> [<PAIRWISE DIST FILE 2> <PAIRWISE DIST FILE 3>]'
    parser = OptionParser(usage=usage)
    parser.add_option('-f', '--anchors_file', dest='anchorsFile', help='File' + \
            ' containing anchoring information for the matching.',
            metavar='FILE')
    parser.add_option('-3', '--3-matching', dest='do3matching',
            action='store_true', default=False, help='enforce *complete*, ' + \
                    'not partial 3-matching [default=%default]')
    parser.add_option('-o', '--omit-3matching-constraints',
            dest='omit3matching', help='Omit partial 3-matching constraints,' + \
                    ' i.e. only keep pairwise matching constraints.' + \
                    '[default=%default]', action='store_true', default=False)

    (options, args) = parser.parse_args()

    if (len(args) != 1 and len(args) != 3) or \
            (options.anchorsFile and not isfile(options.anchorsFile)):
        parser.print_help()
        exit(1)

    if options.omit3matching and options.do3matching:
        print('ERROR: Options \'omit 3-matching\' and \'enfore ' + \
                'complete 3-matching\' are incompatible. Exiting', file =
                stderr)
        exit(1)


    logging.basicConfig(filename=LOG_FILENAME,filemode='w', level=logging.INFO,
            format= "%(levelname)s\t%(asctime)s\t++ %(message)s")

    dists = dict()
    geneorders = dict()

    for f in args:
        LOG.info('Reading pairwise dist file %s' %f)
        g1name, g2name = basename(f).split('.', 2)[0].split('_')

        g1name = g1name.replace('-', 'abcdefghijklmnopqrs')
        g2name = g2name.replace('-', 'abcdefghijklmnopqrs')

        _, g1, g2, pwDists= readDistsAndOrder(open(f), 0)
        dists[(g1name, g2name)] = pwDists

        if g1name in geneorders:
            geneorders[g1name] = sorted(set(chain(geneorders[g1name], g1)))
        else:
            geneorders[g1name] = g1

        if g2name in geneorders:
            geneorders[g2name] = sorted(set(chain(geneorders[g2name], g2)))
        else:
            geneorders[g2name] = g2

    if len(dists.keys()) < 3 and not options.omit3matching:
        LOG.info('Pairwise matchings don\'t span all three genoems. Exiting')
        exit()


    LOG.info('Constructing gene id to pos maps')
    genepos = dict((g1name, dict(zip(g1, range(len(g1))))) for g1name, g1 in
        geneorders.items())


    LOG.info('Sorting distance map.')
    for gs, pwDists in dists.items():
        for k, val in pwDists.items():
            dists[gs][k] = OrderedDict(sorted(val.items()))

    anchors = dict((gs, list()) for gs in dists.keys())
    if options.anchorsFile:
        LOG.info('Read anchors')
        anchors = readAnchors(open(options.anchorsFile), geneorders.keys())


    revDists = dict(((y, x), reverseDistMap(d)) for ((x,y), d) in dists.items())
    #
    # remove edges from graph that are conflicting with anchors
    #
    LOG.info('Remove edges from graph that are conflicting with anchors')
    for gs, adjs in anchors.items():
        # all anchors are stored twice in map, for back and forth
        if gs not in dists:
            continue
        for g1i, g2k in adjs:
            if g1i and g2k:
                for g2l in dists[gs][g1i].keys():
                    if g2l != g2k:
                        del dists[gs][g1i][g2l]
                        del revDists[(gs[1], gs[0])][g2l][g1i]
                for g1j in revDists[(gs[1], gs[0])][g2k].keys():
                    if g1j != g1i:
                        del dists[gs][g1j][g2k]
                        del revDists[(gs[1], gs[0])][g2k][g1j]

    #
    # where to output?
    #
    out = stdout


    LOG.info('Start constructing linear program')

    #
    # write linear program
    #

    # objective function
    LOG.info('Write objective function')
    writeObjectiveFunction(dists, out)
    # constraints
    print('\nsubject to', file = out)
    LOG.info('Write pairwise matching constraints')
    write3MatchingConstraints(dists, revDists, out)

    if not options.omit3matching and len(args) == 3:
        if options.do3matching:
            LOG.info('Write 3-matching constraints')
            writeComplete3MatchingConstraints(geneorders, dists, revDists, out)
        else:
            LOG.info('Write partial matching constraints')
            writePartialMatchingConstraints(dists, revDists, out)

    LOG.info('Write fixed variables in anchors')
    writeFixedVariables(anchors, dists, revDists, geneorders, out)
    print( '\nbinaries', file = out)
    LOG.info('Write variables')
    writeVariables(geneorders, dists, out)
    print('\nend', file = out)

