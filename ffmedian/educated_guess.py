#!/usr/bin/env python3

from sys import stdout, exit, argv, stderr, path
from os.path import basename, dirname, abspath
import networkx as nx
from itertools import combinations, chain
from math import sqrt, pow

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDistsAndOrder, DIRECTION_CRICK_STRAND, \
        DIRECTION_WATSON_STRAND,TELOMERE_START,TELOMERE_END


# construct the similarity graph (parameter is the graph)
def compute_candidate_genes(similarity_graph):
    all_triangles = set()
    # extract connected components
    connected_comp = sorted(nx.connected_components(similarity_graph), key=len,reverse=True)
    # extract triangles from each component
    count_all = 0
    for C in connected_comp:
        if len(C)<3:
            break
        triplets = combinations(C,3);
    #Largest component contains 79 nodes. That makes 79k triplets to check.(PAM10)
    #This can be optimized by categorizing genes by their genomes and
    #obtaining triplets in one at a genome fashion.
    #I discard it for now and leave it as an TODO optimization
        for T in triplets:
            n1,n2,n3 = T
            if similarity_graph.has_edge(n1,n2) and similarity_graph.has_edge(n2,n3) and similarity_graph.has_edge(n3,n1):
    # create a list of all triangles
                t = sorted([n1,n2,n3])
                all_triangles.add(tuple(t))
                count_all +=1

    return all_triangles

#Creates a bin for each gene and adds a copy of every candidate gene containing
#that gene to the bin
def binned_triangles(all_triangles, gene_pos):

    res = dict()
    for g0name, g0pos in gene_pos.items():
        res[g0name] = [set() for _ in range(len(g0pos))]
    for T in all_triangles:
        n1,n2,n3 = T
        res[n1[0]][gene_pos[n1[0]][n1[1:]]].add(T)
        res[n2[0]][gene_pos[n2[0]][n2[1:]]].add(T)
        res[n3[0]][gene_pos[n3[0]][n3[1:]]].add(T)
    return res

#Returns a list of no-conflict candidate genes. These genes are part of the median surely.
def compute_zero_conflict_triangles(all_triangles, gene_pos, bins):
    zero_conflict_triangles = set()
    for T in all_triangles:
        count = 0
        for g in T:
            count += len(bins[g[0]][gene_pos[g[0]][g[1:]]])
        if count == 3:
            zero_conflict_triangles.add(T)

    return zero_conflict_triangles

#Keeps a (lazy) table of sigma value of a candidate gene, and returns when requested.
def compute_sigma(G,triangle):
    if triangle in compute_sigma.table:
        return compute_sigma.table[triangle]
    n1,n2,n3 = triangle
    if not G.has_edge(n1,n2) and G.has_edge(n2,n3) and G.has_edge(n1,n3):
        compute_sigma.table[triangle] = 0.0
        return 0.0
    res = pow(G[n1][n2]['weight'][1]*G[n2][n3]['weight'][1]*G[n1][n3]['weight'][1], 1/3.0)
    compute_sigma.table[triangle] = res
    return res

#Gives the next gene on the genome on the given direction.
def give_next_gene(gene,gene_orders,gene_pos, ddir=1):
    #if gene[2] == TELOMERE_END or gene[2] == TELOMERE_START:
    #    return None,None
    if (ddir < 0 and gene_pos[gene[0]][gene[1:]] > 0) or (ddir > 0 and \
            len(gene_orders[gene[0]]) -ddir > gene_pos[gene[0]][gene[1:]]):
        return (gene[0],)+gene_orders[gene[0]][gene_pos[gene[0]][gene[1:]]+ddir]
    return None

#A candidate median gene(i.e. triangle) has one left and one right 3-adjacent triangle.
#This function finds and returns.
def give_left_and_right_neighbors(G,triangle,gene_orders,gene_pos):
    n1,n2,n3 = triangle
    n1_prev = give_next_gene(n1, gene_orders, gene_pos, -1)
    n1_next = give_next_gene(n1, gene_orders, gene_pos, 1)
    if G[n1][n2]['weight'][0] == DIRECTION_CRICK_STRAND:
        n2_prev = give_next_gene(n2, gene_orders, gene_pos, -1)
        n2_next = give_next_gene(n2, gene_orders, gene_pos, 1)
    else:
        n2_next = give_next_gene(n2, gene_orders, gene_pos, -1)
        n2_prev = give_next_gene(n2, gene_orders, gene_pos, 1)
    if G[n1][n3]['weight'][0] == DIRECTION_CRICK_STRAND:
        n3_prev = give_next_gene(n3, gene_orders, gene_pos, -1)
        n3_next = give_next_gene(n3, gene_orders, gene_pos, 1)
    else:
        n3_next = give_next_gene(n3, gene_orders, gene_pos, -1)
        n3_prev = give_next_gene(n3, gene_orders, gene_pos, 1)

    if n1_prev==None or n2_prev==None or n3_prev==None:
        left_n = (None, None, None)
    else:
        left_n = (n1_prev,n2_prev,n3_prev)
    if n1_next==None or n2_next==None or n3_next==None:
        right_n = (None, None, None)
    else:
        right_n = (n1_next,n2_next,n3_next)
    return left_n, right_n

#When two candidate median genes are given, this computes the maximum number of
#adjacencies shared in extants. Codomain is [0,3].
def max_preserved_adjacencies_in_extant(G,triangle1,triangle2,gene_orders,gene_pos):
    t1,t2 = map(set,[triangle1,triangle2])
    if triangle2[2] == TELOMERE_START or triangle2[2] == TELOMERE_END \
            or triangle1[2] == TELOMERE_END or triangle1[2] == TELOMERE_START:
        return 0

    t1l,t1r = map(set,give_left_and_right_neighbors(G,triangle1,gene_orders,gene_pos))
    t2l,t2r = map(set,give_left_and_right_neighbors(G,triangle2,gene_orders,gene_pos))

    res = max(min(len(t1l.intersection(t2)),len(t2r.intersection(t1))),min(len(t1r.intersection(t2)),len(t2l.intersection(t1))))
    return res

#Each median candidate gene has two directionalities, head an tail, and can establish
#0, 1, 2, or 3-adjacency in these directions with some other median candidate gene.
#Given two triangle (median candidate gene), this function computes head-tail,
#head-head, tail-tail, tail-head adjacency count between them.
def ht_hh_tt_th(G,triangle1, triangle2, gene_orders, gene_pos):
    res = [0,0,0,0]
    g1,h1,i1 = triangle1
    g1p = give_next_gene(g1,gene_orders,gene_pos, -1)
    g1n = give_next_gene(g1,gene_orders,gene_pos, 1)
    h1p = give_next_gene(h1,gene_orders,gene_pos, -1)
    h1n = give_next_gene(h1,gene_orders,gene_pos, 1)
    i1p = give_next_gene(i1,gene_orders,gene_pos, -1)
    i1n = give_next_gene(i1,gene_orders,gene_pos, 1)

    g2,h2,i2 = triangle2
    g2p = give_next_gene(g2,gene_orders,gene_pos, -1)
    g2n = give_next_gene(g2,gene_orders,gene_pos, 1)
    h2p = give_next_gene(h2,gene_orders,gene_pos, -1)
    h2n = give_next_gene(h2,gene_orders,gene_pos, 1)
    i2p = give_next_gene(i2,gene_orders,gene_pos, -1)
    i2n = give_next_gene(i2,gene_orders,gene_pos, 1)

    if g1n==g2:
        res[0]+=1
    if g2n==g1: #can be circular, so not elif (not our case, but still...)
        res[3]+=1
    if h1n==h2:
        if G[g1][h1]['weight'][0] == G[g2][h2]['weight'][0]:#same relative direction
            if G[g1][h1]['weight'][0] == DIRECTION_CRICK_STRAND:
                res[0]+=1
            else:
                res[3]+=1
        else:
            if G[g1][h1]['weight'][0] == DIRECTION_CRICK_STRAND:
                res[1]+=1
            else:
                res[2]+=1
    if h1p==h2:
        if G[g1][h1]['weight'][0] == G[g2][h2]['weight'][0]:#same relative direction
            if G[g1][h1]['weight'][0] == DIRECTION_CRICK_STRAND:
                res[3]+=1
            else:
                res[0]+=1
        else:
            if G[g1][h1]['weight'][0] == DIRECTION_CRICK_STRAND:
                res[2]+=1
            else:
                res[1]+=1
    if i1n==i2:
        if G[g1][i1]['weight'][0] == G[g2][i2]['weight'][0]:#same relative direction
            if G[g1][i1]['weight'][0] == DIRECTION_CRICK_STRAND:
                res[0]+=1
            else:
                res[3]+=1
        else:
            if G[g1][i1]['weight'][0] == DIRECTION_CRICK_STRAND:
                res[1]+=1
            else:
                res[2]+=1
    if i1p==i2:
        if G[g1][i1]['weight'][0] == G[g2][i2]['weight'][0]:#same relative direction
            if G[g1][i1]['weight'][0] == DIRECTION_CRICK_STRAND:
                res[3]+=1
            else:
                res[0]+=1
        else:
            if G[g1][i1]['weight'][0] == DIRECTION_CRICK_STRAND:
                res[2]+=1
            else:
                res[1]+=1
    return res

#Cheks whether or not the genes in triangle has all the same reading direction.
def has_same_directionality(G,left_T,T):
    return G[left_T[0]][left_T[1]]['weight'][0] == G[T[0]][T[1]]['weight'][0] \
            and G[left_T[0]][left_T[2]]['weight'][0] == G[T[0]][T[2]]['weight'][0]

#Return list of contiguous 3-adjacent median candidate gene sequences. These are
#the optimal substructures we want to identify since they are more likely to be
#part of the median.
def detect_runs(G, all_triangles,gene_orders,gene_pos):
    runs = []
    visited=dict.fromkeys(all_triangles,False)
    for T in all_triangles:
        if visited[T] == True:
            continue
        visited[T] = True
        left_T, right_T = give_left_and_right_neighbors(G,T,gene_orders,gene_pos)
        if not left_T:
            continue
        left_list = []
        right_list = []
        if left_T in all_triangles and has_same_directionality(G,left_T,T) :
            visited[left_T] = True
            left_next = left_T
            left_list.append(left_T)
            while True:
                left_next, _ = give_left_and_right_neighbors(G,left_next,gene_orders,gene_pos)
                if not left_next or left_next not in all_triangles \
                        or not has_same_directionality(G,left_next,T):
                    break
                visited[left_next] = True
                left_list.append(left_next)
        left_list = left_list[::-1]
        if right_T in all_triangles and has_same_directionality(G,right_T,T):
            visited[right_T] = True
            right_next = right_T
            right_list.append(right_T)
            while True:
                _, right_next = give_left_and_right_neighbors(G,right_next,gene_orders,gene_pos)
                if not right_next or right_next not in all_triangles\
                        or not has_same_directionality(G,right_next,T):
                    break
                visited[right_next] = True
                right_list.append(right_next)

        if left_list or right_list:
            therun = left_list + [T] + right_list
            if len(therun)>0: #filtering out small runs
                runs.append(therun)

    return runs

#Given a median candidate gene, returns a list of all conflicting median candidate genes.
def conflicting_triangles(triangle,bins,gene_pos):
    all_crossing = set()
    for N in triangle:
        crossing_triangles = bins[N[0]][gene_pos[N[0]][N[1:]]]
        for ct in crossing_triangles:
            if ct[0] == triangle[0] and ct[1] == triangle[1] and ct[2] == triangle[2]:
                continue
            all_crossing.add(ct)
    return all_crossing


#OBSOLETE
#(given a run, returns all candidate median genes that are in conflict with the run.
#OBSOLETE
def set_of_conflicting_triangles(therun,bins,gene_pos):
    subgraph_triangles = set()
    for T in therun:
        subgraph_triangles = subgraph_triangles.union(conflicting_triangles(T,bins,gene_pos))
    return subgraph_triangles

#Computes the potential function used in the algorithm (see the paper, educatedGuess algorithm).
#This one is computationally expensive, but more accurate.
def compute_LR_potential(G, T, bins, gene_orders, gene_pos):

    if T in compute_LR_potential.table:
        return compute_LR_potential.table[T]
    lPot = rPot = 0
    for gene in T:
        lG = give_next_gene(gene,gene_orders,gene_pos, -1)
        rG = give_next_gene(gene,gene_orders,gene_pos, 1)
        if lG != None:
            for lT in bins[lG[0]][gene_pos[lG[0]][lG[1:]]]:
                lM = max_preserved_adjacencies_in_extant(G,lT,T,gene_orders,gene_pos)
                if lM:
                    lPot = max(lPot, sqrt(compute_sigma(G, lT)*compute_sigma(G, T))*lM)
        if rG != None:
            for rT in bins[rG[0]][gene_pos[rG[0]][rG[1:]]]:
                rM = max_preserved_adjacencies_in_extant(G,rT,T,gene_orders,gene_pos)
                if rM:
                    rPot = max(rPot, sqrt(compute_sigma(G, rT)*compute_sigma(G, T))*rM)
    compute_LR_potential.table[T] = (lPot,rPot)
    return lPot, rPot

#Computes the potential function used in the algorithm (see the paper).
#This one is computationally cheap, but overestimates the actual potential.
def weakly_compute_LR_potential(G, T, bins, gene_orders, gene_pos):

    rcumul=0
    lcumul=0
    binPot = 0
    for gene in T:
        lG = give_next_gene(gene,gene_orders,gene_pos, -1)
        rG = give_next_gene(gene,gene_orders,gene_pos, 1)
        if lG != None:
            for lT in bins[lG[0]][gene_pos[lG[0]][lG[1:]]]:
                binPot = max(binPot, compute_sigma(G, lT))
        lcumul+=sqrt(binPot*compute_sigma(G,T))
        binPot=0
        if rG != None:
            for rT in bins[rG[0]][gene_pos[rG[0]][rG[1:]]]:
                binPot = max(binPot, compute_sigma(G, rT))
        rcumul+=sqrt(binPot*compute_sigma(G,T))
        binPot=0
    return lcumul,rcumul

#Given a run, determines internal conflicting triangles and outgoing conflicting triangles.
def test_subgraph(G, therun,bins,gene_orders,gene_pos):
    first_triangle = therun[0]
    last_triangle  = therun[-1]
    range_g0 = sorted((gene_pos[first_triangle[0][0]][first_triangle[0][1:]],
        gene_pos[last_triangle[0][0]][last_triangle[0][1:]]))
    range_g1 = sorted((gene_pos[first_triangle[1][0]][first_triangle[1][1:]],
        gene_pos[last_triangle[1][0]][last_triangle[1][1:]]))
    range_g2 = sorted((gene_pos[first_triangle[2][0]][first_triangle[2][1:]],
        gene_pos[last_triangle[2][0]][last_triangle[2][1:]]))

    __f1__ = lambda x,y: x <= y[0] or x >= y[1]
    __f2__ = lambda x: __f1__(gene_pos[x[0][0]][x[0][1:]], range_g0) or \
            __f1__(gene_pos[x[1][0]][x[1][1:]], range_g1) or \
            __f1__(gene_pos[x[2][0]][x[2][1:]], range_g2)

    internal_conflicts = set()
    outgoing_conflicts = dict()
    for i in range(len(therun)):
        T = therun[i]
        ct = conflicting_triangles(T,bins,gene_pos)
        Cs = set(filter(__f2__, ct))
        lCPots = list()
        rCPots = list()
        cNodes = set()
        for C in Cs:
            cNodes.update(set(C).intersection(T))
            lP, rP = weakly_compute_LR_potential(G,C,bins,gene_orders,gene_pos)
            lCPots.append(lP)
            rCPots.append(rP)
        lCPots.sort(reverse=True)
        rCPots.sort(reverse=True)
        lCPot = sum(lCPots[:len(cNodes)])
        rCPot = sum(rCPots[:len(cNodes)])

        internal_conflicts.update(ct.difference(Cs))
        if lCPot + rCPot > 0:
            outgoing_conflicts[T] = lCPot + rCPot

    return (therun,internal_conflicts,outgoing_conflicts)

#Runs maximum weight matching on run subgraph + outgoing conflicts. If the run
#is preserved, then we have found and optimal substructure.
def run_matching_graph(G, gene_pos, gene_orders, bins, run, internal_conflicts,
        outgoing_conflicts):

    all_triangles = set(run).union(internal_conflicts)
    mG = nx.Graph()
    runAdj = list()

    val = True
    res = list()

    for i in range(len(run)-1):
        mG.add_edge((run[i], 'h'), (run[i+1], 't'), weight=sqrt(compute_sigma(G,
            run[i])*compute_sigma(G, run[i+1]))*3)
        runAdj.append(((run[i], 'h'), (run[i+1], 't')))
        if run[i] in outgoing_conflicts:
            mG.add_edge((run[i], 't'), (run[i], 'h'),
                    weight=outgoing_conflicts[run[i]])
    if run and run[-1] in outgoing_conflicts:
        mG.add_edge((run[-1], 't'), (run[-1], 'h'),
                weight=outgoing_conflicts[run[-1]])

    for T in internal_conflicts:
        for gene in T:
            lG = give_next_gene(gene,gene_orders,gene_pos, -1)
            rG = give_next_gene(gene,gene_orders,gene_pos, 1)
            if lG != None:
                for lT in all_triangles.intersection(bins[lG[0]][gene_pos[lG[0]][lG[1:]]]):
                    lM = ht_hh_tt_th(G,lT,T,gene_orders,gene_pos)
                    w = sqrt(compute_sigma(G, lT) * compute_sigma(G, T))
                    if lM[0]:
                        mG.add_edge((lT,'h'),(T,'t'), weight=lM[0]*w)
                    if lM[1]:
                        mG.add_edge((lT,'h'),(T,'h'), weight=lM[1]*w)
                    if lM[2]:
                        mG.add_edge((lT,'t'),(T,'t'), weight=lM[2]*w)
                    if lM[3]:
                        mG.add_edge((lT,'t'),(T,'h'), weight=lM[3]*w)
            if rG != None:
                for rT in all_triangles.intersection(bins[rG[0]][gene_pos[rG[0]][rG[1:]]]):
                    rM = ht_hh_tt_th(G,rT,T,gene_orders,gene_pos)
                    w = sqrt(compute_sigma(G, rT) * compute_sigma(G, T))
                    if rM[0]:
                        mG.add_edge((rT,'h'),(T,'t'), weight=rM[0]*w)
                    if rM[1]:
                        mG.add_edge((rT,'h'),(T,'h'), weight=rM[1]*w)
                    if rM[2]:
                        mG.add_edge((rT,'t'),(T,'t'), weight=rM[2]*w)
                    if rM[3]:
                        mG.add_edge((rT,'t'),(T,'h'), weight=rM[3]*w)

    MWM = nx.max_weight_matching(mG)
    MWMGraph = nx.Graph(MWM)
    m_weight = 0
    prev = -1
    for i in range(len(runAdj)):
        u, v = runAdj[i]
        if not MWMGraph.has_edge(u,v):
            val = False
            if MWMGraph.has_edge(u, (u[0], 't')):
                if i-prev > 2:
                    res.append(run[prev+1:i])
                prev = i
            elif MWMGraph.has_edge(v, (v[0], 'h')):
                if i-prev > 1:
                    res.append(run[prev+1:i+1])
                prev = i+1

        m_weight += mG[u][v]['weight']

    t_weight = sum(mG[u][v]['weight'] for u,v in MWM)/2
    if val and m_weight < float(t_weight)/2:
        return False, list()
    return val, res

def test_ht_hh_tt_th():
    #TEST FOR ht_hh_tt_th
    success = True
    for T in all_triangles:
        left_T, right_T = give_left_and_right_neighbors(G, T, gene_orders,gene_pos)

        if left_T in all_triangles:
            res = ht_hh_tt_th(G,left_T,T,gene_orders,gene_pos)
            if (has_same_directionality(G,left_T,T) and res[0]!=3) \
                    or (not has_same_directionality(G,left_T,T) and res[0]==3):
                success = False
                break

        if right_T in all_triangles:
            res = ht_hh_tt_th(G,right_T,T,gene_orders,gene_pos)
            if (has_same_directionality(G,right_T,T) and res[3]!=3) \
                    or (not has_same_directionality(G,right_T,T) and res[3]==3):
                success = False
                break

    if success:
        stderr.write("ht_hh_tt_th.....pass!\n")
    else:
        stderr.write("ht_hh_tt_th.....fail!\n")
    #TEST FOR ht_hh_tt_th

def test_detect_runs():
    #TEST FOR detect_runs
    a = sum(map(lambda x : len(x)-1,runs))
    b=0
    c=0
    for T in all_triangles:
        l,r = give_left_and_right_neighbors(G,T,gene_orders,gene_pos)
        if l in all_triangles:
            b+=1
        if r in all_triangles:
            c+=1
    if a<=min(b,c):
        stderr.write("detect_runs.....pass!\n")
    else:
        stderr.write("detect_runs.....fail!\n")
    #TEST FOR detect_runs

if __name__ == '__main__':

    if len(argv) != 4:
        print(('\tusage: %s <PAIRWISE DIST FILE 1> <PAIRWISE DIST FILE 2> ' + \
                '<PAIRWISE DIST FILE 3>') %argv[0], file = stdout)
        exit(1)

    G = nx.Graph()
    pairwise_distances = dict()
    gene_orders = dict()
    potentials=dict()
    compute_sigma.table = dict()
    compute_LR_potential.table = dict()
    for f in [argv[1],argv[2],argv[3]]:

        g1name, g2name = basename(f).split('.', 2)[0].split('_', 1)
        _, G1, G2, pwDists= readDistsAndOrder(open(f), addTelomeres=True)


        pairwise_distances[tuple(sorted([g1name, g2name]))] = pwDists
        if g1name in gene_orders:
            gene_orders[g1name] = sorted(set(chain(gene_orders[g1name], G1)))
        else:
            gene_orders[g1name] = G1

        if g2name in gene_orders:
            gene_orders[g2name] = sorted(set(chain(gene_orders[g2name], G2)))
        else:
            gene_orders[g2name] = G2

        dists = pwDists
        for (chr0, g0), g1s in dists.items():
            for (chr1, g1) in g1s.keys():
                G.add_edge((g1name, chr0, g0), (g2name, chr1, g1), weight=g1s[(chr1,g1)])


    gene_pos = dict((g1name, dict(zip(g1, range(len(g1))))) for g1name, g1 in
        gene_orders.items())
    all_triangles = compute_candidate_genes(G)

    binned = binned_triangles(all_triangles,gene_pos)
    zero_conflict_triangles = compute_zero_conflict_triangles(all_triangles,gene_pos,binned)
    print("initial all and 0-conflict ",
            len(all_triangles),len(zero_conflict_triangles), file = stderr)

    runs = detect_runs(G, all_triangles,gene_orders,gene_pos)

    #some optional tests
    #test_ht_hh_tt_th()
    #test_detect_runs()

    pruned_triangles = set()
    identified_triangles = 0
    identified_adjacencies = 0

    subruns = runs
    while subruns:
        run, internal_conflicts, outgoing_conflicts = test_subgraph(G,subruns.pop(),binned,gene_orders,gene_pos)
        stderr.write(('testing component of size %s with %s internal and ' + \
                '%s outgoing conflicts...') %(len(run),
                    len(internal_conflicts), len(outgoing_conflicts)))
        isSuccess = True
        if len(internal_conflicts) or len(outgoing_conflicts):
            isSuccess, newsub = run_matching_graph(G, gene_pos, gene_orders, binned, run,
                    internal_conflicts, outgoing_conflicts)
        if isSuccess:
            stderr.write(' success!\n')
            stdout.write('\n'.join('\t'.join(map(str, chain(*T))) for T in
                run))
            stdout.write('\n\n')
            stdout.flush()
            identified_triangles+=len(run)
            identified_adjacencies+=len(run)-1
            for T in run:
                pruned_triangles = pruned_triangles.union(conflicting_triangles(T,binned,gene_pos))
        else:
            subruns.extend(newsub)
            stderr.write(' fail ;-(\n')
    stderr.write("REPORT: triangles that are pruned, identified triangles, identified adjacencies, all triangles\n%d\t%d\t%d\t%d\n"%(len(pruned_triangles),identified_triangles,identified_adjacencies,len(all_triangles)))

