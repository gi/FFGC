#!/usr/bin/env python3

from sys import stdout, stderr, exit, argv, path
from os.path import basename, isfile, dirname, abspath
from optparse import OptionParser
from itertools import chain
from functools import reduce
from collections import OrderedDict
from math import sqrt, pow
import logging
import csv

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDistsAndOrder, DIRECTION_CRICK_STRAND, \
        DIRECTION_WATSON_STRAND, TELOMERE_START, TELOMERE_END

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]

def readAnchors(data):

    genes = list()
    adjs = list()

    m1 = None
    for line in csv.reader(data, delimiter='\t'):
        if not line:
            m1 = None
            continue
        G1, chr1, g1i = line[:3]
        G2, chr2, g2k = line[3:6]
        G3, chr3, g3m = line[6:]
        g1i, g2k, g3m = map(int, (g1i, g2k, g3m))
        chr1, chr2, chr3 = map(lambda x: x.replace('_', ','), (chr1, chr2,
            chr3))
        m2 = (G1, (chr1, g1i), G2, (chr2, g2k), G3, (chr3, g3m))
        if m1:
            adjs.append((m1, 'h', m2, (g1i == TELOMERE_START or g1i ==
                TELOMERE_END) and 'h' or 't'))
        m1 = m2
        genes.append(m1)
    return genes, adjs

def computeCandidateGenes(dists, genepos, anchorGenes=None):

    res = dict()
    (g1, g2, g3) = sorted(genepos.keys())

    for g0name, g0pos in genepos.items():
        res[g0name] = [set() for _ in range(len(g0pos))]

    anchorGenesS = ()
    if anchorGenes:
        for m in anchorGenes:
            res[m[0]][genepos[m[0]][m[1]]].add(m)
            res[m[2]][genepos[m[2]][m[3]]].add(m)
            res[m[4]][genepos[m[4]][m[5]]].add(m)
        anchorGenesS = set(reduce(lambda x,y: x + list((y[:2],y[2:4],y[4:])),
            anchorGenes, list()))

    for g1i in dists[(g1, g2)].keys():
        for g2k in dists[(g1, g2)][g1i].keys():
            if g2k not in dists[(g2, g3)]:
                continue
            for g3m in dists[(g2, g3)][g2k].keys():
                if g1i in dists[(g1, g3)] and g3m in dists[(g1,g3)][g1i] and \
                        (g1, g1i) not in anchorGenesS and (g2, g2k) not \
                        in anchorGenesS and (g3, g3m) not in anchorGenesS:
                    m = (g1, g1i, g2, g2k, g3, g3m)
                    res[g1][genepos[g1][g1i]].add(m)
                    res[g2][genepos[g2][g2k]].add(m)
                    res[g3][genepos[g3][g3m]].add(m)
    return res

def computeCandidateAdjacencies(candidateorders, dists):
    res = dict()

    g1, g2, g3 = sorted(candidateorders.keys())
    for g0, g0seq in candidateorders.items():
        for i in range(len(g0seq)-1):
                j = i+1
                while j < len(g0seq) and not g0seq[j]:
                    j+=1
                if j >= len(g0seq) or not g0seq[j]:
                    break
                for m1 in g0seq[i]:
                    ext1 = 'h'
                    if m1[1][1] == TELOMERE_START or m1[1][1] == TELOMERE_END:
                        ext1 = 'h'
                    elif g0 != g1:
                        if g0 == m1[2]:
                            ext1 = dists[(g1, g0)][m1[1]][m1[3]][0] == DIRECTION_CRICK_STRAND and 'h' or 't'
                        if g0 == m1[4]:
                            ext1 = dists[(g1, g0)][m1[1]][m1[5]][0] == DIRECTION_CRICK_STRAND and 'h' or 't'

                    for m2 in g0seq[j]:
                        ext2 = 't'
                        if m2[1][1] == TELOMERE_START or m2[1][1] == TELOMERE_END:
                            ext2 = 'h'
                        elif g0 != g1:
                            if g0 == m2[2]:
                                ext2 = dists[(g1, g0)][m2[1]][m2[3]][0] == DIRECTION_CRICK_STRAND and 't' or 'h'
                            if g0 == m2[4]:
                                ext2 = dists[(g1, g0)][m2[1]][m2[5]][0] == DIRECTION_CRICK_STRAND and 't' or 'h'

                        if m1[1] != m2[1] and m1[3] != m2[3] and m1[5] != m2[5]:
                            adj = (m1, ext1, m2, ext2)
                            if (m1, ext1) > (m2, ext2):
                                adj = (m2, ext2, m1, ext1)

                            if adj in res:
                                res[adj] += 1
                            else:
                                res[adj] = 1
    return res

def writeObjectiveFunction(candidate_adjacencies, dists, out):

    w_count={1:0, 2:0, 3:0}

    print('max ', file = out)

    prev = ""
    for (m1, ext1, m2, ext2), w in candidate_adjacencies.items():
        w_count[w] += 1
        if prev:
            out.write(prev + ' + ')

        w1 = dists[(m1[0], m1[2])][m1[1]][m1[3]][1]
        w2 = dists[(m1[0], m1[4])][m1[1]][m1[5]][1]
        w3 = dists[(m1[2], m1[4])][m1[3]][m1[5]][1]
        w4 = dists[(m2[0], m2[2])][m2[1]][m2[3]][1]
        w5 = dists[(m2[0], m2[4])][m2[1]][m2[5]][1]
        w6 = dists[(m2[2], m2[4])][m2[3]][m2[5]][1]

        prev = '%sb_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s ' %(w*pow(w1*w2*w3*w4*w5*w6, 1.0/6),
                m1[0], m1[1][0], m1[1][1], m1[2], m1[3][0], m1[3][1], m1[4],
                m1[5][0], m1[5][1], ext1, m2[0], m2[1][0], m2[1][1], m2[2], m2[3][0],
                m2[3][1], m2[4], m2[5][0], m2[5][1], ext2)
    print(prev, file = out)

    print( w_count, file = stderr)

def writeConstraints(candidateorders, candidate_adjacencies, genepos, out):

    for g0 in sorted(genepos.keys()):
        for g_set in candidateorders[g0]:
            prev = ''
            for (g1, g1i, g2, g2k, g3, g3m) in g_set:
                if prev:
                    out.write(prev + ' + ')

                prev = 'a_%s_%s_%s_%s_%s_%s_%s_%s_%s' %(g1, g1i[0], g1i[1], g2,
                        g2k[0], g2k[1], g3, g3m[0], g3m[1])
            if prev:
                print(prev + ' <= 1', file = out)

    tmp = dict()
    for m1, ext1, m2, ext2 in candidate_adjacencies.keys():

        if (m1, ext1) not in tmp:
            tmp[(m1, ext1)] = set()
        tmp[(m1, ext1)].add((m1, ext1, m2, ext2))

        if (m2, ext2) not in tmp:
            tmp[(m2, ext2)] = set()
        tmp[(m2, ext2)].add((m1, ext1, m2, ext2))

        print(('2b_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s' + \
                ' - a_%s_%s_%s_%s_%s_%s_%s_%s_%s - a_%s_%s_%s_%s_%s_%s_%s_%s_%s <= 0') %(m1[0],
                        m1[1][0], m1[1][1], m1[2], m1[3][0], m1[3][1], m1[4],
                        m1[5][0], m1[5][1], ext1, m2[0], m2[1][0], m2[1][1], m2[2],
                        m2[3][0], m2[3][1], m2[4], m2[5][0], m2[5][1], ext2,
                        m1[0], m1[1][0], m1[1][1], m1[2], m1[3][0], m1[3][1],
                        m1[4], m1[5][0], m1[5][1], m2[0], m2[1][0], m2[1][1],
                        m2[2], m2[3][0], m2[3][1], m2[4], m2[5][0], m2[5][1]),
                        file = out)

    for g_set in tmp.values():
        prev = ''
        for m1, ext1, m2, ext2 in g_set:
            if prev:
                out.write(prev + ' + ')
            prev = 'b_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s ' %(m1[0],
                    m1[1][0], m1[1][1], m1[2], m1[3][0], m1[3][1], m1[4], m1[5][0],
                    m1[5][1], ext1, m2[0], m2[1][0], m2[1][1], m2[2], m2[3][0],
                    m2[3][1], m2[4], m2[5][0], m2[5][1], ext2)
        print(prev + ' <= 1', file = out)

def writeAnchors(genes, adjs, out):
    for (g1, g1i, g2, g2k, g3, g3m) in genes:
        print('a_%s_%s_%s_%s_%s_%s_%s_%s_%s = 1' %(g1, g1i[0], g1i[1],
                g2, g2k[0], g2k[1], g3, g3m[0], g3m[1]), file = out)

    for m1, ext1, m2, ext2 in adjs:
        print('b_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s = 1' %(m1[0],
                m1[1][0], m1[1][1], m1[2], m1[3][0], m1[3][1], m1[4], m1[5][0],
                m1[5][1], ext1, m2[0], m2[1][0], m2[1][1], m2[2], m2[3][0],
                m2[3][1], m2[4], m2[5][0], m2[5][1], ext2), file = out)


def writeVariables(candidateorders, candidate_adjacencies, out):

    candidategenes = set()
    for g0 in sorted(candidateorders.keys()):
        for g_set in candidateorders[g0]:
            candidategenes.update(g_set)

    for (g1, g1i, g2, g2k, g3, g3m) in candidategenes:
        print('a_%s_%s_%s_%s_%s_%s_%s_%s_%s' %(g1, g1i[0], g1i[1], g2,
                g2k[0], g2k[1], g3, g3m[0], g3m[1]), file = out)

    for m1, ext1, m2, ext2 in candidate_adjacencies.keys():
        print('b_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s ' %(m1[0],
                m1[1][0], m1[1][1], m1[2], m1[3][0], m1[3][1], m1[4], m1[5][0],
                m1[5][1], ext1, m2[0], m2[1][0], m2[1][1], m2[2], m2[3][0],
                m2[3][1], m2[4], m2[5][0], m2[5][1], ext2), file = out)


if __name__ == '__main__':

    usage = 'usage: %prog [options] <PAIRWISE DIST FILE 1> <PAIRWISE DIST FILE 2> <PAIRWISE DIST FILE 3>'
    parser = OptionParser(usage=usage)

    parser.add_option('-a', '--anchor_file', dest='anchor',
            help='File with anchors that are known to be part of the median',
            type=str, metavar='FILE')

    (options, args) = parser.parse_args()

    if len(args) != 3:
        parser.print_help()
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.ERROR)
    ch.setFormatter(logging.Formatter('!! %(message)s'))
    cf = logging.FileHandler(LOG_FILENAME, mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(cf)
    LOG.addHandler(ch)

    dists = dict()
    geneorders = dict()

    anchorGenes = None
    anchorAdjs = None
    if options.anchor:
        anchorGenes, anchorAdjs = readAnchors(open(options.anchor))


    for f in args:
        LOG.info('Reading pairwise dist file %s' %f)
        g1name, g2name = basename(f).rsplit('.', 2)[0].split('_')

        g1name = g1name.replace('-', 'abcdefghijklmnopqrs')
        g2name = g2name.replace('-', 'abcdefghijklmnopqrs')

        _, g1, g2, pwDists= readDistsAndOrder(open(f))
        g1 = map(lambda x: (x[0].replace('_', ','), x[1]), g1)
        g2 = map(lambda x: (x[0].replace('_', ','), x[1]), g2)

        fixed_pwDists = dict()

        for (chr1, g1i), d in pwDists.items():
            k = (chr1.replace('_', ','), g1i)
            fixed_pwDists[k] = dict()
            for (chr2, g2k), x in d.items():
                fixed_pwDists[k][(chr2.replace('_', ','), g2k)] = x

        dists[(g1name, g2name)] = fixed_pwDists

        if g1name in geneorders:
            geneorders[g1name] = sorted(set(chain(geneorders[g1name], g1)))
        else:
            geneorders[g1name] = g1

        if g2name in geneorders:
            geneorders[g2name] = sorted(set(chain(geneorders[g2name], g2)))
        else:
            geneorders[g2name] = g2

    LOG.info('Constructing gene id to pos maps')
    genepos = dict((g1name, dict(zip(g1, range(len(list(g1)))))) for g1name, g1 in
        geneorders.items())

    LOG.info('Sorting distance map.')
    for gs, pwDists in dists.items():
        for k, val in pwDists.items():
            dists[gs][k] = OrderedDict(sorted(val.items()))

    #
    # where to output?
    #
    out = stdout


    LOG.info('Start constructing linear program')
    #
    # compute possible adjacencies
    #

    possible_adjs = dict()
    anchored_adjs = dict()

    candidate_geneorders = computeCandidateGenes(dists, genepos, anchorGenes)
    candidate_adjacencies = computeCandidateAdjacencies(candidate_geneorders, dists)

    #
    # write linear program
    #

    # objective function
    LOG.info('Write objective function')
    writeObjectiveFunction(candidate_adjacencies, dists, out)

    # constraints
    print('\nsubject to', file = out)
    LOG.info('Write constraints')
    writeConstraints(candidate_geneorders, candidate_adjacencies, genepos, out)
    if options.anchor:
        writeAnchors(anchorGenes, anchorAdjs, out)

    print('\nbinaries', file = out)
    LOG.info('Write variables')
    writeVariables(candidate_geneorders, candidate_adjacencies, out)
    print('\nend', file = out)

