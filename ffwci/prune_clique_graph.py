#!/usr/bin/env python3

#from multiprocessing import Pool, cpu_count
from optparse import OptionParser
from sys import exit, stdout, stderr, maxsize, argv, path
from functools import partial
from itertools import tee
from os.path import basename, dirname, abspath
import networkx as nx
import logging
import csv

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDists, reverseDistMap, \
        DIRECTION_CRICK_STRAND, DIRECTION_WATSON_STRAND, \
        TELOMERE_END, TELOMERE_START

CHUNK_SIZE = 1000000
#MAX_NO_THREADS = cpu_count()

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]

def count(data, quorum):
    # initializing the previous variables
    int_p = None
    prevGy = None

    counter = dict()
    # opening and reading files
    for line in csv.reader(data, delimiter = '\t'):
        int_x = (line[0], line[1], int(line[2]), int(line[3]))
        int_y = (line[4], line[5], int(line[6]), int(line[7]))

        if int_x != int_p and int_x not in counter:
            counter[int_x] = [0, None]

        # deal with interval int_x
        if int_x != int_p or prevGy != int_y[0]:
            counter[int_x][0] += 1

        # If the y node does not exist then it is added
        if int_y not in counter:
            counter[int_y] = [0, None]

        # Update count for int_y if and only if last observed genome for this
        # interval is different
        if counter[int_y][1] != int_x[0]:
            counter[int_y][0] += 1
            counter[int_y][1] = int_x[0]

        prevGy = int_y[0]
        # update int_p
        int_p = int_x

    return counter


def constructIntervalGraph(data, counter, quorum):
    # Create the graph
    G = nx.Graph()

    # opening and reading files
    for line in csv.reader(data, delimiter = '\t'):
        int_x = (line[0], line[1], int(line[2]), int(line[3]))
        int_y = (line[4], line[5], int(line[6]), int(line[7]))

        if counter[int_x][0]+1 >= options.quorum and counter[int_y][0]+1 >= options.quorum:
            G.add_edge(int_x, int_y)
    return G


def get_hits_interval(G0, chr0, start0, end0, G1, chr1, start1, end1):

    d = 0
    g0set = [set((chr1, y) for y in range(start1, end1+1)).intersection(dists[(G0, \
            G1)].get((chr0, x), dict()).keys()) for x in range(start0, end0+1)]
    g1set = [set((chr0, x) for x in range(start0, end0+1)).intersection(dists[(G1, \
            G0)].get((chr1, y), dict()).keys()) for y in range(start1, end1+1)]

    hits = len(list(filter(None, g0set))) + len(list(filter(None, g1set)))
    gaps = len(g0set)+len(g1set)-hits
    return g0set, g1set, hits, gaps


def testInterval(G, intt, dists):

    (Gx, chrx, startx, endx) = intt

#   XXX debug rosid_sankoff_cds/n4/G0_G2.macsi.d0_m4
#    if chrx == '6' and startx == 40558 and endx == 40559:
#        import pdb; pdb.set_trace()

    valid_L = False
    valid_R = False
    neighs = list(G.neighbors(intt))

    i = 0
    while i < len(neighs):
        (Gy, chry, starty, endy) = neighs[i]
        cur_valid_L = (chrx, startx) in dists[(Gx, Gy)] and \
                filter(lambda x: x[0] == chry and x[1] >= starty and \
                x[1] <= endy, dists[(Gx, Gy)][(chrx, startx)].keys())
        cur_valid_R = (chrx, endx) in dists[(Gx, Gy)] and \
                filter(lambda x: x[0] == chry and x[1] >= starty and \
                x[1] <= endy, dists[(Gx, Gy)][(chrx, endx)].keys())
        valid_L |= not not cur_valid_L
        valid_R |= not not cur_valid_R

        if cur_valid_L and cur_valid_R:
            i += 1
        else:
            del neighs[i]

    if valid_L and valid_R:
        lint = (Gx, chrx, startx, endx+1)
        rint = (Gx, chrx, startx-1, endx)
        mint = (Gx, chrx, startx-1, endx+1)

        for bint in (lint, rint, mint):
            valid_L = not G.has_node(bint) or \
                    not set(neighs).issubset(G.neighbors(bint))
            if not valid_L:
                break

    return valid_L and valid_R


def readPWDists(args):
    dists = dict()
    genomes = set()

    for f in args:
        gname1, gname2  = basename(f).split('.', 2)[0].split('_', 1)
        _, dist = readDists(open(f), 0)

        genomes.update((gname1, gname2))

        if not dist:
            LOG.fatal('Distance table in file %s is empty. Exiting.' %f)
            exit(1)
        dists[(gname1, gname2)] = dist
        dists[(gname2, gname1)] = reverseDistMap(dist)

    return genomes, dists

def check_nodes(node, G=None, dists=None, q=None):
    #res = list()
    #for node in nodes:
    (_, chrx, startx, endx) = node
    if len(set(map(lambda x: x[0], G.neighbors(node)))) + 1 < q \
            or not testInterval(G, node, dists):
        return node
#                print >> stderr, ('remove node because it violated %s ' + \
#                        'constraint') %(len(set(map(lambda x: x[0],
#                            G.neighbors(node)))) + 1 < options.quorum and
#                            'quorum' or 'closedness')
#            res.append(node)
#       return res
    return None

if __name__ == '__main__':

    #Create parser, selects quorum parameter and strigency

    usage = '%prog [options] <WCI FILE> <PAIRWISE DIST FILE 1> ... <PAIRWISE DIST FILE N>'
    parser = OptionParser(usage=usage)
    parser.add_option('-q', '--quorum',
                      dest='quorum', default=0, type = 'int', metavar='2,...,n',
                      help='Quorum of genomes in which a WCI set needs to ' + \
                            'occurl. Zero means that a WCI set must occur' + \
                            ' in all genomes.  [default: %default]')
    parser.add_option('-i', '--max_iterations',
                      dest='max_iterations', default=10, type='int', metavar='INT',
                      help='Maximal number of iterations after which the pruning will stop. [default: %default]')
    (options, args) = parser.parse_args()

    # check if input file is given, if so, call function with data
    if len(args) < 2:
        parser.print_help()
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.ERROR)
    ch.setFormatter(logging.Formatter('!! %(message)s'))
    cf = logging.FileHandler(LOG_FILENAME, mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(cf)
    LOG.addHandler(ch)

    LOG.info('Read pairwise distances')
    genomes, dists = readPWDists(args[1:])

    # adjust quorum parameter
    if not options.quorum:
        LOG.info('Setting quorum to %s' %(len(genomes)))
        options.quorum = len(genomes)

    out = stdout

    LOG.info('Counting interval connections')
    counter = count(open(args[0]), options.quorum)

    G = constructIntervalGraph(open(args[0]), counter, options.quorum)


#    pool = Pool(processes=MAX_NO_THREADS)
#    print >> stderr, 'Start segmentation using at most %s threads' %MAX_NO_THREADS
    LOG.info('Iteratively pruning interval graph...')
    hasChanged = True
    i = 0

    while hasChanged and i < options.max_iterations:
        LOG.info('  iteration %s, cur. #intervals: %s' %(i, nx.number_of_nodes(G)))
        i += 1
        hasChanged = False
        aryRes = list()
        __fun__ = partial(check_nodes, G=G, dists=dists, q=options.quorum)
        res1, res2 = tee(map(__fun__, G.nodes()))
        #res1, res2 = tee(pool.map(__fun__, G.nodes(), CHUNK_SIZE))
        G.remove_nodes_from(list(res1))
        for x in res2:
            if x != None:
                hasChanged |= True
                counter[x][0] = -1
#    pool.close()
#    pool.join()
    LOG.info('... done. Output')

    for line in csv.reader(open(args[0]), delimiter = '\t'):
        int_x = (line[0], line[1], int(line[2]), int(line[3]))
        int_y = (line[4], line[5], int(line[6]), int(line[7]))

        if counter[int_x][0]+1 >= options.quorum and counter[int_y][0]+1 >= options.quorum:
            print('\t'.join(line), file = out)

