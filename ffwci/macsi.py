#!/usr/bin/env python3

from itertools import repeat, chain
from sys import stdout, stderr, argv, exit, maxsize, path
from multiprocessing import Pool, cpu_count
from optparse import OptionParser
from functools import partial
from collections import deque
from os.path import basename, dirname, abspath
from bisect import bisect
from copy import deepcopy
import logging
import csv

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDists, reverseDistMap, \
        DIRECTION_CRICK_STRAND, DIRECTION_WATSON_STRAND, \
        TELOMERE_END, TELOMERE_START

MAX_NO_THREADS = cpu_count()
CONTIG_BOUNDARY = (-1, maxsize)

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]

def establish_linear_genome_order(genes):

    chromosomes = dict()
    for (chr1, g1i) in genes:
        if chr1 not in chromosomes:
            chromosomes[chr1] = [maxsize, 0]
        if g1i < chromosomes[chr1][0]:
            chromosomes[chr1][0] = g1i
        if g1i > chromosomes[chr1][1]:
            chromosomes[chr1][1] = g1i

    chrnames = sorted(chromosomes.keys())

    g = list()
    for k in chrnames:
        # stopper for extend()
        g.append(CONTIG_BOUNDARY)
        g.extend([(k, i) for i in range(chromosomes[k][0], chromosomes[k][1]+1)])
    # stopper for extend()
    g.append(CONTIG_BOUNDARY)
    return  g

def createPos(fromSeq, toSeq, dist):
    toSeqMap = dict(zip(toSeq, range(len(toSeq))))
    res = list()
    for i in fromSeq:
        if i in dist:
            res.append(sorted(map(lambda x: toSeqMap[x], dist[i].keys())))
        else:
            res.append(list())
    return res


def __check_trans_delta_env__(y, cDA1_p, tDA1_p, ck_genomes, latest_segs, d, curA,
        mask, deltaArys, dist_from_origin, neg_quorum):

    hasHit = False
    for x in range(len(tDA1_p)):
        if ck_genomes[x] and x != y:
            active_segments = 1
            if cDA1_p[x] == -1:
                d[x] -= 1
                if d[x] >= 0:
                    # every ridge that accumulated already d[x]-1 errors must
                    # now be masked
                    mask[x] |= deltaArys[x][d[x]]
            else:
                if latest_segs[x] != -1:
                    d[x] = min(d[x], len(deltaArys[x])-abs(latest_segs[x] -
                        cDA1_p[x]))
                else:
                    latest_segs[x] = cDA1_p[x]

                if d[x] >= 0:
                    a = (curA[x] ^ tDA1_p[x]) & ~mask[x]

                    # ridges that have been observed before, but are now (i.e.
                    # in tDA1_p) not active
                    a1 = curA[x] & a
                    # ridges that became active in tDA1_p but have not been
                    # observed before
                    a2 = tDA1_p[x] & a

                    i = 0
                    while a1 and i < d[x]:
                        tmp = deltaArys[x][i] & a1
                        deltaArys[x][i] |= a1
                        a1 = tmp
                        i += 1

                    mask[x] |= a1
                    # a2 contains only ridges that never occured before. by
                    # definition, there should not be any occupied ridge in
                    # deltaArys[x][i] if this ridge never occured before.
                    if a2 and dist_from_origin <= d[x]:
                        i = 0
                        while i < dist_from_origin:
                            deltaArys[x][i] |= a2
                            i += 1
                    elif a2:
                        mask[x] |= a2

                    hasHit |= not not (tDA1_p[x] & ~mask[x])
                    curA[x] |= tDA1_p[x]
                    active_segments = curA[x] & ~mask[x]

            if d[x] < 0 or (not active_segments and \
                    dist_from_origin > len(deltaArys[x])):
                d[x] = -1
                neg_quorum -= 1
                ck_genomes[x] = False
                if neg_quorum < 0:
                    break
    return hasHit

def __getDeltaEnv_helper__(ref, y, pos, p, M, cDA, tDA, ck_genomes,  i, j,
        options):
    charset = set()
    boundAry = deque()

    pos2 = pos[(y, ref)]
    # original value can be i <= x <= i+delta
    ii = pos2[p][M[y][p]]

    neg_quorum = len(ck_genomes) - options.quorum
    d = [options.delta] * len(ck_genomes)
    cp = p
    curA = list(tDA[y][p])
    deltaArys = [[0] * options.delta for _ in ck_genomes]
    latest_segs = list(cDA[y][p])
    ck_genomes_b = list(ck_genomes)

    mask = [0] * len(tDA)
    transHit = not not filter(None, [tDA[y][p][x] and ck_genomes_b[x] for x in
        range(len(ck_genomes_b))])

    while d[y] >= 0 and cDA[y][cp][ref] != maxsize and \
            ck_genomes_b.count(False)-1 <= neg_quorum:
        hasHit = transHit
        if cDA[y][cp][ref] == -1:
            d[y] -= 1
        else:
            x = M[y][cp]
            while x < len(pos2[cp]) and pos2[cp][x] <= j:
                charset.add(pos2[cp][x])
                x += 1
                hasHit = True
            if x == M[y][cp]:
                d[y] -= 1
        boundAry.append(hasHit)

        if cp + 1 < len(pos2):
            transHit = __check_trans_delta_env__(y, cDA[y][cp+1], tDA[y][cp+1],
                    ck_genomes_b, latest_segs, d, curA, mask, deltaArys,
                    cp+1-p, neg_quorum)
        cp += 1

    right_bound = cp - 1
    right_dy = d[y]

    # reuse lists
    for x in range(len(ck_genomes)):
        d[x] = options.delta
        curA[x] = tDA[y][p][x]
        latest_segs[x] = cDA[y][p][x]
        ck_genomes_b[x] = ck_genomes[x]
        mask[x] = 0
        for z in range(options.delta):
            deltaArys[x][z] = 0
    cp = p-1

    if cp >= 0:
        transHit = __check_trans_delta_env__(y, cDA[y][cp], tDA[y][cp],
                ck_genomes_b, latest_segs, d, curA, mask, deltaArys, p-cp,
                neg_quorum)

    while d[y] >= 0 and cDA[y][cp][ref] != maxsize and \
            ck_genomes_b.count(False)-1 <= neg_quorum:

        hasHit = transHit
        if cDA[y][cp][ref] == -1:
            d[y] -= 1
        else:
            x = M[y][cp]
            # break on the left if any character lower/equal to ii is observed:
            while x < len(pos2[cp]) and pos2[cp][x] <= j and pos2[cp][x] > ii:
                charset.add(pos2[cp][x])
                x += 1
                hasHit = True
            if x == M[y][cp]:
                d[y] -= 1

        boundAry.appendleft(hasHit)
        if cp >= 0:
            transHit = __check_trans_delta_env__(y, cDA[y][cp-1], tDA[y][cp-1],
                    ck_genomes_b, latest_segs, d, curA, mask, deltaArys,
                    p-cp+1, neg_quorum)
        cp -= 1

    left_bound = cp + 1
    left_dy = d[y]

    # XXX move below shrinking?
    if len(charset) + right_bound - left_bound + 1 - \
            2*options.delta + right_dy + left_dy < options.min:
        charset.clear()
        boundAry.clear()

    # shrink boundary to relevant size
    while boundAry and not boundAry[0]:
        left_bound  += 1
        boundAry.popleft()
    while boundAry and not boundAry[-1]:
        boundAry.pop()

    return charset, (left_bound, boundAry)

def getDeltaEnv(ref, pos, M, cisDeltaArys, transDeltaArys, i, options):
    cDA1 = cisDeltaArys[ref]
    tDA1 = transDeltaArys[ref]

    ck_genomes = [ True ] * len(tDA1[i])
    ck_genomes[ref] = False
    #
    # determine initial set of characters
    #
    curA = list(tDA1[i])
    d = [options.delta+(z == -1 and z or 0) for z in cDA1[i]]
    deltaArys = [[0] * options.delta for _ in ck_genomes]
    latest_segs = list(cDA1[i])
    ck_genomes_i = list(ck_genomes)
    mask = [0] * len(tDA1[i])
    # by definition, last j always fails
    j = i
    while j+1 < len(cDA1) and ck_genomes_i.count(True) + 1 >= options.quorum:
        j += 1
        __check_trans_delta_env__(ref, cDA1[j], tDA1[j], ck_genomes_i,
                latest_segs, d, curA, mask, deltaArys, j-i, len(ck_genomes) -
                options.quorum - ck_genomes_i.count(False) + 1)

    #
    # refine character set by running through all hits
    #

    prev_j = maxsize
    charset_total = range(i, j+1)
    charsets = list()

    while i in charset_total and prev_j != charset_total[-1] and \
            ck_genomes.count(True)+1 >= options.quorum:

        charsets = [[list() for _ in range(options.delta+1)] for _ in ck_genomes]
        prev_j = charset_total[-1]
        charset_total = set()

        for y in range(len(ck_genomes)):
            if not ck_genomes[y] or (ref, y) not in pos:
                continue
            for ii in range(i, min(i + options.delta+1, len(tDA1))):
                for p in pos[(ref, y)][ii]:
                    c, boundAry = __getDeltaEnv_helper__(ref, y, pos,
                            p, M, cisDeltaArys, transDeltaArys, ck_genomes,
                            i, prev_j, options)
                    charsets[y][ii-i].append((c, boundAry))
                    charset_total.update(c)

        d = [options.delta] * len(ck_genomes)
        intersect = [False] * len(ck_genomes)

        # clean up all char sets sets
        j = i
        while charset_total and len(list(filter(lambda x: x >= 0, d))) \
                >= options.quorum and j <= max(charset_total):
            for y in range(len(ck_genomes)):
                if not ck_genomes[y]:
                    continue
                jInY = False
                for ii in range(len(charsets[y])):
                    for p in range(len(charsets[y][ii])):
                        if j in charsets[y][ii][p][0]:
                            jInY = True
                        elif charsets[y][ii][p][0] and len(charsets[y][ii][p][0]) + \
                                options.delta > j - i and j+1-i-len([c \
                                for c in charsets[y][ii][p][0] if c <= j]) > \
                                options.delta:
                            # we aim to remove all j's from charsets[y][ii][p][0]
                            # that are larger than j if this list contains more
                            # than delta missing values
                            #
                            # (c for c in charsets[y][ii][p][0] if c <= j) is a
                            # costly check. Hence, check first if
                            # len(charsets[y][ii][p][0]) + options.delta > j-i.
                            # This must be true for pidgeon hole principle kind
                            # of reasons if charsets[y][ii][p][0] should contain
                            # values larger or equal to j
                            charsets[y][ii][p][0].intersection_update(range(i, j))
                if not jInY:
                    d[y] -= 1
                intersect[y] |= jInY
            j += 1
        ck_genomes = [ck_genomes[y] & intersect[y] for y in
                range(len(ck_genomes))]
        charset_total = sorted(z for z in charset_total if z <= j-1)

    if i not in charset_total or ck_genomes.count(True) + 1 < options.quorum:
        return list(), set()
    return charsets, charset_total

def constructCisDeltaArrays(gene_orders, pos, delta):

    # the delta array data structure is a data structure that corresponds to an
    # array of integers, indicating the number of indels up to the current
    # position in the genome.  the deltaArys dictionary contains the delta
    # arrays for all genomes of the current dataset
    deltaArys = [[[-1] * len(gene_orders) for _ in gene_orders[x]] for x in
            range(len(gene_orders))]

    for (x, y), pos12 in pos.items():
        g1chr = None
        c = - delta - 1
        for i in range(len(gene_orders[x])):
            g1i = gene_orders[x][i]
            if g1i == CONTIG_BOUNDARY:
                deltaArys[x][i][y] = maxsize
                continue

            if g1chr != g1i[0]:
                c += delta + 1
                g1chr = g1i[0]

            if not pos12[i]:
                c += 1
            else:
                deltaArys[x][i][y] = c

    return deltaArys

def constructS2R(segment_ids, active_ridges, delta):
    segment_ids = sorted(segment_ids)

    s2r = dict()
    new_ridges = set()
    seg_sets = [(segment_ids[0], set([segment_ids[0]]))]

    for i in range(1, len(segment_ids)):
        seg_id = segment_ids[i]
        if seg_id-seg_sets[-1][0] > delta:
            seg_sets.append((seg_id, set([sid for sid in seg_sets[-1][1] if
                seg_id-sid <= delta])))
        seg_sets[-1][1].add(seg_id)

    r = -1
    for _, seg_ids in seg_sets:
        r += 1
        while r in active_ridges:
            r += 1

        for seg_id in seg_ids:
            if seg_id not in s2r:
                s2r[seg_id] = list()
            s2r[seg_id].append(r)

        new_ridges.add(r)

    return s2r, new_ridges


def constructTransDeltaArrays(cisDeltaArys, pos, delta):
    mx_bit_length = 0

    # the delta array data structure is a data structure that corresponds to an
    # array of integers, indicating the number of indels up to the current
    # position in the genome.  the deltaArys dictionary contains the delta
    # arrays for all genomes of the current dataset
    transDeltaArys = [None] * len(cisDeltaArys)

    for (x, y), pos12 in pos.items():

        if not transDeltaArys[x]:
            transDeltaArys[x] = [[0] * len(cisDeltaArys) for _ in pos12]


        cDA1 = cisDeltaArys[x]
        cDA2 = cisDeltaArys[y]
        tDA = transDeltaArys[x]

        active_ridges = deque()
        trans_segs = deque()

        pseg_id = -delta - 1

        for i in range(len(pos12)+1):

            seg_id = pseg_id + delta + 1
            if i < len(pos12):
                seg_id = cDA1[i][y]

            if seg_id == -1 or seg_id == maxsize:
                continue

            if pseg_id != seg_id:
                # update active_ridges
                while active_ridges and pseg_id - active_ridges[-1][0] > delta:
                    active_ridges.pop()

                # write ridges for each interval which has more than delta
                # distance to current position
                while trans_segs and seg_id - cDA1[trans_segs[-1][0]][y] > delta:
                    j, trans_segs_j = trans_segs.pop()
                    s2r, new_ridges = constructS2R(trans_segs_j,
                            set(chain(*map(lambda l: l[1], active_ridges))),
                            delta)
                    active_ridges.appendleft((pseg_id, new_ridges))

                    seg_id_j = cDA1[j][y]
                    while j < len(cDA1) and (cDA1[j][y] == -1 or \
                            cDA1[j][y] - seg_id_j <= delta):
                        if cDA1[j][y] != -1:
                            for k in pos12[j]:
                                for r in s2r[cDA2[k][x]]:
                                    tDA[j][y] |= 2**r
                        mx_bit_length = max(mx_bit_length,
                                tDA[j][y].bit_length())
                        j += 1

                trans_segs.appendleft((i, set()))

            for j, trans_segs_j in trans_segs:
                if j < len(cDA1) and seg_id-cDA1[j][y] <= delta:
                    for k in pos12[i]:
                        trans_segs_j.add(cDA2[k][x])
                else:
                    break

            pseg_id = seg_id

    return transDeltaArys, mx_bit_length


def discoverWCIs(ref, y, ref_closed_wcis, pos2, M, i, j, intervals_j, boundAry,
        options):

    left_bound, boundAry = boundAry
    right_bound = left_bound + len(boundAry)-1

    for (l, k, di, dk, OCC) in ref_closed_wcis:
        ll = l
        while di >= 0 and len(OCC)+k-ll+1-2*options.delta+dk+di>= options.min:
            dR = dk
            diR = di
            OCC_R = list(OCC)
            kk = k
            # contracting everything on the right, then on the left, and repeat
            while dR >= 0 and diR >= 0 and len(OCC_R)+kk-ll+1-2*options.delta \
                    + dR + diR >= options.min:
                kkk = kk
                # expanding everything on the right, then on the left, and repeat
                while kkk-kk <= dR:
                    lll = ll
                    while kkk-kk+ll-lll <= dR:
                        if boundAry[lll-left_bound] and boundAry[kkk-left_bound]:
                            intervals_j[y].add((lll, kkk))
                        if lll <= l and lll > left_bound and \
                                (not pos2[lll-1] or M[lll-1] >= len(pos2[lll-1]) \
                                or pos2[lll-1][M[lll-1]] > j):
                            lll -= 1
                        else:
                            break
                    if kkk >= k and kkk < right_bound and \
                            (not pos2[kkk+1] or M[kkk+1] >= len(pos2[kkk+1]) \
                            or pos2[kkk+1][M[kkk+1]] > j):
                        kkk += 1
                    else:
                        break

                u = M[kk]
                while u < len(pos2[kk]) and pos2[kk][u] <= j and diR >= 0:
                    OCC_R[pos2[kk][u]-i] -= 1
                    # only validates if occurrence is zero. Negative occurrences
                    # should not happen at all
                    if not OCC_R[pos2[kk][u]-i]:
                        diR -= 1
                    u += 1
                if u == M[kk]:
                    break
                kk -= 1

            u = M[ll]
            while u < len(pos2[ll]) and pos2[ll][u] <= j and di >= 0:
                OCC[pos2[ll][u]-i] -= 1
                # only validates if occurrence is zero. Negative occurrences should
                # not happen at all
                if not OCC[pos2[ll][u]-i]:
                    di -= 1
                u += 1
            if u == M[ll]:
                break
            ll += 1


def test_interval(ref, i, j, y, l, k):
    G0 = id2genomes[ref]
    G1 = id2genomes[y]

    d = 0
    g0set = set(gene_orders[ref][x] for x in range(i, j+1))
    g1set = set(gene_orders[y][z] for z in range(l, k+1))

    for x in range(i, j+1):
        d += not g1set.intersection(dists[(G0, G1)].get(gene_orders[ref][x],
            dict()).keys()) and 1 or 0
    for z in range(l, k+1):
        d += not g0set.intersection(dists[(G1, G0)].get(gene_orders[y][z],
            dict()).keys()) and 1 or 0

    return d


def macsi(M, minI, maxJ, ref, ck_genomes, pos, cisDeltaArys, transDeltaArys,
        options):
    res = list()

    # assumes that reference is set to False in ck_genomes
    active_Gs = ck_genomes.count(True) + 1

    LOG.info('Start macsi on G%s[%s,%s]' %(ref, minI, maxJ))

    cDA1 = cisDeltaArys[ref]

    for i in range(minI, maxJ):
        # do not start at an empty position
        if not filter(None, (not not pos[(ref, y)][i] for y in \
                range(len(cisDeltaArys)) if (ref, y) in pos)):
            continue

        charsets, charset_total = getDeltaEnv(ref, pos, M, cisDeltaArys,
                transDeltaArys, i, options)

        for j in charset_total:
            intervals_j = [set() for _ in cisDeltaArys]

            for y in range(len(cisDeltaArys)):
                if not ck_genomes[y]:
                    continue

                pos1 = pos[(ref, y)]
                pos2 = pos[(y, ref)]
                cDA2 = cisDeltaArys[y]

                for ii in range(i, i+options.delta+1):
                    if not charsets[y][ii-i]:
                        continue
                    for x in range(len(pos1[ii])):
                        p = pos1[ii][x]

                        # last check is computationally expensive, hence we
                        # employ a weak pre-check that filters out obvious
                        # misses
                        if not charsets[y][ii-i][x][0] or len(charsets[y][ii-i][x][0]) \
                                + options.delta < j-i+1 or len(set(range(i, \
                                j+1)).difference(charsets[y][ii-i][x][0])) > options.delta:
                            continue

                        left_bound, boundAry = charsets[y][ii-i][x][1]
                        right_bound = left_bound + len(boundAry) - 1

                        OCC_L = [0] * (j-i+1)
                        # find first left and right border
                        l, count_L = extend(ref, pos2, p, -1, M[y], i, j,
                                OCC_L, 0, left_bound-1)
                        kk, count_L = extend(ref, pos2, p+1, 1, M[y], i, j,
                                OCC_L, count_L, right_bound+1)

                        d_L = options.delta
                        prev_l = float('inf')

                        ref_closed_wcis = list()

                        while l >= left_bound-1 and d_L >= 0 and (not x or l > pos1[ii][x-1]):
                            if l+1 != prev_l:
                                # extend to the right
                                OCC = list(OCC_L)
                                count = count_L
                                k = kk
                                prev_k = float('-inf')
                                d = d_L
                                while d >= 0 and k <= right_bound + 1:
                                    gaps = j-i+1-count + options.delta-d
                                    if k-1 != prev_k and j-i+1 <= count + options.delta\
                                            and j-i+k-l-gaps >= options.min:
                                        ref_closed_wcis.append((l+1, k-1,
                                            options.delta-(j-i+1-count), d,
                                            list(OCC)))
                                    prev_k = k
                                    if d and k <= right_bound:
                                        k, count = extend(ref, pos2, k+1, 1,
                                                M[y], i, j, OCC, count,
                                                right_bound+1)
                                    else:
                                        # this prevents re-iteration when
                                        # right_bound is reached
                                        k += 1
                                    d -= 1
                            prev_l = l
                            if d_L and l >= left_bound:
                                l, count_L = extend(ref, pos2, l-1, -1, M[y],
                                        i, j, OCC_L, count_L, left_bound-1)
                            else:
                                l -= 1
                            d_L -= 1

                        discoverWCIs(ref, y, ref_closed_wcis, pos2, M[y], i, j,
                                intervals_j, (left_bound, boundAry), options)

            if intervals_j.count(set([]))-1 <= 2*len(cisDeltaArys)-active_Gs \
                    - options.quorum:
                for y in range(len(intervals_j)):
                    for l, k in sorted(intervals_j[y]):
#                        test_d = test_interval(ref, i, j, y, l, k)
#                        if  test_d > options.delta:
#                            import pdb; pdb.set_trace()
                        res.append((ref, i, j, y, l, k))

        for y in range(len(cisDeltaArys)):
            if (ref, y) in pos:
                for p in pos[(ref, y)][i]:
                    M[y][p] += 1

    LOG.info('Finished macsi on G%s[%s,%s]' %(ref, minI, maxJ))

    return res

def extend(ref, pos2, k, step, M, i, j, OCC, count, bound):

    while (step < 0 and k > bound) or (step > 0 and k < bound):
        u = M[k]
        while u < len(pos2[k]) and pos2[k][u] <= j:
            if not OCC[pos2[k][u]-i]:
                count += 1
            OCC[pos2[k][u]-i] += 1
            u += 1
        if u == M[k]:
            break
        k += step
    return k, count


def parseInput(args, options):
    isMultiChrom = False
    gene_orders_dict = dict()
    dists = dict()

    for f in args:
        isMultiChrom_f, dist = readDists(open(f), 0)
        isMultiChrom = isMultiChrom or isMultiChrom_f

        gname1, gname2  = basename(f).split('.', 2)[0].split('_', 1)
        if gname1 not in gene_orders_dict:
            gene_orders_dict[gname1] = set()
        if gname2 not in gene_orders_dict:
            gene_orders_dict[gname2] = set()

        if not dist:
            LOG.fatal('Distance table in file %s is empty. Exiting.' %f)
            exit(1)

        revDist = reverseDistMap(dist)

        gene_orders_dict[gname1].update(dist.keys())
        gene_orders_dict[gname2].update(revDist.keys())

        dists[(gname1, gname2)] = dist
        dists[(gname2, gname1)] = revDist

    genomes2id = dict(zip(sorted(gene_orders_dict.keys()),
        range(len(gene_orders_dict))))

    gene_orders = [None] * len(genomes2id)
    for gname, gorder in gene_orders_dict.items():
        gene_orders[genomes2id[gname]] = establish_linear_genome_order(gorder)

    # discard isMultiChrom information for now
    return genomes2id, gene_orders, dists


if __name__ == '__main__':

    usage = '%prog [options] <PAIRWISE DIST FILE 1> ... <PAIRWISE DIST FILE N>'
    parser = OptionParser(usage=usage)
    parser.add_option('-d', '--delta',
                      dest='delta', default=0, type='int',
                      help='Approximate weak common intervals parameter, ' + \
                              'allows for <delta> indels [default: %default]')
    parser.add_option('-q', '--quorum',
                      dest='quorum', default=0, type='int', metavar='2, ..., n',
                      help='Quorum of genomes in which a WCI set needs to ' + \
                              'occur. Zero means that a WCI set must occur' + \
                              ' in all genomes.  [default: %default]')
    parser.add_option('-m', '--min',
                      dest='min', default=3, type='int', help='Minimum ' + \
                              'number of positions a WCI pair [default:' + \
                              ' %default]')
    parser.add_option('-b', '--blocksize', dest='blocksize', default=0,
            type='int', help='Size of blocks that should be computed in ' + \
                              'parallel.  If set to zero, parallel ' + \
                              'computation is disabled. [default: %default]')

    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.print_help()
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.ERROR)
    ch.setFormatter(logging.Formatter('!! %(message)s'))
    cf = logging.FileHandler(LOG_FILENAME, mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(cf)
    LOG.addHandler(ch)


    genomes2id, gene_orders, dists = parseInput(args, options)
    id2genomes = sorted(genomes2id.keys())

    if not options.quorum:
        options.quorum = len(gene_orders)

    if options.quorum < 2 or options.quorum > len(gene_orders):
        LOG.fatal('Invalid parameter setting for \'-q|--quorum\'. Exiting')
        parser.print_help()
        exit(1)

    pos = dict()
    for (gname1, gname2), dist in dists.items():
        x = genomes2id[gname1]
        y = genomes2id[gname2]
        pos[(x, y)] = createPos(gene_orders[x], gene_orders[y], dist)

    cisDeltaArys = constructCisDeltaArrays(gene_orders, pos, options.delta)
    LOG.info('constructing trans-delta arrays ...')
    transDeltaArys, mx_bit_length = constructTransDeltaArrays(cisDeltaArys, pos,
            options.delta)
    LOG.info('... done, maximum bit length is %s' %mx_bit_length)

    doParallel = not not options.blocksize
    if not options.blocksize:
        options.blocksize = maxsize

    if doParallel:
        applyResList = list()
        pool = Pool(processes=MAX_NO_THREADS)
        LOG.info('Creating pool with %s threads' %MAX_NO_THREADS)

    intervals = list()
    ck_genomes = [True] * len(gene_orders)
    for ref in range(len(genomes2id)-1):
        ck_genomes[ref] = False
        M = [[0] * len(pos.get((y, ref), ())) for y in
                range(len(cisDeltaArys))]
        for i in range(0, len(cisDeltaArys[ref]), options.blocksize):
            j = min(i+options.blocksize,len(cisDeltaArys[ref]))
            if doParallel:
                applyResList.append(pool.apply_async(macsi, args=(deepcopy(M),
                    i, j, ref, deepcopy(ck_genomes), pos, cisDeltaArys,
                    transDeltaArys, deepcopy(options))))

                if i + options.blocksize < len(cisDeltaArys[ref]):
                    for y in range(len(cisDeltaArys)):
                        if (ref, y) in pos:
                            for p in range(len(pos[(y, ref)])):
                                M[y][p] = bisect(pos[(y, ref)][p], j-1, M[y][p])
            else:
                macsiOut = macsi(M, i, j, ref, ck_genomes, pos, cisDeltaArys,
                        transDeltaArys, options)
                intervals.append(macsiOut)

    if doParallel:
        for aR in applyResList:
            intervals.append(aR.get())

    LOG.info('writing results')
    out = stdout
    for (Gx, xstart, xend, Gy, ystart, yend) in chain(*intervals):
        print('\t'.join(map(str, (id2genomes[Gx], gene_orders[Gx][xstart][0],
            gene_orders[Gx][xstart][1], gene_orders[Gx][xend][1],
            id2genomes[Gy], gene_orders[Gy][ystart][0],
            gene_orders[Gy][ystart][1], gene_orders[Gy][yend][1]))), file =
            stdout)

