#!/usr/bin/env python3

from sys import stdout, stderr, exit, argv, maxsize, path
from optparse import OptionParser
from os.path import basename, dirname, abspath
import logging
import csv

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDistsAndOrder, reverseDistMap, \
        DIRECTION_CRICK_STRAND, DIRECTION_WATSON_STRAND, \
        TELOMERE_END, TELOMERE_START

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]


class NotMutuallyClosed(Exception):
    """ exception thrown if interval pair is not mutually closed """

    def __init__(self, var):
        self.var = var

    def getVar(self):
        return self.var

def createPos(fromSeq, toSeq, dist):
    toSeqMap = dict(zip(toSeq, range(len(toSeq))))
    res = list()
    for i in fromSeq:
        if i in dist:
            res.append(sorted(map(lambda x: toSeqMap[x], dist[i].keys())))
        else:
            res.append(list())
    return res


def __getDeltaEnv_helper__(pos2, p, M, g1, g2, delta, i, j):
    charset = set()

    g1chr = g1[i][0]
    g2chr = g2[p][0]

    d = delta
    cp = p

    while d >= 0 and cp < len(pos2) and g2chr == g2[cp][0]:
        x = M[cp]
        if x > 0 and pos2[cp][x-1] == i-1:
            break
        while x < len(pos2[cp]) and pos2[cp][x] <= j and \
                g1[pos2[cp][x]][0] == g1chr:
            charset.add(pos2[cp][x])
            x += 1
        if x == M[cp]:
            d -= 1
        cp += 1

    d = delta
    cp = p-1
    while d >= 0 and cp >= 0 and g2chr == g2[cp][0]:
        x = M[cp]
        if x > 0 and pos2[cp][x-1] == i-1:
            break
        while x < len(pos2[cp]) and pos2[cp][x] <= j and \
                g1[pos2[cp][x]][0] == g1chr:
            # break on the left if another character i is observed:
            if pos2[cp][x] == i:
                d = -1
                break
            charset.add(pos2[cp][x])
            x += 1
        if x == M[cp]:
            d -= 1
        cp -= 1

    return sorted(charset)

def getDeltaEnv(pos2, p, M, g1, g2, delta, i):
    prev_set = list()

    cur_set = __getDeltaEnv_helper__(pos2, p, M, g1, g2, delta, i, float('inf'))

    # optimize cur_set
    while len(prev_set) != len(cur_set):
        d = delta
        x = 1
        while x < len(cur_set) and d >= 0:
            if cur_set[x-1] != cur_set[x]-1:
                d -= cur_set[x]-cur_set[x-1]-1
            if d >= 0:
                x += 1

        prev_set = cur_set
        if x < len(cur_set):
            cur_set = __getDeltaEnv_helper__(pos2, p, M, g1, g2, delta, i, cur_set[x-1])
    return cur_set

def acsi(pos1, pos2, g1, g2, options):
    res = list()

    M = [0] * len(pos2)

    for i in range(len(pos1)):
        for x in range(len(pos1[i])):
            p = pos1[i][x]
            g2chr = g2[p][0]
            charset = getDeltaEnv(pos2, p, M, g1, g2, options.delta, i)

            for j in charset:
                OCC_L = [0] * len(pos1)
                # find first left and right border
                try:
                    l, count_L = extend(pos2, g2, g2chr, p,-1, M, i, j, OCC_L, 0)
                    kk, count_L = extend(pos2, g2, g2chr, p, 1, M, i, j, OCC_L, count_L)
                except NotMutuallyClosed as e:
                    if e.getVar() == 'i':
                        break
                    else:
                        continue
                d_L = options.delta
                prev_l = float('inf')

                while l >= 0 and d_L >= 0 and (not x or l > pos1[i][x-1]):

                    if l+1 != prev_l:
                        # extend to the right
                        OCC = list(OCC_L)
                        count = count_L
                        k = kk
                        try:
                            prev_k = float('-inf')
                            for d in range(d_L, -1, -1):
                                if k >= len(pos2):
                                    break
                                gaps = j-i+1-count + options.delta-d
                                if OCC[j] and j-i+1 <= count + options.delta\
                                        and j-i+k-l-gaps >= options.min \
                                        and k-1 != prev_k:
                                    res.append((i, j, l+1, k-1))
                                prev_k = k
                                if d:
                                    if M[k] > 0 and pos2[k][M[k]-1] == i-1:
                                        raise NotMutuallyClosed('i')
                                    if M[k] < len(pos2[k]) and \
                                            pos2[k][M[k]] == j+1:
                                        raise NotMutuallyClosed('j')
                                    k, count = extend(pos2, g2, g2chr, k+1, 1, M, i, j,
                                            OCC, count)

                        except NotMutuallyClosed:
                            pass
                    prev_l = l
                    try:
                        if d_L:
                            if M[l] > 0 and pos2[l][M[l]-1] == i-1:
                                raise NotMutuallyClosed('i')
                            if M[l] < len(pos2[l]) and pos2[l][M[l]] == j+1:
                                raise NotMutuallyClosed('j')
                            l, count_L = extend(pos2, g2, g2chr, l-1, -1, M, i, j, OCC_L,
                                    count_L)
                    except NotMutuallyClosed:
                        break

                    d_L -= 1
        for p in pos1[i]:
            M[p] += 1
    return res

def extend(pos2, g2, g2chr, k, step, M, i, j, OCC, count):

    while k < len(pos2) and k >= 0 and g2[k][0] == g2chr:
        u = M[k]
        if u > 0 and pos2[k][u-1] == i-1 and u < len(pos2[k]) and pos2[k][u] <= j:
            raise NotMutuallyClosed(var='i')
        while u < len(pos2[k]) and pos2[k][u] <= j:
            if not OCC[pos2[k][u]]:
                OCC[pos2[k][u]] += 1
                count += 1
            u += 1
        if u == M[k]:
            break
        if u < len(pos2[k]) and pos2[k][u] == j+1:
            raise NotMutuallyClosed(var='j')
        k += step
    return k, count

def printResults(intervals, g1, g2, dist, revDist, isMultiChrom, out):

    res = list()
    for i, j, k, l in intervals:
        s, setDist, hitCount, _  = score_interval(i, j, k, l, g1, g2, dist,
                revDist)
        if isMultiChrom:
            res.append((g1[i][0], g1[i][1], g1[j][1], g2[k][0], g2[k][1], g2[l][1],
                s, setDist, hitCount))
        else:
            res.append((g1[i][1], g1[j][1], g2[k][1], g2[l][1], s, setDist,
                hitCount))
    res.sort()

    for x in res:
        print('\t'.join(map(str, x)), file = out)

def __score_helper__(i, j, dist):
    res = 0
    setDist = 0
    hitCount = 0
    inside = list()
    outside = list()
    for ((_, y), (_, s)) in dist.items():
        if y >= i[1] and y <= j[1]:
            inside.append(s)
            hitCount += 1
        outside.append(s)
    if inside:
        res += min(max(inside)/max(outside), 1)
    else:
        setDist += 1
    return res, setDist, hitCount

def score_interval(i, j, k, l, g1, g2, dist, revDist):
    res = 0
    setDist = 0
    hitCount = 0
    g2_d = set()
    for x in range(i, j+1):
        if g1[x] in dist:
            res_1, sD1, hitCount_1 = __score_helper__(g2[k], g2[l], dist[g1[x]])
            res += res_1
            setDist += sD1
            hitCount += hitCount_1
        else:
            setDist += 1
    for y in range(k, l+1):
        if g2[y] in revDist:
            res_2, sD2, _ = __score_helper__(g1[i], g1[j], revDist[g2[y]])
            res += res_2
            setDist += sD2
            if sD2:
                g2_d.add(y)
        else:
            setDist += 1
            g2_d.add(y)
    return res, setDist, hitCount, g2_d


if __name__ == '__main__':

    usage = '%prog [options] <PAIRWISE DIST FILE>'
    parser = OptionParser(usage=usage)
    parser.add_option('-d', '--delta', dest='delta', default=0, type='int',
                      help='Approximate consimilar intervals parameter, ' + \
                              'allows for <delta> indels [default: %default]')
    parser.add_option('-m', '--min', dest='min', default=3, type='int',
                      help='Minimum size of intervals [default: %default]')

    (options, args) = parser.parse_args()

    if len(args) != 1:
        parser.print_help()
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.ERROR)
    ch.setFormatter(logging.Formatter('!! %(message)s'))
    cf = logging.FileHandler(LOG_FILENAME, mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(cf)
    LOG.addHandler(ch)

    f = args[0]
    isMultiChrom, g1, g2, dist = readDistsAndOrder(open(f), addTelomeres=False)

    if not dist:
        LOG.fatal('Distance table in file %s is empty. Exiting.' %f)
        exit(1)

    revDist = reverseDistMap(dist)

    pos1 = createPos(g1, g2, dist)
    pos2 = createPos(g2, g1, revDist)

    intervals1 = acsi(pos1, pos2, g1, g2, options)
    printResults(intervals1, g1, g2, dist, revDist, isMultiChrom, stdout)

