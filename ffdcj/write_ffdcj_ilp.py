#!/usr/bin/env python3
# -*- coding: utf-8
from itertools import chain
import networkx as nx
import argparse
import sys
import re


__author__ = 'pedro'

class GSGraph(nx.Graph):
    def add_genome_node(self,idx, genome):
        self.add_node(GSGraph.node_name(idx, genome), bipartite=genome)

    def add_genome_edge(self, g1, g2, w):
            self.add_genome_node(g1,0)
            self.add_genome_node(g2,1)
            self.add_edge(GSGraph.node_name(g1, 0), GSGraph.node_name(g2, 1), weight=w)

    @staticmethod
    def next_node(u):
        return u[0] + str(int(u[1:])+1)

    @staticmethod
    def previous_node(u):
        return u[0] + str(int(u[1:])-1)

    @staticmethod
    def node_name(idx, genome):
        if genome == 0:
            return "A%d" % idx
        else:
            return "B%d" % idx


class FFProblem():
    def __init__(self, filename, threshold=0.0):
        self.G = None
        self.n1 = 0
        self.n2 = 0
        self._read_ff_file(filename, threshold, isolated=True)

    def size(self, genome):
        return self.n1 if genome == 0 else self.n2

    def g1_degrees_sorted(self):
        g1_nodes = set(n for n, d in self.G.nodes(data=True) if d['bipartite'] == 0)
        return sorted(self.G.degree_iter(g1_nodes),key=itemgetter(1))

    def _read_ff_file(self, filename, threshold, isolated=True):
        G = GSGraph()
        last = 0
        max_g2 = 0
        g1 = 0  # will store the max g1 at end
        for l in open(filename):
            if not l.strip(): continue
            chr1, g1, chr2, g2, orient, w = l.split()
            g1 = int(g1)
            g2 = int(g2)
            w = float(w) * int(orient)
            if abs(w) < threshold:
                continue
            if isolated:
                if g1 > last:
                    for i in range(last + 1, g1 + 1):
                        G.add_genome_node(i,0)
                    last = g1
            G.add_genome_edge(g1,g2,w)
            if g2 > max_g2:
                max_g2 = g2
        if isolated:
            for i in range(1, max_g2 + 1):
                G.add_genome_node(i,1)
        self.G = G
        self.n1 = g1
        self.n2 = max_g2

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split('(\d+)', text) ]

def all_extremities(P):
    for g in range(2):
        for i in range(1, P.size(g) + 1):
            yield GSGraph.node_name(i, g) + "h"
            yield GSGraph.node_name(i, g) + "t"


def all_extremities_of_genome(P, g):
    for i in range(1, P.size(g) + 1):
        yield GSGraph.node_name(i, g) + "h"
        yield GSGraph.node_name(i, g) + "t"


def weighted_edge_var(u, v, w):
    if w > 0:
        return [edge_var(u, 't', v, 't'), edge_var(u, 'h', v, 'h')]
    else:
        return [edge_var(u, 't', v, 'h'), edge_var(u, 'h', v, 't')]


def edge_var(u, ext_u, v, ext_v):
    return "x_%s_%s" % tuple(sorted([u + ext_u, v + ext_v]))


def test_edge(G, a, b, positive, nodes, tolerance=1):
    v_a = GSGraph.node_name(a, 0)
    v_b = GSGraph.node_name(b, 1)
    if v_a in nodes or v_b in nodes or not G.has_edge(v_a, v_b):
        return False
        #print >> sys.stderr,v_a,v_b, "Edge"
    w = G[v_a][v_b]['weight']
    #print >> sys.stderr, w, positive
    if (w > 0 and positive) or (w < 0 and not positive): # either w>0 and positive == True, or w<0 and positive == False
        # test all neightbours
        for u in G.neighbors(v_a):
            w_u = G[v_a][u]['weight']
            if (w_u > 0 and positive or w_u < 0 and not positive) and abs(w_u) > abs(w) * tolerance:
                return False
        for u in G.neighbors(v_b):
            w_u = G[v_b][u]['weight']
            if (w_u > 0 and positive or w_u < 0 and not positive) and abs(w_u) > abs(w) * tolerance:
                return False
    return True


def fix_adjacencies(G, edges, nodes, tolerance=1):
    new_edges = []
    # print >> sys.stderr, "EDGES:", edges
    # print >> sys.stderr, "NODES:", nodes
    while len(edges) > 0:
        # pick one edge:
        a, b = edges.pop()
        # edge vertices are not always in A,B order, fix:
        if a[0] == 'B':
            b, a = a, b
            # get weight:
        w = G[a][b]['weight']
        a, b = map(int, [a[1:], b[1:]])
        if w > 0:
            adj = [(a + 1, b + 1, True), (a - 1, b - 1, True)]
        else:
            adj = [(a - 1, b + 1, False), (a + 1, b - 1, False)]
        for va, vb, positive in adj:
            r = test_edge(G, va, vb, positive, nodes, tolerance)
            if r:
                v_a = GSGraph.node_name(va, 0)
                v_b = GSGraph.node_name(vb, 1)
                # print >> sys.stderr, "add", v_a, v_b
                # remove edges:
                for u in G.neighbors(v_a):
                    if u != v_b: G.remove_edge(v_a, u)
                for u in G.neighbors(v_b):
                    if u != v_a: G.remove_edge(v_b, u)

                new_edges.append([v_a, v_b])
                edges.append([v_a, v_b])
                nodes.append(v_a)
                nodes.append(v_b)

    print("Fixed %d edges." % len(new_edges), file = sys.stderr)
    return new_edges

def generate_lp(P, alpha=0.5, self_edge_cost=0, MATCHING_SIZE=False, MAXIMAL_MATCHING=False, tolerance=0):
    G = P.G

    # Find K2 components, to fix edges:
    k2 = [c for c in nx.connected_components(G) if len(c) == 2]

    # try to fix adjacencies:
    if tolerance > 0:
        print("Fixing edges...", file = sys.stderr)
        new_edges = fix_adjacencies(G, list(k2), list(chain.from_iterable(k2)), tolerance)
        k2 += new_edges

    k2_vertices = list(chain.from_iterable(k2))

    # objective function:
    print("Printing ILP ...", file = sys.stderr)
    print("Minimize", file = sys.stdout)
    print("obj:", file = sys.stdout)
    if not MATCHING_SIZE:
        #print P.n1 + P.n2, "g",
        sys.stdout.write(str((P.n1 + P.n2)/2.0) + "g ") #new alpha

        # CHANGED: Kevin, 28.01.2016
        # Since we've implemented the model of blocks, we don't have to take the average of the length
        # of the two genomes. The smaller genome is good enough, because all "missing" genes might be
        # deleted with one InDel operation. Even if we need more InDels, this will be represented in
        # the sum over all l_i.
        # EDIT: This is also done by the option -ms.


        # print P.n1 if P.n1 < P.n2 else P.n2, "g",

        # Since the weight of "parallel edges" is the same, and I need to divide by 2, and also parallel edges are
    # forced to be together, I only include one of each in the OF (only edges[0] below)
    for u, v in G.edges():
        w = G[u][v]['weight']
        edges = weighted_edge_var(u, v, w)
        # edge_w = 2 - abs(w) if MATCHING_SIZE else -abs(w)
        edge_w = (1 if MATCHING_SIZE else 0) + (alpha-1)*abs(w)
        sys.stdout.write("%+f %s " % (edge_w, edges[0]))

    if self_edge_cost != 0:
        for g in range(2): # each genome
            for i in range(1, P.size(g) + 1):
                u = GSGraph.node_name(i, g)
                # ignore k2 vertices, they are already fixed;
                if u in k2_vertices:
                    continue
                sys.stdout.write("%+.2f %s " % ( self_edge_cost, edge_var(u,
                    "t", u, "h")))

    # print "-", " - ".join(["%.2f z_%s" % (cycle_weight, x) for x in all_extremities_of_genome(P, 0) if x[-1] == 'h']),
    sys.stdout.write(" - " + " - ".join(["%.3f z_%s" % (alpha, x) for x in
        all_extremities_of_genome(P, 0) if x[-1] == 'h']))
    if set(G.nodes).symmetric_difference(k2_vertices):
        sys.stdout.write(" + " + " + ".join("%.3f b_%s" % (alpha, x) for x in
            sorted(G.nodes) if not x in k2_vertices))
    print('', file = sys.stdout)

    # constraints:
    print("Subject To", file = sys.stdout)

    if not MATCHING_SIZE:
        print("g = 1", file = sys.stdout)

    ## Consistency:
    print("\ consistency (parallel edges)", file = sys.stdout)
    for v, u in G.edges():
        w = G[v][u]['weight']
        if u in k2_vertices and v in k2_vertices:
            for e in weighted_edge_var(u, v, w):
                print(e + " = 1", file = sys.stdout)
        else:
            print(" - ".join(weighted_edge_var(u, v, w)), " = 0", file =
                    sys.stdout)

    ## One edge per vertex:
    print("\ degree 1 for each vertex", file = sys.stdout)
    for g in range(2): # each genome
        for i in range(1, P.size(g) + 1):
            u = GSGraph.node_name(i, g)
            # ignore k2 vertices, they are already fixed;
            if u in k2_vertices:
                continue
            nodes_h = []
            nodes_t = []
            for v in G.neighbors(u):
                edges = weighted_edge_var(u, v, G[v][u]['weight'])
                nodes_t.append(edges[0])
                nodes_h.append(edges[1])
                # self edges: (edges to complete the matching for unsaturated vertices
            nodes_t.append(edge_var(u, "t", u, "h"))
            nodes_h.append(edge_var(u, "t", u, "h"))
            # print "# %st" % u
            print(" + ".join(nodes_t), " = 1", "    \\", u + "t", file = sys.stdout)
            # print "# %sh" % u
            print(" + ".join(nodes_h), " = 1", "    \\", u + "h", file = sys.stdout)

    # Maximal matching:
    if MAXIMAL_MATCHING:
        print("\ Maximal matching", file = sys.stdout)
        for u, v in G.edges():
            if u in k2_vertices:
                continue
            print("%s + %s <= 1" % (edge_var(u, "t", u, "h"), edge_var(v, "t", v, "h")), file = sys.stdout)

    # Adjacent vertices, same label:
    m = (P.n1 + P.n2) * 2
    print("\ Label - adj vertices with same label", file = sys.stdout)
    # matching edges:
    for u, v in G.edges():
        edges = weighted_edge_var(u, v, G[u][v]['weight'])
        for i in [0, 1]:
            x, v1, v2 = edges[i].split("_")
            # print
            if u in k2_vertices:
                print("y_%s - y_%s = 0" % (v1, v2), file = sys.stdout)
            else:
                print("y_%s - y_%s  + %d %s <= %d" % (v1, v2, m, edges[i], m), file = sys.stdout)
                print("y_%s - y_%s  + %d %s <= %d" % (v2, v1, m, edges[i], m), file = sys.stdout)
                # self edges:
    for g in range(2):
        for i in range(1, P.size(g) + 1):
            u = GSGraph.node_name(i, g)
            # k2 vertices do not have self edges:
            if u in k2_vertices:
                continue
            print("y_%sh - y_%st  + %d %s <= %d" % (u, u, m, edge_var(u, 'h',
                u, 't'), m), "\  Self edge for " + u, file = sys.stdout)
            print("y_%st - y_%sh  + %d %s <= %d" % (u, u, m, edge_var(u, 'h',
                u, 't'), m), file = sys.stdout)
            #        nodes_t.append(edge_var(u, "t", u, "h"))

    # adjacency edges:
    print('', file = sys.stdout)
    for g in range(2):
        for i in range(1, P.size(g)):
            print("y_%s - y_%s = 0" % (GSGraph.node_name(i, g) + "h", GSGraph.node_name(i + 1, g) + "t"), file = sys.stdout)
            ## Assuming circular genome:
        print("y_%s - y_%s = 0" % (GSGraph.node_name(P.size(g), g) + "h", GSGraph.node_name(1, g) + "t"), file = sys.stdout)

    # cycle counter:
    for c, e in enumerate(all_extremities_of_genome(P, 0)):
        if e[-1] == 't': continue
        print("%d z_%s - y_%s <= 0" % (c + 1, e, e), file = sys.stdout)

    print('', file = sys.stdout)

    for g in range(2):
        for i in range(1, P.size(g)+1):
            currentGene = GSGraph.node_name(i,g)
            if not (currentGene in k2_vertices):
                if i == P.size(g):
                    #continue
                    nextGene = GSGraph.node_name(1,g)
                else:
                    nextGene = GSGraph.node_name(i+1,g)
                if not nextGene in k2_vertices:
                    print("%s - %s - b_%s <= 0" % (edge_var(currentGene, 'h',currentGene, 't'), edge_var(nextGene,'h',nextGene,'t'), currentGene), file = sys.stdout)
                else:
                    print("%s - b_%s = 0" % (edge_var(currentGene, 'h',currentGene, 't'), currentGene), file = sys.stdout)
        print('', file = sys.stdout)

    # Bounds:
    print('', file = sys.stdout)
    print("Bounds", file = sys.stdout)
    # Node labels:
    c = 1
    for e in all_extremities(P):
        print("y_%s <= %d" % (e, c), file = sys.stdout)
        c += 1

    print('', file = sys.stdout)
    ##### Variable types:
    print("Binary", file = sys.stdout)
    # matching edges
    for u, v in G.edges():
        w = G[u][v]['weight']
        edges = weighted_edge_var(u, v, w)
        print("\n".join(edges), file = sys.stdout)
        # self edges:
    for g in range(2):
        for i in range(1, P.size(g) + 1):
            u = GSGraph.node_name(i, g)
            # k2 vertices do not have self edges:
            if u in k2_vertices:
                continue
            print(edge_var(u, 'h', u, 't'), file = sys.stdout)
        # z_ : cycle counters (only vertices in A of type 'head')
    print("\n".join("z_" + x for x in all_extremities_of_genome(P, 0) if x[-1] == 'h'), file = sys.stdout)
    print('', file = sys.stdout)
    print("\n".join("b_" + x for x in sorted(G.nodes) if x not in k2_vertices), file = sys.stdout)
    if not MATCHING_SIZE:
        print("g", file = sys.stdout)
    print("General", file = sys.stdout)
    print("\n".join("y_" + x for x in all_extremities(P)), file = sys.stdout)
    print('', file = sys.stdout)
    print("End", file = sys.stdout)


if __name__ == '__main__':


    parser = argparse.ArgumentParser(description='Generates a FFDCJ ILP from a gene similarity file.')
    parser.add_argument('-s', default=0.0, type=float, help="Self edge weight")
    parser.add_argument('-a', '--alpha', default=0.5, type=float, help="Alpha coefficient for the rearrangement/sequence balance")
    parser.add_argument('-x', '--fix_adj_tolerance', default=0, type=float,
                        help="Weight tolerance for fixing adjacencies.")
    parser.add_argument('-max', '--maximal', action="store_true", default=False, help="Force maximal matchings")
    parser.add_argument('-ms', '--matching_size', action="store_true", default=False, help="Matching size in the F.O.")

    parser.add_argument("file", help="Gene similarity file")

    param = parser.parse_args()
    print(param, file = sys.stderr)

    #Read problem
    P = FFProblem(param.file)

    # print ILP:
    print( "Generating ILP...", file = sys.stderr)
    generate_lp(P, self_edge_cost=param.s, alpha=param.alpha, MATCHING_SIZE=param.matching_size,
                MAXIMAL_MATCHING=param.maximal, tolerance=param.fix_adj_tolerance)

