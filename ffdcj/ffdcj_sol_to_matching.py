#!/usr/bin/env python3
# -*- coding: utf-8
from sys import stdout, stderr
import argparse

__author__ = 'pedro'
import xml.etree.ElementTree as et
import re

#x_A569t_B768t
EDGE_PATTERN = re.compile(r'^x_A([0-9]+)t_B([0-9]+)([ht])')

def readVariablesCPLEX(filename):
    sol_list = list()
    print("Opening ...", file = stderr)
    tree = et.parse(filename)
    root = tree.getroot()
    print("Parsing ...", file = stderr)
    best_obj = None
    solutions = root.iterfind('./CPLEXSolution') if root.tag == 'CPLEXSolutions' else [root]
    sol = solutions[0]
    obj = float(sol[0].attrib['objectiveValue'])
    res = {}
    for var in sol.iterfind('./variables/variable'):
        name = var.get('name')
        value = int(round(float(var.get('value'))))
        if abs(1 - value) > 0.1:
            continue

        m = EDGE_PATTERN.match(name)
        if m:
            (g1, g2, ext2) = m.groups()
            g1, g2 = map(int, (g1,g2))
            res[g1] = g2

    import pdb; pdb.set_trace()
    return res

def readVariablesGurobi(filename):
    print("Opening ...", file=stderr)
    fmst = open(filename)
    print("Parsing ...", file=stderr)
    match = dict()
    for l in fmst:
        l = l.strip()
        if not l.startswith('x_'):
            continue

        name, value = l.split()
        value = float(value)

        if value < 0.01:  # x edge not chosen, 0 may be 0.00001
            continue

        g1, g2 = name[2:].split('_')

        genome1, num1, ext1 = g1[0], g1[1:-1], g1[-1]   # gene number, genome and extremity type
        genome2, num2, ext2 = g2[0], g2[1:-1], g2[-1]

        #import pdb; pdb.set_trace() # TODO: on write ilp, make k2 list empty to see if the ilp generated is the same

        if genome1 == genome2:  # indel or adjacency edge
            continue

        match[int(num1)] = int(num2)

    fmst.close()

    return match

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Outputs a gene matching from an ILP solution file.')
    parser.add_argument("--gurobi", help="Gurobi solution file.")
    parser.add_argument("--cplex", help="CPLEX solution file.")
    parser.add_argument("pwfile", help="Pairwise similarities file.")

    param = parser.parse_args()

    if not param.gurobi and not param.cplex:
        print('Error: one of --gurobi or --cplex must be provided.', file=stderr)
        exit(1)

    if param.cplex:
        result = readVariablesCPLEX(param.cplex)
    else:
        result = readVariablesGurobi(param.gurobi)
    pw = {}
    for l in open(param.pwfile):
        if not l.strip(): continue
        chr1, g1, chr2, g2, orient, w = l.split()
        pw["A%sB%s" % (g1,g2)] = (orient,w)

    for g1 in sorted(result):
        orient, w = pw["A%sB%s" % (g1, result[g1])]
        print("%d\t%d\t%s\t%s" % (g1, result[g1], orient, w), file = stdout)


