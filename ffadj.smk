# Workflows for Family-Free Adjacencies

FFADJ_PYEXEC = ' '.join(filter(None, [config['python_bin'], config['ff_bin'] + 'ffadj/']))

ENUM_ADJS_OUT = '%s/%s' %(config['enum_adjs_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR])))
MATCH_ADJS_OUT = '%s/%s' %(config['match_simple_adjs_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR])))

FFADJ_OUT = '%s/ffadj_%s' %(config['ffadj_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR, 'a%s' %config['ffadj_alpha']])))
FFADJMCS_OUT = '%s/ffadjmcs_%s' %(config['ffadj_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR, 'a%s' %config['ffadj_alpha']])))

rule all_ffadj_exact:
    input:
        expand(FFADJ_OUT + '/{pw_dist}.active.sim', pw_dist=PW_SIMS)

rule all_ffadj_heuristic:
    input:
        expand(FFADJMCS_OUT + '/{pw_dist}.active.sim', pw_dist=PW_SIMS)

rule all_total_adjs:
    input:
        expand(ENUM_ADJS_OUT + '/{pw_dist}.adj', pw_dist=PW_SIMS)

rule all_adjs_matching:
    input:
        expand(MATCH_ADJS_OUT + '/{pw_dist}.adj', pw_dist=PW_SIMS)

rule enumerate_adjs:
    input:
        PW_OUT + '/{pw_sim}.active.sim'
    output:
        ENUM_ADJS_OUT + '/{pw_sim}.adj'
    shell:
        FFADJ_PYEXEC + 'enumerate_adjs' + PYSUF + ' {input} > {output}'

rule match_simple_adjs:
    input:
        ENUM_ADJS_OUT + '/{pw_sim}.adj'
    output:
        MATCH_ADJS_OUT + '/{pw_sim}.adj'
    shell:
        FFADJ_PYEXEC + 'match_simple_adjs' + PYSUF + ' {input} > {output}'

rule identify_anchors:
    input:
        PW_OUT + '/{pw_sim}.active.sim'
    params:
        alpha = config['ffadj_alpha']
    output:
        FFADJ_OUT + '/{pw_sim}.anchors'
    shell:
        FFADJ_PYEXEC + 'identify_anchors' + PYSUF + ' -a {params.alpha} {input} > {output}'

rule run_write_ffadj_ilp:
    input:
        pw_sim = PW_OUT + '/{pw_sim}.active.sim',
        pw_anchors = FFADJ_OUT + '/{pw_sim}.anchors'
    params:
        alpha = config['ffadj_alpha']
    output:
        FFADJ_OUT + '/{pw_sim}.lp'
    shell:
        FFADJ_PYEXEC + 'write_ffadj_ilp' + PYSUF + ' -a {params.alpha} -f {input.pw_anchors} '
        '{input.pw_sim} > {output}'

rule run_ffadj_cplex:
    input:
        FFADJ_OUT + '/{pw_sim}.lp'
    params:
        time = config['solver_time'],
        memory = config['solver_memory']
    output:
        FFADJ_OUT + '/{pw_sim}.sol'
    threads:
        config['solver_cpus']
    shell:
        FF_PYSCRIPTS + 'run_cplex' + PYSUF + ' -t {threads} -m ' 
        '{params.memory} -l {params.time} -L {input}'

rule run_ffadj_gurobi:
    input:
        FFADJ_OUT + '/{pw_dist}.lp'
    params:
        threads = config['solver_cpus'],
        time = config['solver_time'],
        gap = ('%g' % float(config['solver_gap'])) if 'solver_gap' in config else ''
    output:
        FFADJ_OUT + '/{pw_dist}.mst'
    threads:
        int(config['solver_cpus'])
    shell:
        'THREADS={params.threads} GAP={params.gap} LIMIT={params.time} %s/run_gurobi_local.sh {input}' % SCRIPTS_DIR

rule ffadj_sol_to_matching:
    input:
        sol = FFADJ_OUT + '/{pw_sim}.%s' % ('mst' if SOLVER == 'gurobi' else 'sol'),
        pw_sim = PW_OUT + '/{pw_sim}.active.sim'
    output:
        FFADJ_OUT + '/{pw_sim}.active.sim'
    shell:
        FFADJ_PYEXEC + 'ffadj_sol_to_matching' + PYSUF + ' {input.sol} {input.pw_sim};'
        'mv {input.sol}.{wildcards.pw_sim} {output}'

rule run_ffadj_mcs:
    input:
        PW_OUT + '/{pw_sim}.active.sim'
    output:
        FFADJMCS_OUT + '/{pw_sim}.active.sim'
    shell:
        FFADJ_PYEXEC + 'ffadj_mcs' + PYSUF + ' {input} > {output}'

