# Workflows for FFDCJID (Family-Free DCJ InDel) and DIFFMGC (orthology prediction)

# Copyright 2021,2022 Diego Rubert
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software. If not, see <https://www.gnu.org/licenses/>

FFDCJID_PYEXEC = ' '.join(filter(None, [config['python_bin'], config['ff_bin'] + 'ffdcjid/']))

FFDCJID_DEFAULT_ALPHA = '1.0'
FFDCJID_ALPHA = config.get('ffdcjid_alpha', FFDCJID_DEFAULT_ALPHA)
FFDCJID_MAXIMAL_MATCHING = config.get('ffdcjid_force_maximal_matching', False)

HCAPPING_ABS = config.get('ffdcjid_heuristic_capping_absolute_threshold', '')
HCAPPING_REL = config.get('ffdcjid_heuristic_capping_relative_threshold', '')
CAPPING_STR = config.get('ffdcjid_heuristic_capping', True) and 'HC' or None
if CAPPING_STR and HCAPPING_ABS: CAPPING_STR += 'a%d' % HCAPPING_ABS
if CAPPING_STR and HCAPPING_REL: CAPPING_STR += 'r%g' % HCAPPING_REL

FFDCJID_OUT_ILP = '%s/%s' % (config['ffdcjid_out'], '_'.join(filter(None, [ALIGN_PARSTR,
                                                                           PW_DIR,
                                                                      'a%s' % FFDCJID_ALPHA,
                                                                      'mm' if FFDCJID_MAXIMAL_MATCHING else None,
                                                                           CAPPING_STR
                                                                           ])))
FFDCJID_OUT_H = '%s/%s' %(config['ffdcjid_out'], '_'.join(filter(None, [ALIGN_PARSTR,
                                                                        PW_DIR,
                                                                        'H'
                                                                        ])))
USE_FFDCJID_HEURISTIC = config.get('ffdcjid_heuristic', False)
FFDCJID_OUT = FFDCJID_OUT_H if USE_FFDCJID_HEURISTIC else FFDCJID_OUT_ILP
FFDCJID_FAM_LEVEL = config['ffdcjid_families_level']
FFDCJID_FAM_0 = FFDCJID_OUT + '/families.l0'
FFDCJID_FAM_1 = FFDCJID_OUT + '/families.l1'
FFDCJID_FAM_2 = FFDCJID_OUT + '/families.l2'
FFDCJID_PLAINTXT_FMT = config.get('ffdcjid_families_plaintext_format_option', '')
MCL_INFLATION = config['mcl_inflation'] if isinstance(config['mcl_inflation'], list) else [ config['mcl_inflation'] ]

FFDCJID_H_BIN = config.get('ffdcjid_heuristic_bin', 'ffdcjid-heuristic')

USE_SIF = False  # otherwise, abc (recent mcl versions have a bug when using .sif as input)


###########
# FFDCJID #
###########

rule ffdcjid_distances:
    input:
        expand('%s/{pw_dist}.dst.info' % FFDCJID_OUT, pw_dist=PW_SIMS)

rule ffdcjid_results:
    input:
        expand('%s/{pw_dist}.txt.sim' % FFDCJID_OUT, pw_dist=PW_SIMS),
        expand('%s/{pw_dist}.dst.info' % FFDCJID_OUT, pw_dist=PW_SIMS)

rule ffdcjid_families:
    input:
        expand('%s.{ext}' % FFDCJID_FAM_0, ext=['xml', 'log', 'txt']),
        expand('%s.{ext}' % FFDCJID_FAM_1, ext=['xml', 'log', 'txt', 'ambiguous.txt', 'resolved.txt'] if FFDCJID_FAM_LEVEL > 0 else []),
        expand('%s/{pw_dist}.txt.sim' % FFDCJID_OUT, pw_dist=PW_SIMS if FFDCJID_FAM_LEVEL > 0 else []),
        expand('%s.I{inflation}.{ext}' % FFDCJID_FAM_2, inflation=MCL_INFLATION, ext=['xml', 'log', 'txt'] if FFDCJID_FAM_LEVEL > 1 else [])

rule ffdcjid_heuristic:
    input:
        genomes_cfg = GENOMES_DIR + '/genomes.cfg',
        pw_sim = PW_OUT + '/{g1}_{g2}.sim'
    output:
        matching = FFDCJID_OUT_H + '/{g1}_{g2}.sim',
        info = report(FFDCJID_OUT_H + '/{g1}_{g2}.dst.info', category='FFDCJID Distances')
    shell:
        FFDCJID_H_BIN + ' -A {wildcards.g1} -B {wildcards.g2} -C {input.genomes_cfg} '
        '--linear --mixed-matching -o {output.matching} -d {output.info} {input.pw_sim} '

rule all_ffdcjid_ilps:
    input:
        expand('%s/{pw_dist}.lp' % FFDCJID_OUT_ILP, pw_dist=PW_SIMS)

rule run_write_ffdcjid_ilp:
    input:
        genomes_cfg = GENOMES_DIR + '/genomes.cfg',
        pw_sim = PW_OUT + '/{g1}_{g2}.sim'
    params:
        alpha = FFDCJID_ALPHA,
        matching = 'max' if FFDCJID_MAXIMAL_MATCHING else 'any',
        heuristic = '--heuristic-capping' if config.get('ffdcjid_heuristic_capping', True) else '',
        absolute = '--absolute-threshold %d' % HCAPPING_ABS if HCAPPING_ABS else '',
        relative = '--relative-threshold %g' % HCAPPING_REL if HCAPPING_REL else ''
    output:
        lp = FFDCJID_OUT_ILP + '/{g1}_{g2}.lp',
        mipstart = FFDCJID_OUT_ILP + '/{g1}_{g2}.initial.mst',
        log = FFDCJID_OUT_ILP + '/{g1}_{g2}.ilog',
        gidmap = FFDCJID_OUT_ILP + '/{g1}_{g2}.gidmap',
        alpha = (FFDCJID_OUT_ILP + '/{g1}_{g2}.lp.alpha') if FFDCJID_ALPHA == 'auto' else expand([]),
    shell:
        FFDCJID_PYEXEC + 'write_ffdcjid_ilp' + PYSUF + ' -c '
        '-1 {wildcards.g1} -2 {wildcards.g2} -C {input.genomes_cfg} '
        '-S {input.pw_sim} -u {output.gidmap} -l {output.log} -o {output.lp} '
        '-a {params.alpha} -M {params.matching} -I {output.mipstart} '
        '{params.heuristic} {params.absolute} {params.relative}'

rule run_ffdcjid_cplex:
    input:
        lp = FFDCJID_OUT_ILP + '/{pw_dist}.lp',
        mipstart= FFDCJID_OUT_ILP + '/{pw_dist}.initial.mst'
    params:
        threads = config['solver_cpus'],
        time = config['solver_time'],
        memory = config['solver_memory'],
        gapopt = ('-g %g' % float(config['solver_gap'])) if 'solver_gap' in config else ''
    output:
        FFDCJID_OUT_ILP + '/{pw_dist}.sol',
        report(FFDCJID_OUT_ILP + '/{pw_dist}.log', category='FFDCJID ILPs') if SOLVER == 'cplex' else expand([])
    benchmark:
        FFDCJID_OUT_ILP + '/{pw_dist}.benchmark.txt'
    threads:
        int(config['solver_cpus'])
    shell:
        FF_PYSCRIPTS + 'run_cplex' + PYSUF + ' -t {params.threads} -m '
        '{params.memory} -l {params.time} {params.gapopt} -L {input.lp}'

rule run_ffdcjid_gurobi:
    input:
        lp = FFDCJID_OUT_ILP + '/{pw_dist}.lp',
        mipstart = FFDCJID_OUT_ILP + '/{pw_dist}.initial.mst'
    params:
        threads = config['solver_cpus'],
        time = config['solver_time'],
        gap = ('%g' % float(config['solver_gap'])) if 'solver_gap' in config else ''
    output:
        FFDCJID_OUT_ILP + '/{pw_dist}.mst',
        report(FFDCJID_OUT_ILP + '/{pw_dist}.log', category='FFDCJID ILPs') if SOLVER == 'gurobi' else expand([]),
    benchmark:
        FFDCJID_OUT_ILP + '/{pw_dist}.benchmark.txt'
    threads:
        int(config['solver_cpus'])
    shell:
        'THREADS={params.threads} GAP={params.gap} LIMIT={params.time} %s/run_gurobi_local.sh {input.lp}' % SCRIPTS_DIR

rule ffdcjid_sol_to_matching:
    input:
        genomes_cfg = GENOMES_DIR + '/genomes.cfg',
        sim = PW_OUT + '/{pw_dist}.sim',
        gidmap = FFDCJID_OUT_ILP + '/{pw_dist}.gidmap',
        sol = FFDCJID_OUT_ILP + '/{pw_dist}.' + SOLVER_EXT
    output:
        FFDCJID_OUT_ILP + '/{pw_dist}.sim'
    shell:
        FFDCJID_PYEXEC + 'ffdcjid_sol2matching' + PYSUF + ' -C {input.genomes_cfg} '
        '-i {input.sim} --%s {input.sol} -g {input.gidmap} -o {output}' % SOLVER_EXT

rule ffdcjid_sol_to_matching_textual:
    input:
        genomes_cfg = GENOMES_DIR + '/genomes.cfg',
        matching = FFDCJID_OUT + '/{pw_dist}.sim'
    output:
        FFDCJID_OUT + '/{pw_dist}.txt.sim'
    shell:
        FFDCJID_PYEXEC + 'ffdcjid_sim2translatedsim' + PYSUF + ' -C {input.genomes_cfg} '
        '-i {input.matching} -o {output} %s ' % FFDCJID_PLAINTXT_FMT

rule ffdcjid_distance_info:
    input:
        genomes_cfg = GENOMES_DIR + '/genomes.cfg',
        sim = PW_OUT + '/{pw_dist}.sim',
        gidmap = FFDCJID_OUT_ILP + '/{pw_dist}.gidmap',
        sol = FFDCJID_OUT_ILP + '/{pw_dist}.' + SOLVER_EXT,
        log = FFDCJID_OUT_ILP + '/{pw_dist}.log',
        alpha = FFDCJID_OUT_ILP + '/{pw_dist}.lp.alpha' if FFDCJID_ALPHA == 'auto' else expand([])
    output:
        report(FFDCJID_OUT_ILP + '/{pw_dist}.dst.info', category='FFDCJID Distances')
    params:
        alpha = FFDCJID_ALPHA
        #alpha = FFDCJID_ALPHA if FFDCJID_ALPHA != 'auto' else get_alpha_from_file
        #FFDCJID_ALPHA if FFDCJID_ALPHA != 'auto' else float(open(expand(FFDCJID_OUT + '/{g1}_{g2}.alpha', allow_missing=True)[0]).readline())
    shell:
        FFDCJID_PYEXEC + 'ffdcjid_parse_sol' + PYSUF + ' -C {input.genomes_cfg} '
        '-i {input.sim} -g {input.gidmap} --%s {input.sol} -l {input.log} ' % SOLVER_EXT +
        '-o {output} ' + ('-a {params.alpha}' if FFDCJID_ALPHA != 'auto' else '-a $(cat {input.alpha})')


####################
# LEVEL 0 FAMILIES #
####################

rule ffdcjid_families_level_0:
    input:
        genomes_cfg = GENOMES_DIR + '/genomes.cfg',
        sim = expand(PW_OUT + '/{pair}.sim', pair=PW_SIMS+SELF_SIMS)
    params:
        level = 0
    output:
        families = FFDCJID_FAM_0 + '.xml',
        log = report(FFDCJID_FAM_0 + '.log', category='FFDCJID Families'),
        sif = FFDCJID_FAM_0 + '.sif',
        abc = FFDCJID_FAM_0 + '.abc',
        txt = FFDCJID_FAM_0 + '.txt'
    shell:
        FFDCJID_PYEXEC + 'ffdcjid_families' + PYSUF + ' -C {input.genomes_cfg} '
        '-i {input.sim} -L {params.level} -a {output.abc} '
        '%s -f {output.families} -l {output.log} -S {output.sif} -T {output.txt} '
        % FFDCJID_PLAINTXT_FMT

####################
# LEVEL 1 FAMILIES #
####################

rule ffdcjid_families_level_1:
    input:
        genomes_cfg = GENOMES_DIR + '/genomes.cfg',
        sim = expand(PW_OUT + '/{pair}.sim', pair=PW_SIMS+SELF_SIMS),
        sol = expand('%s/{pw_dist}.%s' % (FFDCJID_OUT, SOLVER_EXT if not USE_FFDCJID_HEURISTIC else 'sim'), pw_dist=PW_SIMS),
        gidmap = expand('%s/{pw_dist}.gidmap' % FFDCJID_OUT, pw_dist=PW_SIMS) if not USE_FFDCJID_HEURISTIC else expand([]),
    params:
        level = 1
    output:
        families = FFDCJID_FAM_1 + '.xml',
        log = report(FFDCJID_FAM_1 + '.log', category='FFDCJID Families'),
        sif = FFDCJID_FAM_1 + '.sif',
        sif_ambiguous = FFDCJID_FAM_1 + '.ambiguous.sif',
        sif_resolved = FFDCJID_FAM_1 + '.resolved.sif',
        abc = FFDCJID_FAM_1 + '.abc',
        abc_ambiguous = FFDCJID_FAM_1 + '.ambiguous.abc',
        abc_resolved = FFDCJID_FAM_1 + '.resolved.abc',
        txt = FFDCJID_FAM_1 + '.txt',
        txt_ambiguous = FFDCJID_FAM_1 + '.ambiguous.txt',
        txt_resolved = FFDCJID_FAM_1 + '.resolved.txt',
        dump = FFDCJID_FAM_1 + '.dump',
        dump_resolved = FFDCJID_FAM_1 + '.resolved.dump',
    shell:
        FFDCJID_PYEXEC + 'ffdcjid_families' + PYSUF + ' -C {input.genomes_cfg} '
        '-i {input.sim} -L {params.level} --%s {input.sol} '
        '%s -f {output.families} -l {output.log} -S {output.sif} -a {output.abc} -T {output.txt} '
        '-D {output.dump} --ambiguous-extra --resolved-extra --resolved-complete-extra '
        % (SOLVER_EXT if not USE_FFDCJID_HEURISTIC else 'matching', FFDCJID_PLAINTXT_FMT)


####################
# LEVEL 2 FAMILIES #
####################

rule merge_pre_ffdcjid_families_level_2:
    input:
        l2_from_ambiguous_l1 = FFDCJID_FAM_2 + '.I{inflation}.from_ambiguous_l1.dump',
        resolved_l1 = FFDCJID_FAM_1 + '.resolved.dump'
    output:
        FFDCJID_FAM_2 + '.I{inflation}.dump'
    shell:
        'cat {input.resolved_l1} {input.l2_from_ambiguous_l1} > {output}'

rule ffdcjid_families_level_2:
    input:
        genomes_cfg = GENOMES_DIR + '/genomes.cfg',
        sim = expand(PW_OUT + '/{pair}.sim', pair=PW_SIMS+SELF_SIMS),
        sol = expand('%s/{pw_dist}.%s' % (FFDCJID_OUT, SOLVER_EXT if not USE_FFDCJID_HEURISTIC else 'sim'), pw_dist=PW_SIMS),
        gidmap = expand('%s/{pw_dist}.gidmap' % FFDCJID_OUT, pw_dist=PW_SIMS) if not USE_FFDCJID_HEURISTIC else expand([]),
        dump = FFDCJID_FAM_2 + '.I{inflation}.dump'
    params:
        level = 2
    output:
        families = FFDCJID_FAM_2 + '.I{inflation}.xml',
        log = report(FFDCJID_FAM_2 + '.I{inflation}.log', category='FFDCJID Families'),
        sif = FFDCJID_FAM_2 + '.I{inflation}.sif',
        abc = FFDCJID_FAM_2 + '.I{inflation}.abc',
        txt = FFDCJID_FAM_2 + '.I{inflation}.txt'
    shell:
        FFDCJID_PYEXEC + 'ffdcjid_families' + PYSUF + ' -C {input.genomes_cfg} '
        '-i {input.sim} -L {params.level} --%s {input.sol} '
        '%s -f {output.families} -l {output.log} -S {output.sif} -a {output.abc} -T {output.txt} '
        '-d {input.dump} --ambiguous-extra --resolved-extra --resolved-complete-extra '
        % (SOLVER_EXT if not USE_FFDCJID_HEURISTIC else 'matching', FFDCJID_PLAINTXT_FMT)


#############
# MCL STUFF #
#############

# we generate an .clusters.mci for level0 to compare with the reference clustering
# and see if levels 1 and 2 improve, but only if we get to level 2 and have mcl
if USE_SIF:
    rule ffdcjid_mcl_convert_edges_0_sif:
        input:
            FFDCJID_FAM_0 + '.sif'
        output:
            tab = FFDCJID_FAM_0 + '.tab',
            mci = FFDCJID_FAM_0 + '.edges.mci',
            clustering = FFDCJID_FAM_0 + '.clusters.mci',
            tmp = temp(FFDCJID_FAM_0 + '.sif.no-colon')
        shell:
            # need to remove colons (we'll replace by @) from gene identifiers in .sif file,
            # otherwise mcxload will mess up everything because of --expect-values
            'sed \'s/\([0-9]\+\):\([0-9]\+\)/\\1@\\2/g\' {input} > {output.tmp} '
            # the mci generated by mcxload is a matrix of edges,
            # however we need a matrix of clusters to compare to
            # level 2 results, so we run our own script to generate
            # that mci file with clusters (--expect-values is needed
            # only for reading .sif files with edge weights)
            '&& mcxload -sif {output.tmp} --expect-values --stream-mirror '
            '-write-tab {output.tab} -o {output.mci} '
            # place colons back into tab file
            '&& sed -i \'s/\([0-9]\+\)@\([0-9]\+\)/\\1:\\2/g\' {output.tab} '
            '&& ' + FFDCJID_PYEXEC + 'ffdcjid_edges2mci' + PYSUF + ' '
            '--sif {input} --tab {output.tab} --mci {output.clustering} '
            '--ignore-absent'
else:
    rule ffdcjid_mcl_convert_edges_0_abc:
        input:
            FFDCJID_FAM_0 + '.abc'
        output:
            tab = FFDCJID_FAM_0 + '.tab',
            mci = FFDCJID_FAM_0 + '.edges.mci',
            clustering = FFDCJID_FAM_0 + '.clusters.mci',
        shell:
            'mcxload -abc {input} --expect-values --stream-mirror '
            '-write-tab {output.tab} -o {output.mci} '
            '&& ' + FFDCJID_PYEXEC + 'ffdcjid_edges2mci' + PYSUF + ' '
                                                                   '--abc {input} --tab {output.tab} --mci {output.clustering} '
                                                                   '--ignore-absent'

# same as above, but for level 1, and only for ambiguous families (that will be disambiguated by mcl)
if USE_SIF:
    rule ffdcjid_mcl_convert_edges_1_sif:
        input:
            sif = FFDCJID_FAM_1 + '.ambiguous.sif',
            tab = FFDCJID_FAM_0 + '.tab' # need to use the same translation table to keep all data consistent
        output:
            mci = FFDCJID_FAM_1 + '.ambiguous.edges.mci',
            clustering = FFDCJID_FAM_1 + '.ambiguous.clusters.mci',
            tmpsif = temp(FFDCJID_FAM_1 + '.ambiguous.sif.no-colon'),
            tmptab = temp(FFDCJID_FAM_0 + '.tab.no-colon')
        shell:
            # need to remove colons as in ffdcjid_mcl_convert_edges_0
            'sed \'s/\([0-9]\+\):\([0-9]\+\)/\\1@\\2/g\' {input.sif} > {output.tmpsif} '
            # will also need a temporary .tab with replaced colons
            '&& sed \'s/\([0-9]\+\):\([0-9]\+\)/\\1@\\2/g\' {input.tab} > {output.tmptab} '
            # we use as input the .tab map generated at ffdcjid_mcl_convert_edges_0
            '&& mcxload -sif {output.tmpsif} --expect-values --stream-mirror '
            '-strict-tab {output.tmptab} -o {output.mci} '
            '&& ' + FFDCJID_PYEXEC + 'ffdcjid_edges2mci' + PYSUF + ' '
            '--sif {input.sif} --tab {input.tab} --mci {output.clustering} '
            '--ignore-absent'
else:
    rule ffdcjid_mcl_convert_edges_1_abc:
        input:
            abc = FFDCJID_FAM_1 + '.ambiguous.abc',
            tab = FFDCJID_FAM_0 + '.tab' # need to use the same translation table to keep all data consistent
        output:
            mci = FFDCJID_FAM_1 + '.ambiguous.edges.mci',
            clustering = FFDCJID_FAM_1 + '.ambiguous.clusters.mci',
        shell:
            # we use as input the .tab map generated at ffdcjid_mcl_convert_edges_0
            'mcxload -abc {input.abc} --expect-values --stream-mirror '
            '-strict-tab {input.tab} -o {output.mci} '
            '&& ' + FFDCJID_PYEXEC + 'ffdcjid_edges2mci' + PYSUF + ' '
            '--abc {input.abc} --tab {input.tab} --mci {output.clustering} '
            '--ignore-absent'

# everything related to mcl (level 2) assumes level 1 is computed (no sense going from 0 to 2) 
rule run_ffdcjid_mcl:
    input:
        mci = FFDCJID_FAM_1 + '.ambiguous.edges.mci'
    output:
        FFDCJID_FAM_2 + '.I{inflation,[0-9.]+}.from_ambiguous_l1.mci'
    params:
        inflation = '{inflation}'
    shell:
        'mcl {input} -I {params.inflation} -o {output}'

# not needed anymore, now generated by ffdcjid_families.py
#rule run_ffdcjid_mcl_dump_0_1:
#    input:
#        tab = FFDCJID_FAM_0 + '.tab',
#        mci = FFDCJID_OUT + '/families.l{level}.clusters.mci'
#    output:
#        FFDCJID_OUT + '/families.l{level,\d+}.dump'
#    shell:
#        'mcxdump -icl {input.mci} -tabr {input.tab} -o {output}'

rule run_ffdcjid_mcl_dump_2:
    input:
        tab = FFDCJID_FAM_0 + '.tab',
        mci = FFDCJID_FAM_2 + '.I{inflation}.from_ambiguous_l1.mci'
    output:
        FFDCJID_FAM_2 + '.I{inflation,[0-9.]+}.from_ambiguous_l1.dump'
    shell:
        'mcxdump -icl {input.mci} -tabr {input.tab} -o {output}'

rule run_ffdcjid_mcl_all: # run to get just the various mcl clusterizations (families level 2 required)
    input:
        FFDCJID_FAM_0 + '.clusters.mci',
        FFDCJID_FAM_1 + '.ambiguous.clusters.mci',
        expand('%s.I{inflation}.from_ambiguous_l1.dump' % FFDCJID_FAM_2, inflation=MCL_INFLATION)



