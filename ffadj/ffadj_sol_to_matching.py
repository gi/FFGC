#!/usr/bin/env python3

from sys import stdout, stderr, exit, argv, path
from itertools import product, chain
from os.path import basename, dirname, abspath, splitext
import xml.etree.ElementTree as et
from math import sqrt
import logging
import csv
import re

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDistsAndOrder, \
        reverseDistMap, DIRECTION_CRICK_STRAND, DIRECTION_WATSON_STRAND, \
        TELOMERE_START, TELOMERE_END

VARIABLE_PATTERN = re.compile(r'^Variable ([^_]+) has value ([0-9.]+)$')
EDGE_PATTERN = re.compile(r'^a_(\w+)_([^_]+)_(\w+)_([^_]+)_([0-9]+)_([0-9]+)$')
NODE_PATTERN = re.compile(r'^b_(\w+)_([^_]+)_([0-9]+)$')
ADJ_PATTERN = re.compile(r'^d_(\w+)_([^_]+)_(\w+)_([^_]+)_([0-9]+)_([0-9]+)_([0-9]+)_([0-9]+)$')

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]


def readVariables(fname):
    data = open(fname)

    res = dict()
    forbidden_edges = set()
    forbidden_nodes = set()
    adjs = set()
    nodes = set()

    soltype = splitext(fname)[1][1:]
    variables = []
    if soltype == 'sol':
        tree = et.parse(data)
        root = tree.getroot()
        for var in root.iterfind('./variables/variable'):
            name = var.get('name')
            value = int(round(float(var.get('value'))))
            variables.append((name, value))
    else:  # soltype == 'mst':
        for line in data:
            if len(line) < 2 or line.startswith('#'):
                continue
            name, value = line.split()
            value = int(round(float(value)))
            if value > 0:
                variables.append((name, value))

    for name, value in variables:
        m = EDGE_PATTERN.match(name)
        if m:
            (G1, chr1, G2, chr2, g1i, g2k) = m.groups()
            g1i, g2k = list(map(int, (g1i, g2k)))
#            if g1i == 0:
#                g1i = -1
#            if g2k == 0:
#                g2k = -1

            G1 = G1.replace('abcdefghijklmnopqrs', '-')
            G2 = G2.replace('abcdefghijklmnopqrs', '-')
            chr1 = chr1.replace(',', '_')
            chr2 = chr2.replace(',', '_')


            if (G1, G2) not in res:
                res[(G1, G2)] = dict()
            if value == 1: 
                if (chr1, g1i) not in res[(G1, G2)]:
                    res[(G1, G2)][(chr1, g1i)] = dict()
                res[(G1, G2)][(chr1, g1i)][(chr2, g2k)] = ()
            else:
                forbidden_edges.add(((G1, chr1, g1i), (G2, chr2, g2k)))
            continue

        m = NODE_PATTERN.match(name)
        if m:
            (G1, chr1, g1i) = m.groups()
            G1 = G1.replace('abcdefghijklmnopqrs', '-')
            chr1 = chr1.replace(',', '_')

            if g1i == "0":
                g1i = -1
            if value:
                nodes.add((G1, chr1, int(g1i)))
            else:
                forbidden_nodes.add((G1, chr1, int(g1i)))

        m = ADJ_PATTERN.match(name)
        if m and value:
            (G1, chr1, G2, chr2, g1i, g2k, g1j, g2l) = m.groups()
            g1i, g2k, g1j, g2l = list(map(int, (g1i, g2k, g1j, g2l)))
#            if g1i == 0:
#                g1i = -1
#            if g2k == 0:
#                g2k = -1
#            if g2l == 0:
#                g2l = -1
            # g1j can never be start telomere, because it always comes after g1i

            G1 = G1.replace('abcdefghijklmnopqrs', '-')
            G2 = G2.replace('abcdefghijklmnopqrs', '-')

            chr1 = chr1.replace(',', '_')
            chr2 = chr2.replace(',', '_')

            adjs.add((((G1, chr1, g1i), (G2, chr2, g2k)), ((G1, chr1, g1j),
                (G2, chr2, g2l))))

    data.close()
    return res, forbidden_edges, forbidden_nodes, adjs, nodes


def check_output(dists, revDists, forbidden_edges, forbidden_nodes, adjs, nodes):
    #
    # check usage of forbidden nodes
    #
    for node in forbidden_nodes:
        for gs, pwDists in list(dists.items()):
            if (gs[0] == node[0] and (node[1], node[2]) in pwDists) or \
                    (gs[1] == node[0] and (node[1], node[2]) in
                            revDists[(gs[1], gs[0])]):
                LOG.error(('Node %s is unsaturated (corresp. variable ' + \
                        'set to zero), it occurs in a matched edge.') %str(node))
    #
    # check adjacency constraints and usage of forbidden edges
    #

    # check for unused nodes
    used_nodes = set()
    # construct gene orders
    geneorders = dict()
    for (G1, _), pwDists in list(dists.items()):
        for g1i in list(pwDists.keys()):
            if G1 not in geneorders:
                geneorders[G1] = set()
            geneorders[G1].add(g1i)
            used_nodes.add((G1, g1i[0], g1i[1]))

    for (G1, _), pwDists in list(revDists.items()):
        for g1i in list(pwDists.keys()):
            if G1 not in geneorders:
                geneorders[G1] = set()
            geneorders[G1].add(g1i)
            used_nodes.add((G1, g1i[0], g1i[1]))

    # sort & construct index mapping
    genepos = dict()
    for gs in list(dists.keys()):
        genepos[gs] = dict()
        g1 = sorted(dists[gs].keys())
        g2 = sorted(set(chain(*list(dists[gs].values()))))
        genepos[gs][gs[0]] = dict((g1[i], i) for i in range(len(g1)))
        genepos[gs][gs[1]] = dict((g2[i], i) for i in range(len(g2)))

    unused_nodes = nodes.difference(used_nodes)
    if unused_nodes:
        print(file=LOG.warning(('There are %s activated nodes in the ' + \
                'matching that are not incident to a matched edge.') %len(unused_nodes)))

    # it looks crazy but it's not - G1's and G2's etc are identical
    for (((G1, chr1, g1i), (G2, chr2, g2k)), ((G1, chr1, g1j), (G2, chr2,
        g2l))) in adjs:
        if ((G1, chr1, g1i), (G2, chr2, g2k)) in forbidden_edges:
            LOG.error(('Edge %s is unsaturated (corresp. variable set ' + \
                    'to zero), but contained in adjacency with edge %s') %(str(((G1, chr1,
                        g1i), (G2, chr2, g2k))), str(((g1, chr1, g1j), (g2,
                            chr2, g2l)))))
        if ((G1, chr1, g1j), (G2, chr2, g2l)) in forbidden_edges:
            LOG.error(('Edge %s is unsaturated (corresp. variable set ' + \
                    'to zero), but contained in adjacency with edge %s') %(str(((G1,
                        chr1, g1j), (G2, chr2, g2l))), str(((G1, chr1, g1i),
                            (G2, chr2, g2k)))))

        if (chr1, g1i) not in dists[(G1, G2)] or (chr2, g2k) not in dists[(G1,
            G2)][(chr1, g1i)]:
            LOG.error(('Edge %s is unsaturated (not contained in ' + \
                    'matching graph), but contained in adjacency with edge %s') %(str(((G1, 
                        chr1, g1i), (G2, chr2, g2k))), str(((G1, chr1, g1j),
                            (G2, chr2, g2l)))))

        if (chr1, g1j) not in dists[(G1, G2)] or (chr2, g2l) not in dists[(G1,
            G2)][(chr1, g1j)]:
            LOG.error(('Edge %s is unsaturated (not contained in ' + \
                    'matching graph), but contained in adjacency with edge %s') %(str(((G1,
                        chr1, g1j), (G2, chr2, g2l))), str(((G1, chr1, g1i),
                            (G2, chr2, g2k)))))

        pwPos = genepos[(G1, G2)]
        if pwPos[G1][(chr1, g1i)] + 1 != pwPos[G1][(chr1, g1j)]:
            LOG.error(('%s and %s are part of an adjacency, ' + \
                    'but saturated genes are in between') %(str((G1, 
                        chr1, g1i)), str((G1, chr1, g1j))))

        if g2k < g2l and pwPos[G2][(chr2, g2k)] + 1 != pwPos[G2][(chr2, g2l)]:
            LOG.error(('%s and %s are part of an adjacency, ' + \
                    'but saturated genes are in between') %(str((G2, 
                        chr2, g2k)), str((G2, chr2, g2l))))

        if g2k > g2l and pwPos[G2][(chr2, g2k)] - 1 != pwPos[G2][(chr2, g2l)]:
            LOG.error(('%s and %s are part of an adjacency, ' + \
                    'but saturated genes are in between') %(str((G2, 
                        chr2, g2l)), str((G2, chr2, g2k))))

def computeObjective(adjs, mDists, dists, alpha):
    res = 0
    for ((G0, chr1, g1i), (G1, chr2, g2k)), ((_, _, g1j), (_, _, g2l)) in adjs:
        we = dists[(G0, G1)][(chr1, g1i)][(chr2, g2k)][1]
        wf = dists[(G0, G1)][(chr1, g1j)][(chr2, g2l)][1]
        res += alpha*sqrt(we*wf)

    for gs, edges in list(mDists.items()):
        for g1i, val in list(edges.items()):
            res += (1-alpha) * dists[gs][g1i][list(val.keys())[0]][1]

    return res

if __name__ == '__main__':
    
    if len(argv) < 3:
        print('\tusage: %s <.sol or .mst FILE> <PW DIST FILE 1> ... <PW DIST FILE N>' %argv[0])
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.ERROR)
    ch.setFormatter(logging.Formatter('!! %(message)s'))
    cf = logging.FileHandler(LOG_FILENAME, mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(cf)
    LOG.addHandler(ch)

    dists = dict()
    for f in argv[2:]:
        g1name, g2name = basename(f).rsplit('.', 2)[0].split('_')
        _, _, _, pwDists= readDistsAndOrder(open(f), 0)
        dists[(g1name, g2name)] = pwDists

    mDists, forbidden_edges, forbidden_nodes, adjs, nodes = readVariables(argv[1])
    revMDists = dict(((y, x), reverseDistMap(d)) for ((x,y), d) in list(mDists.items()))

    LOG.info(('Objective value: %s' %computeObjective(adjs, mDists, dists,
        0.9)))
    check_output(mDists, revMDists, forbidden_edges, forbidden_nodes, adjs, nodes)

    for gs, pwDists in list(mDists.items()):
        if gs not in dists:
            LOG.fatal('Oops, pairwise distance file not supplied in input. Exiting!')
            exit(1)
        outFile = open('%s.%s_%s' %(argv[1], gs[0], gs[1]), 'w')
        for g1i in sorted(pwDists.keys()):
            for g2k in sorted(pwDists[g1i].keys()):
                direction, weight = dists[gs][g1i][g2k]
                if direction == DIRECTION_CRICK_STRAND and direction == DIRECTION_WATSON_STRAND:
                    if (g1i[1] == TELOMERE_START and g2k[1] == TELOMERE_START) \
                            or (g1i[1] == TELOMERE_END and g2k[1] == TELOMERE_END):
                        direction = 1
                    elif (g1i[1] == TELOMERE_START and g2k[1] == TELOMERE_END) or \
                            (g1i[1] == TELOMERE_END and g2k[1] == TELOMERE_START):
                        direction = -1
                    else:
                        direction = 0
                elif direction == DIRECTION_CRICK_STRAND:
                    direction = 1
                else:
                    direction = -1

                i = g1i[1]
                k = g2k[1]

                if i == TELOMERE_START:
                    i = 'TELOMERE_START'
                if i == TELOMERE_END:
                    i = 'TELOMERE_END'
                if k == TELOMERE_START:
                    k = 'TELOMERE_START'
                if k == TELOMERE_END:
                    k = 'TELOMERE_END'
                print('\t'.join(map(str, (g1i[0], g1i[1], g2k[0], g2k[1], direction, weight))), file=outFile)
        outFile.close()

