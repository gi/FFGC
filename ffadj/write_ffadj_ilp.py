#!/usr/bin/env python3

from sys import stdout, stderr, exit, argv, maxsize, path
from os.path import basename, isfile, dirname, abspath
from optparse import OptionParser
from itertools import chain, product, combinations
from collections import deque, OrderedDict
from bisect import bisect, bisect_left
from math import sqrt
#import networkx as nx
import logging
import csv

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDistsAndOrder, reverseDistMap, \
        readDists, DIRECTION_CRICK_STRAND, DIRECTION_WATSON_STRAND, \
        TELOMERE_START, TELOMERE_END


LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]


def readAnchors(data, subgenomes=None):
    res = dict()

    for row in csv.reader(data, delimiter='\t'):
        if len(row) == 4:
            if subgenomes and (row[0] not in subgenomes or row[2] not in
                    subgenomes):
                continue
            x = row[1]
            y = row[3]
            if x == 'TELOMERE_START':
                x = TELOMERE_START
            elif x == 'TELOMERE_END':
                x = TELOMERE_END
            if y == 'TELOMERE_START':
                y = TELOMERE_START
            elif y == 'TELOMERE_END':
                y = TELOMERE_END

            val = (row[1] and (0, int(x)) or None, row[3] and (0, int(y)) or
                    None)
            key = (row[0], row[2])

        elif len(row) == 6:
            if subgenomes and (row[0] not in subgenomes or row[3] not in
                    subgenomes):
                continue

            x = row[2]
            y = row[5]
            if x == 'TELOMERE_START':
                x = TELOMERE_START
            elif x == 'TELOMERE_END':
                x = TELOMERE_END
            if y == 'TELOMERE_START':
                y = TELOMERE_START
            elif y == 'TELOMERE_END':
                y = TELOMERE_END
            val = (row[1] and (row[1].replace('_', ','), int(x)) or None,
                    row[4] and (row[4].replace('_', ','), int(y)) or None)
            key = (row[0], row[3])

        else:
            print('Oops, row %s in anchor file is of illegal length. Exiting.'
                    %str(row), file = stderr)
            exit(1)

        if key not in res:
            res[key] = set()
        res[key].add(val)

    for gs, adjs in res.items():
        res[gs] = sorted(adjs)
    return res

def readMatching(data):

    g1_adj = dict()
    g2_adj = dict()

    matching = set()
    g2 = set()

    # read matching
    isHeader = True
    for row in csv.reader(data, delimiter='\t'):
        try:
            x = (0, int(row[0]))
            y = (0, int(row[1]))
        except:
            x = None
            y=None
            if row[0] == 'TELOMERE_START':
                x = (0, TELOMERE_START)
            if row[1] == 'TELOMERE_START':
                y = (0, TELOMERE_START)
            if row[0] == 'TELOMERE_END':
                x = (0, TELOMERE_END)
            if row[1] == 'TELOMERE_END':
                y = (0, TELOMERE_END)

            if not x or not y:
                continue


        matching.add((x, y, row[2] == '1' and DIRECTION_CRICK_STRAND or \
                DIRECTION_WATSON_STRAND, float(row[3])))
        g2.add(y)

    g2 = sorted(g2)
    g2_map = dict(zip(g2, range(len(g2))))

    matching = sorted(matching)
    # construct adjacencies
    px, py, porient, _ = matching[0]
    for i in range(1, len(matching)):
        x, y, orient, _ = matching[i]
        # don't bother if relative directions don't match
        if orient != porient:
            px, py, porient = x, y, orient
            continue
        # adjacency - unreversed/reversed
        if orient == DIRECTION_CRICK_STRAND and py < y and (g2_map[y] and
                g2[g2_map[y]-1] or None) == g2[g2_map[py]]:
            g1_adj[(px, x)] = (py, y, orient)
            g2_adj[(py, y)] = (px, x, orient)
        elif orient == DIRECTION_WATSON_STRAND and py > y and (g2_map[py] and
                g2[g2_map[py]-1] or None) == g2[g2_map[y]]:
            g1_adj[(px, x)] = (py, y, orient)
            g2_adj[(y, py)] = (x, px, orient)
#        else:
#            import pdb; pdb.set_trace()
        px, py, porient = x, y, orient
    return (g1_adj, g2_adj, matching)

def computeObjective(g1_adj, g2_adj, matching, alpha):
    g1map = dict()

    edg = 0
    for x, _ , _, weight in matching:
        edg += weight
        g1map[x] = weight

    adj = 0
    for x1, x2 in g1_adj.keys():
        adj += sqrt(g1map[x1] * g1map[x2])

    return alpha * adj + (1-alpha) * edg

def computePossibleAdjs(pwDists, pwAnchors, g1, g2, g1pos, g2pos, maxGapSize, lowerBound, G):

    if G:
        print("XXX UNFINISHED IMPLEMENTATION, EXITING", file = stderr)
        exit(1)
        match = nx.max_weight_matching(G)
        # the map contains every edge twice
        mwmScore = sum(map(lambda x: G[x[0]][x[1]]['weight'], match.items()))/2

    res = list()
    if pwAnchors:
        g1_anchors, g2_anchors = map(lambda x: list(filter(None, x)), zip(*pwAnchors))
        g1_anchor_map = dict(pwAnchors)
        g1_anchors.sort(reverse=True)
        g2_anchors.sort()
        next_g1_anchor = g1_anchors.pop()
        next_g1_anchor_pos = g1pos[next_g1_anchor]
    else:
        g1_anchors = list()
        g2_anchors = list()
        next_g1_anchor_pos = len(g1)

    prev_g1_anchor_pos = -1
    # in order to make this algorithm work, the next anchor must be _always_
    # larger than i
    if next_g1_anchor_pos == 0:
        prev_g1_anchor_pos = next_g1_anchor_pos
        next_g1_anchor = g1_anchors.pop()
        next_g1_anchor_pos = g1pos[next_g1_anchor]


    i = 0
    while i < (len(g1) - 1):
#        progress = float(i*100)/len(g1)
#        if (progress % 10) < 0.03 and progress:
#            LOG.info('%.0f%% percent completed' %progress)

        g1i = g1[i]

        # process as long as
        #   (a) g1i has actually edges into G2 (might not necessarily be the
        #       case!) and
        #   (b) i and j are not in an anchored adjacency
        #   (c) j is smaller then the length of the genome and
        #   (d) g1i and g1j are located on the same chromosome

        j = i+1
        while g1i in pwDists and \
                (i != prev_g1_anchor_pos or j != next_g1_anchor_pos) and \
                j < len(g1) and j <= next_g1_anchor_pos and \
                j-i-1 <= maxGapSize and g1i[0] == g1[j][0]:

            g1j = g1[j]

            remainingGapSize = maxGapSize-j+i+1

            if g1j not in pwDists:
                j += 1
                continue

            # genes in g2l_list are ordered. we will determine the start and
            # stop based on maxGapSize
            g2l_list = list(pwDists[g1j].items())
            for (g2k, (dir_ik, w_ik)) in pwDists[g1i].items():

                if i == prev_g1_anchor_pos and g1_anchor_map[g1i] and g1_anchor_map[g1i] != g2k:
                    continue

                if dir_ik == DIRECTION_CRICK_STRAND:
                    p = bisect(g2_anchors, g2k)
                    if p == len(g2_anchors):
                        g2a = (g2[-1][0], g2[-1][1] + 1)
                    else:
                        g2a = g2_anchors[p]

                    # g2 genes must have same relative direction to g1 genes and
                    # also lie on the same chromosome, but should not be the
                    # same gene. Further no anchors should interfere.

                    # on the crick strand (forward direction), the next possible
                    # adjacency would be right of g2k.
                    if g2k == g2[-1]:
                        continue
                    start = bisect_left(g2l_list, (g2[g2pos[g2k]+1], (dir_ik, 0)))

                    # gene furthest away from g2k but within max gaplength
                    stop_g2l = g2[min(len(g2)-1, g2pos[g2k]+remainingGapSize+1)]

                    for x in range(start, len(g2l_list)):
                        g2l, (dir_jl, _) = g2l_list[x]
                        # if the two adjacencies are not in the same direction,
                        # go to the next
                        if not (dir_jl == dir_ik):
                            continue
                        if g2l[0] > g2k[0] or stop_g2l <= g2l or g2a < g2l:
                            break
                        res.append(((g1i, g2k), (g1j, g2l)))

                if dir_ik == DIRECTION_WATSON_STRAND:
                    p = bisect_left(g2_anchors, g2k)
                    if p == 0:
                        g2a = (g2[0][0], g2[0][1]-1)
                    else:
                        g2a = g2_anchors[p-1]

                    # on the watson strand (backward direction) the first
                    # possible adjacency should be within maxGapSize
                    start = 0
                    if g2pos[g2k]-remainingGapSize-1 > 0:
                        start = bisect_left(g2l_list,
                                (g2[g2pos[g2k]-remainingGapSize-1], (dir_ik, 0)))

                    for x in range(start, len(g2l_list)):
                        g2l, (dir_jl, _) = g2l_list[x]
                        if g2l >= g2k:
                            break
                        if not (dir_jl == dir_ik) or g2l[0] < g2k[0] or g2a > g2l:
                            continue
                        res.append(((g1i, g2k), (g1j, g2l)))
            j += 1

        if i+1 >= next_g1_anchor_pos:
            prev_g1_anchor_pos = next_g1_anchor_pos
            if g1_anchors:
                next_g1_anchor = g1_anchors.pop()
                next_g1_anchor_pos = g1pos[next_g1_anchor]
            else:
                next_g1_anchor_pos = len(g1)

        i += 1

    return res

def computeAnchorAdjs(pwAnchors, pwDists, g1, g2, g1pos, g2pos):
    res = list()
    pwAnchors.sort()
    for i in range(len(pwAnchors)-1):
        g1i, g2k = pwAnchors[i]
        g1j, g2l = pwAnchors[i+1]
        if g1i == None or g2k == None or g1j == None or g2l == None:
            continue

        o1 = pwDists[g1i][g2k][0]
        o2 = pwDists[g1j][g2l][0]

        if g1i[0] == g1j[0] and g2k[0] == g2l[0] and o1 == o2 and \
                g1[g1pos[g1i]+1] == g1j and g2k[0] == g2l[0] and \
                ((o1 == DIRECTION_CRICK_STRAND and len(g2) > g2pos[g2k]+1 and g2[g2pos[g2k]+1] == g2l) or \
                (o1 == DIRECTION_WATSON_STRAND and g2pos[g2k] > 0 and g2[g2pos[g2k]-1] == g2l)):
            res.append(((g1i, g2k), (g1j, g2l)))

    return res


def computePWAnchorAdjs(pwAnchors, pwDists):
    res = list()
    pwAnchors.sort()
    g2 = sorted(filter(None, map(lambda x: x[1], pwAnchors)))
    g2pos = dict((g2[i], i) for i in range(len(g2)))

    for i in range(len(pwAnchors)-1):
        g1i, g2k = pwAnchors[i]
        g1j, g2l = pwAnchors[i+1]

        if g1i == None or g2k == None or g1j == None or g2l == None:
            continue

        o1 = pwDists[g1i][g2k][0]
        o2 = pwDists[g1j][g2l][0]
        if g1i[0] == g1j[0] and g2k[0] == g2l[0] and o1 == o2 and \
                ((o1 == DIRECTION_CRICK_STRAND and g2l == g2[g2pos[g2k]+1]) or \
                (o1 == DIRECTION_WATSON_STRAND and g2l == g2[g2pos[g2k]-1])):
            res.append(((g1i, g2k), (g1j, g2l)))
    return res

def writeObjectiveFunction(adjs, dists, alpha, out):
    out.write('max ')
    for gs, pwAdjs in adjs.items():
        for (g1i, g2k), (g1j, g2l) in pwAdjs:
            if g1i == None or g2k == None or g1j == None or g2l == None:
                continue

            s = alpha * sqrt(dists[gs][g1i][g2k][1] * dists[gs][g1j][g2l][1])
            out.write('%s d_%s_%s_%s_%s_%s_%s_%s_%s + ' %(s, gs[0], g1i[0],
                gs[1], g2k[0], g1i[1], g2k[1], g1j[1], g2l[1]))

    isFirst = True

    for gs, pwDists in dists.items():
        for g1i, g2genes in pwDists.items():
            for g2k, (_, w) in g2genes.items():
                if isFirst:
                    isFirst = None
                else:
                    out.write(' + ')

                out.write('%s a_%s_%s_%s_%s_%s_%s' %((1-alpha) * w, gs[0],
                    g1i[0], gs[1], g2k[0], g1i[1], g2k[1]))

    out.write('\n')

def writePartialMatchingConstraints(genomes, dists, revDists, out):
    for gx1 in genomes:

        gx2, gx3 = genomes.difference((gx1, ))
        dists12 = (gx1, gx2) in dists and dists[(gx1, gx2)] or revDists[(gx1, gx2)]
        dists13 = (gx1, gx3) in dists and dists[(gx1, gx3)] or revDists[(gx1, gx3)]
        dists23 = (gx2, gx3) in dists and dists[(gx2, gx3)] or revDists[(gx2, gx3)]
        dists32 = (gx3, gx2) in dists and dists[(gx3, gx2)] or revDists[(gx3, gx2)]

        for g1i, g2dists in dists12.items():
            if g1i not in dists13:
                continue
            for g2k, g3z in product(g2dists.keys(), dists13[g1i].keys()):

                condition1213 = ''
                if gx1 < gx2:
                    condition1213 = 'a_%s_%s_%s_%s_%s_%s + ' %(gx1, g1i[0], gx2,
                            g2k[0], g1i[1], g2k[1])
                else:
                    condition1213 = 'a_%s_%s_%s_%s_%s_%s + ' %(gx2, g2k[0], gx1,
                            g1i[0], g2k[1], g1i[1])
                if gx1 < gx3:
                    condition1213 += 'a_%s_%s_%s_%s_%s_%s <= 2\n' %(gx1, g1i[0],
                            gx3, g3z[0], g1i[1], g3z[1])
                else:
                    condition1213 += 'a_%s_%s_%s_%s_%s_%s <= 2\n' %(gx3, g3z[0],
                            gx1, g1i[0], g3z[1], g1i[1])

                if g2k in dists23:
                    for g3y in dists23[g2k]:
                        if g3y != g3z:
                            if gx2 < gx3:
                                out.write('a_%s_%s_%s_%s_%s_%s + ' %(gx2,
                                    g2k[0], gx3, g3y[0], g2k[1], g3y[1]))
                            else:
                                out.write('a_%s_%s_%s_%s_%s_%s + ' %(gx3,
                                    g3y[0], gx2, g2k[0], g3y[1], g2k[1]))
                    out.write(condition1213)

                if g3z in dists32:
                    for g2l in dists32[g3z]:
                        if g2l != g2k:
                            if gx2 < gx3:
                                out.write('a_%s_%s_%s_%s_%s_%s + ' %(gx2, g2l[0], gx3,
                                    g3z[0], g2l[1], g3z[1]))
                            else:
                                out.write('a_%s_%s_%s_%s_%s_%s + ' %(gx3, g3z[0], gx2,
                                    g2l[0], g3z[1], g2l[1]))
                    out.write(condition1213)


def writeAdjsConstraints(all_adjs, geneorders, genepos, dists, revDists, out):
    for gs, adjs in all_adjs.items():
        g1pos = genepos[gs[0]]
        g2pos = genepos[gs[1]]

        for (g1i, g2k), (g1j, g2l) in adjs:

            dikjl = 'd_%s_%s_%s_%s_%s_%s_%s_%s' %(gs[0], g1i[0], gs[1], g2k[0],
                    g1i[1], g2k[1], g1j[1], g2l[1])

            print(('a_%s_%s_%s_%s_%s_%s + a_%s_%s_%s_%s_%s_%s - ' + \
                    '2 %s >= 0') %(gs[0], g1i[0], gs[1], g2k[0], g1i[1], g2k[1],
                            gs[0], g1j[0], gs[1], g2l[0], g1j[1], g2l[1],
                            dikjl), file = out)

            if len(geneorders.keys()) > 2:
                for i in range(g1pos[g1i] +1, g1pos[g1j]):
                    g1x = geneorders[gs[0]][i]
                    edges = ['a_%s_%s_%s_%s_%s_%s' %(gs[0], g1x[0], gs[1],
                        g2y[0], g1x[1], g2y[1]) for g2y in dists[gs].get(g1x,
                            dict()).keys()]

                    if len(edges):
                        print('%s + %s <= 1' %(' + '.join(edges), dikjl), file
                                = out)

                start, stop = sorted((g2pos[g2k], g2pos[g2l]))
                for k in range(start+1, stop):
                    g2y = geneorders[gs[1]][k]
                    edges = ['a_%s_%s_%s_%s_%s_%s' %(gs[0], g1x[0], gs[1],
                        g2y[0], g1x[1], g2y[1]) for g1x in revDists[(gs[1],
                            gs[0])].get(g2y, dict()).keys()]

                    if len(edges):
                        print('%s + %s <= 1' %(' + '.join(edges), dikjl), file
                                = out)
            else:
                for i in range(g1pos[g1i] +1, g1pos[g1j]):
                    g1x = geneorders[gs[0]][i]
                    print('b_%s_%s_%s + %s <= 1' %(gs[0], g1x[0], g1x[1],
                        dikjl), file = out)

                start, stop = sorted((g2pos[g2k], g2pos[g2l]))
                for k in range(start+1, stop):
                    g2y = geneorders[gs[1]][k]
                    print('b_%s_%s_%s + %s <= 1' %(gs[1], g2y[0], g2y[1],
                        dikjl), file = out)

def write3MatchingConstraints(dists, revDists, out):

    for gs, pwDists in dists.items():
        for g1i, g2Dists in pwDists.items():
            if not g2Dists:
                continue
            print('b_%s_%s_%s - %s >= 0' %(gs[0], g1i[0], g1i[1],
                ' - '.join(map( lambda g2k: 'a_%s_%s_%s_%s_%s_%s' %(gs[0],
                    g1i[0], gs[1], g2k[0], g1i[1], g2k[1]), g2Dists.keys()))),
                file = out)

    for gs, pwDists in revDists.items():
        for g2k, g1Dists in pwDists.items():
            if not g1Dists:
                continue
            print('b_%s_%s_%s - %s >= 0' %(gs[0], g2k[0], g2k[1],
                    ' - '.join(map( lambda g1i: 'a_%s_%s_%s_%s_%s_%s' %(gs[1],
                        g1i[0], gs[0], g2k[0], g1i[1], g2k[1]),
                        g1Dists.keys())), ), file = out)


def writeFixedVariables(anchors, anchored_adjs, dists, revDists, geneorders, out):

    already_fixed = set()
    for gs, edge in anchors.items():
        for g1i, g2k in edge:
            if g1i and g2k:
                print('a_%s_%s_%s_%s_%s_%s = 1' %(gs[0], g1i[0], gs[1],
                        g2k[0], g1i[1], g2k[1]), file = out)
            if g1i:
                bx = (gs[0], g1i[0], g1i[1])
                if bx not in already_fixed:
                    already_fixed.add(bx)
                    print('b_%s_%s_%s = 1' %bx, file = out)

                if g2k == None:
                    continue
                for g2l in dists[gs][g1i].keys():
                    if g2l != g2k:
                        print('a_%s_%s_%s_%s_%s_%s = 0' %(gs[0], g1i[0], gs[1],
                            g2l[0], g1i[1], g2l[1]), file = out)
            if g2k:
                bx = (gs[1], g2k[0], g2k[1])
                if bx not in already_fixed:
                    already_fixed.add(bx)
                    print('b_%s_%s_%s = 1' %bx, file = out)

                if g1i == None:
                    continue
                for g1j in revDists[(gs[1], gs[0])][g2k].keys():
                    if g1j != g1i:
                        print('a_%s_%s_%s_%s_%s_%s = 0' %(gs[0], g1j[0], gs[1],
                            g2k[0], g1j[1], g2k[1]), file = out)

    for gs, adjs in anchored_adjs.items():
        for (g1i, g2k), (g1j, g2l) in adjs:
            print('d_%s_%s_%s_%s_%s_%s_%s_%s = 1' %(gs[0], g1i[0], gs[1],
                g2k[0], g1i[1], g2k[1], g1j[1], g2l[1]), file = out)

    genomes = set(geneorders.keys())
    for gx, g1 in geneorders.items():
        for g1i in g1:
            unconnected = True
            for gy in genomes.difference([gx]):
                distxy = ((gx, gy) in dists) and dists[(gx, gy)] or revDists[(gx, gy)]
                if g1i in distxy and distxy[g1i]:
                    unconnected = False
                    break
            if unconnected:
                print('b_%s_%s_%s = 0' %(gx, g1i[0], g1i[1]), file = out)


def writeVariables(geneorders, dists, all_adjs, out):
    for gs, g1 in geneorders.items():
        for g1i in g1:
            print('b_%s_%s_%s' %(gs, g1i[0], g1i[1]), file = out)

    for gs, pwDists in dists.items():
        for g1i, g2genes in pwDists.items():
            for g2k in g2genes.keys():
                print('a_%s_%s_%s_%s_%s_%s' %(gs[0], g1i[0], gs[1], g2k[0],
                    g1i[1], g2k[1]), file = out)

    for gs, adjs in all_adjs.items():
        for (g1i, g2k), (g1j, g2l) in adjs:
            print('d_%s_%s_%s_%s_%s_%s_%s_%s' %(gs[0], g1i[0], gs[1], g2k[0],
                g1i[1], g2k[1], g1j[1], g2l[1]), file = out)

if __name__ == '__main__':

    usage = 'usage: %prog [options] <PAIRWISE DIST FILE 1> [<PAIRWISE DIST FILE 2> <PAIRWISE DIST FILE 3>]'
    parser = OptionParser(usage=usage)
    parser.add_option('-a', '--alpha', dest='alpha',
            help='Alpha parameter for FFAdjacencies optimization. [default=%default]',
            type=float, default=0.5, metavar='[0, 1]')

    parser.add_option('-f', '--anchors_file', dest='anchorsFile', help='File' + \
            ' containing anchoring information for the matching.',
            metavar='FILE')
    parser.add_option('-m', '--matching_files', dest='matchingFiles', action='append',
            metavar='<PW DIST FILE>', help='Fix edges in the ILP that are contained in' + \
            'the given matching, but do not establish fixed adjacencies ' + \
            'between them.')
    parser.add_option('-p', '--enforce-pairwise-adjacencies',
            dest='enforcePwAdj', help='Only consider anchored genes between' + \
                    ' the corresponding genomes to identify adjacenices. ' + \
                    'That is, even if a gene within an adjacency matches ' + \
                    'to a gene in an uninvolved, it does not break the ' + \
                    'adjacency.  This option is useful if anchors orginate' + \
                    ' from a pairwise matching. (Requires -f) ' + \
                    '[default=%default]', action='store_true', default=False)

    parser.add_option('-o', '--omit-3matching-constraints',
            dest='omit3matching', help='Omit partial 3-matching constraints,' + \
                    ' i.e. only keep pairwise matching constraints.' + \
                    '[default=%default]', action='store_true', default=False)

    parser.add_option('-g', '--restricted_gap_size', dest='maxGapSize',
            help='Restrict maximal number of deleted genes between two ' + \
                    'adjacency to a certain amount. [default=%default '
                    '(unrestricted)]', type=int, default=-1, metavar='INT')

    parser.add_option('-r', '--remaining_subgraph_test', dest='heuristicSolution',
            help='Perform remaining subgraph test by using the solution of the ' + \
                    'specified matching file as lower bound for the optimal ' + \
                    'solution.', metavar='FILE')

    (options, args) = parser.parse_args()

    if (len(args) != 1 and len(args) != 3) or \
            (options.alpha < 0 or options.alpha > 1) or \
            (options.anchorsFile and not isfile(options.anchorsFile)) or \
            (options.enforcePwAdj and not options.anchorsFile):
        parser.print_help()
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.ERROR)
    ch.setFormatter(logging.Formatter('!! %(message)s'))
    cf = logging.FileHandler(LOG_FILENAME, mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(cf)
    LOG.addHandler(ch)

    if options.maxGapSize == -1:
        options.maxGapSize = maxsize

    dists = dict()
    geneorders = dict()

    for f in args:
        LOG.info('Reading pairwise dist file %s' %f)
        g1name, g2name = basename(f).split('.', 2)[0].split('_')

        g1name = g1name.replace('-', 'abcdefghijklmnopqrs')
        g2name = g2name.replace('-', 'abcdefghijklmnopqrs')

        _, g1, g2, pwDists= readDistsAndOrder(open(f))
        g1 = [ x for x in map(lambda x: (x[0].replace('_', ','), x[1]), g1) ]
        g2 = [ x for x in map(lambda x: (x[0].replace('_', ','), x[1]), g2) ]
        fixed_pwDists = dict()

        for (chr1, g1i), d in pwDists.items():
            k = (chr1.replace('_', ','), g1i)
            fixed_pwDists[k] = dict()
            for (chr2, g2k), x in d.items():
                fixed_pwDists[k][(chr2.replace('_', ','), g2k)] = x

        dists[(g1name, g2name)] = fixed_pwDists

        if g1name in geneorders:
            geneorders[g1name] = sorted(set(chain(geneorders[g1name], g1)))
        else:
            geneorders[g1name] = g1

        if g2name in geneorders:
            geneorders[g2name] = sorted(set(chain(geneorders[g2name], g2)))
        else:
            geneorders[g2name] = g2


    LOG.info('Constructing gene id to pos maps')
    genepos = dict((g1name, dict(zip(g1, range(len(list(g1)))))) for g1name, g1 in
        geneorders.items())

    anchors = dict((gs, list()) for gs in dists.keys())
    if options.anchorsFile:
        LOG.info('Read anchors')
        anchors = readAnchors(open(options.anchorsFile), geneorders.keys())
        if len(anchors) == 0: # empty anchors file, will add key manually to prevent exceptions later
            anchors[(g1name,g2name)] = []

    if options.matchingFiles:
        LOG.info('Read %s matching file:' %len(options.matchingFiles))
        for f in options.matchingFiles:
            g1name, g2name = basename(f).split('.', 2)[0].split('_')
            if (g1name, g2name) not in dists:
                continue
            LOG.info('\t%s' %f)
            _, pwDists = readDists(open(f))
            for g1i, g2dists in pwDists.items():
                g2k = g2dists.keys()[0]
                for g2l in list(dists[(g1name, g2name)][g1i].keys()):
                    if g2l != g2k:
                        del dists[(g1name, g2name)][g1i][g2l]

    LOG.info('Sorting distance map.')
    for gs, pwDists in dists.items():
        for k, val in pwDists.items():
            dists[gs][k] = OrderedDict(sorted(val.items()))


    if options.heuristicSolution and len(args) == 1:
        LOG.info('Determine lower bound of optimal solution from %s' %options.heuristicSolution)
        g1_adj, g2_adj, matching = readMatching(open(options.heuristicSolution))
        lowerBound = computeObjective(g1_adj, g2_adj, matching, options.alpha)
        # construct graph
        G = nx.Graph()
        for ((chr1, g1i), (chr2, g2k), direction, weight) in matching:
            G.add_edge((g1name, chr1, g1i), (g2name, chr2, g2k), direction=direction, weight=weight)

    revDists = dict(((y, x), reverseDistMap(d)) for ((x,y), d) in dists.items())
    #
    # remove edges from graph that are conflicting with anchors
    #
    LOG.info('Remove edges from graph that are conflicting with anchors')
    for gs, adjs in anchors.items():
        # all anchors are stored twice in map, for back and forth
        if gs not in dists:
            continue
        for g1i, g2k in adjs:
            if g1i and g2k:
                for g2l in list(dists[gs][g1i].keys()):
                    if g2l != g2k:
                        del dists[gs][g1i][g2l]
                        del revDists[(gs[1], gs[0])][g2l][g1i]
                for g1j in list(revDists[(gs[1], gs[0])][g2k].keys()):
                    if g1j != g1i:
                        del dists[gs][g1j][g2k]
                        del revDists[(gs[1], gs[0])][g2k][g1j]

    #
    # where to output?
    #
    out = stdout


    LOG.info('Start constructing linear program')
    #
    # compute possible adjacencies
    #

    possible_adjs = dict()
    anchored_adjs = dict()

    for gs in dists.keys():
        LOG.info('Computing possible adjacencies between genomes %s-%s'%gs)
        if options.heuristicSolution and len(args) == 1:
            possible_adjs[gs] = computePossibleAdjs(dists[gs], anchors[gs],
                    geneorders[gs[0]], geneorders[gs[1]], genepos[gs[0]],
                    genepos[gs[1]], options.maxGapSize, lowerBound, G)
        else:
            possible_adjs[gs] = computePossibleAdjs(dists[gs], anchors[gs],
                    geneorders[gs[0]], geneorders[gs[1]], genepos[gs[0]],
                    genepos[gs[1]], options.maxGapSize, 0, None)

        if options.enforcePwAdj:
            anchored_adjs[gs] = computePWAnchorAdjs(anchors[gs], dists[gs])
        else:
            anchored_adjs[gs] = computeAnchorAdjs(anchors[gs], dists[gs],
                    geneorders[gs[0]], geneorders[gs[1]], genepos[gs[0]],
                    genepos[gs[1]])

    #
    # write linear program
    #

    # objective function
    LOG.info('Write objective function')
    writeObjectiveFunction(dict([(gs, chain(possible_adjs[gs],
        anchored_adjs[gs])) for gs in possible_adjs.keys()]), dists,
        options.alpha, out)
    # constraints
    print('\nsubject to', file = out)
    LOG.info('Write pairwise matching constraints')
    write3MatchingConstraints(dists, revDists, out)
    LOG.info('Write adjacency constraints')
    writeAdjsConstraints(dict([(gs, chain(possible_adjs[gs],
        anchored_adjs[gs])) for gs in possible_adjs.keys()]), geneorders,
        genepos, dists, revDists, out)

    if not options.omit3matching and len(args) == 3:
        LOG.info('Write partial matching constraints')
        writePartialMatchingConstraints(set(geneorders.keys()), dists, revDists, out)
    LOG.info('Write fixed variables in anchors')
    writeFixedVariables(anchors, anchored_adjs, dists, revDists, geneorders,
            out)
    print('\nbinaries', file = out)
    LOG.info('Write variables')
    writeVariables(geneorders, dists, dict([(gs, chain(possible_adjs[gs],
        anchored_adjs[gs])) for gs in possible_adjs.keys()]), out)
    print('\nend', file = out)

