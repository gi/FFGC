#!/usr/bin/env python3

from sys import stdout, stderr, exit
from optparse import OptionParser
from os.path import basename
from functools import cmp_to_key
from time import time
import networkx as nx
import csv

def readAdjGraph(data, G1, G2):
    G = nx.Graph()
    oMap = dict()
    gDict = dict()

    for chr1, g1i, g1j, chr2, g2k, g2l, orient in csv.reader(data,
            delimiter='\t'):
        g1i = g1i == 'TELOMERE' and g1i or int(g1i)
        g1j = g1j == 'TELOMERE' and g1j or int(g1j)
        g2k = g2k == 'TELOMERE' and g2k or int(g2k)
        g2l = g2l == 'TELOMERE' and g2l or int(g2l)

        G.add_edge((G1, chr1, g1i, g1j), (G2, chr2, g2k, g2l))
        if (G1, chr1, g1i, g1j) not in gDict:
            gDict[(G1, chr1, g1i, g1j)] = set()
        gDict[(G1, chr1, g1i, g1j)].add((G2, chr2, g2k, g2l))

        if (chr1, g1i, g1j) not in oMap:
            oMap[(chr1, g1i, g1j)] = dict()
        oMap[(chr1, g1i, g1j)][(chr2, g2k, g2l)] = orient

    return G, oMap, gDict

def cmp(a, b):
    if a < b:
        return -1
    if a > b:
        return 1
    return 0

def __cmp_mod__(x, y):
    (x1, x2) = x
    (y1, y2) = y
    c = cmp(x1[1], y1[1])
    if c:
        return c

    if x1[2] == 'TELOMERE':
        return -1
    if y1[2] == 'TELOMERE':
        return 0
    return cmp(x, y)

def compute_matching(G, top_partition_name):
    top_nodes = [n for n in G.nodes if n[0] == top_partition_name]
    return nx.bipartite.hopcroft_karp_matching(G, top_nodes=top_nodes)

if __name__ == '__main__':
    usage = '%prog [options] <ADJ FILE 1>'
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.print_help()
        exit(1)

    G1, G2 = basename(args[0]).rsplit('.', 2)[0].split('_', 1)
    G, oMap, gDict = readAdjGraph(open(args[0]), G1, G2)
    M = compute_matching(G, G1)
    M = sorted(filter(lambda x: x[0][0]== G1, M.items()), key=cmp_to_key(__cmp_mod__))

    for (Gx, chr1, g1i, g1j), (_, chr2, g2k, g2l) in M:
        if Gx != G1:
            continue
        orient = oMap[(chr1, g1i, g1j)][(chr2, g2k, g2l)]
        print('\t'.join(map(str, (chr1, g1i, g1j, chr2, g2k, g2l,
            orient))), file = stdout)
