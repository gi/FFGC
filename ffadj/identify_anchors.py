#!/usr/bin/env python3

from sys import stdout, stderr, exit, argv, maxsize, path
from optparse import OptionParser
from os.path import basename, dirname, abspath
from math import sqrt
import logging

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDistsAndOrder, reverseDistMap, \
        DIRECTION_CRICK_STRAND, DIRECTION_WATSON_STRAND

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)
LOG_FILENAME = '%s.log' %basename(argv[0]).rsplit('.py', 1)[0]

def identifyAnchors(alpha, dists, revDists, geneorders, max_weight):
    res = set()


    g1, g2 = next(iter(dists.keys()))
    g1dist = next(iter(dists.values()))
    g2dist = next(iter(revDists.values()))
    g2pos = dict((geneorders[g2][k], k) for k in range(len(geneorders[g2])))

    for i in range(len(geneorders[g1])-1):
        j = i+1
        g1i = geneorders[g1][i]
        g1j = geneorders[g1][j]
        # check if on same chromosome
        if g1i[0] != g1j[0]:
            continue

        #
        # XXX CHANGE OF VARIABLE NAMING CONVENTION: NOW g1 AND g2 ARE GENES OF
        # GENOME G1, AND h1, h2 ARE GENES OF GENOME G2
        #


        w1 = max(map(lambda x: x[1], g1dist[g1i].values()))
        w2 = max(map(lambda x: x[1], g1dist[g1j].values()))


        for g2k, (dir1, w2k) in g1dist[g1i].items():
            k = g2pos[g2k]

            if w2k != w1:
                continue
            if dir1 == DIRECTION_CRICK_STRAND and g2pos[g2k] < len(geneorders[g2])-1:
                l = g2pos[g2k]+1
            elif dir1 == DIRECTION_WATSON_STRAND and g2pos[g2k] > 0:
                l = g2pos[g2k]-1
            else:
                continue


            g2l = geneorders[g2][l]
            # check if on same chromosome
            if g2k[0] != g2l[0]:
                continue

            # check if g2l is connected to g1j with maximal weight w2
            if g1j not in g2dist[g2l] or g2dist[g2l][g1j][1] != w2:
                continue

            g1_crick = max(list(map(lambda x: x[1][1], filter(lambda x: x[1][0] ==
                DIRECTION_CRICK_STRAND and x[0] != g2k, g1dist[g1i].items()))) + [0])
            g1_watson = max(list(map(lambda x: x[1][1], filter(lambda x: x[1][0] ==
                DIRECTION_WATSON_STRAND and x[0] != g2k, g1dist[g1i].items()))) + [0])
            g2_crick = max(list(map(lambda x: x[1][1], filter(lambda x: x[1][0] ==
                DIRECTION_CRICK_STRAND and x[0] != g2l, g1dist[g1j].items()))) + [0])
            g2_watson = max(list(map(lambda x: x[1][1], filter(lambda x: x[1][0] ==
                DIRECTION_WATSON_STRAND and x[0] != g2l, g1dist[g1j].items()))) + [0])

            g1_p = max(g1_crick, g1_watson)
            g2_p = max(g2_crick, g2_watson)

            h1_crick = max(list(map(lambda x: x[1][1], filter(lambda x: x[1][0] ==
                DIRECTION_CRICK_STRAND and x[0] != g1i, g2dist[g2k].items()))) + [0])
            h1_watson = max(list(map(lambda x: x[1][1], filter(lambda x: x[1][0] ==
                DIRECTION_WATSON_STRAND and x[0] != g1i, g2dist[g2k].items()))) + [0])
            h2_crick = max(list(map(lambda x: x[1][1], filter(lambda x: x[1][0] ==
                DIRECTION_CRICK_STRAND and x[0] != g1j, g2dist[g2l].items()))) + [0])
            h2_watson = max(list(map(lambda x: x[1][1], filter(lambda x: x[1][0] ==
                DIRECTION_WATSON_STRAND and x[0] != g1j, g2dist[g2l].items()))) + [0])

            h1_p = max(h1_crick, h1_watson)
            h2_p = max(h2_crick, h2_watson)

            # check if there is no edge of higher weight adjacent to h1 and h2

            if h1_p > w1 or h2_p > w2:
                continue

            #
            # check for identifying anchor
            #

            wp = max_weight


            # anchor weight

            aw =  alpha * sqrt(w1*w2) + (1-alpha) * (w1+w2)

            if i != 0 and k != 0 and l != 0 and j != len(geneorders[g1]) \
                    and k != len(geneorders[g2]) and l != len(geneorders[g2]) \
                    and alpha*wp >= aw:
                LOG.info(("No anchor: edges (%s, %s) -- (%s, %s) can be " +
                    "skipped entirely by perfect conserved adjacency ") %(g1i,
                        g2k, g1j, g2l))
                break

            if alpha * (sqrt(g1_p * wp) + sqrt(g2_p * wp) + sqrt(h1_p * wp) + \
                    sqrt(h2_p * wp)) + (1-alpha) * (g1_p + g2_p + h1_p + \
                    h2_p) >= aw:
                LOG.info(("No anchor: up to four incident edges of (%s, %s) " +
                    "and (%s, %s) can establish stronger perfect conserved "
                    "adjacencies") %(g1i, g2k, g1j, g2l))
                break

            g_crick_adj = alpha * (sqrt(g1_crick * g2_crick) + sqrt(g1_crick * \
                    wp) + sqrt(g2_crick * wp)) + (1-alpha) * (g1_crick + g2_crick)
            g_watson_adj = alpha * (sqrt(g1_watson* g2_watson) + sqrt(g1_watson* \
                    wp) + sqrt(g2_watson * wp)) + (1-alpha) * (g1_watson + g2_watson)
            h_crick_adj = alpha * (sqrt(h1_crick * h2_crick) + sqrt(h1_crick * \
                    wp) + sqrt(h2_crick * wp)) + (1-alpha) * (h1_crick + h2_crick)
            h_watson_adj = alpha * (sqrt(h1_watson* h2_watson) + sqrt(h1_watson* \
                    wp) + sqrt(h2_watson * wp)) + (1-alpha) * (h1_watson + h2_watson)

            if max(g_crick_adj, g_watson_adj) + max(h_crick_adj, h_watson_adj) >= aw:
                LOG.info(("No anchor: up to four incident edges of (%s, %s) " +
                    "and (%s, %s) can establish an alternative conserved " +
                    "adjacency") %(g1i, g2k, g1j, g2l))
                break

            # check crick scenario
            if k < l and (max(alpha * (sqrt(w1*g2_crick) + sqrt(g2_crick * wp)) + \
                    (1-alpha) * (g2_crick+w1), alpha * (sqrt(w1*h2_crick) + \
                    sqrt(h2_crick * wp)) + (1-alpha) * (h2_crick + w1), \
                    alpha * sqrt(w1*wp) + (1-alpha) * w1) >= aw or \
                    max(alpha * (sqrt(w2*g1_crick) + sqrt(g1_crick * wp)) + \
                    (1-alpha) * (g1_crick+w2), alpha * (sqrt(w2*h1_crick) + \
                    sqrt(h1_crick * wp)) + (1-alpha) * (h1_crick+w2), \
                    alpha * sqrt(w2*wp) + (1-alpha)*w2) >= aw):
                LOG.info(("No anchor: either edge (%s, %s) or edge (%s, %s) " +
                    "can establish an alternative conserved adjacency with " +
                    "incident edges of %s, %s, %s, %s or a perfect edge " +
                    "wp") %(g1i, g2k, g1j, g2l, g1i, g1j, g2k, g2l))
                break
#           check watson scenario
            elif k > l and (max(alpha * (sqrt(w1*g2_watson) + sqrt(g2_watson * wp)) + \
                    (1-alpha) * (g2_watson+w1), alpha * (sqrt(w1*h2_watson) + \
                    sqrt(h2_watson * wp)) + (1-alpha) * (h2_watson+w1), \
                    alpha * sqrt(w1*wp) + (1-alpha)*w1) >= aw or \
                    max(alpha * (sqrt(w2*g1_watson) + sqrt(g1_watson * wp)) + \
                    (1-alpha) * g1_watson, alpha * (sqrt(w2*h1_watson) + \
                    sqrt(h1_watson * wp)) + (1-alpha) * (h1_watson+w2), \
                    alpha * sqrt(w2*wp) + (1-alpha) * w2) >= aw):
                LOG.info(("No anchor: either edge (%s, %s) or edge (%s, %s) " +
                    "can establish an alternative conserved adjacency with " +
                    "incident edges of %s, %s, %s, %s or a perfect edge " +
                    "wp") %(g1i, g2k, g1j, g2l, g1i, g1j, g2k, g2l))
                break

            LOG.info("FOUND ANCHOR: (%s, %s) -- (%s, %s)" %(g1i, g2k, g1j, g2l))
            res.add((g1i, g2k))
            res.add((g1j, g2l))

    return sorted(res)


if __name__ == '__main__':

    usage = 'usage: %prog [options] <PAIRWISE DIST FILE>'
    parser = OptionParser(usage=usage)
    parser.add_option('-a', '--alpha', dest='alpha',
            help='Alpha parameter for FFAdjacencies optimization. [default=%default]',
            type=float, default=0.5, metavar='[0, 1]')

    parser.add_option('-g', '--restricted_gap_size', dest='maxGapSize',
            help='Restrict maximal number of deleted genes between two ' + \
                    'adjacency to a certain amount. [default=%default '
                    '(unrestricted)]', type=int, default=-1, metavar='INT')


    (options, args) = parser.parse_args()

    if len(args) != 1 or (options.alpha < 0 or options.alpha > 1):
        parser.print_help()
        exit(1)

    # setup logging
    ch = logging.StreamHandler(stderr)
    ch.setLevel(logging.ERROR)
    ch.setFormatter(logging.Formatter('!! %(message)s'))
    cf = logging.FileHandler(LOG_FILENAME, mode='w', delay=True)
    cf.setLevel(logging.INFO)
    cf.setFormatter(logging.Formatter('%(levelname)s\t%(asctime)s\t%(message)s'))
    LOG.addHandler(cf)
    LOG.addHandler(ch)

    if options.maxGapSize == -1:
        options.maxGapSize = maxsize

    dists = dict()
    geneorders = dict()

    f = args[0]

    LOG.info('Reading pairwise dist file %s' %f)
    g1name, g2name = basename(f).split('.', 2)[0].split('_')

    g1name = g1name.replace('-', 'abcdefghijklmnopqrs')
    g2name = g2name.replace('-', 'abcdefghijklmnopqrs')

    _, g1, g2, pwDists= readDistsAndOrder(open(f))
    dists[(g1name, g2name)] = pwDists
    revDists = dict(((y, x), reverseDistMap(d)) for ((x,y), d) in dists.items())
    geneorders = dict()
    geneorders[g1name] = sorted(dists[(g1name, g2name)].keys())
    geneorders[g2name] = sorted(revDists[(g2name, g1name)].keys())

    max_weight = 0
    for d in dists[(g1name, g2name)].values():
        max_weight = max(max_weight, max(map(lambda x: x[1], d.values())))

    #
    # where to output?
    #
    out = stdout
    #
    # compute possible adjacencies
    #

    LOG.info('Identifying anchors...')
    anchors = identifyAnchors(options.alpha, dists, revDists, geneorders,
            max_weight)
    for g1i, g2k in anchors:
        print('%s\t%s\t%s\t%s\t%s\t%s' %(g1name, g1i[0], g1i[1],
                g2name, g2k[0], g2k[1]), file = stdout)

