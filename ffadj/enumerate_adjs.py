#!/usr/bin/env python3

from sys import stdout, stderr, exit, path
from os.path import dirname, abspath
from itertools import chain, product
from optparse import OptionParser
from os.path import basename

# to import from parent folder
path.append(dirname(dirname(abspath(__file__))))
from pairwise_similarities import readDists, reverseDistMap, \
        TELOMERE_START, TELOMERE_END, DIRECTION_CRICK_STRAND, \
        DIRECTION_WATSON_STRAND

def enumerateConservedGappedAdjacencies(L, theta):
    res = list()

    eps = [1] * theta
    epps = [1] * theta
    for (i, j, orient) in L:
        if orient == DIRECTION_CRICK_STRAND:
            for x in range(len(eps)):
                ep = eps[x]
                if x:
                    ep = max(eps[x-1], ep)

                while ep < len(L) and L[ep][0] < i+1+x:
                    ep += 1
                while ep < len(L) and L[ep][0] == i+1+x and L[ep][1] < j+1:
                    ep += 1

                eps[x] = ep
                while ep < len(L) and L[ep][0] == i+1+x and \
                        L[ep][1] <= j+theta:
                    if L[ep][2] == DIRECTION_CRICK_STRAND:
                        res.append((i, L[ep][0], j, L[ep][1],
                            DIRECTION_CRICK_STRAND))
                    ep += 1
        else:
            for x in range(len(epps)):
                epp = epps[x]
                if x:
                    epp = max(epps[x-1], epp)

                while epp < len(L) and L[epp][0] < i+1+x:
                    epp += 1
                while epp < len(L) and L[epp][0] == i+1+x and \
                        L[epp][1] < j-theta:
                    epp += 1
                epps[x] = epp
                while epp < len(L) and L[epp][0] == i+1+x and \
                        L[epp][1] <= j-1:
                    if L[epp][2] == DIRECTION_WATSON_STRAND:
                        res.append((i, L[epp][0], L[epp][1], j,
                            DIRECTION_WATSON_STRAND))
                    epp += 1
    return res

def enumerateConservedAdjacencies(L):
    res = list()

    ep = epp = 1
    for (i, j, orient) in L:
        if orient == DIRECTION_CRICK_STRAND:
            while ep < len(L) and L[ep][0] < i+1:
                ep += 1
            while ep < len(L) and L[ep][0] == i+1 and L[ep][1] < j+1:
                ep += 1
            if ep < len(L) and L[ep] == (i+1, j+1, DIRECTION_CRICK_STRAND):
                res.append((i, i+1, j, j+1, DIRECTION_CRICK_STRAND))
        else:
            while epp < len(L) and L[epp][0] < i+1:
                epp += 1
            while epp < len(L) and L[epp][0] == i+1 and L[epp][1] < j-1:
                epp += 1
            if epp < len(L) and L[epp] == (i+1, j-1, DIRECTION_WATSON_STRAND):
                res.append((i, i+1, j-1, j, DIRECTION_WATSON_STRAND))
    return res

def __gorder_helper__(gorder, theta):
    res = list()

    g0 = sorted(gorder)
    chrs = [g0[0][0]]

    res.append((chrs[-1], TELOMERE_START))
    for (chr1, g1i) in g0:
        if chrs[-1] != chr1:
            # add end, spacer, start
            res.append((chrs[-1], TELOMERE_END))
            for _ in range(theta+1):
                res.append((None, None))
            res.append((chr1, TELOMERE_START))
            # add chromosome
            chrs.append(chr1)
        res.append((chr1, g1i))
    res.append((chrs[-1], TELOMERE_END))

    return res, chrs

def constructGeneorders(dist, theta):
    g1, chr1s = __gorder_helper__(list(dist.keys()), theta)
    g2, chr2s, = __gorder_helper__(set(chain(*(list(v.keys()) for v in
        list(dist.values())))), theta)

    for (chr1, chr2) in product(chr1s, chr2s):
        tel1Start = (chr1, TELOMERE_START)
        tel1End = (chr1, TELOMERE_END)
        if tel1Start not in dist:
            dist[tel1Start] = dict()
        if tel1End not in dist:
            dist[tel1End] = dict()

        dist[tel1Start][(chr2, TELOMERE_START)] = (DIRECTION_CRICK_STRAND, 1)
        dist[tel1Start][(chr2, TELOMERE_END)] = (DIRECTION_WATSON_STRAND, 1)
        dist[tel1End][(chr2, TELOMERE_START)] = (DIRECTION_WATSON_STRAND, 1)
        dist[tel1End][(chr2, TELOMERE_END)] = (DIRECTION_CRICK_STRAND, 1)

    return g1, g2

if __name__ == '__main__':

    usage = 'usage: %prog [options] <PAIRWISE DIST FILE>'

    parser = OptionParser(usage=usage)
    parser.add_option('-t', '--theta',
                      dest='theta', default=1, type='int', metavar='1, 2, ...',
                      help='Compute gapped adjacencies, allowing for up ' + \
                              'to <THETA>-1 many genes to reside between ' + \
                              'two gapped adjacencies [default: %default]')
    (options, args) = parser.parse_args()

    if len(args) != 1 or options.theta < 1:
        parser.print_help()
        exit(1)

    _, dist = readDists(open(args[0]))

    if not dist:
        print('Distance table in file %s is empty. Exiting.' %args[0], file=stderr)
        exit(1)


    g1, g2 = constructGeneorders(dist, options.theta)
    g2pos = dict(list(zip(g2, list(range(len(g2))))))

    L = list()
    for i in range(len(g1)):
        g1i = g1[i]
        if g1i in dist:
            for g2k, (orient, _) in sorted(dist[g1i].items()):
                L.append((i, g2pos[g2k], orient))
    #
    # main part
    #

    if options.theta > 0:
        consAdjs = enumerateConservedGappedAdjacencies(L, options.theta)
    else:
        consAdjs = enumerateConservedAdjacencies(L)

    #
    # output
    #

    for (i, j, k, l, orient) in consAdjs:

        g1i = g1[i]
        g1j = g1[j]
        g2k = g2[k]
        g2l = g2[l]

        if g1i[1] == TELOMERE_START:
            g1i = (g1i[0], 'TELOMERE')
        if g1j[1] == TELOMERE_END:
            g1j = (g1j[0], 'TELOMERE')
        if g2k[1] in (TELOMERE_START, TELOMERE_END):
            g2k = (g2k[0], 'TELOMERE')
        if g2l[1] in (TELOMERE_START, TELOMERE_END):
            g2l = (g2l[1], 'TELOMERE')

        print('%s\t%s\t%s\t%s\t%s\t%s\t%s' %(g1i[0], g1i[1], g1j[1], g2k[0],
                g2k[1], g2l[1], orient), file=stdout)

