# Workflows for Family-Free Medians

FFMEDIAN_PYEXEC = ' '.join(filter(None, [config['python_bin'], config['ff_bin'] + 'ffmedian/']))
FFADJ_PYEXEC = ' '.join(filter(None, [config['python_bin'], config['ff_bin'] + 'ffadj/']))

FFMEDIAN_OUT = '%s/%s' %(config['ffmedian_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR])))
FFMEDIAN_TRIPLES = config['ffmedian_triples']

FFADJAFTERMEDIAN_OUT = '%s/%s' %(config['ffadjaftermedian_out'], '_'.join(filter(None, [ALIGN_PARSTR, PW_DIR, 'a%s' %config['ffadj_alpha']])))
FFADJAFTERMEDIAN_TRIPLES = config['ffadjaftermedian_triples']

rule all_ffmedian_exact:
    input:
        expand(FFMEDIAN_OUT + '/{median_triple}.cars', median_triple=FFMEDIAN_TRIPLES)

rule all_ffadj_am:
    input:
        expand(FFADJAFTERMEDIAN_OUT + '/{ffam_triple}', ffam_triple=FFADJAFTERMEDIAN_TRIPLES)

rule simplify_ffmedian:
    input:
        G1_G2=PW_OUT + '/{pw_sim1}_{pw_sim2}.active.sim',
        G1_G3=PW_OUT + '/{pw_sim1}_{pw_sim3}.active.sim',
        G2_G3=PW_OUT + '/{pw_sim2}_{pw_sim3}.active.sim'
    output:
        FFMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.anchors'
    log:
        FFMEDIAN_OUT + '/%s_%s_%s.anchors.log' % ('{pw_sim1}', '{pw_sim2}',
                                                  '{pw_sim3}')
    shell:
        FFMEDIAN_PYEXEC + 'educated_guess' + PYSUF + ' {input} > {output} 2> {log}'

rule write_ffmedian_ilp:
    input:
        G1_G2=PW_OUT + '/{pw_sim1}_{pw_sim2}.active.sim',
        G1_G3=PW_OUT + '/{pw_sim1}_{pw_sim3}.active.sim',
        G2_G3=PW_OUT + '/{pw_sim2}_{pw_sim3}.active.sim',
        anchors=FFMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.anchors'
    output:
        FFMEDIAN_OUT + '/%s_%s_%s.lp' % ('{pw_sim1}', '{pw_sim2}', '{pw_sim3}')
    shell:
        FFMEDIAN_PYEXEC + 'write_ffmedian_ilp' + PYSUF + ' -a {input.anchors} '
                                                   '{input.G1_G2} {input.G1_G3} {input.G2_G3} > {output}'

rule run_ffmedian_cplex:
    input:
        FFMEDIAN_OUT + '/{median_triple}.lp'
    params:
        threads=config['solver_cpus'],
        time=config['solver_time'],
        memory=config['solver_memory'],
        gapopt=('-g %s' % config['solver_gap']) if 'solver_gap' in config else ''
    output:
        FFMEDIAN_OUT + '/{median_triple}.sol'
    threads:
        config['solver_cpus']
    shell:
        FF_PYSCRIPTS + 'run_cplex' + PYSUF + ' -t {params.threads} -m '
                                          '{params.memory} -l {params.time} {params.gapopt} -L {input}'

rule ffmedian_sol_to_cars:
    input:
        FFMEDIAN_OUT + '/{median_triple}.sol'
    output:
        FFMEDIAN_OUT + '/{median_triple}.cars'
    shell:
        FFMEDIAN_PYEXEC + 'sol_to_cars' + PYSUF + ' {input} > {output}'

rule ffmedian_to_anchors:
    input:
        G1_G2=PW_OUT + '/{pw_sim1}_{pw_sim2}.active.sim',
        G1_G3=PW_OUT + '/{pw_sim1}_{pw_sim3}.active.sim',
        G2_G3=PW_OUT + '/{pw_sim2}_{pw_sim3}.active.sim',
        sol=FFMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.sol'
    output:
        FFADJAFTERMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.anchors'
    shell:
        FFMEDIAN_PYEXEC + 'sol_to_pwsims' + PYSUF + ' -a {input.sol} {input.G1_G2} '
                                              '{input.G1_G3} {input.G2_G3} > {output}'

rule run_write_ffadj_am_ilp:
    input:
        G1_G2=PW_OUT + '/{pw_sim1}_{pw_sim2}.active.sim',
        G1_G3=PW_OUT + '/{pw_sim1}_{pw_sim3}.active.sim',
        G2_G3=PW_OUT + '/{pw_sim2}_{pw_sim3}.active.sim',
        anchors=FFADJAFTERMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.anchors'
    params:
        alpha=config['ffadj_alpha']
    output:
        FFADJAFTERMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.lp'
    shell:
        FFADJ_PYEXEC + 'write_ffadj_ilp' + PYSUF + ' -a {params.alpha} -f '
                                                '{input.anchors} {input.G1_G2} {input.G1_G3} {input.G2_G3} > {output}'

rule run_ffadj_am_cplex:
    input:
        FFADJAFTERMEDIAN_OUT + '/{median_triple}.lp'
    params:
        threads=config['solver_cpus'],
        time=config['solver_time'],
        memory=config['solver_memory'],
        gapopt=('-g %s' % config['solver_gap']) if 'solver_gap' in config else ''
    threads:
        config['solver_cpus']
    output:
        FFADJAFTERMEDIAN_OUT + '/{median_triple}.sol'
    shell:
        FF_PYSCRIPTS + 'run_cplex' + PYSUF + ' -t {params.threads} -m '
                                          '{params.memory} -l {params.time} {params.gapopt} -L {input}'

rule ffadj_am_to_pwsims:
    input:
        G1_G2=PW_OUT + '/{pw_sim1}_{pw_sim2}.active.sim',
        G1_G3=PW_OUT + '/{pw_sim1}_{pw_sim3}.active.sim',
        G2_G3=PW_OUT + '/{pw_sim2}_{pw_sim3}.active.sim',
        sol=FFADJAFTERMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.sol'
    output:
        FFADJAFTERMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.sol.{pw_sim1}_{pw_sim2}',
        FFADJAFTERMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.sol.{pw_sim1}_{pw_sim3}',
        FFADJAFTERMEDIAN_OUT + '/{pw_sim1}_{pw_sim2}_{pw_sim3}.sol.{pw_sim2}_{pw_sim3}'
    shell:
        FFMEDIAN_PYEXEC + 'sol_to_pwsims' + PYSUF + ' {input.sol} {input.G1_G2} '
                                              '{input.G1_G3} {input.G2_G3}'

