# Family Free Genome Comparison (FFGC) workflow - README

Contents of this file:

* [Introduction](#introduction)
* [Installation](#installation)
* [Overview on input files and workflow rules](#overview-on-input-files-and-workflow-rules)
* [Tutorial](#tutorial)
* [Directory & file structure](#directory-file-structure)
* [File formats](#file-formats)
* [OrthoFFGC Extension (former DɪꜰꜰMGC)](#orthoffgc-extension)
* [OrthoFFGC≈ Extension (former DɪꜰꜰMGCʜ≈)](#orthoffgc-extension-1)
* [Configuration table](#configuration-table)

# Introduction

Family Free Genome Comparison (FFGC) is a self-contained workflow system that
provides functionality for all steps of a family-free gene order analysis
starting from annotated genome sequences.

_Family-free_ methods for gene order analyses do not require prior knowledge of
evolutionary relationships between the genes across the studied genomes. This
tool features a complete workflow for genome comparison, requiring nothing
but annotated genome sequences as input.

A comprehensive description is available in:

<pre>
[1] Doerr, D., Feijão, P., Stoye, J. <i> Family-Free Genome Comparison</i>. 
    Comparative Genomics - Methods in Molecular Biology, vol 1704, pp. 331-342. 2018. 
    DOI: https://doi.org/10.1007/978-1-4939-7463-4_12
</pre>

FFGC includes subworkflows for inferring gene families across several species, simultaneously
based on gene similarities and family-free genome rearrangements
([OrthoFFGC](#orthoffgc-extension) and
[OrthoFFGC≈](#orthoffgc-extension-1) extensions).

FFGC is available for download at our
[git repository](https://gitlab.ub.uni-bielefeld.de/gi/FFGC)
or as a
[Conda package at Bioconda](https://anaconda.org/bioconda/ffgc)
(see [Installation](#installation) section for details). The advantage
of the later method is that FFGC can be installed by **simply** running a
single command.


# Installation

FFGC can be installed by downloading stable releases at or cloning
our git repository, or using [Conda](https://docs.conda.io/en/latest/).

### Dependencies

The dependencies of FFGC are listed bellow. Notice, however, that
**when installing FFGC via Conda, all dependencies except Gurobi or CPLEX
are handled automatically**.

* For FFGC scripts:
  - Python ≥ 3.7
  - BioPython ≥ 1.65 (≥ 1.78 recommended)
  - NetworkX ≥ 2.4
  - ruamel.yaml ≥ 0.15 (not necessary but highly recommended, otherwise yaml config files will have no comments)
* snakemake ≥ 5.10 (≥ 7.24 recommended) 
* BLAST+ or Diamond aligner (Executables must be included in system path)
* Partially required: IBM CPLEX or Gurobi Optimizer (executable **must be included in
  system path**, all subworkflows work with Gurobi except the one for FF-Median)
* Partially required: Python3 library lxml for OrthoFFGC/OrthoFFGC≈ extensions
* Partially required: MCL package (a cluster algorithm for graphs) if family
  refinement with MCL (level 2) is intended in OrthoFFGC/OrthoFFGC≈ extensions (see https://micans.org/mcl/, executables
  **must be included in system path**)

### Installation via Conda

Provided you have already a conda environment, run:

`conda install -c bioconda ffgc`

### Installation via git repository

1. Download or clone this repository for the latest development version, or download a stable release at [releases](https://gitlab.ub.uni-bielefeld.de/gi/FFGC/-/releases) page
2. Make sure that your system meets all required dependencies (see above)
3. Optional: Install the partially required dependencies and add the folders with the
   binaries to the system path
4. FFGC is ready to be used



# Overview on input files and workflow rules

In general, three major steps are performed: (1) the computation of local
sequence alignment scores between genes of two or more gene order sequences
using BLAST+ or Diamond; (2) the establishment of gene relationships; and
(3) the actual family-free gene order analysis.

Supplied with a set of genome sequences, FFGC creates a project associated with
an autarkic folder hierarchy. Creating the directories, FFGC includes parameter
values in folder names for all steps of the workflow, thereby enabling the
simultaneous execution, allocation, and simple maintenance of generated data
from several analyses with varying parameter settings.

The `ffgc_create_project.py` command of FFGC admits three alternative input variants: 
    (i) files in genbank format, 
    (ii) pairs of (multi-record) FASTA and general feature format (GFF) files,
    (iii) FASTA files that are already in an anticipated formatting.

For input variant (ii), each FASTA file must be associated with a single
organism and each of its records must correspond to a contig or chromosome of
the organism's genome. Further, their corresponding GFF files must share the
same name -- except for the file ending '.gff'.

For input variant (iii), each FASTA record must correspond to an atomic unit of
a gene order sequence, featuring a unique record ID that indicates its
association to a chromosome or contig (i.e. it must have a '|chromosome|\<id\>|'
substring in its record where \<id\> corresponds to a unique chromosome
identifier). Further, **the order of genes/markers in the FASTA files must already
correspond to the order in which they appear on their respective chromosomes**.
The precise format of each record ID is:  
`>gb|<protein ID>|locus|<locus tag>|<location>|chromosome|<chromosome ID>|strand|<+ or ->`  
For instance:  
`>gb|NP_001162627.1|locus|Dmel_CG17707|143002:147332|chromosome|NC_004354|strand|-`  
Because of the many different ways metadata can be encoded in Genbank or GFF files,
this input variant works as a fallback from (i) and (ii), as any input data can be
preprocessed easily to follow this specification.

For input variants (i) and (ii), `ffgc_create_project.py` allows annotated genomic
features to be filtered by type. The selected annotations are then extracted
and in further analysis considered as the atomic units of gene order sequences
that are henceforth denoted as genes. FFGC enables the performance of
family-free analysis on either the nucleotide or the protein sequence level.

After the project is created, the genome sequences, intermediary data, and
outputs of the various family-free analyses are maintained by the rule-based
workflow management system snakemake. The workflow and parameter settings of
individual family-free analyses can be adjusted in the configuration file
config.yaml located in the root directory of each FFGC project.

FFGC provides the following subworkflows (**for gene families**, see
[OrthoFFGC](#orthoffgc-extension) and
[OrthoFFGC≈](#orthoffgc-extension-1) sections):

<pre>
snakemake_rule__________________description______________________________________________
all_weak_common_intervals_pw    reports all mutually-closed weak common
                                intervals between all pairs of genomes [2]
all_weak_common_intervals_multi reports all closed weak common intervals in
                                the input genome data        
all_total_adjs                  reports all conserved adjacencies between all
                                genome pairs ([3, total adj. model, prob. 1])       
all_adjs_matching               computes a matching of conserved adjacencies for
                                each genome pair ([3, adj. matching model, prob. 3])
all_ffadj_exact                 solves problem FF-Adjacencies [4]
all_ffadj_heuristic             heuristic for problem FF-Adjacencies [4]
all_ffadj_am                    implementation of FFAdj-AM, a method for
                                inferring positional orthologies [5]
all_ffmedian_exact              solves FF-Median of three genomes [5] 
all_ffdcj_exact                 solves FF DCJ distance  [6]
all_ffdcjid_exact               solves FF DCJ-Indel distance [7]
ffdcjid_families                infers gene families via OrthoFFGC [8] or OrthoFFGC≈ [9]
                                
[2] Doerr, D., Stoye, J., Böcker, S., Jahn, K. <i>Identifying gene clusters by 
    discovering common intervals in indeterminate strings</i>. Proc of RECOMB-CG
    2014. BMC Genomics, 15(Suppl 6), S2. 2014. 
[3] Kowada, L.A.B., Doerr, D., Dantas, S., Stoye, J. <i>New genome similarity
    measures based on conserved gene adjacencies</i>. Proc. of RECOMB 2016. LNCS,
    Vol. 9649, pp. 204–224. 2016
[4] Doerr, D., Thévenin, A. and Stoye, J. <i>Gene family assignment-free comparative 
    genomics</i>. BMC Bioinformatics Vol. 13, Article number: S3. 2012.
[5] Doerr, D., Balaban, M., Feijão, P. and Chauve, C. <i>The Gene Family-Free
    Median of Three</i>. Algorithms for Molecular Biology, 12(14). 2017.
[6] Martinez, F. V., Feijão, P., Braga, M. D. V., & Stoye, J. <i>On the family-free DCJ 
    distance and similarity</i>. Algorithms for Molecular Biology, 10(13). 2015.
[7] Rubert, Diego P., Martinez, Fabio V. and Braga, Marilia D.V. <i>Natural
    family-free genomic distance</i>. Algorithms for Molecular Biology, 16(1). 2021.
[8] Rubert, Diego P., Doerr, D., and Braga, Marilia D.V. <i>The potential of
    family-free rearrangements towards gene orthology inference</i>. J Bioinform
    Comput Biol, 19(6):2140014, 2021.
[9] Rubert, Diego P. and Braga, Marilia D.V. <i>Efficient gene orthology inference via
    large-scale rearrangements</i>. Algorithms for Molecular Biology, 18(14). 2023.
</pre>

# Tutorial

1. Prepare your genome sequences such that:
    * Genome sequences of each genome are organized either in one (i) annotated
      genbank file; (ii) multi-FASTA file alongside a GFF file containing
      annotations; (iii) multi-FASTA file with sequences ordered according to
      their appearance on the genome sequence and each record id contains a
      '|chromosome|<id>|' substring where <id> corresponds to a unique
      chromosome identifier. Further information can be found in the
      INTRODUCTION section above. 
    * Sequence records have unique IDs within each FASTA file
    * Input files can be compressed (.gz, .bz2, .xz)

2. Call `ffgc_create_project.py` to create a snakemake project
  (use with `--help` to check the options)

3. Adjust parameters of the tools (and location of BLAST+ or Diamond, if necessary)
   in the `config.yaml` file

4. Run `snakemake --cores <number of cores you want to allocate for running
   FFGC> <TARGET RULE>` inside the created snakemake project directory to follow a workflow until the end, where `<TARGET RULE>` is one of the main FFGC rules describe above. If you use the `all` rule or simply run this command without any rule, snakemake will follow the initial steps that are common to all subworkflows, that is, the output of `run_pairwise_similarities_active` and `run_pairwise_similarities_all` rules. The first computes the gene similarity graph only for genes that have a candidate homolog in a different genome, while the second computes the gene similarity graph for all genes. From there, you can run any of the main FFGC subworkflows above. In addition, if you are using a subworkflow that needs a solver to run ILPs, you may want to just generate the ILP files to run them by yourself in some cluster system (instead of running the complete subworkflow). In that case, you can use the `all_*_ilps` rules, for instance, `all_ffdcjid_ilps` (check snakemake files `Snakefile` and `*.smk` to find all such rules).

# Directory & file structure

The following diagram depicts the base directory and file structure, where `Project dir` is the directory created by the `ffgc_create_project.py` script of FFGC. Only a small portion of those directories and files exist right after the project creation, most of them will be created during the initial steps common to all subworkflows.

```
Project directory
├── config.yaml
├── Snakefile
├── *.log
├── genomes/
│   ├── *.gos
│   └── genomes.cfg
├── blast/
│   ├── <blast database files>
│   └── <blast parameters>/
├── diamond/
│   ├── <diamond database files>
│   └── <diamond parameters>/
├── gene_relationships/
│   └── <blast or diamond + similarity parameters>/
│       ├── *.sim
│       └── genome_map.cfg
└── <specific subworkflow directories>/
```
* `config.yaml`: file where all configurations of the subworkflows are defined. Important parameters are `align_prg` (should be `'blast'` or `'diamond'`), `align_evalue`, `blast_params` (for BLAST) or `diamond_params` (for Diamond) , `pw_params` (for pariwise genome comparisons, run `pairwise_similarities.py --help` for more information), and those related to the solver for subworkflows that use ILP formulations (especially which solver to use in `solver`, which may be `cplex` or `gurobi`).
* `Snakefile`: project's snakemake file that only imports the main FFGC's `Snakefile` with the rules common to all subworkflows. The main FFGC file import other FFGC's `*.smk`, which are snakefiles of subworkflows with their rules in separate files for better organization.
* `*.log`: log files of project creation, pairwise alignments, and possibly others.
* `genomes/`: stores fasta files processed by FFGC (`*.gos`) and metadata of genomes (`genomes.cfg`). Genomes are renamed as `Gx` where `x` is the order of GenBank or FASTA files passed to `ffgc_create_project.py`, starting from 1. The names of species are extracted and stored as metadata in genome_map.cfg, if possible. Such name assignments are recorded in `ffgc_create_project.log`.
* `blast/`: holds base files of BLAST database (`<blast database files>`) and processed alignment data (inside `<blast parameters>/`) using the BLAST parameters set in `config.yaml`. The name of the directory containing processed data depends on the BLAST parameters.
* `diamond/`: the same as `blast/`, but created when using Diamond instead of BLAST.
* `gene_relationships/`: contains folders with pairwise similarity data whose names depend on the parameters used for BLAST/Diamond and for pairwise genome comparisons (`<blast or diamond + similarity parameters>/`). Inside them, files with the normalized similarity scores between genes of each genome pair (`*.sim`) and corresponding metadata (`genome_map.cfg`) are located. Depending on the subworkflow, two different types of `.sim` files can be created: `<genome_pair>.sim` and `<genome_pair>.active.sim`. The differences are described in the section "File formats" bellow.
* `<specific subworkflow directories>/`: this is the place where the data processed by each specific subworkflow is stored, e.g. `ff_dcjid` will be created and store data processed by the Family-Free DCJ InDel (FFDCJID) subworkflow, like ILP files, solver logs, orthology relations defined in the FFDCJID distance calculations (matching edges), computed distances, etc. The names of subdirectories may also depend on parameters used in the steps of the subworkflow.

The structure of FFGC is summarized as follows:

```
FFGC
├── ffgc_create_project.py
├── config.yaml
├── Snakefile
├── *.smk
├── *.py
├── *.sh
└──  ffdcjid/
```
* `ffgc_create_project.py`: the Python script that takes the input files (GenBank, FASTA, possibly compressed with gz, bz2 or xz) an creates a separate project working directory.
* `config.yaml`: base file where all configurations of the subworkflows are defined. It is copied to the project created by `ffgc_create_project.py`.
* `Snakefile`: main file with snakemake rules common to all subworkflows. Includes other subworkflow files.
 * `*.smk`: snakefiles of subworkflows with their rules in separate files for better organization. They are imported by the main file `Snakefile`.
* `*.py` and `*.sh`: multiple Python and Shell scripts, most of the times they will be called by snakemake and not used manually.
* `ffdcjid/`: contains scripts related to FFDCJID computations.

# File formats

Apart from the BLAST/Diamond database files, FFGC stores data and metadata in a number of different formats. Each one is described here briefly.

### .yaml - Configuration of parameters

All parameters for different steps/tools used in subworkflows are `config.yaml`, which is coded in [YAML](https://yaml.org/) format. Most of them are self-explicative, but a complete description of what each parameter means can be found by invoking the related program/script with the `--help` argument, or in the reference papers above ([1], [2], [3], etc). Reading the files with snakemake rules it is possible to verify to which program/script each parameter of the configuration file is provided.

### Snakefile and .smk - Snakemake rules

[Snakemake](https://snakemake.readthedocs.io/en/stable/) files with rules of subworkflows. The main file `Snakefile` imports rules from other `.smk` files.

### .gos - Records of gene sequences

Those are [FASTA](https://en.wikipedia.org/wiki/FASTA_format) files with sequences of each species. For each input species (i.e. a GenBank or a FASTA file) given to `ffgc_create_project.py`, the sequences are extracted, sorted in start-based order (**NOT** left-based order) of their location in chromosomes, and stored in those files. The ID of each record is a concatenated string of key/value pairs delimited by `|`, the most important being the `chromosome|<chromosome ID>` pair.

### .cfg - Metadata of genomes and pairwise comparisons

Metadata of genomes (`genomes.cfg`) or pairwise comparisons (`genome_map.cfg`) in Python's [ConfigParser](https://docs.python.org/3/library/configparser.html) format (a format similar to Microsoft Windows INI files). The `genome_map.cfg` file is generated only by subworkflows that also generate `.active.sim` files (see below).

### .sim and .active.sim - Gene similarities

Each file in subdirectories of `gene_relationships/` stores normalized gene similarities for a pair of genomes (a.k.a. *Similarity graphs*, see the reference [7] above). The files are tab-separated, and for each pair of genomes X and Y, the columns are: (i) chromosome ID in X, (ii) numeric gene ID in X, (iii) chromosome ID in Y, (iv) numeric gene ID in Y, (v) relative orientation (1 of genes are in the same strand, -1 otherwise), and (vi) normalized gene similarity. The numeric ID of a gene is its position in the order of all genes/records stored in the `.gos` file of the corresponding species.

The difference between `.sim` and `.active.sim` files bellow `gene_relationships/` is that in the former the numeric IDs of genes are defined by their order in the corresponding `.gos` file as described above, whereas in the latter the order is that considering only active genes of the `.gos` file. An **active gene** is a gene that is similar to another gene in at least one similarity graph (i.e. it appears in at least one file with gene similarities). Some subworkflows simply ignore genes that are not similar to any other or do not consider indels and therefore will generate `.active.sim` files, while those subworkflows that consider all genes or indels will generate `.sim` files. 

Inside specific subworkflow directories, a `.sim` file is simply a subset of those described above with the gene relations chosen among all by the computations of that subworkflow.

There may also exist `.txt.sim` files, which are simply the `.sim` files with numeric gene IDs replaced by their complete textual IDs (as in the `.gos` files).

### .lp - Integer Linear Programming formulation

Constraints of the Integer Linear Programming (ILP) formulation of some subworkflow. Those usually refer to a formulation to compute the distance of some pair of genomes (e.g. FFDCJID distance) or the median of three genomes. They are in the LP format (see [here](https://www.gurobi.com/documentation/9.1/refman/lp_format.html) and [here](https://www.ibm.com/docs/en/icos/12.10.0?topic=cplex-lp-file-format-algebraic-representation)).

### .gidmap - Map of numeric gene IDs

Only for FFDCJID, for internal use. Map original to unique numeric gene IDs to be used in `.lp` files (both compared genomes have the same numeric gene IDs 1, 2, ...)

### .sol and .mst - Solutions given by ILP solvers

Files with solutions of ILPs given by solvers. CPLEX provides solutions in [SOL format](https://www.ibm.com/docs/en/icos/12.10.0?topic=cplex-sol-file-format-solution-files), and Gurobi provides solutions in [MST](https://www.gurobi.com/documentation/9.1/refman/mst_format.html) format.

### .dst.info - Human-readable information on computed distances

Saves information on computed FFDCJID distances in human-readable format, obtained by parsing the log and solution files given by the ILP solver.

### .log - Log files

Log files may store entries of the programs/scripts of FFGC, or external tools such as the ILP solvers.


# OrthoFFGC Extension

The OrthoFFGC pipeline (formerly called DɪꜰꜰMGC) extends FFGC into a subworkflow for inferring gene families across several genomes (Snakemake rules in `ffdcjid.smk`). It is based on the ILP formulation FFDCJID for the Family-Free DCJ InDel distance, which requires the pairwise gene similarities computed by FFGC: given a pair of genomes, FFDCJID computes an optimal matching of the genes taking into account simultaneously local mutations, given by gene similarities (whose computation is part of FFGC), and large-scale genome rearrangements modeled by double-cut-and-joins (DCJs) and indels. More specifically, FFDCJID (described in reference [7] above) computes a gene matching such that the <i>weighted DCJ-indel distance</i> between the two given genomes is minimized.  

Given a set of genomes, OrthoFFGC first computes all pairwise optimal gene matchings, which are then integrated into gene families in the second step. The details of this extension are described here:

<pre>
[8] Rubert, Diego P., Doerr, D., and Braga, Marilia D.V. <i>The potential of family-free rearrangements
    towards gene orthology inference</i>. J Bioinform Comput Biol, 19(6):2140014, 2021.
</pre>


<b>For running the experiments described in the work mentioned above:</b>


1. Download and extract genomes from:

    <i>Drosophila</i> genomes: https://www.cebitec.uni-bielefeld.de/~mbraga/Drosophila.tar.xz

    X chromosomes of primates: https://www.cebitec.uni-bielefeld.de/~mbraga/Primates.tar.xz


2. Create the FFGC project:
    ```
    ffgc_create_project.py <project_name> <genbank_input_files>
    ```
   where `<project_name>` is a name to be used in the folder that will contain the FFGC project, and `<genbank_input_files>`  is the paths of input files (for instance, the GenBank files of some Drosophila OR of the X chromosomes of primates). We also recommend using the options in the example bellow (run `ffgc_create_project.py --help` for more information):
   ```
   ffgc_create_project.py drosophila --only_longest --use_protein_sequence --ignore-organelles --ignore-unknown-chr Dmelanogaster_Release_6_plus_ISO1_MT_genomic.gbff Dsimulans_ASM75419v2_genomic.gbff Dyakuba_dyak_caf1_genomic.gbff
   ```

3. Go to the project folder and change default parameters of `config.yaml`. The parameters for computing the pairwise gene similarities that we used are the following: `pw_params: -NS -n1 -l10 -s0.8`. (For obtaining their description, run the script `pairwise_similarities.py --help` inside FFGC folder - not in the project folder.) 


4. Run the OrthoFFGC subworkflow inside the project folder:
   ```
   snakemake --cores <number_of_threads> ffdcjid_families
   ```


5. When the pipeline is complete, the resulting OrthoFF families can be found in:
   ```
   path_to_project/ff_dcjid/<blast or diamond + pw + name>/families.l1.txt
   ```
   where `<blast or diamond + pw + name>` is a folder name that depends on the parameters in `config.yaml` for BLAST/Diamond and pairwise similarities computations.

   Additionally, the pipeline produces superfamilies, exclusively based on gene similarities and including intra-genome connections. The set of superfamilies can be found in:
   ```
   path_to_project/ff_dcjid/<blast or diamond + pw + name>/families.l0.txt
   ```
   
## Subworkflow overview

The following figure depicts the dependencies of rules in DɪꜰꜰMGC extension for the default level = 1 (see "Family levels" below). Level 2 adds multiple rules and dependencies for converting files to be used with MCL.

![OrthoFFGC Subworkflow](./figures/diffmgc-workflow.png)

The most important rules are:

* `ffdcjid_families`: runs the complete subworkflow.
* `all_ffdcjid_ilps` (not shown above): useful if you want to generate all ILPs, solve them using CPLEX or Gurobi in some cluster (not locally), and then proceed with the subworkflow using the rule above. In this case, the processed files must have the same name and be placed in the same directory as they would be if using the rule above. Run `snakemake -fpn all_ffdcjid_exact` to perform a dry-run and check the file names and directories. Using the script `run_cplex.py` and `run_gurobi_local.sh` for running manually or in a cluster facilitates this task by already creating output files with the correct names.

### Family levels

The gene families computed can be more or less refined depending on the level selected by setting the `ffdcjid_families_level` parameter in `config.yaml`.

* **Level 0**: connected components of the similarity graphs, with additional intra-genome connections.
* **Level 1**: families based on the orthology relations (matching edges) inferred in FFDCJID computations (as in the OrthoFFGC reference paper [7]).
* **Level 2**: results from refining ambiguous families from level 1 with the help of [MCL algorithm](https://micans.org/mcl/) (may be very similar to level 1 if its families are already mostly unambiguous or if its ambiguous families are strongly connected).


## Output files with families

After the subworkflow is complete, the following files are stored inside the folder `ff_dcjid/<blast or diamond + similarity + ffdcjid parameters>/` (the name depends on parameters used). Let `x` be a level (levels may vary from 0 to 2 depending on the subworkflow configuration):

* `families.l<x>.txt`: families of level `x` in plain format, each line is a set of complete gene identifiers separated by spaces and defines genes in a family.
* `families.l<x>.xml`: XML file with families in a hierarchical representation (levels 0 to 2, if computed), similar to [OrthoXML](https://www.orthoxml.org/xml/Main.html).  
* `families.l<x>.log`: stores useful information about the families and graphs defining families.
* `families.l<x>.sif`: graph in SIF format (Simple Interaction File, used by MCL) with components defining families.


# OrthoFFGC≈ Extension

This extension (formerly called DɪꜰꜰMGCʜ≈) is a variation of OrthoFFGC and implements a heuristic solution instead of the original optimal solution. In the original optimal solution there is an exponential growth of the solution space, relative to the number of linear segments (chromosomes, contigs, scaffolds) in the analyzed genomes. The heuristic solution amortizes the growth of the solution space, allowing OrthoFFGC≈ to lift the limitation of requiring chromosome-level assembled genomes. While the subworkflow with optimal solution cannot handle even one genome with a hundred contigs, the lighter heuristic solution allows OrthoFFGC≈ to handle genome pairs with hundreds or even a thousand contigs. Furthermore, in our experiments described in [9], the heuristic did not impact negatively the quality of the inferred gene families, being very close to the original (optimal) gene families.

<pre>
[9] Rubert, Diego P. and Braga, Marilia D.V. <i>Efficient gene orthology inference via
    large-scale rearrangements</i>. Algorithms for Molecular Biology, 18(14). 2023.
</pre>

(Instructions and input data for running the experiments described in [9] are provided below. Furthermore, extra supplementary material on the experiment involving 11 partially assembled <i>Drosophilas</i> is available at https://www.cebitec.uni-bielefeld.de/~mbraga/OrthoFFGCh/Rubert-Braga-Drosophila-supplement.pdf.)

OrthoFFGC≈, described in reference [8] above, is based on the ILP formulation FFDCJID for the Family-Free DCJ InDel distance, which requires the pairwise gene similarities computed by FFGC. Given a pair of genomes, FFDCJID computes a heuristic matching of the genes, simultaneously taking into account local mutations given by gene similarities (whose computation is part of FFGC), and large-scale genome rearrangements modeled by double-cut-and-joins (DCJs) and indels. More specifically, FFDCJID computes a gene matching such that the <i>heuristic weighted DCJ-indel distance</i> between the two given genomes is minimized. Given a set of genomes, OrthoFFGC≈ first computes all pairwise heuristic gene matchings, which are then integrated into gene families in the second step. 

In order to use the heuristic solution, simply set the parameter `ffdcjid_heuristic_capping` to `true` in `config.yaml` (notice that it may come enabled by default). In addition, the parameters `ffdcjid_heuristic_capping_absolute_threshold` and `ffdcjid_heuristic_capping_relative_threshold` correspond to the thresholds τ and ε as described in the reference papers [8,9]. (Note that the heuristic parameter can also be directly applied for computing the FF-DCJ-Indel distance of two genomes with the `all_ffdcjid_exact` subworkflow [6].) Other than that, OrthoFFGC≈ is used exactly in the same way as OrthoFFGC. See the example below.

<b>For running the experiments described in the work [9] mentioned above:</b>

1. Download and extract genomes from:

    5 completely assembled primates: 
    https://www.cebitec.uni-bielefeld.de/~mbraga/OrthoFFGCh/Primates.zip

    11 partially assembled <i>Drosophilas:</i>
    https://www.cebitec.uni-bielefeld.de/~mbraga/OrthoFFGCh/Drosophila.zip


2. Create the FFGC project:
    ```
    ffgc_create_project.py <project_name> <genbank_input_files>
    ```
   where `<project_name>` is a name to be used in the folder that will contain the FFGC project, and `<genbank_input_files>`  is the paths of input files (for instance, the GenBank files of some Drosophila). Supposing you have extracted the files in the `tar` file above, we recommend using the options in the example bellow (run `ffgc_create_project.py --help` for more information):
   ```
   ffgc_create_project.py drosophila --only_longest --use_protein_sequence --ignore-organelles --ignore-patches *.gbff.xz
   ```
   keep in mind that using the options `--ignore-unknown-chr` or `--ignore-scaffolds` would probably remove all contigs/scaffolds not placed in chromosomes, which is not necessary when using the OrthoFFGC≈ extension.

3. Go to the project folder and change default parameters of `config.yaml`. The parameters that we used for computing the gene orthologies are the following: `pw_params: -NS -n1 -l10 -s0.8`, `ffdcjid_heuristic_capping: true` and `ffdcjid_families_level: 2`. Also you will probably want to find and adjust the ILP settings accordingly in the same file. (For obtaining the description of parameters for `pw_params`, run the script `pairwise_similarities.py --help` inside FFGC folder - not in the project folder.) 


4. Run the OrthoFFGC≈ subworkflow inside the project folder:
   ```
   snakemake --cores <number_of_threads> ffdcjid_families
   ```


5. When the pipeline is complete, the resulting OrthoFFGC≈ families can be found in:
   ```
   path_to_project/ff_dcjid/<blast or diamond + pw + name>/families.l1.txt
   ```
   where `<blast or diamond + pw + name>` is a folder name that depends on the parameters in `config.yaml` for BLAST/Diamond and pairwise similarities computations.


# Configuration table

Many different aspects of the workflow can be configured in the `config.yaml` file. When a FFGC project is created, a template of such configuration file is placed in the project directory with default parameters and their documentation. The table below lists the most relevant parameters for the general steps of the workflow and steps specific of each subworkflow.


| **Parameter** | **Description** |
|---|---|
| | |
| ***General parameters*** | |
| `python_bin` | Path to Python 3 binary, in case the *source* distribution is used |
| `align_prg` | Program used for aligning sequences (one between `blast` or `diamond`) |
| `blast_dir` & `diamond_bin` | Path to BLAST binaries or DIAMOND binary, in case the *source* distribution is used |
| `blast_params` & `diamond_params` | Parameter string for `blastn`/`blastp` (depending whether local blasthits are identified on nucleotide or protein sequence level) or `diamond` commands |
| `pw_params` | String of one or more of the following parameter options using in constructing and pruning the gene relationship graph (see the help message of script `pairwise_similarities.py`) <ul><li>**-t &lt;float&gt;** Absolute threshold parameter in the interval [0,1], edges with smaller weights are filtered out</li><li>**-s &lt;float&gt;** Local threshold parameter in the interval [0,1] for the relative stringency filter</li><li>**-n &lt;int&gt;** minimum number of genomes to which a gene must exhibit similarities</li><li>**-l &lt;int&gt;** minimum accepted size of a contig/chromosome, measured in numbers of "non-zero similarity" genes</li></ul> |
| `solver` | The ILP solver for subworkflows that require it (one between `gurobi` or `cplex`) |
| `solver_cpus` | Limit of parallel threads for the ILP solver |
| `solver_time` | Time limit for optimizer after which the solver returns best, but possibly suboptimal solution (in *minutes*) |
| | |
| ***Subworkflow for FF-ADJACENCIES*** | |
| `ffadj_alpha` | Convex combination parameter 𝛼 ∈ [0,1] in solving problem FF-ADJACENCIES, used by both the exact algorithm and heuristic FFAdj-MCS. |
| | |
| ***Subworkflow for Weak common intervals*** | |
| `wci_delta` | Approximate weak common intervals parameter, allowing for 𝛿 ≥ 0 indels, used for both pairwise and multi-way comparisons |
| `wci_min` | Minimum number of genes with shared similarity in a weak common intervals pair, used for both pairwise and multi-way comparisons |
| `wci_quorum` | Quorum of genomes in which a set of weak common intervals needs to occur; if set to zero, it must span all genomes; only used in multi-way comparisons |
| `wci_blocksize` | Size of genomic segments that are computed in parallel; if set to zero, parallel computation is disabled; only used in multi-way comparisons |
| | |
| ***Subworkflow for FF-DCJ-Indel*** | |
| `ffdcjid_force_maximal_matching` | Force a maximal matching of orthologs in the gene similarity graph instead of any matching |
| `ffdcjid_heuristic_capping` | Use the heuristic capping of contigs in order to reduce the search space of capping-sets, which grows exponentially on the number of contigs (also defines which method would be used for gene family inference, OrthoFFGC or OrthoFFGC≈) |
| `ffdcjid_heuristic_capping_absolute_threshold` | Absolute threshold for the heuristic capping of contigs (the maximum number of edges for each vertex of the contig graph) |
| `ffdcjid_heuristic_capping_relative_threshold` | Relative threshold for the heuristic capping of contigs (the minimum number of homolog candidates between a contig pair w.r.t. the contig sharing most homolog candidates with one contig of that pair) |
| | |
| ***Subworkflow for OrthoFFGC and OrthoFFGC≈*** | |
| `ffdcjid_families_level` | Refinement level of families (see [family levels](#family-levels)) |
| `mcl_inflation` | A single or a list of inflation values for the MCL algorithm (usually somewhere in the range [1.2,5.0], larger values lead to fine-grained clusterings, smaller to coarse grained clusterings) |

